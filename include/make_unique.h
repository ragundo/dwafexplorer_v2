#ifndef MAKE_UNIQUE_H
#define MAKE_UNIQUE_H
#include <memory>

//#if __cplusplus >= 201402L // C++14 and beyond
//using std::make_unique;
//#else
namespace rdf
{
template<typename T, typename... Args>
std::unique_ptr<T> make_unique(Args&&... args)
{
    static_assert(!std::is_array<T>::value, "arrays not supported");
    return std::unique_ptr<T>(new T(std::forward<Args>(args)...));
}
} // namespace rdf
//#endif

#endif //MAKE_UNIQUE_H