#include <cstdint>
#include "MetadataServer.h"
#include <map>
#include <vector>
#include <utility>


using namespace std;
namespace rdf
{


map<string,string> addornements_globals_metadata{
			{"created_item_type","v{int16_t}"},
			{"cur_rain","[1280"},
			{"cur_snow","[256"},
			{"current_weather","[5[5{int8_t}"},
			{"flows","v*"},
			{"gamemode","{int32_t}"},
			{"gametype","{int32_t}"},
			{"jobvalue","[260"},
			{"jobvalue_setter","[260"},
			{"timed_events","v*"},
			{"ui_building_assign_items","v*"},
			{"ui_building_assign_units","v*"},
			{"ui_lever_target_type","{int8_t}"},
			{"ui_menu_width","[2"}
};


        tl::optional<string> CMetadataServer::get_addornements_globals_metadata(const string& p_field_name)
        {
            auto it = addornements_globals_metadata.find(p_field_name);
            if (it != addornements_globals_metadata.end())
            {
                return it->second;
            }
            return tl::nullopt;
        }

        } // namespace rdf


