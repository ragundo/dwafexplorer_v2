#include <cstdint>
#include <map>
#include <array>
#include "tl/tl_optional.h"
#include "MetadataServer.h"


namespace rdf
{

std::map<DF_Type, DF_Type> linked_list_metadata
{
	{ DF_Type::block_burrow_link, DF_Type::block_burrow},
	{ DF_Type::cave_column_link, DF_Type::cave_column},
	{ DF_Type::job_list_link, DF_Type::job},
	{ DF_Type::proj_list_link, DF_Type::projectile}};


    using namespace std;

    tl::optional<DF_Type> CMetadataServer::get_linked_list_type(DF_Type p_df_type)
    {
        auto it = linked_list_metadata.find(p_df_type);
        if (it == linked_list_metadata.end())
            return {};
        return it->second;
    }

    } // namespace rdf

