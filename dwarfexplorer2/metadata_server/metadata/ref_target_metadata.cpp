#include <cstdint>
#include <map>
#include "tl/tl_optional.h"
#include "MetadataServer.h"


namespace rdf
{

std::map<DF_Type, std::map<std::string,std::pair<std::string,std::string>>> ref_target_metadata
{
	{
		DF_Type::abstract_building,
		{
			{ "child_building_ids" , {"abstract_building", "" }},
			{ "parent_building_id" , {"abstract_building", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "site_owner_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::abstract_building__T_inhabitants,
		{
			{ "histfig_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::abstract_building__T_unk1,
		{
			{ "hfig" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::abstract_building_contents,
		{
			{ "building_ids" , {"building", "" }}
		}
	},
	{
		DF_Type::abstract_building_libraryst,
		{
			{ "copied_artifacts" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::abstract_building_unk,
		{
			{ "histfigs" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::abstract_building_unk__T_anon_1,
		{
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::activity_entry,
		{
			{ "army_controller" , {"army_controller", "" }}
		}
	},
	{
		DF_Type::activity_event,
		{
			{ "activity_id" , {"activity_entry", "" }},
			{ "parent_event_id" , {"activity_event", "$$.activity_id" }}
		}
	},
	{
		DF_Type::activity_event__T_unk_v42_1,
		{
			{ "item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::activity_event__T_unk_v42_2,
		{
			{ "item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::activity_event_combat_trainingst,
		{
			{ "building_id" , {"building", "" }},
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_conflictst__T_sides,
		{
			{ "histfig_ids" , {"historical_figure", "" }},
			{ "unit_ids" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_conversationst,
		{
			{ "floor_holder" , {"unit", "" }},
			{ "floor_holder_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::activity_event_conversationst__T_participants,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_conversationst__T_turns,
		{
			{ "speaker" , {"unit", "" }},
			{ "speaker_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::activity_event_copy_written_contentst,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_discuss_topicst,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::activity_event_fill_service_orderst,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_individual_skill_drillst,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::activity_event_participants,
		{
			{ "activity_id" , {"activity_entry", "" }}
		}
	},
	{
		DF_Type::activity_event_performancest,
		{
			{ "dance_form" , {"dance_form", "" }},
			{ "event" , {"history_event", "" }},
			{ "music_form" , {"musical_form", "" }},
			{ "poetic_form" , {"poetic_form", "" }},
			{ "written_content_id" , {"written_content", "" }}
		}
	},
	{
		DF_Type::activity_event_performancest__T_participant_actions,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_ponder_topicst,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::activity_event_prayerst,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::activity_event_ranged_practicest,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::activity_event_readst,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::activity_event_researchst,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::activity_event_skill_demonstrationst,
		{
			{ "building_id" , {"building", "" }},
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_socializest,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::activity_event_sparringst,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::activity_event_sparringst__T_groups,
		{
			{ "building_id" , {"building", "" }},
			{ "units" , {"unit", "" }}
		}
	},
	{
		DF_Type::activity_event_worshipst,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::adventure_environment_unit_suck_bloodst,
		{
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::adventure_movement_attack_creaturest,
		{
			{ "anon_1" , {"unit", "" }}
		}
	},
	{
		DF_Type::adventure_movement_building_interactst,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::adventure_movement_hold_itemst,
		{
			{ "item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::adventure_movement_item_interactst,
		{
			{ "item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_citizenship,
		{
			{ "applicant" , {"agreement_party", "" }},
			{ "government" , {"agreement_party", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_demonic_binding,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "demon" , {"agreement_party", "" }},
			{ "site" , {"world_site", "" }},
			{ "summoner" , {"agreement_party", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_join_party,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "figure" , {"historical_figure", "" }},
			{ "member" , {"agreement_party", "" }},
			{ "party" , {"agreement_party", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_location,
		{
			{ "applicant" , {"agreement_party", "" }},
			{ "government" , {"agreement_party", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_parley,
		{
			{ "party_id" , {"agreement_party", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_plot_abduct,
		{
			{ "target_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_plot_assassination,
		{
			{ "target_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_plot_frame_treason,
		{
			{ "anon_1" , {"historical_figure", "" }},
			{ "fool_id" , {"historical_figure", "" }},
			{ "victim_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_plot_induce_war,
		{
			{ "attacker" , {"historical_entity", "" }},
			{ "defender" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_plot_sabotage,
		{
			{ "victim_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_plot_steal_artifact,
		{
			{ "artifact_id" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_position_corruption,
		{
			{ "target_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_promise_position,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::agreement_details_data_residency,
		{
			{ "applicant" , {"agreement_party", "" }},
			{ "government" , {"agreement_party", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::agreement_party,
		{
			{ "entity_ids" , {"historical_entity", "" }},
			{ "histfig_ids" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::army,
		{
			{ "controller_id" , {"army_controller", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::army_controller,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "general_hf" , {"historical_figure", "" }},
			{ "master_hf" , {"historical_figure", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "visitor_nemesis_id" , {"nemesis_record", "" }}
		}
	},
	{
		DF_Type::army_controller_invasion__T_anon_1,
		{
			{ "army_id" , {"army", "" }}
		}
	},
	{
		DF_Type::army_controller_sub17,
		{
			{ "artifact_id" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::army_controller_villain_visiting,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::art_image,
		{
			{ "artist" , {"historical_figure", "" }},
			{ "event" , {"history_event", "" }},
			{ "id" , {"art_image_chunk", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "site" , {"world_site", "" }},
			{ "subid" , {"art_image", "$$.id" }}
		}
	},
	{
		DF_Type::art_image_element_creaturest,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::art_image_element_itemst,
		{
			{ "item_id" , {"item", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::art_image_element_plantst,
		{
			{ "plant_id" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::art_image_element_shapest,
		{
			{ "shape_id" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::art_image_element_treest,
		{
			{ "plant_id" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::art_image_ref,
		{
			{ "civ_id" , {"historical_entity", "" }},
			{ "id" , {"art_image_chunk", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "subid" , {"art_image", "$$.id" }}
		}
	},
	{
		DF_Type::artifact_claim,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "feature_layer_id" , {"world_underground_region", "" }},
			{ "holder_hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "subregion" , {"world_region", "" }}
		}
	},
	{
		DF_Type::artifact_record,
		{
			{ "direct_claims" , {"historical_figure", "" }},
			{ "entity_claims" , {"historical_entity", "" }},
			{ "feature_layer" , {"world_underground_region", "" }},
			{ "holder_hf" , {"historical_figure", "" }},
			{ "loss_region" , {"world_region", "" }},
			{ "owner_hf" , {"historical_figure", "" }},
			{ "remote_claims" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "storage_site" , {"world_site", "" }},
			{ "storage_structure_local" , {"abstract_building", "" }},
			{ "structure_local" , {"abstract_building", "" }},
			{ "subregion" , {"world_region", "" }}
		}
	},
	{
		DF_Type::battlefield,
		{
			{ "event_collections" , {"history_event_collection", "" }},
			{ "hfs_killed" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::belief_system,
		{
			{ "deities" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::block_burrow,
		{
			{ "id" , {"burrow", "" }}
		}
	},
	{
		DF_Type::block_square_event_grassst,
		{
			{ "plant_index" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::block_square_event_item_spatterst,
		{
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::block_square_event_material_spatterst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::block_square_event_mineralst,
		{
			{ "inorganic_mat" , {"inorganic_raw", "" }}
		}
	},
	{
		DF_Type::block_square_event_world_constructionst,
		{
			{ "construction_id" , {"world_construction", "" }}
		}
	},
	{
		DF_Type::build_req_choice_genst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::building,
		{
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "owner_id" , {"unit", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "world_data_id" , {"world_object_data", "" }}
		}
	},
	{
		DF_Type::building__T_activities,
		{
			{ "activity_id" , {"activity_entry", "" }},
			{ "event_id" , {"activity_event", "$$.activity_id" }}
		}
	},
	{
		DF_Type::building_armorstandst,
		{
			{ "specific_squad" , {"squad", "" }}
		}
	},
	{
		DF_Type::building_bedst,
		{
			{ "specific_squad" , {"squad", "" }}
		}
	},
	{
		DF_Type::building_boxst,
		{
			{ "specific_squad" , {"squad", "" }}
		}
	},
	{
		DF_Type::building_cabinetst,
		{
			{ "specific_squad" , {"squad", "" }}
		}
	},
	{
		DF_Type::building_cagest,
		{
			{ "assigned_items" , {"item", "" }},
			{ "assigned_units" , {"unit", "" }}
		}
	},
	{
		DF_Type::building_civzonest,
		{
			{ "assigned_items" , {"item", "" }},
			{ "assigned_units" , {"unit", "" }}
		}
	},
	{
		DF_Type::building_def_item,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "metal_ore" , {"inorganic_raw", "" }}
		}
	},
	{
		DF_Type::building_design,
		{
			{ "architect" , {"historical_figure", "" }},
			{ "builder1" , {"historical_figure", "" }},
			{ "builder2" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::building_display_furniturest,
		{
			{ "displayed_items" , {"item", "" }}
		}
	},
	{
		DF_Type::building_furnacest,
		{
			{ "custom_type" , {"building_def", "" }}
		}
	},
	{
		DF_Type::building_nest_boxst,
		{
			{ "claimed_by" , {"unit", "" }}
		}
	},
	{
		DF_Type::building_squad_use,
		{
			{ "squad_id" , {"squad", "" }}
		}
	},
	{
		DF_Type::building_stockpilest,
		{
			{ "container_item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::building_trapst,
		{
			{ "observed_by_civs" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::building_users,
		{
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::building_weaponrackst,
		{
			{ "specific_squad" , {"squad", "" }}
		}
	},
	{
		DF_Type::building_workshopst,
		{
			{ "custom_type" , {"building_def", "" }}
		}
	},
	{
		DF_Type::burrow,
		{
			{ "units" , {"unit", "" }}
		}
	},
	{
		DF_Type::caravan_state,
		{
			{ "animals" , {"unit", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "goods" , {"item", "" }}
		}
	},
	{
		DF_Type::caste_raw__T_misc,
		{
			{ "bone_mat" , {"material", "$$.bone_matidx" }},
			{ "itemcorpse_materialtype" , {"material", "$$.itemcorpse_materialindex" }}
		}
	},
	{
		DF_Type::caste_raw__T_secretion,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::caste_raw__T_unknown2,
		{
			{ "infect_all" , {"syndrome", "" }},
			{ "infect_local" , {"syndrome", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::coin_batch,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "ruler" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::coin_batch__T_image_back,
		{
			{ "id" , {"art_image_chunk", "" }},
			{ "subid" , {"art_image", "$$.id" }}
		}
	},
	{
		DF_Type::coin_batch__T_image_front,
		{
			{ "id" , {"art_image_chunk", "" }},
			{ "subid" , {"art_image", "$$.id" }}
		}
	},
	{
		DF_Type::color_modifier_raw,
		{
			{ "pattern_index" , {"descriptor_pattern", "" }}
		}
	},
	{
		DF_Type::construction,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::conversation,
		{
			{ "unk_30" , {"unit", "" }},
			{ "unk_34" , {"historical_figure", "" }},
			{ "unk_3c" , {"unit", "" }},
			{ "unk_40" , {"historical_figure", "" }},
			{ "unk_48" , {"unit", "" }},
			{ "unk_4c" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::conversation__T_speech,
		{
			{ "speaker" , {"unit", "" }}
		}
	},
	{
		DF_Type::creature_interaction,
		{
			{ "type_id" , {"interaction", "" }}
		}
	},
	{
		DF_Type::creature_interaction_effect,
		{
			{ "syn_id" , {"syndrome", "" }}
		}
	},
	{
		DF_Type::creature_interaction_effect_body_mat_interactionst,
		{
			{ "interaction_id" , {"interaction", "" }}
		}
	},
	{
		DF_Type::creature_interaction_effect_body_transformationst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::creature_interaction_effect_material_force_adjustst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::creature_raw,
		{
			{ "source_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::crime,
		{
			{ "convicted_hf" , {"historical_figure", "" }},
			{ "convicted_hf_2" , {"historical_figure", "" }},
			{ "convicted_hf_3" , {"historical_figure", "" }},
			{ "criminal" , {"unit", "" }},
			{ "criminal_hf" , {"historical_figure", "" }},
			{ "criminal_hf_2" , {"historical_figure", "" }},
			{ "criminal_hf_3" , {"historical_figure", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "incident_id" , {"incident", "" }},
			{ "item_id" , {"item", "" }},
			{ "site" , {"world_site", "" }},
			{ "victim_hf" , {"historical_figure", "" }},
			{ "victim_hf_2" , {"historical_figure", "" }},
			{ "victim_hf_3" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::crime__T_convict_data,
		{
			{ "convicted" , {"unit", "" }}
		}
	},
	{
		DF_Type::crime__T_counterintelligence,
		{
			{ "identified_hf" , {"historical_figure", "" }},
			{ "identified_hf_2" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::crime__T_reports,
		{
			{ "accused_id" , {"historical_figure", "" }},
			{ "accused_id_2" , {"historical_figure", "" }},
			{ "death_id" , {"incident", "" }}
		}
	},
	{
		DF_Type::crime__T_victim_data,
		{
			{ "victim" , {"unit", "" }}
		}
	},
	{
		DF_Type::cultural_identity,
		{
			{ "civ_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::cultural_identity__T_group_log,
		{
			{ "group_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::dance_form,
		{
			{ "music_written_content_id" , {"written_content", "" }},
			{ "original_author" , {"historical_figure", "" }},
			{ "originating_entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::descriptor_color,
		{
			{ "words" , {"language_word", "" }}
		}
	},
	{
		DF_Type::descriptor_pattern,
		{
			{ "colors" , {"descriptor_color", "" }}
		}
	},
	{
		DF_Type::descriptor_shape,
		{
			{ "words" , {"language_word", "" }}
		}
	},
	{
		DF_Type::divination_set,
		{
			{ "owner_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::dye_info,
		{
			{ "dyer" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::embark_feature,
		{
			{ "global_feature_idx" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::embark_item_choice__T_list,
		{
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::embark_location,
		{
			{ "reclaim_site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::embark_profile,
		{
			{ "caste" , {"caste_raw", "" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::engraving,
		{
			{ "art_id" , {"art_image_chunk", "" }},
			{ "art_subid" , {"art_image", "$$.art_id" }},
			{ "artist" , {"historical_figure", "" }},
			{ "masterpiece_event" , {"history_event", "" }}
		}
	},
	{
		DF_Type::entity_activity_statistics,
		{
			{ "found_minerals" , {"inorganic_raw", "" }}
		}
	},
	{
		DF_Type::entity_claim_mask__T_map,
		{
			{ "entities" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::entity_entity_link,
		{
			{ "target" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_abandon,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "parent_entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_abduction,
		{
			{ "abductor_id" , {"historical_figure", "" }},
			{ "event" , {"history_event", "" }},
			{ "histfig_id" , {"historical_figure", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_accept_peace_offer,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_accept_tribute_demand,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_accept_tribute_offer,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_destroyed,
		{
			{ "artifact_id" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_in_feature_layer,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "feature_layer_id" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_in_inventory,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "hist_figure_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_in_site,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "structure_id" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_in_subregion,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "subregion_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_not_in_feature_layer,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "feature_layer_id" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_not_in_inventory,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "hist_figure_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_not_in_site,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "structure_id" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_artifact_not_in_subregion,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "subregion_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_beast,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "region_id" , {"world_region", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_cease_tribute_offer,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_claim,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "histfig_id" , {"historical_figure", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_flee,
		{
			{ "army_entity_id" , {"historical_entity", "" }},
			{ "army_leader_hf_id" , {"historical_figure", "" }},
			{ "from_site_id" , {"world_site", "" }},
			{ "refugee_entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_founded,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "parent_entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_founding,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "region_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_group,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_harass,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_incident,
		{
			{ "incident_id" , {"incident", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_insurrection,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_insurrection_end,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_invasion,
		{
			{ "attack_leader_hf" , {"historical_figure", "" }},
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_leave,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_occupation,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_reclaimed,
		{
			{ "behalf_entity_id" , {"historical_entity", "" }},
			{ "leader_hf" , {"historical_figure", "" }},
			{ "reclaimer_entity_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_reclaiming,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "first_settler_hf" , {"historical_figure", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_refuse_peace_offer,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_refuse_tribute_demand,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_refuse_tribute_offer,
		{
			{ "entity1_id" , {"historical_entity", "" }},
			{ "entity2_id" , {"historical_entity", "" }},
			{ "histfig1_id" , {"historical_figure", "" }},
			{ "histfig2_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_event__T_data__T_succession,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "former_histfig_id" , {"historical_figure", "" }},
			{ "histfig_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::entity_occasion,
		{
			{ "event" , {"history_event", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_occasion_info,
		{
			{ "events" , {"history_event", "" }}
		}
	},
	{
		DF_Type::entity_population,
		{
			{ "civ_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::entity_position,
		{
			{ "allowed_creature" , {"creature_raw", "" }},
			{ "appointed_by_civ" , {"historical_entity", "" }},
			{ "commander_civ" , {"historical_entity", "" }},
			{ "rejected_creature" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::entity_position_assignment,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "histfig2" , {"historical_figure", "" }},
			{ "squad_id" , {"squad", "" }}
		}
	},
	{
		DF_Type::entity_position_raw,
		{
			{ "allowed_creature" , {"creature_raw", "" }},
			{ "rejected_creature" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::entity_raw,
		{
			{ "creature_ids" , {"creature_raw", "" }},
			{ "gem_shapes" , {"descriptor_shape", "" }},
			{ "source_hfid" , {"historical_figure", "" }},
			{ "stone_shapes" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::entity_raw__T_equipment,
		{
			{ "ammo_id" , {"itemdef_ammost", "" }},
			{ "armor_id" , {"itemdef_armorst", "" }},
			{ "digger_id" , {"itemdef_weaponst", "" }},
			{ "gloves_id" , {"itemdef_glovesst", "" }},
			{ "helm_id" , {"itemdef_helmst", "" }},
			{ "instrument_id" , {"itemdef_instrumentst", "" }},
			{ "pants_id" , {"itemdef_pantsst", "" }},
			{ "shield_id" , {"itemdef_shieldst", "" }},
			{ "shoes_id" , {"itemdef_shoesst", "" }},
			{ "siegeammo_id" , {"itemdef_siegeammost", "" }},
			{ "tool_id" , {"itemdef_toolst", "" }},
			{ "toy_id" , {"itemdef_toyst", "" }},
			{ "trapcomp_id" , {"itemdef_trapcompst", "" }},
			{ "weapon_id" , {"itemdef_weaponst", "" }}
		}
	},
	{
		DF_Type::entity_raw__T_workshops,
		{
			{ "permitted_building_id" , {"building_def", "" }},
			{ "permitted_reaction_id" , {"reaction", "" }}
		}
	},
	{
		DF_Type::entity_recipe,
		{
			{ "subtype" , {"itemdef_foodst", "" }}
		}
	},
	{
		DF_Type::entity_site_link,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "target" , {"world_site", "" }}
		}
	},
	{
		DF_Type::entity_uniform_item,
		{
			{ "image_thread_color" , {"descriptor_color", "" }},
			{ "item_color" , {"descriptor_color", "" }},
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::entity_unk_v47_1,
		{
			{ "agreement" , {"agreement", "" }},
			{ "unk_v47_1" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::feature_init_deep_special_tubest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::feature_init_deep_surface_portalst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::feature_init_magma_core_from_layerst,
		{
			{ "layer" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::feature_init_subterranean_from_layerst,
		{
			{ "layer" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::feature_init_underworld_from_layerst,
		{
			{ "layer" , {"world_underground_region", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::flow_guide_item_cloudst,
		{
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::flow_info,
		{
			{ "guide_id" , {"flow_guide", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::general_ref_abstract_buildingst,
		{
			{ "building_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::general_ref_activity_eventst,
		{
			{ "activity_id" , {"activity_entry", "" }}
		}
	},
	{
		DF_Type::general_ref_artifact,
		{
			{ "artifact_id" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::general_ref_building,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::general_ref_creaturest,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::general_ref_dance_formst,
		{
			{ "dance_form_id" , {"dance_form", "" }}
		}
	},
	{
		DF_Type::general_ref_entity,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::general_ref_entity_art_image,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::general_ref_entity_popst,
		{
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::general_ref_feature_layerst,
		{
			{ "underground_region_id" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::general_ref_historical_eventst,
		{
			{ "event_id" , {"history_event", "" }}
		}
	},
	{
		DF_Type::general_ref_historical_figurest,
		{
			{ "hist_figure_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::general_ref_interactionst,
		{
			{ "interaction_id" , {"interaction", "" }},
			{ "source_id" , {"interaction_source", "$$.interaction_id" }}
		}
	},
	{
		DF_Type::general_ref_item,
		{
			{ "item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::general_ref_item_type,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::general_ref_mapsquare,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::general_ref_musical_formst,
		{
			{ "musical_form_id" , {"musical_form", "" }}
		}
	},
	{
		DF_Type::general_ref_nemesis,
		{
			{ "nemesis_id" , {"nemesis_record", "" }}
		}
	},
	{
		DF_Type::general_ref_poetic_formst,
		{
			{ "poetic_form_id" , {"poetic_form", "" }}
		}
	},
	{
		DF_Type::general_ref_projectile,
		{
			{ "projectile_id" , {"projectile", "" }}
		}
	},
	{
		DF_Type::general_ref_sitest,
		{
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::general_ref_subregionst,
		{
			{ "region_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::general_ref_unit,
		{
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::general_ref_written_contentst,
		{
			{ "written_content_id" , {"written_content", "" }}
		}
	},
	{
		DF_Type::glowing_barrier,
		{
			{ "buildings" , {"building", "" }}
		}
	},
	{
		DF_Type::hauling_stop,
		{
			{ "cart_id" , {"item", "" }}
		}
	},
	{
		DF_Type::histfig_entity_link,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::histfig_hf_link,
		{
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::histfig_site_link,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::historical_entity,
		{
			{ "children" , {"historical_entity", "" }},
			{ "founding_site_government" , {"historical_entity", "" }},
			{ "histfig_ids" , {"historical_figure", "" }},
			{ "nemesis_ids" , {"nemesis_record", "" }},
			{ "populations" , {"entity_population", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "squads" , {"squad", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_derived_resources,
		{
			{ "armor_chain" , {"itemdef_armorst", "" }},
			{ "armor_cover" , {"itemdef_armorst", "" }},
			{ "armor_leather" , {"itemdef_armorst", "" }},
			{ "armor_over" , {"itemdef_armorst", "" }},
			{ "armor_plate" , {"itemdef_armorst", "" }},
			{ "armor_under" , {"itemdef_armorst", "" }},
			{ "gloves_chain" , {"itemdef_glovesst", "" }},
			{ "gloves_cover" , {"itemdef_glovesst", "" }},
			{ "gloves_leather" , {"itemdef_glovesst", "" }},
			{ "gloves_over" , {"itemdef_glovesst", "" }},
			{ "gloves_plate" , {"itemdef_glovesst", "" }},
			{ "gloves_under" , {"itemdef_glovesst", "" }},
			{ "helm_chain" , {"itemdef_helmst", "" }},
			{ "helm_cover" , {"itemdef_helmst", "" }},
			{ "helm_leather" , {"itemdef_helmst", "" }},
			{ "helm_over" , {"itemdef_helmst", "" }},
			{ "helm_plate" , {"itemdef_helmst", "" }},
			{ "helm_under" , {"itemdef_helmst", "" }},
			{ "pants_chain" , {"itemdef_pantsst", "" }},
			{ "pants_cover" , {"itemdef_pantsst", "" }},
			{ "pants_leather" , {"itemdef_pantsst", "" }},
			{ "pants_over" , {"itemdef_pantsst", "" }},
			{ "pants_plate" , {"itemdef_pantsst", "" }},
			{ "pants_under" , {"itemdef_pantsst", "" }},
			{ "shoes_chain" , {"itemdef_shoesst", "" }},
			{ "shoes_cover" , {"itemdef_shoesst", "" }},
			{ "shoes_leather" , {"itemdef_shoesst", "" }},
			{ "shoes_over" , {"itemdef_shoesst", "" }},
			{ "shoes_plate" , {"itemdef_shoesst", "" }},
			{ "shoes_under" , {"itemdef_shoesst", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_relations,
		{
			{ "belief_systems" , {"belief_system", "" }},
			{ "deities" , {"historical_figure", "" }},
			{ "known_sites" , {"world_site", "" }},
			{ "official" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_relations__T_constructions,
		{
			{ "construction" , {"construction", "" }},
			{ "destination_site" , {"world_site", "" }},
			{ "source_site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_relations__T_diplomacy,
		{
			{ "group_id" , {"historical_entity", "" }},
			{ "historic_events" , {"history_event", "" }},
			{ "historic_events_collection" , {"history_event_collection", "" }},
			{ "war_event_collection" , {"history_event_collection", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_resources,
		{
			{ "ammo_type" , {"itemdef_ammost", "" }},
			{ "armor_type" , {"itemdef_armorst", "" }},
			{ "background_color" , {"descriptor_color", "" }},
			{ "digger_type" , {"itemdef_weaponst", "" }},
			{ "egg_castes" , {"caste_raw", "$$._parent.egg_races[$._key]" }},
			{ "egg_races" , {"creature_raw", "" }},
			{ "fish_castes" , {"caste_raw", "$$._parent.fish_races[$._key]" }},
			{ "fish_races" , {"creature_raw", "" }},
			{ "foreground_color" , {"descriptor_color", "" }},
			{ "gems" , {"inorganic_raw", "" }},
			{ "gloves_type" , {"itemdef_glovesst", "" }},
			{ "helm_type" , {"itemdef_helmst", "" }},
			{ "instrument_type" , {"itemdef_instrumentst", "" }},
			{ "metals" , {"inorganic_raw", "" }},
			{ "pants_type" , {"itemdef_pantsst", "" }},
			{ "shield_type" , {"itemdef_shieldst", "" }},
			{ "shoes_type" , {"itemdef_shoesst", "" }},
			{ "shrub_fruit_growths" , {"plant_growth", "$$._parent.shrub_fruit_plants[$._key]" }},
			{ "shrub_fruit_plants" , {"plant_raw", "" }},
			{ "siegeammo_type" , {"itemdef_siegeammost", "" }},
			{ "stones" , {"inorganic_raw", "" }},
			{ "tool_type" , {"itemdef_toolst", "" }},
			{ "toy_type" , {"itemdef_toyst", "" }},
			{ "training_weapon_type" , {"itemdef_weaponst", "" }},
			{ "trapcomp_type" , {"itemdef_trapcompst", "" }},
			{ "tree_fruit_growths" , {"plant_growth", "$$._parent.tree_fruit_plants[$._key]" }},
			{ "tree_fruit_plants" , {"plant_raw", "" }},
			{ "weapon_type" , {"itemdef_weaponst", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_resources__T_animals,
		{
			{ "exotic_pet_castes" , {"caste_raw", "$$._parent.unk728_races[$._key]" }},
			{ "exotic_pet_races" , {"creature_raw", "" }},
			{ "minion_castes" , {"caste_raw", "$$._parent.minion_races[$._key]" }},
			{ "minion_races" , {"creature_raw", "" }},
			{ "mount_castes" , {"caste_raw", "$$._parent.mount_races[$._key]" }},
			{ "mount_races" , {"creature_raw", "" }},
			{ "pack_animal_castes" , {"caste_raw", "$$._parent.pack_animal_races[$._key]" }},
			{ "pack_animal_races" , {"creature_raw", "" }},
			{ "pet_castes" , {"caste_raw", "$$._parent.pet_races[$._key]" }},
			{ "pet_races" , {"creature_raw", "" }},
			{ "wagon_castes" , {"caste_raw", "$$._parent.wagon_races[$._key]" }},
			{ "wagon_puller_castes" , {"caste_raw", "$$._parent.wagon_puller_races[$._key]" }},
			{ "wagon_puller_races" , {"creature_raw", "" }},
			{ "wagon_races" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_unk_v47_1,
		{
			{ "foiled_plot_agreements" , {"agreement", "" }}
		}
	},
	{
		DF_Type::historical_entity__T_unknown2,
		{
			{ "weapon_proficiencies" , {"job_skill", "" }}
		}
	},
	{
		DF_Type::historical_figure,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "civ_id" , {"historical_entity", "" }},
			{ "cultural_identity" , {"cultural_identity", "" }},
			{ "family_head_id" , {"historical_figure", "" }},
			{ "nemesis_id" , {"nemesis_record", "" }},
			{ "population_id" , {"entity_population", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::historical_figure__T_vague_relationships,
		{
			{ "hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_curse,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "original_caste" , {"caste_raw", "$$.original_race" }},
			{ "original_histfig_id" , {"historical_figure", "" }},
			{ "original_race" , {"creature_raw", "" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_known_info,
		{
			{ "known_dance_forms" , {"dance_form", "" }},
			{ "known_identities" , {"identity", "" }},
			{ "known_musical_forms" , {"musical_form", "" }},
			{ "known_poetic_forms" , {"poetic_form", "" }},
			{ "known_written_contents" , {"written_content", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_known_info__T_unk_a8,
		{
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_masterpieces,
		{
			{ "events" , {"history_event", "" }},
			{ "events2" , {"history_event", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_relationships,
		{
			{ "identities" , {"cultural_identity", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_relationships__T_artifact_claims,
		{
			{ "artifact_id" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_relationships__T_hf_historical,
		{
			{ "histfig_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_relationships__T_hf_visual,
		{
			{ "histfig_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_relationships__T_intrigues__T_intrigue,
		{
			{ "hf_1" , {"historical_figure", "" }},
			{ "hf_2" , {"historical_figure", "" }},
			{ "unk9_10" , {"historical_entity", "" }},
			{ "unk9_6" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_relationships__T_intrigues__T_plots,
		{
			{ "actor_nemesis_id" , {"nemesis_record", "" }},
			{ "agreement" , {"agreement", "" }},
			{ "delegated_plot_hfid" , {"historical_figure", "" }},
			{ "parent_plot_hfid" , {"historical_figure", "" }},
			{ "plotter_nemesis_id" , {"nemesis_record", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_reputation,
		{
			{ "all_identities" , {"identity", "" }},
			{ "cur_identity" , {"identity", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_reputation__T_anon_1,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_reputation__T_wanted__T_unk,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_skills__T_employment_held__T_employment,
		{
			{ "employer" , {"historical_entity", "" }},
			{ "held_honors" , {"honors_type", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_whereabouts,
		{
			{ "army_id" , {"army", "" }},
			{ "region_id" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "underground_region_id" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::historical_figure_info__T_wounds,
		{
			{ "events" , {"history_event", "" }}
		}
	},
	{
		DF_Type::historical_kills,
		{
			{ "events" , {"history_event", "" }},
			{ "killed_caste" , {"caste_raw", "$$._parent.killed_race[$._key]" }},
			{ "killed_race" , {"creature_raw", "" }},
			{ "killed_region" , {"world_region", "" }},
			{ "killed_site" , {"world_site", "" }},
			{ "killed_underground_region" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::history_era__T_details,
		{
			{ "power_hf1" , {"historical_figure", "" }},
			{ "power_hf2" , {"historical_figure", "" }},
			{ "power_hf3" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_era__T_title,
		{
			{ "histfig_1" , {"historical_figure", "" }},
			{ "histfig_2" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_add_entity_site_profile_flagst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_add_hf_entity_honorst,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_add_hf_entity_linkst,
		{
			{ "appointer_hfid" , {"historical_figure", "" }},
			{ "civ" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "promise_to_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_add_hf_hf_linkst,
		{
			{ "hf" , {"historical_figure", "" }},
			{ "hf_target" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_add_hf_site_linkst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_agreement_concludedst,
		{
			{ "concluder_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_agreements_voidedst,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_claim_formedst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_copiedst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "entity_dest" , {"historical_entity", "" }},
			{ "entity_src" , {"historical_entity", "" }},
			{ "site_dest" , {"world_site", "" }},
			{ "site_src" , {"world_site", "" }},
			{ "structure_dest" , {"abstract_building", "" }},
			{ "structure_src" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_createdst,
		{
			{ "artifact_id" , {"artifact_record", "" }},
			{ "creator_hfid" , {"historical_figure", "" }},
			{ "creator_unit_id" , {"unit", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_destroyedst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "destroyer_civ" , {"historical_entity", "" }},
			{ "destroyer_hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_droppedst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_foundst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_givenst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "giver_entity" , {"historical_entity", "" }},
			{ "giver_hf" , {"historical_figure", "" }},
			{ "receiver_entity" , {"historical_entity", "" }},
			{ "receiver_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_hiddenst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_lostst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "site" , {"world_site", "" }},
			{ "subregion_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_possessedst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "subregion_id" , {"world_region", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_recoveredst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_storedst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_artifact_transformedst,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "new_artifact" , {"artifact_record", "" }},
			{ "old_artifact" , {"artifact_record", "" }},
			{ "site" , {"world_site", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::history_event_assume_identityst,
		{
			{ "identity" , {"identity", "" }},
			{ "target" , {"historical_entity", "" }},
			{ "trickster" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_body_abusedst,
		{
			{ "bodies" , {"historical_figure", "" }},
			{ "civ" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "victim_entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_body_abusedst__T_abuse_data__T_Animated,
		{
			{ "interaction" , {"interaction", "" }}
		}
	},
	{
		DF_Type::history_event_body_abusedst__T_abuse_data__T_Flayed,
		{
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_body_abusedst__T_abuse_data__T_Hung,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "tree" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::history_event_body_abusedst__T_abuse_data__T_Impaled,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::history_event_building_profile_acquiredst,
		{
			{ "acquirer_entity" , {"historical_entity", "" }},
			{ "acquirer_hf" , {"historical_figure", "" }},
			{ "previous_owner_hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_ceremonyst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_change_creature_typest,
		{
			{ "changee" , {"historical_figure", "" }},
			{ "changer" , {"historical_figure", "" }},
			{ "new_caste" , {"caste_raw", "$$.race_to" }},
			{ "new_race" , {"creature_raw", "" }},
			{ "old_caste" , {"caste_raw", "$$.race_from" }},
			{ "old_race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::history_event_change_hf_body_statest,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_change_hf_jobst,
		{
			{ "hfid" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_change_hf_moodst,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_change_hf_statest,
		{
			{ "hfid" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_circumstance_info__T_data,
		{
			{ "AfterAbducting" , {"historical_figure", "" }},
			{ "Death" , {"historical_figure", "" }},
			{ "Defeated" , {"historical_figure", "" }},
			{ "DreamAbout" , {"historical_figure", "" }},
			{ "HistEventCollection" , {"history_event_collection", "" }},
			{ "Murdered" , {"historical_figure", "" }},
			{ "Prayer" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_collection,
		{
			{ "collections" , {"history_event_collection", "" }},
			{ "events" , {"history_event", "" }}
		}
	},
	{
		DF_Type::history_event_collection_abductionst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "snatcher_hf" , {"historical_figure", "" }},
			{ "victim_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_collection_battlest,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "attacker_hf" , {"historical_figure", "" }},
			{ "attacker_mercs" , {"historical_entity", "" }},
			{ "attacker_squad_entity_pop" , {"entity_population", "" }},
			{ "attacker_squad_races" , {"creature_raw", "" }},
			{ "attacker_squad_sites" , {"world_site", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "defender_hf" , {"historical_figure", "" }},
			{ "defender_mercs" , {"historical_entity", "" }},
			{ "defender_squad_entity_pops" , {"entity_population", "" }},
			{ "defender_squad_races" , {"creature_raw", "" }},
			{ "defender_squad_sites" , {"world_site", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "noncombat_hf" , {"historical_figure", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_beast_attackst,
		{
			{ "attacker_hf" , {"historical_figure", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_ceremonyst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "parent_collection" , {"history_event_collection", "" }}
		}
	},
	{
		DF_Type::history_event_collection_competitionst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "parent_collection" , {"history_event_collection", "" }}
		}
	},
	{
		DF_Type::history_event_collection_duelst,
		{
			{ "attacker_hf" , {"historical_figure", "" }},
			{ "defender_hf" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_entity_overthrownst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_insurrectionst,
		{
			{ "site" , {"world_site", "" }},
			{ "target_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_collection_journeyst,
		{
			{ "traveler_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_collection_occasionst,
		{
			{ "civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_collection_performancest,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "parent_collection" , {"history_event_collection", "" }}
		}
	},
	{
		DF_Type::history_event_collection_persecutionst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_processionst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "parent_collection" , {"history_event_collection", "" }}
		}
	},
	{
		DF_Type::history_event_collection_purgest,
		{
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_raidst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "thieves" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_collection_site_conqueredst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_collection_theftst,
		{
			{ "layer" , {"world_underground_region", "" }},
			{ "parent_collection" , {"history_event_collection", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "stolen_item_ids" , {"item", "" }},
			{ "thief_civ" , {"historical_entity", "" }},
			{ "thief_hf" , {"historical_figure", "" }},
			{ "victim_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_collection_warst,
		{
			{ "anon_1" , {"historical_entity", "" }},
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_collection_warst__T_unk,
		{
			{ "attacker_entity_leader" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_competitionst,
		{
			{ "competitor_hf" , {"historical_figure", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "winner_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_context,
		{
			{ "abstract_building_id" , {"abstract_building", "" }},
			{ "activity_id" , {"activity_entry", "" }},
			{ "agreement_id" , {"agreement", "" }},
			{ "army_controller_id" , {"army_controller", "" }},
			{ "army_id" , {"army", "" }},
			{ "artifact_id" , {"artifact_record", "" }},
			{ "battlefield_id" , {"battlefield", "" }},
			{ "belief_system_id" , {"belief_system", "" }},
			{ "breed_id" , {"breed", "" }},
			{ "caste" , {"caste_raw", "" }},
			{ "creation_zone_id" , {"world_object_data", "" }},
			{ "crime_id" , {"crime", "" }},
			{ "cultural_identity_id" , {"cultural_identity", "" }},
			{ "dance_form_id" , {"dance_form", "" }},
			{ "divination_set_id" , {"divination_set", "" }},
			{ "entity_id" , {"historical_entity", "" }},
			{ "histfig_id" , {"historical_figure", "" }},
			{ "identity_id" , {"identity", "" }},
			{ "image_set_id" , {"image_set", "" }},
			{ "incident_id" , {"incident", "" }},
			{ "interaction_instance_id" , {"interaction_instance", "" }},
			{ "layer_id" , {"world_underground_region", "" }},
			{ "musical_form_id" , {"musical_form", "" }},
			{ "occupation_id" , {"occupation", "" }},
			{ "poetic_form_id" , {"poetic_form", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "region_id" , {"world_region", "" }},
			{ "region_weather_id" , {"region_weather", "" }},
			{ "rhythm_id" , {"rhythm", "" }},
			{ "scale_id" , {"scale", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "speaker_id" , {"historical_figure", "" }},
			{ "squad_id" , {"squad", "" }},
			{ "vehicle_id" , {"vehicle", "" }},
			{ "written_content_id" , {"written_content", "" }}
		}
	},
	{
		DF_Type::history_event_context__T_unk_10,
		{
			{ "interrogator_id" , {"historical_figure", "" }},
			{ "subject_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_create_entity_positionst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_created_buildingst,
		{
			{ "builder_hf" , {"historical_figure", "" }},
			{ "civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_created_sitest,
		{
			{ "builder_hf" , {"historical_figure", "" }},
			{ "civ" , {"historical_entity", "" }},
			{ "resident_civ_id" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_created_world_constructionst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "site1" , {"world_site", "" }},
			{ "site2" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_creature_devouredst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "eater" , {"historical_figure", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "victim" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_dance_form_createdst,
		{
			{ "form" , {"dance_form", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_diplomat_lostst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "involved" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_entity_actionst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_entity_alliance_formedst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "joining_entities" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_entity_breach_feature_layerst,
		{
			{ "civ_entity" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_entity_createdst,
		{
			{ "creator_hfid" , {"historical_figure", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_entity_dissolvedst,
		{
			{ "entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_entity_equipment_purchasest,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "hfs" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_entity_expels_hfst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "expelled" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_entity_fled_sitest,
		{
			{ "fled_civ_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_entity_incorporatedst,
		{
			{ "join_entity" , {"historical_entity", "" }},
			{ "leader_hfid" , {"historical_figure", "" }},
			{ "migrant_entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_entity_lawst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_entity_overthrownst,
		{
			{ "conspirator_hfs" , {"historical_figure", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "instigator_hf" , {"historical_figure", "" }},
			{ "overthrown_hf" , {"historical_figure", "" }},
			{ "position_taker_hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_entity_persecutedst,
		{
			{ "expelled_hfs" , {"historical_figure", "" }},
			{ "persecuting_entity" , {"historical_entity", "" }},
			{ "persecuting_hf" , {"historical_figure", "" }},
			{ "property_confiscated_from_hfs" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "target_entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_entity_rampaged_in_sitest,
		{
			{ "rampage_civ_id" , {"historical_entity", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_entity_razed_buildingst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_entity_searched_sitest,
		{
			{ "searcher_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_failed_frame_attemptst,
		{
			{ "convicter_entity" , {"historical_entity", "" }},
			{ "fooled_hf" , {"historical_figure", "" }},
			{ "framer_hf" , {"historical_figure", "" }},
			{ "plotter_hf" , {"historical_figure", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_failed_intrigue_corruptionst,
		{
			{ "corruptor_hf" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_first_contact_failedst,
		{
			{ "contactor" , {"historical_entity", "" }},
			{ "rejector" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_first_contactst,
		{
			{ "contacted" , {"historical_entity", "" }},
			{ "contactor" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_gamblest,
		{
			{ "hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hf_act_on_artifactst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_hf_act_on_buildingst,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_hf_attacked_sitest,
		{
			{ "attacker_hf" , {"historical_figure", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_hf_confrontedst,
		{
			{ "accuser" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "target" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_convictedst,
		{
			{ "behest_of_hf" , {"historical_figure", "" }},
			{ "co_conspirator_hf" , {"historical_figure", "" }},
			{ "convicted_hf" , {"historical_figure", "" }},
			{ "convicter_entity" , {"historical_entity", "" }},
			{ "corrupt_hf" , {"historical_figure", "" }},
			{ "fooled_hf" , {"historical_figure", "" }},
			{ "framer_hf" , {"historical_figure", "" }},
			{ "implicated_hfs" , {"historical_figure", "" }},
			{ "recognized_by_entity" , {"historical_entity", "" }},
			{ "recognized_by_hf" , {"historical_figure", "" }},
			{ "surveillance_hf" , {"historical_figure", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_destroyed_sitest,
		{
			{ "attacker_hf" , {"historical_figure", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_hf_does_interactionst,
		{
			{ "doer" , {"historical_figure", "" }},
			{ "interaction" , {"interaction", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"interaction_source", "$$.interaction" }},
			{ "target" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_enslavedst,
		{
			{ "enslaved_hf" , {"historical_figure", "" }},
			{ "moved_to_site" , {"world_site", "" }},
			{ "payer_entity" , {"historical_entity", "" }},
			{ "seller_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_freedst,
		{
			{ "freeing_civ" , {"historical_entity", "" }},
			{ "freeing_hf" , {"historical_figure", "" }},
			{ "holding_civ" , {"historical_entity", "" }},
			{ "rescued_hfs" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_hf_gains_secret_goalst,
		{
			{ "histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_interrogatedst,
		{
			{ "arresting_entity" , {"historical_entity", "" }},
			{ "implicated_hfs" , {"historical_figure", "" }},
			{ "interrogator_hf" , {"historical_figure", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_learns_secretst,
		{
			{ "artifact" , {"artifact_record", "" }},
			{ "interaction" , {"interaction", "" }},
			{ "student" , {"historical_figure", "" }},
			{ "teacher" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_preachst,
		{
			{ "entity1" , {"historical_entity", "" }},
			{ "entity2" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "speaker_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_ransomedst,
		{
			{ "moved_to_site" , {"world_site", "" }},
			{ "payer_entity" , {"historical_entity", "" }},
			{ "payer_hf" , {"historical_figure", "" }},
			{ "ransomed_hf" , {"historical_figure", "" }},
			{ "ransomer_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hf_razed_buildingst,
		{
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_hf_recruited_unit_type_for_entityst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hf_relationship_deniedst,
		{
			{ "layer" , {"world_underground_region", "" }},
			{ "reason_id" , {"historical_figure", "" }},
			{ "region" , {"world_region", "" }},
			{ "seeker_hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hfs_formed_intrigue_relationshipst,
		{
			{ "corruptor_hf" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hfs_formed_reputation_relationshipst,
		{
			{ "histfig1" , {"historical_figure", "" }},
			{ "histfig2" , {"historical_figure", "" }},
			{ "identity1" , {"identity", "" }},
			{ "identity2" , {"identity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_abductedst,
		{
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "snatcher" , {"historical_figure", "" }},
			{ "target" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_diedst,
		{
			{ "feature_layer" , {"world_underground_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "slayer_caste" , {"caste_raw", "$$.slayer_race" }},
			{ "slayer_hf" , {"historical_figure", "" }},
			{ "slayer_race" , {"creature_raw", "" }},
			{ "subregion" , {"world_region", "" }},
			{ "victim_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_new_petst,
		{
			{ "group" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "pets" , {"creature_raw", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_reach_summitst,
		{
			{ "group" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_reunionst,
		{
			{ "assistant" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "missing" , {"historical_figure", "" }},
			{ "region" , {"world_region", "" }},
			{ "reunited_with" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_revivedst,
		{
			{ "actor_hfid" , {"historical_figure", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "interaction" , {"interaction", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_simple_actionst,
		{
			{ "group_hfs" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_simple_battle_eventst,
		{
			{ "group1" , {"historical_figure", "" }},
			{ "group2" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_travelst,
		{
			{ "group" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_hist_figure_woundedst,
		{
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "woundee" , {"historical_figure", "" }},
			{ "woundee_caste" , {"caste_raw", "$$.woundee_race" }},
			{ "woundee_race" , {"creature_raw", "" }},
			{ "wounder" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_insurrection_endedst,
		{
			{ "site" , {"world_site", "" }},
			{ "target_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_insurrection_startedst,
		{
			{ "site" , {"world_site", "" }},
			{ "target_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_item_stolenst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "item" , {"item", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "mattype" , {"material", "$$.matindex" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_knowledge_discoveredst,
		{
			{ "hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_created_dye_itemst,
		{
			{ "dye_mat_type" , {"material", "$$.dye_mat_index" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_created_engravingst,
		{
			{ "art_id" , {"art_image_chunk", "" }},
			{ "art_subid" , {"art_image", "$$.art_id" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_created_foodst,
		{
			{ "item_id" , {"item", "" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_created_item_improvementst,
		{
			{ "art_id" , {"art_image_chunk", "" }},
			{ "art_subid" , {"art_image", "$$.art_id" }},
			{ "imp_mat_type" , {"material", "$$.imp_mat_index" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_created_itemst,
		{
			{ "item_id" , {"item", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_createdst,
		{
			{ "maker" , {"historical_figure", "" }},
			{ "maker_entity" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_masterpiece_lostst,
		{
			{ "creation_event" , {"history_event", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_merchantst,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_modified_buildingst,
		{
			{ "hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_musical_form_createdst,
		{
			{ "form" , {"musical_form", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_performancest,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_poetic_form_createdst,
		{
			{ "form" , {"poetic_form", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_processionst,
		{
			{ "entity" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_reason_info__T_data,
		{
			{ "artifact_is_heirloom_of_family_hfid" , {"historical_figure", "" }},
			{ "artifact_is_symbol_of_entity_position" , {"historical_entity", "" }},
			{ "glorify_hf" , {"historical_figure", "" }},
			{ "sanctify_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_reclaim_sitest,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_regionpop_incorporated_into_entityst,
		{
			{ "join_entity" , {"historical_entity", "" }},
			{ "pop_layer" , {"world_underground_region", "" }},
			{ "pop_race" , {"creature_raw", "" }},
			{ "pop_region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_remove_hf_entity_linkst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_remove_hf_hf_linkst,
		{
			{ "hf" , {"historical_figure", "" }},
			{ "hf_target" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_remove_hf_site_linkst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }}
		}
	},
	{
		DF_Type::history_event_replaced_buildingst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "new_structure" , {"abstract_building", "" }},
			{ "old_structure" , {"abstract_building", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_sabotagest,
		{
			{ "saboteur_hf" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }},
			{ "target_entity" , {"historical_entity", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_site_diedst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_site_disputest,
		{
			{ "entity_1" , {"historical_entity", "" }},
			{ "entity_2" , {"historical_entity", "" }},
			{ "site_1" , {"world_site", "" }},
			{ "site_2" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_site_retiredst,
		{
			{ "civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_site_surrenderedst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_sneak_into_sitest,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_spotted_leaving_sitest,
		{
			{ "leaver_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }},
			{ "spotter_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::history_event_squad_vs_squadst,
		{
			{ "a_hfid" , {"historical_figure", "" }},
			{ "a_leader_hfid" , {"historical_figure", "" }},
			{ "a_race" , {"creature_raw", "" }},
			{ "d_hfid" , {"historical_figure", "" }},
			{ "d_leader_hfid" , {"historical_figure", "" }},
			{ "d_race" , {"creature_raw", "" }},
			{ "feature_layer" , {"world_underground_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }},
			{ "subregion" , {"world_region", "" }}
		}
	},
	{
		DF_Type::history_event_tactical_situationst,
		{
			{ "a_tactician_hfid" , {"historical_figure", "" }},
			{ "d_tactician_hfid" , {"historical_figure", "" }},
			{ "feature_layer" , {"world_underground_region", "" }},
			{ "site" , {"world_site", "" }},
			{ "structure" , {"abstract_building", "" }},
			{ "subregion" , {"world_region", "" }}
		}
	},
	{
		DF_Type::history_event_topicagreement_concludedst,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_topicagreement_madest,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_topicagreement_rejectedst,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_tradest,
		{
			{ "dest_site" , {"world_site", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "hf" , {"historical_figure", "" }},
			{ "source_site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_event_war_attacked_sitest,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "attacker_general_hf" , {"historical_figure", "" }},
			{ "attacker_merc_enid" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "defender_general_hf" , {"historical_figure", "" }},
			{ "defender_merc_enid" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_destroyed_sitest,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_field_battlest,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "attacker_general_hf" , {"historical_figure", "" }},
			{ "attacker_merc_enid" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "defender_general_hf" , {"historical_figure", "" }},
			{ "defender_merc_enid" , {"historical_entity", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }}
		}
	},
	{
		DF_Type::history_event_war_peace_acceptedst,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_peace_rejectedst,
		{
			{ "destination" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "source" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_plundered_sitest,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_site_new_leaderst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "new_leaders" , {"historical_figure", "" }},
			{ "new_site_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_site_taken_overst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "new_site_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_war_site_tribute_forcedst,
		{
			{ "attacker_civ" , {"historical_entity", "" }},
			{ "defender_civ" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "site_civ" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::history_event_written_content_composedst,
		{
			{ "content" , {"written_content", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "layer" , {"world_underground_region", "" }},
			{ "region" , {"world_region", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::history_hit_item,
		{
			{ "item" , {"item", "" }},
			{ "mattype" , {"material", "$$.matindex" }},
			{ "shooter_item" , {"item", "" }},
			{ "shooter_mattype" , {"material", "$$.shooter_matindex" }}
		}
	},
	{
		DF_Type::honors_type,
		{
			{ "honored" , {"historical_figure", "" }},
			{ "required_former_position" , {"entity_position", "" }},
			{ "required_position" , {"entity_position", "" }}
		}
	},
	{
		DF_Type::identity,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "entity_id" , {"historical_entity", "" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::incident,
		{
			{ "activity_id" , {"activity_entry", "" }},
			{ "crime_id" , {"crime", "" }},
			{ "criminal" , {"unit", "" }},
			{ "criminal_caste" , {"caste_raw", "$$.killer_race" }},
			{ "criminal_race" , {"creature_raw", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "entity1" , {"historical_entity", "" }},
			{ "entity2" , {"historical_entity", "" }},
			{ "site" , {"world_site", "" }},
			{ "victim" , {"unit", "" }},
			{ "victim_caste" , {"caste_raw", "$$.victim_race" }},
			{ "victim_race" , {"creature_raw", "" }},
			{ "witnesses" , {"unit", "" }}
		}
	},
	{
		DF_Type::incident_hfid,
		{
			{ "hfid" , {"historical_figure", "" }},
			{ "unk_hfid" , {"historical_figure", "" }},
			{ "unk_hfid2" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::incident_sub6_performance,
		{
			{ "dance_form_id" , {"dance_form", "" }},
			{ "musical_form_id" , {"musical_form", "" }},
			{ "poetic_form_id" , {"poetic_form", "" }}
		}
	},
	{
		DF_Type::incident_sub7,
		{
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::inorganic_raw,
		{
			{ "economic_uses" , {"reaction", "" }},
			{ "source_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::interaction,
		{
			{ "source_enid" , {"historical_entity", "" }},
			{ "source_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::interaction_effect,
		{
			{ "interaction_id" , {"interaction", "" }}
		}
	},
	{
		DF_Type::interaction_instance,
		{
			{ "affected_units" , {"unit", "" }},
			{ "interaction_id" , {"interaction", "" }}
		}
	},
	{
		DF_Type::interaction_target_materialst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::interface_button_building_material_selectorst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::interface_button_building_new_jobst,
		{
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::interface_button_construction_building_selectorst,
		{
			{ "custom_type" , {"building_def", "" }}
		}
	},
	{
		DF_Type::interrogation_report,
		{
			{ "officer_hf" , {"historical_figure", "" }},
			{ "officer_hf2" , {"historical_figure", "" }},
			{ "subject_hf" , {"historical_figure", "" }},
			{ "subject_hf2" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::invasion_info,
		{
			{ "civ_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::item,
		{
			{ "world_data_id" , {"world_object_data", "" }}
		}
	},
	{
		DF_Type::item_ballistaarrowheadst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_barst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_blocksst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_body_component,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "caste2" , {"caste_raw", "$$.race2" }},
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "hist_figure_id2" , {"historical_figure", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "race2" , {"creature_raw", "" }},
			{ "unit_id" , {"unit", "" }},
			{ "unit_id2" , {"unit", "" }}
		}
	},
	{
		DF_Type::item_body_component__T_bone1,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_body_component__T_bone2,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_boulderst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_branchst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_cheesest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_coinst,
		{
			{ "coin_batch" , {"coin_batch", "" }}
		}
	},
	{
		DF_Type::item_crafted,
		{
			{ "maker" , {"historical_figure", "" }},
			{ "maker_race" , {"creature_raw", "" }},
			{ "masterpiece_event" , {"history_event", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_critter,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::item_eggst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "fathers_caste" , {"caste_raw", "$$.race" }},
			{ "hatchling_civ_id" , {"historical_entity", "" }},
			{ "hatchling_mother_id" , {"unit", "" }},
			{ "hatchling_population_id" , {"entity_population", "" }},
			{ "mothers_caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::item_filter_spec,
		{
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::item_fish_rawst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::item_fishst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::item_foodst,
		{
			{ "entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::item_foodst__T_ingredients,
		{
			{ "maker" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_gemst,
		{
			{ "shape" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::item_globst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_kill_info,
		{
			{ "slayers" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::item_liquid,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_meatst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_petst,
		{
			{ "owner_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::item_plant_growthst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_plantst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_powder,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_remainsst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::item_rockst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_roughst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_seedsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_skin_tannedst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_slabst,
		{
			{ "topic" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::item_smallgemst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "shape" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::item_stockpile_ref,
		{
			{ "id" , {"building", "" }}
		}
	},
	{
		DF_Type::item_threadst,
		{
			{ "dye_mat_type" , {"material", "$$.dye_mat_index" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::item_toolst,
		{
			{ "vehicle_id" , {"vehicle", "" }}
		}
	},
	{
		DF_Type::item_totemst,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::item_woodst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::itemdef,
		{
			{ "source_enid" , {"historical_entity", "" }},
			{ "source_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::itemdef_toolst,
		{
			{ "shape_category" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::itemimprovement,
		{
			{ "maker" , {"historical_figure", "" }},
			{ "masterpiece_event" , {"history_event", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::itemimprovement_bandsst,
		{
			{ "shape" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::itemimprovement_coveredst,
		{
			{ "shape" , {"descriptor_shape", "" }}
		}
	},
	{
		DF_Type::itemimprovement_pagesst,
		{
			{ "contents" , {"written_content", "" }}
		}
	},
	{
		DF_Type::itemimprovement_sewn_imagest__T_cloth,
		{
			{ "unit_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::itemimprovement_writingst,
		{
			{ "contents" , {"written_content", "" }}
		}
	},
	{
		DF_Type::job,
		{
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::job_item,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "metal_ore" , {"inorganic_raw", "" }},
			{ "reaction_id" , {"reaction", "" }}
		}
	},
	{
		DF_Type::job_item_filter,
		{
			{ "burrows" , {"burrow", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "metal_ore" , {"inorganic_raw", "" }},
			{ "reaction_id" , {"reaction", "" }}
		}
	},
	{
		DF_Type::language_name,
		{
			{ "language" , {"language_translation", "" }}
		}
	},
	{
		DF_Type::machine__T_components,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::machine_info,
		{
			{ "machine_id" , {"machine", "" }}
		}
	},
	{
		DF_Type::manager_order,
		{
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "workshop_id" , {"building", "" }}
		}
	},
	{
		DF_Type::manager_order_condition_item,
		{
			{ "inorganic_bearing" , {"inorganic_raw", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::manager_order_condition_order,
		{
			{ "order_id" , {"manager_order", "" }}
		}
	},
	{
		DF_Type::manager_order_template,
		{
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::mandate,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::map_block,
		{
			{ "global_feature" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::material_common__T_hardens_with_water,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::meeting_diplomat_info,
		{
			{ "agreement_entity" , {"historical_entity", "" }},
			{ "associate_id" , {"historical_figure", "" }},
			{ "civ_id" , {"historical_entity", "" }},
			{ "contact_entity" , {"historical_entity", "" }},
			{ "diplomat_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::mission,
		{
			{ "army_controller" , {"army_controller", "" }},
			{ "army_controller2" , {"army_controller", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "histfig" , {"historical_figure", "" }},
			{ "messengers" , {"occupation", "" }},
			{ "squads" , {"squad", "" }},
			{ "target_site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::mission__T_details__T_recovery,
		{
			{ "artifact" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::mission__T_details__T_request,
		{
			{ "workers" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::mission__T_details__T_rescue,
		{
			{ "histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::mission_campaign_report,
		{
			{ "event_id" , {"history_event", "" }}
		}
	},
	{
		DF_Type::moving_party,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "region_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::moving_party__T_members,
		{
			{ "nemesis_id" , {"nemesis_record", "" }}
		}
	},
	{
		DF_Type::musical_form,
		{
			{ "devotion_target" , {"historical_figure", "" }},
			{ "original_author" , {"historical_figure", "" }},
			{ "originating_entity" , {"historical_entity", "" }},
			{ "poetic_form_id" , {"poetic_form", "" }},
			{ "rhythm_id" , {"rhythm", "" }},
			{ "scale_id" , {"scale", "" }},
			{ "written_content_id" , {"written_content", "" }}
		}
	},
	{
		DF_Type::musical_form_instruments,
		{
			{ "instrument_subtype" , {"itemdef_instrumentst", "" }}
		}
	},
	{
		DF_Type::nemesis_record,
		{
			{ "group_leader_id" , {"nemesis_record", "" }},
			{ "save_file_id" , {"unit_chunk", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::occupation,
		{
			{ "group_id" , {"historical_entity", "" }},
			{ "histfig_id" , {"historical_figure", "" }},
			{ "location_id" , {"abstract_building", "$$.site_id" }},
			{ "site_id" , {"world_site", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::plant,
		{
			{ "material" , {"plant_raw", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "srb_id" , {"site_realization_building", "$$.site_id" }}
		}
	},
	{
		DF_Type::plant_growth,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::plant_raw,
		{
			{ "source_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::plant_raw__T_material_defs,
		{
			{ "type_basic_mat" , {"material", "$$.idx_basic_mat" }},
			{ "type_drink" , {"material", "$$.idx_drink" }},
			{ "type_extract_barrel" , {"material", "$$.idx_extract_barrel" }},
			{ "type_extract_still_vial" , {"material", "$$.idx_extract_still_vial" }},
			{ "type_extract_vial" , {"material", "$$.idx_extract_vial" }},
			{ "type_mill" , {"material", "$$.idx_mill" }},
			{ "type_seed" , {"material", "$$.idx_seed" }},
			{ "type_thread" , {"material", "$$.idx_thread" }},
			{ "type_tree" , {"material", "$$.idx_tree" }}
		}
	},
	{
		DF_Type::plot_agreement,
		{
			{ "actor_id" , {"historical_figure", "" }},
			{ "agreement_id" , {"agreement", "" }}
		}
	},
	{
		DF_Type::poetic_form,
		{
			{ "original_author" , {"historical_figure", "" }},
			{ "originating_entity" , {"historical_entity", "" }},
			{ "subject_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::poetic_form_perspective,
		{
			{ "histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::poetic_form_subject_target__T_Histfig,
		{
			{ "subject_histfig" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::projectile,
		{
			{ "bow_id" , {"item", "" }},
			{ "unk_item_id" , {"item", "" }},
			{ "unk_unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::property_ownership,
		{
			{ "owner_entity_id" , {"historical_entity", "" }},
			{ "owner_hfid" , {"historical_figure", "" }},
			{ "unk_hfid" , {"historical_figure", "" }},
			{ "unk_owner_entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::punishment,
		{
			{ "chain" , {"building", "" }},
			{ "criminal" , {"unit", "" }},
			{ "officer" , {"unit", "" }},
			{ "victims" , {"unit", "" }}
		}
	},
	{
		DF_Type::reaction,
		{
			{ "source_enid" , {"historical_entity", "" }},
			{ "source_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::reaction_product_item_improvementst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::reaction_product_itemst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::reaction_reagent_itemst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "metal_ore" , {"inorganic_raw", "" }}
		}
	},
	{
		DF_Type::region_map_entry,
		{
			{ "geo_index" , {"world_geo_biome", "" }},
			{ "landmass_id" , {"world_landmass", "" }},
			{ "region_id" , {"world_region", "" }}
		}
	},
	{
		DF_Type::region_weather,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::relationship_event,
		{
			{ "source_hf" , {"historical_figure", "" }},
			{ "target_hf" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::relationship_event_supplement,
		{
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_ammost,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_anvilst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_armor_bodyst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_armor_bootsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_armor_glovesst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_armor_helmst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_armor_pantsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_backpackst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_bagst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_bedst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_bonest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_boxst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_cabinetst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_chairst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_cheesest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_clothing_bodyst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_clothing_bootsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_clothing_glovesst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_clothing_helmst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_clothing_pantsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_clothst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_craftsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_cropst,
		{
			{ "mat_type" , {"plant_raw", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_extractst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "mat_type2" , {"material", "$$.mat_index2" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_flaskst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_gemsst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_hornst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_leatherst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_meatst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_metalst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_pearlst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_powderst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_quiverst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_shellst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_skinst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "mat_type2" , {"material", "$$.mat_index2" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_soapst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_stonest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_tablest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_tallowst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_threadst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_toothst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_weapon_meleest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_weapon_rangedst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::resource_allotment_specifier_woodst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::route_stockpile_link,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::setup_character_info,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "cultural_identity" , {"cultural_identity", "" }},
			{ "deity_id" , {"historical_figure", "" }},
			{ "entity" , {"historical_entity", "" }},
			{ "home_site_id" , {"world_site", "" }},
			{ "nemesis" , {"nemesis_record", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::site_building_item,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::site_realization_building__T_unk_4c,
		{
			{ "owner" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::spatter_common,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::spoils_report,
		{
			{ "creature_castes" , {"caste_raw", "$$._parent.creature_races[$._key]" }},
			{ "creature_races" , {"creature_raw", "" }},
			{ "mat_types" , {"material", "" }}
		}
	},
	{
		DF_Type::squad,
		{
			{ "activity" , {"activity_entry", "" }},
			{ "entity_id" , {"historical_entity", "" }},
			{ "rack_combat" , {"building", "" }},
			{ "rack_training" , {"building", "" }}
		}
	},
	{
		DF_Type::squad__T_rooms,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::squad_order_cause_trouble_for_entityst,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::squad_order_defend_burrowsst,
		{
			{ "burrows" , {"burrow", "" }}
		}
	},
	{
		DF_Type::squad_order_kill_hfst,
		{
			{ "histfig_id" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::squad_order_retrieve_artifactst,
		{
			{ "artifact_id" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::squad_position,
		{
			{ "activities" , {"activity_entry", "" }},
			{ "backpack" , {"item", "" }},
			{ "events" , {"activity_event", "$._global.activities[$._key]" }},
			{ "flask" , {"item", "" }},
			{ "occupant" , {"historical_figure", "" }},
			{ "quiver" , {"item", "" }}
		}
	},
	{
		DF_Type::squad_uniform_spec,
		{
			{ "item" , {"item", "" }}
		}
	},
	{
		DF_Type::temple_deity_data,
		{
			{ "Deity" , {"historical_figure", "" }},
			{ "Religion" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::tissue,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::tissue_template,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::training_assignment,
		{
			{ "animal_id" , {"unit", "" }},
			{ "trainer_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::ui,
		{
			{ "aid_requesters" , {"unit", "" }},
			{ "civ_id" , {"historical_entity", "" }},
			{ "farm_crops" , {"plant_raw", "" }},
			{ "follow_item" , {"item", "" }},
			{ "follow_unit" , {"unit", "" }},
			{ "group_id" , {"historical_entity", "" }},
			{ "lost_to_siege_civ" , {"historical_entity", "" }},
			{ "meeting_requests" , {"unit", "" }},
			{ "race_id" , {"creature_raw", "" }},
			{ "selected_farm_crops" , {"plant_raw", "" }},
			{ "site_id" , {"world_site", "" }},
			{ "unk_races" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::ui__T_alerts__T_list,
		{
			{ "burrows" , {"burrow", "" }}
		}
	},
	{
		DF_Type::ui__T_burrows,
		{
			{ "sel_id" , {"burrow", "" }}
		}
	},
	{
		DF_Type::ui__T_main__T_dead_citizens,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::ui__T_tax_collection,
		{
			{ "rooms" , {"building", "" }}
		}
	},
	{
		DF_Type::ui_advmode,
		{
			{ "odor_caste" , {"caste_raw", "$$.odor_race" }},
			{ "odor_race" , {"creature_raw", "" }},
			{ "player_id" , {"nemesis_record", "" }},
			{ "travel_odor_caste" , {"caste_raw", "$$.travel_odor_race" }},
			{ "travel_odor_race" , {"creature_raw", "" }},
			{ "unk_24" , {"world_site", "" }}
		}
	},
	{
		DF_Type::ui_advmode__T_assume_identity,
		{
			{ "origin" , {"historical_entity", "" }},
			{ "worship_object" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::ui_advmode__T_companions,
		{
			{ "all_histfigs" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::ui_advmode__T_conversation__T_targets,
		{
			{ "histfig_id" , {"historical_figure", "" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::ui_build_selector,
		{
			{ "custom_type" , {"building_def", "" }}
		}
	},
	{
		DF_Type::ui_look_list__T_items,
		{
			{ "spatter_mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::ui_sidebar_menus__T_workshop_job,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::unit,
		{
			{ "activities" , {"activity_entry", "" }},
			{ "burrows" , {"burrow", "" }},
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "civ_id" , {"historical_entity", "" }},
			{ "conversations" , {"activity_entry", "" }},
			{ "corpse_parts" , {"item", "" }},
			{ "cultural_identity" , {"cultural_identity", "" }},
			{ "hist_figure_id" , {"historical_figure", "" }},
			{ "hist_figure_id2" , {"historical_figure", "" }},
			{ "invasion_id" , {"invasion_info", "" }},
			{ "owned_items" , {"item", "" }},
			{ "population_id" , {"entity_population", "" }},
			{ "pregnancy_caste" , {"caste_raw", "$$._global.race" }},
			{ "pregnancy_spouse" , {"historical_figure", "" }},
			{ "race" , {"creature_raw", "" }},
			{ "riding_item_id" , {"item", "" }},
			{ "schedule_id" , {"schedule_info", "" }},
			{ "social_activities" , {"activity_entry", "" }},
			{ "traded_items" , {"item", "" }}
		}
	},
	{
		DF_Type::unit__T_appearance,
		{
			{ "tissue_style_civ_id" , {"historical_entity", "" }},
			{ "tissue_style_id" , {"entity_tissue_style", "$$._parent.tissue_style_civ_id[$._key]" }}
		}
	},
	{
		DF_Type::unit__T_counters,
		{
			{ "death_id" , {"incident", "" }}
		}
	},
	{
		DF_Type::unit__T_curse,
		{
			{ "interaction_id" , {"creature_interaction_effect", "" }}
		}
	},
	{
		DF_Type::unit__T_enemy,
		{
			{ "army_controller_id" , {"army_controller", "" }},
			{ "normal_caste" , {"caste_raw", "$$.normal_race" }},
			{ "normal_race" , {"creature_raw", "" }},
			{ "unk_unit_id_1" , {"unit", "" }},
			{ "unk_unit_id_2" , {"unit", "" }},
			{ "were_caste" , {"caste_raw", "$$.were_race" }},
			{ "were_race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::unit__T_enemy__T_witness_reports,
		{
			{ "crime_id" , {"crime", "" }},
			{ "death_id" , {"incident", "" }}
		}
	},
	{
		DF_Type::unit__T_meeting,
		{
			{ "target_entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::unit__T_military,
		{
			{ "individual_drills" , {"activity_entry", "" }},
			{ "squad_id" , {"squad", "" }},
			{ "uniform_drop" , {"item", "" }},
			{ "uniform_pickup" , {"item", "" }},
			{ "unk_items" , {"item", "" }}
		}
	},
	{
		DF_Type::unit__T_opponent,
		{
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit__T_status,
		{
			{ "attacker_ids" , {"unit", "" }},
			{ "observed_traps" , {"building", "" }}
		}
	},
	{
		DF_Type::unit__T_syndromes,
		{
			{ "reinfection_type" , {"syndrome", "" }}
		}
	},
	{
		DF_Type::unit_action_data_attack,
		{
			{ "attack_item_id" , {"item", "" }},
			{ "target_unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_action_data_block,
		{
			{ "block_item_id" , {"item", "" }},
			{ "target_action" , {"unit_action", "$$.unit_id" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_action_data_parry,
		{
			{ "parry_item_id" , {"item", "" }},
			{ "target_action" , {"unit_action", "$$.unit_id" }},
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_action_data_suck_blood,
		{
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_action_data_talk,
		{
			{ "activity_event_idx" , {"activity_event", "$$.activity_id" }},
			{ "activity_id" , {"activity_entry", "" }}
		}
	},
	{
		DF_Type::unit_coin_debt,
		{
			{ "recipient" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_demand,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::unit_ghost_info__T_target,
		{
			{ "building" , {"building", "" }},
			{ "item" , {"item", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_health_info,
		{
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_health_info__T_op_history,
		{
			{ "doctor_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_health_info__T_op_history__T_info,
		{
			{ "bed_id" , {"building", "" }}
		}
	},
	{
		DF_Type::unit_health_info__T_op_history__T_info__T_bandage,
		{
			{ "item_id" , {"item", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::unit_health_info__T_op_history__T_info__T_crutch,
		{
			{ "item_id" , {"item", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::unit_item_use,
		{
			{ "id" , {"item", "" }}
		}
	},
	{
		DF_Type::unit_item_wrestle,
		{
			{ "item1" , {"item", "" }},
			{ "item2" , {"item", "" }},
			{ "unit" , {"unit", "" }}
		}
	},
	{
		DF_Type::unit_personality,
		{
			{ "civ_id" , {"historical_entity", "" }},
			{ "cultural_identity" , {"cultural_identity", "" }}
		}
	},
	{
		DF_Type::unit_preference,
		{
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::unit_soul,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::unit_syndrome,
		{
			{ "type" , {"syndrome", "" }}
		}
	},
	{
		DF_Type::unit_wound,
		{
			{ "attacker_hist_figure_id" , {"historical_figure", "" }},
			{ "attacker_unit_id" , {"unit", "" }},
			{ "syndrome_id" , {"syndrome", "" }}
		}
	},
	{
		DF_Type::vehicle,
		{
			{ "item_id" , {"item", "" }},
			{ "route_id" , {"hauling_route", "" }}
		}
	},
	{
		DF_Type::vermin,
		{
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_civlistst__T_artifact_details,
		{
			{ "last_holder_hf" , {"historical_figure", "" }},
			{ "last_site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::viewscreen_civlistst__T_rumors,
		{
			{ "histfig2" , {"historical_figure", "" }},
			{ "site" , {"world_site", "" }}
		}
	},
	{
		DF_Type::viewscreen_createquotast,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::viewscreen_image_creatorst,
		{
			{ "castes_filtered" , {"caste_raw", "" }},
			{ "plants_filtered" , {"plant_raw", "" }},
			{ "races_filtered" , {"creature_raw", "" }},
			{ "shapes_filtered" , {"descriptor_shape", "" }},
			{ "trees_filtered" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_image_creatorst__T_plants,
		{
			{ "plant" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_image_creatorst__T_trees,
		{
			{ "tree" , {"plant_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_layer_arena_creaturest,
		{
			{ "caste" , {"caste_raw", "" }},
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_layer_choose_language_namest,
		{
			{ "list_word" , {"language_word", "" }}
		}
	},
	{
		DF_Type::viewscreen_layer_militaryst__T_equip__T_color,
		{
			{ "id" , {"descriptor_color", "" }}
		}
	},
	{
		DF_Type::viewscreen_layer_noblelistst,
		{
			{ "groups" , {"historical_entity", "" }},
			{ "histfigs" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::viewscreen_layer_noblelistst__T_info,
		{
			{ "group" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::viewscreen_layer_stone_restrictionst,
		{
			{ "use_id" , {"reaction", "" }}
		}
	},
	{
		DF_Type::viewscreen_legendsst,
		{
			{ "artifacts" , {"artifact_record", "" }},
			{ "entities" , {"historical_entity", "" }},
			{ "event_collections" , {"history_event_collection", "" }},
			{ "events" , {"history_event", "" }},
			{ "histfigs" , {"historical_figure", "" }},
			{ "sites" , {"world_site", "" }},
			{ "stack_collections" , {"history_event_collection", "" }},
			{ "structure_sites" , {"world_site", "" }}
		}
	},
	{
		DF_Type::viewscreen_petst,
		{
			{ "known" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_reportlistst,
		{
			{ "last_id" , {"report", "" }},
			{ "mission_reports" , {"mission_report", "" }},
			{ "spoils_reports" , {"spoils_report", "" }}
		}
	},
	{
		DF_Type::viewscreen_requestagreementst,
		{
			{ "civ_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::viewscreen_setupadventurest,
		{
			{ "highlighted_entity_ids" , {"historical_entity", "" }},
			{ "home_entity_ids" , {"historical_entity", "" }},
			{ "nemesis_ids" , {"nemesis_record", "" }},
			{ "race_ids" , {"creature_raw", "" }},
			{ "wild_creature_ids" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_setupadventurest__T_races_info,
		{
			{ "entity_id" , {"historical_entity", "" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_setupdwarfgamest,
		{
			{ "add_mattype" , {"material", "$$.add_matindex" }}
		}
	},
	{
		DF_Type::viewscreen_setupdwarfgamest__T_animals,
		{
			{ "caste" , {"caste_raw", "$$._parent.race[$._key]" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::viewscreen_topicmeeting_fill_land_holder_positionsst,
		{
			{ "candidate_histfig_ids" , {"historical_figure", "" }},
			{ "position_ids" , {"entity_position_raw", "" }},
			{ "selected_histfig_ids" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::viewscreen_tradeagreementst,
		{
			{ "civ_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::viewscreen_workshop_profilest,
		{
			{ "building_id" , {"building", "" }}
		}
	},
	{
		DF_Type::web_cluster,
		{
			{ "ambushers" , {"unit", "" }},
			{ "caste" , {"caste_raw", "$$.race" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::workshop_profile,
		{
			{ "permitted_workers" , {"unit", "" }}
		}
	},
	{
		DF_Type::world,
		{
			{ "busy_buildings" , {"building", "" }}
		}
	},
	{
		DF_Type::world__T_arena_spawn,
		{
			{ "caste" , {"caste_raw", "$$._parent.race[$._key]" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::world__T_arena_spawn__T_item_types,
		{
			{ "mattype" , {"material", "$$.matindex" }}
		}
	},
	{
		DF_Type::world__T_features,
		{
			{ "feature_global_idx" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::world__T_unk_131ef0,
		{
			{ "hfid" , {"historical_figure", "" }},
			{ "unk_hfid" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::world__T_unk_131ef0__T_claims,
		{
			{ "artifact" , {"artifact_record", "" }}
		}
	},
	{
		DF_Type::world_construction_square,
		{
			{ "construction_id" , {"world_construction", "" }}
		}
	},
	{
		DF_Type::world_construction_square_bridgest,
		{
			{ "mat_type" , {"material", "$$.mat_index" }},
			{ "road_id" , {"world_construction", "" }}
		}
	},
	{
		DF_Type::world_construction_square_roadst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::world_construction_square_wallst,
		{
			{ "mat_type" , {"material", "$$.mat_index" }}
		}
	},
	{
		DF_Type::world_data,
		{
			{ "old_sites" , {"world_site", "" }}
		}
	},
	{
		DF_Type::world_data__T_unk_274__T_unk_10,
		{
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::world_geo_layer,
		{
			{ "mat_index" , {"inorganic_raw", "" }},
			{ "vein_mat" , {"inorganic_raw", "" }}
		}
	},
	{
		DF_Type::world_history__T_intrigues,
		{
			{ "event" , {"history_event", "" }}
		}
	},
	{
		DF_Type::world_history__T_intrigues__T_anon_1,
		{
			{ "hf_1" , {"historical_figure", "" }},
			{ "hf_2" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::world_object_data__T_offloaded_items,
		{
			{ "building" , {"building", "" }},
			{ "container" , {"item", "" }}
		}
	},
	{
		DF_Type::world_population,
		{
			{ "owner" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::world_population_ref,
		{
			{ "cave_id" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::world_region,
		{
			{ "forces" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::world_region_details,
		{
			{ "lava_stone" , {"inorganic_raw", "" }}
		}
	},
	{
		DF_Type::world_region_feature,
		{
			{ "layer" , {"world_underground_region", "" }}
		}
	},
	{
		DF_Type::world_site,
		{
			{ "civ_id" , {"historical_entity", "" }},
			{ "cur_owner_id" , {"historical_entity", "" }},
			{ "deaths" , {"historical_figure", "" }}
		}
	},
	{
		DF_Type::world_site__T_subtype_info,
		{
			{ "creator" , {"historical_figure", "" }},
			{ "founding_entity" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::world_site__T_unk_1,
		{
			{ "nemesis" , {"nemesis_record", "" }}
		}
	},
	{
		DF_Type::world_site__T_unk_188__T_unk1,
		{
			{ "hfid" , {"historical_figure", "" }},
			{ "site_id" , {"world_site", "" }}
		}
	},
	{
		DF_Type::world_site__T_unk_1__T_units,
		{
			{ "unit_id" , {"unit", "" }}
		}
	},
	{
		DF_Type::world_site__T_unk_21c,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::world_site__T_unk_v40_4a,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::world_site__T_unk_v40_4d,
		{
			{ "entity_id" , {"historical_entity", "" }}
		}
	},
	{
		DF_Type::world_site_inhabitant,
		{
			{ "founder_outcast_entity_id" , {"historical_entity", "" }},
			{ "outcast_id" , {"historical_entity", "" }},
			{ "race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::wound_curse_info,
		{
			{ "interaction_id" , {"creature_interaction_effect", "" }},
			{ "were_caste" , {"caste_raw", "$$.were_race" }},
			{ "were_race" , {"creature_raw", "" }}
		}
	},
	{
		DF_Type::written_content,
		{
			{ "author" , {"historical_figure", "" }},
			{ "poetic_form" , {"poetic_form", "" }}
		}
	}
};

using namespace std;

tl::optional<const map<string, pair<string, string>>> CMetadataServer::get_ref_target_metadata(DF_Type p_df_type)
{
    auto it = ref_target_metadata.find(p_df_type);
    if (it == ref_target_metadata.end())
        return {};
    return it->second;
}

tl::optional<const pair<string, string>> CMetadataServer::get_ref_target_metadata_field(DF_Type p_df_type, const string& p_field_name)
{
    auto it = ref_target_metadata.find(p_df_type);
    if (it == ref_target_metadata.end())
        return {};
    auto map2 = it->second;
    auto it2  = map2.find(p_field_name);
    if (it2 == map2.end())
        return {};
    return it2->second;
}

} // namespace rdf

