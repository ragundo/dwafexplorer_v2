#include <cstdint>
#include <map>
#include <array>
#include "tl/tl_optional.h"
#include "MetadataServer.h"


namespace rdf
{

std::map<DF_Type, std::map<std::string,std::string>> refers_to_metadata
{
	{
		DF_Type::army,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::art_image_element_itemst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::block_square_event_item_spatterst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::body_component_info,
		{
			{ "layer_cut_fraction" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "layer_dent_fraction" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "layer_effect_fraction" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "layer_status" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "layer_wound_area" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "nonsolid_remaining" , "$$._global._upglobal.caste.ref-target.body_info.nonsolid_layers[$].refers-to" }
		}
	},
	{
		DF_Type::body_part_layer_raw,
		{
			{ "bp_modifiers" , "$$._global._upglobal._upglobal._upglobal.bp_appearance.modifier_idx[$].refers-to" },
			{ "parent_idx" , "$$._global._upglobal.layers[$]" },
			{ "tissue_id" , "$$._upglobal._upglobal._upglobal._upglobal._upglobal.tissue[$]" }
		}
	},
	{
		DF_Type::body_part_raw,
		{
			{ "numbered_idx" , "$$._global._upglobal.numbered_masks[$]" }
		}
	},
	{
		DF_Type::build_req_choice_genst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::building_def_item,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::caste_body_info,
		{
			{ "layer_nonsolid" , "$$._global.layer_part[$].refers-to" }
		}
	},
	{
		DF_Type::caste_raw,
		{
			{ "sound_peaceful_intermittent" , "$$._global.sound[$]" }
		}
	},
	{
		DF_Type::caste_raw__T_bp_appearance,
		{
			{ "layer_idx" , "$$._parent._parent.part_idx[$$._key].refers-to.layers[$]" },
			{ "modifier_idx" , "$$._parent._parent.modifiers[$]" },
			{ "part_idx" , "$$._global.body_info.body_parts[$]" },
			{ "style_layer_idx" , "$$._parent._parent.style_part_idx[$$._key].refers-to.layers[$]" },
			{ "style_list_idx" , "$$._parent._parent.modifier_idx[$].refers-to" },
			{ "style_part_idx" , "$$._global.body_info.body_parts[$]" }
		}
	},
	{
		DF_Type::caste_raw__T_misc,
		{
			{ "egg_mat_index" , "(food-mat-by-idx $Eggs $)" },
			{ "fish_mat_index" , "(food-mat-by-idx $Fish $)" }
		}
	},
	{
		DF_Type::caste_raw__T_secretion,
		{
			{ "body_part_id" , "$$._upglobal.body_info.body_parts[$]" },
			{ "layer_id" , "$$._parent._parent.body_part_id[$$._key].refers-to.layers[$]" }
		}
	},
	{
		DF_Type::color_modifier_raw,
		{
			{ "body_part_id" , "$$._global._upglobal.body_info.body_parts[$]" },
			{ "tissue_layer_id" , "$$._global.body_part_id[$$._key].refers-to.layers[$]" }
		}
	},
	{
		DF_Type::construction,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::embark_item_choice__T_list,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::entity_activity_statistics,
		{
			{ "discovered_creature_foods" , "(find-creature $)" },
			{ "discovered_creatures" , "(find-creature $)" },
			{ "discovered_plant_foods" , "(find-plant-raw $)" },
			{ "discovered_plants" , "(find-plant-raw $)" }
		}
	},
	{
		DF_Type::entity_event__T_data__T_succession,
		{
			{ "position_assignment_id" , "$(find-entity $$._parent.entity_id).positions.assignments[$]" }
		}
	},
	{
		DF_Type::entity_position_assignment,
		{
			{ "position_id" , "(find-by-id $$._global._parent._global.positions.own $id $)" }
		}
	},
	{
		DF_Type::flow_guide_item_cloudst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::gait_info,
		{
			{ "action_string_idx" , "$global.world.raws.creatures.action_strings[$].value" }
		}
	},
	{
		DF_Type::general_ref_item_type,
		{
			{ "subtype" , "(item-subtype-target $$._parent.type $)" }
		}
	},
	{
		DF_Type::histfig_entity_link_former_positionst,
		{
			{ "assignment_id" , "(find-by-id $$._global.entity_id.ref-target.positions.assignments $id $)" }
		}
	},
	{
		DF_Type::histfig_entity_link_position_claimst,
		{
			{ "assignment_id" , "(find-by-id $$._global.entity_id.ref-target.positions.assignments $id $)" }
		}
	},
	{
		DF_Type::histfig_entity_link_positionst,
		{
			{ "assignment_id" , "(find-by-id $$._global.entity_id.ref-target.positions.assignments $id $)" }
		}
	},
	{
		DF_Type::historical_entity__T_resources,
		{
			{ "discovered_creatures" , "(find-creature $)" },
			{ "discovered_plant_foods" , "(find-plant-raw $)" },
			{ "discovered_plants" , "(find-plant-raw $)" }
		}
	},
	{
		DF_Type::historical_entity__T_training_knowledge,
		{
			{ "unk_10" , "(find-instance $creature_raw $)" }
		}
	},
	{
		DF_Type::history_event_body_abusedst__T_abuse_data__T_Impaled,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::history_event_item_stolenst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::history_event_masterpiece_created_dye_itemst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::history_event_masterpiece_created_foodst,
		{
			{ "item_subtype" , "$global.world.raws.itemdefs.food[$]" }
		}
	},
	{
		DF_Type::history_event_masterpiece_created_item_improvementst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::history_event_masterpiece_created_itemst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::history_hit_item,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" },
			{ "shooter_item_subtype" , "(item-subtype-target $$._parent.shooter_item_type $)" }
		}
	},
	{
		DF_Type::item_body_component__T_body,
		{
			{ "bp_modifiers" , "$$._global.caste.ref-target.bp_appearance.modifier_idx[$].refers-to" }
		}
	},
	{
		DF_Type::item_filter_spec,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::job_item,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::job_item_filter,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::manager_order,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::manager_order_condition_item,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::manager_order_template,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::mandate,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::material_common,
		{
			{ "butcher_special_subtype" , "(item-subtype-target $$._parent.butcher_special_type $)" }
		}
	},
	{
		DF_Type::plant_raw,
		{
			{ "stockpile_growths" , "$$._parent._parent.growths[$]" }
		}
	},
	{
		DF_Type::reaction_product_itemst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::reaction_reagent_itemst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::setup_character_info,
		{
			{ "histfig" , "$global.world.nemesis.all[$]" }
		}
	},
	{
		DF_Type::site_building_item,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::spatter,
		{
			{ "body_part_id" , "$$._global._parent._global.body.body_plan.body_parts[$]" }
		}
	},
	{
		DF_Type::squad,
		{
			{ "cur_alert_idx" , "$$._parent.schedule[$]" },
			{ "leader_assignment" , "(find-by-id $$._global.entity_id.ref-target.positions.assignments $id $)" },
			{ "leader_position" , "(find-by-id $$._global.entity_id.ref-target.positions.own $id $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_ammo,
		{
			{ "mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_armor,
		{
			{ "feet" , "$global.world.raws.itemdefs.shoes[$]" },
			{ "hands" , "$global.world.raws.itemdefs.gloves[$]" },
			{ "head" , "$global.world.raws.itemdefs.helms[$]" },
			{ "legs" , "$global.world.raws.itemdefs.pants[$]" },
			{ "mats" , "(material-by-id 0 $)" },
			{ "shield" , "$global.world.raws.itemdefs.shields[$]" }
		}
	},
	{
		DF_Type::stockpile_settings__T_bars_blocks,
		{
			{ "blocks_mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_cloth,
		{
			{ "cloth_metal" , "(food-mat-by-idx $MetalThread $)" },
			{ "cloth_plant" , "(food-mat-by-idx $PlantFiber $)" },
			{ "cloth_silk" , "(food-mat-by-idx $Silk $)" },
			{ "cloth_yarn" , "(food-mat-by-idx $Yarn $)" },
			{ "thread_metal" , "(food-mat-by-idx $MetalThread $)" },
			{ "thread_plant" , "(food-mat-by-idx $PlantFiber $)" },
			{ "thread_yarn" , "(food-mat-by-idx $Yarn $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_food,
		{
			{ "cheese_animal" , "(food-mat-by-idx $CreatureCheese $)" },
			{ "cheese_plant" , "(food-mat-by-idx $PlantCheese $)" },
			{ "drink_animal" , "(food-mat-by-idx $CreatureDrink $)" },
			{ "drink_plant" , "(food-mat-by-idx $PlantDrink $)" },
			{ "egg" , "(food-mat-by-idx $Eggs $)" },
			{ "fish" , "(food-mat-by-idx $Fish $)" },
			{ "glob" , "(food-mat-by-idx $Glob $)" },
			{ "glob_paste" , "(food-mat-by-idx $Paste $)" },
			{ "glob_pressed" , "(food-mat-by-idx $Pressed $)" },
			{ "leaves" , "(food-mat-by-idx $Leaf $)" },
			{ "liquid_animal" , "(food-mat-by-idx $CreatureLiquid $)" },
			{ "liquid_misc" , "(food-mat-by-idx $MiscLiquid $)" },
			{ "liquid_plant" , "(food-mat-by-idx $PlantLiquid $)" },
			{ "plants" , "(food-mat-by-idx $Plants $)" },
			{ "powder_creature" , "(food-mat-by-idx $CreaturePowder $)" },
			{ "powder_plant" , "(food-mat-by-idx $PlantPowder $)" },
			{ "seeds" , "(food-mat-by-idx $Seed $)" },
			{ "unprepared_fish" , "(food-mat-by-idx $UnpreparedFish $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_gems,
		{
			{ "cut_mats" , "(material-by-id 0 $)" },
			{ "cut_other_mats" , "(material-by-id $ -1)" },
			{ "rough_mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_refuse,
		{
			{ "body_parts" , "(find-creature $)" },
			{ "bones" , "(find-creature $)" },
			{ "hair" , "(find-creature $)" },
			{ "horns" , "(find-creature $)" },
			{ "shells" , "(find-creature $)" },
			{ "skulls" , "(find-creature $)" },
			{ "teeth" , "(find-creature $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_weapons,
		{
			{ "mats" , "(material-by-id 0 $)" },
			{ "trapcomp_type" , "$global.world.raws.itemdefs.trapcomps[$]" }
		}
	},
	{
		DF_Type::tissue,
		{
			{ "parent_tissue" , "$$._global._upglobal.tissue[$]" }
		}
	},
	{
		DF_Type::tissue_style_raw,
		{
			{ "layer_idx" , "$$._parent._parent.part_idx[$$._key].refers-to.layers[$]" },
			{ "list_idx" , "$$._global._upglobal.bp_appearance.style_layer_idx[$].refers-to" },
			{ "part_idx" , "$$._global._upglobal.body_info.body_parts[$]" }
		}
	},
	{
		DF_Type::ui,
		{
			{ "economic_stone" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::ui__T_alerts,
		{
			{ "civ_alert_idx" , "$$._parent.list[$]" }
		}
	},
	{
		DF_Type::ui__T_burrows,
		{
			{ "sel_index" , "$$._parent.list[$]" }
		}
	},
	{
		DF_Type::ui__T_economy_prices__T_price_adjustment,
		{
			{ "armor" , "$global.world.raws.itemdefs.armor[$]" },
			{ "footwear" , "$global.world.raws.itemdefs.shoes[$]" },
			{ "handwear" , "$global.world.raws.itemdefs.helms[$]" },
			{ "headwear" , "$global.world.raws.itemdefs.gloves[$]" },
			{ "legwear" , "$global.world.raws.itemdefs.pants[$]" },
			{ "prepared_food" , "$global.world.raws.itemdefs.food[$]" }
		}
	},
	{
		DF_Type::ui__T_economy_prices__T_price_setter,
		{
			{ "armor" , "$global.world.raws.itemdefs.armor[$]" },
			{ "footwear" , "$global.world.raws.itemdefs.shoes[$]" },
			{ "handwear" , "$global.world.raws.itemdefs.helms[$]" },
			{ "headwear" , "$global.world.raws.itemdefs.gloves[$]" },
			{ "legwear" , "$global.world.raws.itemdefs.pants[$]" },
			{ "prepared_food" , "$global.world.raws.itemdefs.food[$]" }
		}
	},
	{
		DF_Type::ui__T_squads,
		{
			{ "sel_indiv_squad" , "$$._parent.list[$]" },
			{ "sel_kill_targets" , "$$._parent.kill_target[$]" }
		}
	},
	{
		DF_Type::ui_advmode__T_conversation,
		{
			{ "cursor_activity" , "$$._parent.activity[$]" },
			{ "cursor_choice" , "$$._parent.choices[$]" },
			{ "cursor_target" , "$$._parent.targets[$]" }
		}
	},
	{
		DF_Type::ui_look_list__T_items,
		{
			{ "spatter_item_subtype" , "(item-subtype-target $$._parent.spatter_item_type $)" }
		}
	},
	{
		DF_Type::ui_sidebar_menus__T_unit_cursor,
		{
			{ "list" , "$global.world.units.active[$]" }
		}
	},
	{
		DF_Type::ui_sidebar_menus__T_unit_skills,
		{
			{ "skill_id" , "$global.ui_selected_unit.index.refers-to.status.current_soul.skills[$]" }
		}
	},
	{
		DF_Type::unit__T_appearance,
		{
			{ "bp_modifiers" , "$$._global.caste.ref-target.bp_appearance.modifier_idx[$].refers-to" },
			{ "tissue_length" , "$$._parent.tissue_style.index-refers-to[$]" },
			{ "tissue_style" , "$$._global.caste.ref-target.bp_appearance.style_layer_idx[$].refers-to" },
			{ "tissue_style_civ_id" , "$$._parent.tissue_style.index-refers-to[$]" },
			{ "tissue_style_id" , "$$._parent.tissue_style.index-refers-to[$]" },
			{ "tissue_style_type" , "(find-by-id $$._global.caste.ref-target.tissue_styles $id $)" }
		}
	},
	{
		DF_Type::unit__T_body,
		{
			{ "weapon_bp" , "$$._parent.body_plan.body_parts[$]" }
		}
	},
	{
		DF_Type::unit__T_curse,
		{
			{ "bp_appearance" , "$$._global.caste.ref-target.bp_appearance.modifier_idx[$].refers-to" },
			{ "own_interaction" , "$$._global.body.body_plan.interactions[$]" }
		}
	},
	{
		DF_Type::unit__T_enemy,
		{
			{ "interaction" , "$$._global.body.body_plan.interactions[$]" },
			{ "unk_v4206_1" , "$global.world.enemy_status_cache.rel_map[$]" }
		}
	},
	{
		DF_Type::unit_appearance,
		{
			{ "bp_modifiers" , "(find-creature $$._parent.race).caste[$$._parent.caste].bp_appearance.modifiers[$]" },
			{ "caste_index" , "$global.world.raws.creatures.list_creature[$]" },
			{ "color_modifiers" , "(find-creature $$._parent.race).caste[$$._parent.caste].color_modifiers[$]" }
		}
	},
	{
		DF_Type::unit_dance_skill,
		{
			{ "id" , "$global.world.dance_forms.all[$].name" }
		}
	},
	{
		DF_Type::unit_demand,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::unit_health_info__T_op_history__T_info__T_crutch,
		{
			{ "item_type" , "(enum-to-key $item_type $)" }
		}
	},
	{
		DF_Type::unit_instrument_skill,
		{
			{ "id" , "$global.world.raws.itemdefs.instruments[$].name" }
		}
	},
	{
		DF_Type::unit_inventory_item,
		{
			{ "body_part_id" , "$$._global._parent._global.body.body_plan.body_parts[$]" }
		}
	},
	{
		DF_Type::unit_item_wrestle,
		{
			{ "other_bp" , "$$._parent.unit.ref-target.body.body_plan.body_parts[$]" },
			{ "self_bp" , "$$._parent._upglobal.body.body_plan.body_parts[$]" }
		}
	},
	{
		DF_Type::unit_musical_skill,
		{
			{ "id" , "$global.world.musical_forms.all[$].name" }
		}
	},
	{
		DF_Type::unit_poetic_skill,
		{
			{ "id" , "$global.world.poetic_forms.all[$].name" }
		}
	},
	{
		DF_Type::unit_preference,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::unit_wound__T_parts,
		{
			{ "body_part_id" , "$$._global._upglobal.caste.ref-target.body_info.body_parts[$]" },
			{ "global_layer_idx" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "jammed_layer_idx" , "$$._global._upglobal.caste.ref-target.body_info.layer_idx[$].refers-to" },
			{ "layer_idx" , "$$._parent.body_part_id.refers-to.layers[$]" }
		}
	},
	{
		DF_Type::viewscreen_buildinglistst,
		{
			{ "cursor" , "$$._parent.buildings[$]" },
			{ "room_value2" , "$$._parent.buildings2[$]" }
		}
	},
	{
		DF_Type::viewscreen_choose_start_sitest,
		{
			{ "civ_idx" , "$$._global.available_civs[$]" },
			{ "notes_order" , "$global.world.world_data.embark_notes[$]" },
			{ "reclaim_idx" , "$global.world.world_data.old_sites[$]" }
		}
	},
	{
		DF_Type::viewscreen_choose_start_sitest__T_finder,
		{
			{ "cursor" , "$$._parent.enabled_options[$]" }
		}
	},
	{
		DF_Type::viewscreen_dwarfmodest,
		{
			{ "jeweler_encrust" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::viewscreen_image_creatorst__T_objects,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent._parent.type[$$._key] $)" }
		}
	},
	{
		DF_Type::viewscreen_layer_arena_creaturest,
		{
			{ "cur_interaction" , "$global.world.arena_spawn.interactions[$]" },
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::viewscreen_layer_militaryst__T_equip,
		{
			{ "prio_in_move" , "$$._parent.squads[$]" }
		}
	},
	{
		DF_Type::viewscreen_layer_unit_actionst,
		{
			{ "reagent" , "$$.cur_reaction.reagents[$]" },
			{ "sel_reagents" , "$$._global.cur_reaction[$]" }
		}
	},
	{
		DF_Type::viewscreen_selectitemst,
		{
			{ "sel_category" , "$$._parent.categories[$]" }
		}
	},
	{
		DF_Type::viewscreen_setupadventurest,
		{
			{ "nemesis_ids" , "$global.world.nemesis.all[$]" }
		}
	},
	{
		DF_Type::viewscreen_setupdwarfgamest,
		{
			{ "add_item_subtype" , "(item-subtype-target $$._parent.add_item_type $)" },
			{ "skill_cursor" , "$$._parent.embark_skills[$]" }
		}
	},
	{
		DF_Type::viewscreen_setupdwarfgamest__T_animals,
		{
			{ "count" , "$$._parent.caste[$].ref-target" },
			{ "profession" , "$$._parent.caste[$].ref-target" }
		}
	},
	{
		DF_Type::world__T_arena_spawn,
		{
			{ "interaction" , "$$._parent.interactions[$]" },
			{ "type" , "$$._parent.caste[$].ref-target" }
		}
	},
	{
		DF_Type::world__T_arena_spawn__T_item_types,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::world__T_flow_engine,
		{
			{ "block_idx" , "$$._global.map.map_blocks[$]" }
		}
	},
	{
		DF_Type::world__T_stockpile,
		{
			{ "cheese" , "(find-creature $)" },
			{ "eggs" , "(find-creature $)" },
			{ "leaves" , "(find-plant-raw $)" },
			{ "liquid_animal" , "(find-creature $)" },
			{ "liquid_builtin" , "(material-by-id $ -1)" },
			{ "liquid_plant" , "(find-plant-raw $)" },
			{ "meat_fish" , "(find-creature $)" },
			{ "plant_powder" , "(find-plant-raw $)" },
			{ "plants" , "(find-plant-raw $)" }
		}
	},
	{
		DF_Type::world_construction_square_bridgest,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::world_construction_square_roadst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::world_construction_square_wallst,
		{
			{ "item_subtype" , "(item-subtype-target $$._parent.item_type $)" }
		}
	},
	{
		DF_Type::world_raws__T_plants,
		{
			{ "grasses_idx" , "$$._parent.grasses[$]" },
			{ "trees_idx" , "$$._parent.trees[$]" }
		}
	},
	{
		DF_Type::world_region,
		{
			{ "tree_tiles_2" , "$$._global.tree_biomes[$].refers-to" },
			{ "tree_tiles_evil" , "$$._global.tree_biomes[$].refers-to" },
			{ "tree_tiles_good" , "$$._global.tree_biomes[$].refers-to" },
			{ "tree_tiles_savage" , "$$._global.tree_biomes[$].refers-to" }
		}
	}
};

    using namespace std;

    tl::optional<const map<string, string>> CMetadataServer::get_refers_to_metadata(DF_Type p_df_type)
    {
        auto it = refers_to_metadata.find(p_df_type);
        if (it == refers_to_metadata.end())
            return {};
        return it->second;
    }

    tl::optional<const string> CMetadataServer::get_refers_to_metadata_field(DF_Type p_df_type, const string& p_field_name)
    {
        auto it = refers_to_metadata.find(p_df_type);
        if (it == refers_to_metadata.end())
            return {};
        auto map2 = it->second;
        auto it2  = map2.find(p_field_name);
        if (it2 == map2.end())
            return {};
        return it2->second;
    }

    } // namespace rdf

