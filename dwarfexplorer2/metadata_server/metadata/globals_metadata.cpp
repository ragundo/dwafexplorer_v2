#include "DataDefs.h"
#include "MetadataServer.h"
#include <cstdint>
#include <df/global_objects.h>
#include <vector>
#include <tuple>
namespace rdf
{


using metadata_global = std::tuple<std::string, //  name
                                   rdf::DF_Type,
                                   rdf::RDF_Type,
                                   std::string, // comment
                                   uint64_t // address
                                   >;


using metadata = std::tuple<std::string, //  name
                            rdf::DF_Type,
                            rdf::RDF_Type,
                            std::string // comment
                            >;
std::vector<metadata_global> globals_metadata{
	{"activity_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::activity_next_id)},
	{"agreement_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::agreement_next_id)},
	{"army_controller_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::army_controller_next_id)},
	{"army_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::army_next_id)},
	{"army_tracking_info_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::army_tracking_info_next_id)},
	{"art_image_chunk_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::art_image_chunk_next_id)},
	{"artifact_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::artifact_next_id)},
	{"basic_seed",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::basic_seed)},
	{"belief_system_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::belief_system_next_id)},
	{"building_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::building_next_id)},
	{"created_item_count",	DF_Type::int32_t,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::created_item_count)},
	{"created_item_matindex",	DF_Type::int16_t,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::created_item_matindex)},
	{"created_item_mattype",	DF_Type::int16_t,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::created_item_mattype)},
	{"created_item_subtype",	DF_Type::int16_t,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::created_item_subtype)},
	{"created_item_type",	DF_Type::item_type,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::created_item_type)},
	{"crime_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::crime_next_id)},
	{"cultural_identity_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::cultural_identity_next_id)},
	{"cur_rain",	DF_Type::coord,	RDF_Type::Array,	"",	reinterpret_cast<uint64_t>(df::global::cur_rain)},
	{"cur_rain_counter",	DF_Type::int16_t,	RDF_Type::int16_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_rain_counter)},
	{"cur_season",	DF_Type::int8_t,	RDF_Type::int8_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_season)},
	{"cur_season_tick",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_season_tick)},
	{"cur_snow",	DF_Type::coord,	RDF_Type::Array,	"",	reinterpret_cast<uint64_t>(df::global::cur_snow)},
	{"cur_snow_counter",	DF_Type::int16_t,	RDF_Type::int16_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_snow_counter)},
	{"cur_year",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_year)},
	{"cur_year_tick",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_year_tick)},
	{"cur_year_tick_advmode",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::cur_year_tick_advmode)},
	{"current_weather",	DF_Type::weather_type,	RDF_Type::Array,	"",	reinterpret_cast<uint64_t>(df::global::current_weather)},
	{"cursor",	DF_Type::global__T_cursor,	RDF_Type::Compound,	"",	reinterpret_cast<uint64_t>(df::global::cursor)},
	{"d_init",	DF_Type::d_init,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::d_init)},
	{"dance_form_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::dance_form_next_id)},
	{"debug_combat",	DF_Type::Bool,	RDF_Type::Bool,	"Functionality unknown, combat-related",	reinterpret_cast<uint64_t>(df::global::debug_combat)},
	{"debug_fastmining",	DF_Type::Bool,	RDF_Type::Bool,	"All dwarves mine as fast as a Legendary Miner",	reinterpret_cast<uint64_t>(df::global::debug_fastmining)},
	{"debug_noberserk",	DF_Type::Bool,	RDF_Type::Bool,	"Insanity can only result in Crazed or Melancholy, never Berserk",	reinterpret_cast<uint64_t>(df::global::debug_noberserk)},
	{"debug_nodrink",	DF_Type::Bool,	RDF_Type::Bool,	"Disables thirst on everything",	reinterpret_cast<uint64_t>(df::global::debug_nodrink)},
	{"debug_noeat",	DF_Type::Bool,	RDF_Type::Bool,	"Disables hunger on everything",	reinterpret_cast<uint64_t>(df::global::debug_noeat)},
	{"debug_nomoods",	DF_Type::Bool,	RDF_Type::Bool,	"Same as ARTIFACTS:NO",	reinterpret_cast<uint64_t>(df::global::debug_nomoods)},
	{"debug_nopause",	DF_Type::Bool,	RDF_Type::Bool,	"Prevents the game from being paused",	reinterpret_cast<uint64_t>(df::global::debug_nopause)},
	{"debug_nosleep",	DF_Type::Bool,	RDF_Type::Bool,	"Disables drowsiness on everything",	reinterpret_cast<uint64_t>(df::global::debug_nosleep)},
	{"debug_showambush",	DF_Type::Bool,	RDF_Type::Bool,	"Makes hidden ambushers visible on-screen and in the units list (but not to your citizens)",	reinterpret_cast<uint64_t>(df::global::debug_showambush)},
	{"debug_turbospeed",	DF_Type::Bool,	RDF_Type::Bool,	"All units move and work at maximum speed",	reinterpret_cast<uint64_t>(df::global::debug_turbospeed)},
	{"debug_wildlife",	DF_Type::Bool,	RDF_Type::Bool,	"Functionality unknown, wildlife-related",	reinterpret_cast<uint64_t>(df::global::debug_wildlife)},
	{"divination_set_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::divination_set_next_id)},
	{"enabler",	DF_Type::enabler,	RDF_Type::Class,	"",	reinterpret_cast<uint64_t>(df::global::enabler)},
	{"entity_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::entity_next_id)},
	{"flow_guide_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::flow_guide_next_id)},
	{"flows",	DF_Type::flow_info,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::flows)},
	{"formation_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::formation_next_id)},
	{"gamemode",	DF_Type::game_mode,	RDF_Type::Enum,	"",	reinterpret_cast<uint64_t>(df::global::gamemode)},
	{"gametype",	DF_Type::game_type,	RDF_Type::Enum,	"",	reinterpret_cast<uint64_t>(df::global::gametype)},
	{"gps",	DF_Type::graphic,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::gps)},
	{"gview",	DF_Type::interfacest,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::gview)},
	{"handleannounce",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::handleannounce)},
	{"hist_event_collection_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::hist_event_collection_next_id)},
	{"hist_event_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::hist_event_next_id)},
	{"hist_figure_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::hist_figure_next_id)},
	{"identity_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::identity_next_id)},
	{"image_set_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::image_set_next_id)},
	{"incident_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::incident_next_id)},
	{"init",	DF_Type::init,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::init)},
	{"interactinvslot",	DF_Type::unit_inventory_item,	RDF_Type::Pointer,	"",	reinterpret_cast<uint64_t>(df::global::interactinvslot)},
	{"interaction_instance_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::interaction_instance_next_id)},
	{"interactitem",	DF_Type::item,	RDF_Type::Pointer,	"",	reinterpret_cast<uint64_t>(df::global::interactitem)},
	{"item_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::item_next_id)},
	{"job_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::job_next_id)},
	{"jobvalue",	DF_Type::int32_t,	RDF_Type::Array,	"",	reinterpret_cast<uint64_t>(df::global::jobvalue)},
	{"jobvalue_setter",	DF_Type::unit,	RDF_Type::Array,	"",	reinterpret_cast<uint64_t>(df::global::jobvalue_setter)},
	{"machine_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::machine_next_id)},
	{"map_renderer",	DF_Type::map_renderer,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::map_renderer)},
	{"min_load_version",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::min_load_version)},
	{"movie_version",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::movie_version)},
	{"musical_form_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::musical_form_next_id)},
	{"nemesis_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::nemesis_next_id)},
	{"occupation_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::occupation_next_id)},
	{"pause_state",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::pause_state)},
	{"poetic_form_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::poetic_form_next_id)},
	{"preserveannounce",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::preserveannounce)},
	{"process_dig",	DF_Type::Bool,	RDF_Type::Bool,	"Requests dig designations to be processed next frame.",	reinterpret_cast<uint64_t>(df::global::process_dig)},
	{"process_jobs",	DF_Type::Bool,	RDF_Type::Bool,	"Requests building jobs to be processed next frame.",	reinterpret_cast<uint64_t>(df::global::process_jobs)},
	{"proj_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::proj_next_id)},
	{"rhythm_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::rhythm_next_id)},
	{"save_on_exit",	DF_Type::Bool,	RDF_Type::Bool,	"Ending the game saves its state back to world.dat or world.sav",	reinterpret_cast<uint64_t>(df::global::save_on_exit)},
	{"scale_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::scale_next_id)},
	{"schedule_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::schedule_next_id)},
	{"selection_rect",	DF_Type::global__T_selection_rect,	RDF_Type::Compound,	"",	reinterpret_cast<uint64_t>(df::global::selection_rect)},
	{"soul_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::soul_next_id)},
	{"squad_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::squad_next_id)},
	{"standing_orders_auto_butcher",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_butcher)},
	{"standing_orders_auto_collect_webs",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_collect_webs)},
	{"standing_orders_auto_fishery",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_fishery)},
	{"standing_orders_auto_kiln",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_kiln)},
	{"standing_orders_auto_kitchen",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_kitchen)},
	{"standing_orders_auto_loom",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_loom)},
	{"standing_orders_auto_other",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_other)},
	{"standing_orders_auto_slaughter",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_slaughter)},
	{"standing_orders_auto_smelter",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_smelter)},
	{"standing_orders_auto_tan",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_auto_tan)},
	{"standing_orders_dump_bones",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_bones)},
	{"standing_orders_dump_corpses",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_corpses)},
	{"standing_orders_dump_hair",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_hair)},
	{"standing_orders_dump_other",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_other)},
	{"standing_orders_dump_shells",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_shells)},
	{"standing_orders_dump_skins",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_skins)},
	{"standing_orders_dump_skulls",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_dump_skulls)},
	{"standing_orders_farmer_harvest",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_farmer_harvest)},
	{"standing_orders_forbid_other_dead_items",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_forbid_other_dead_items)},
	{"standing_orders_forbid_other_nohunt",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_forbid_other_nohunt)},
	{"standing_orders_forbid_own_dead",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_forbid_own_dead)},
	{"standing_orders_forbid_own_dead_items",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_forbid_own_dead_items)},
	{"standing_orders_forbid_used_ammo",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_forbid_used_ammo)},
	{"standing_orders_gather_animals",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_animals)},
	{"standing_orders_gather_bodies",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_bodies)},
	{"standing_orders_gather_food",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_food)},
	{"standing_orders_gather_furniture",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_furniture)},
	{"standing_orders_gather_minerals",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_minerals)},
	{"standing_orders_gather_refuse",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_refuse)},
	{"standing_orders_gather_refuse_outside",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_refuse_outside)},
	{"standing_orders_gather_vermin_remains",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_vermin_remains)},
	{"standing_orders_gather_wood",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_gather_wood)},
	{"standing_orders_job_cancel_announce",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_job_cancel_announce)},
	{"standing_orders_mix_food",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_mix_food)},
	{"standing_orders_use_dyed_cloth",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_use_dyed_cloth)},
	{"standing_orders_zoneonly_drink",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_zoneonly_drink)},
	{"standing_orders_zoneonly_fish",	DF_Type::uint8_t,	RDF_Type::uint8_t,	"",	reinterpret_cast<uint64_t>(df::global::standing_orders_zoneonly_fish)},
	{"task_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::task_next_id)},
	{"texture",	DF_Type::texture_handler,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::texture)},
	{"timed_events",	DF_Type::timed_event,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::timed_events)},
	{"title",	DF_Type::Static_string,	RDF_Type::Static_string,	"",	reinterpret_cast<uint64_t>(df::global::title)},
	{"title_spaced",	DF_Type::Static_string,	RDF_Type::Static_string,	"",	reinterpret_cast<uint64_t>(df::global::title_spaced)},
	{"ui",	DF_Type::ui,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::ui)},
	{"ui_advmode",	DF_Type::ui_advmode,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::ui_advmode)},
	{"ui_build_selector",	DF_Type::ui_build_selector,	RDF_Type::Class,	"",	reinterpret_cast<uint64_t>(df::global::ui_build_selector)},
	{"ui_building_assign_is_marked",	DF_Type::Bool,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_assign_is_marked)},
	{"ui_building_assign_items",	DF_Type::item,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_assign_items)},
	{"ui_building_assign_type",	DF_Type::int8_t,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_assign_type)},
	{"ui_building_assign_units",	DF_Type::unit,	RDF_Type::Vector,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_assign_units)},
	{"ui_building_in_assign",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_in_assign)},
	{"ui_building_in_resize",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_in_resize)},
	{"ui_building_item_cursor",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_item_cursor)},
	{"ui_building_resize_radius",	DF_Type::int16_t,	RDF_Type::int16_t,	"",	reinterpret_cast<uint64_t>(df::global::ui_building_resize_radius)},
	{"ui_lever_target_type",	DF_Type::lever_target_type,	RDF_Type::Enum,	"",	reinterpret_cast<uint64_t>(df::global::ui_lever_target_type)},
	{"ui_look_cursor",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::ui_look_cursor)},
	{"ui_look_list",	DF_Type::ui_look_list,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::ui_look_list)},
	{"ui_menu_width",	DF_Type::int8_t,	RDF_Type::Array,	"",	reinterpret_cast<uint64_t>(df::global::ui_menu_width)},
	{"ui_selected_unit",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::ui_selected_unit)},
	{"ui_sidebar_menus",	DF_Type::ui_sidebar_menus,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::ui_sidebar_menus)},
	{"ui_unit_view_mode",	DF_Type::ui_unit_view_mode,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::ui_unit_view_mode)},
	{"ui_workshop_in_add",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::ui_workshop_in_add)},
	{"ui_workshop_job_cursor",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::ui_workshop_job_cursor)},
	{"unit_chunk_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::unit_chunk_next_id)},
	{"unit_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::unit_next_id)},
	{"updatelightstate",	DF_Type::Bool,	RDF_Type::Bool,	"",	reinterpret_cast<uint64_t>(df::global::updatelightstate)},
	{"vehicle_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::vehicle_next_id)},
	{"version",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::version)},
	{"weathertimer",	DF_Type::int16_t,	RDF_Type::int16_t,	"",	reinterpret_cast<uint64_t>(df::global::weathertimer)},
	{"window_x",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::window_x)},
	{"window_y",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::window_y)},
	{"window_z",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::window_z)},
	{"world",	DF_Type::world,	RDF_Type::Struct,	"",	reinterpret_cast<uint64_t>(df::global::world)},
	{"written_content_next_id",	DF_Type::int32_t,	RDF_Type::int32_t,	"",	reinterpret_cast<uint64_t>(df::global::written_content_next_id)}
};


tl::optional<DF_MetadataGlobal> CMetadataServer::find_global(const std::string& p_global_name)
{
     for (size_t i = 0; i < globals_metadata.size(); i++)
     {
         auto item = globals_metadata[i];
         if (std::get<0>(item) == p_global_name)
         {
             return DF_MetadataGlobal(std::get<0>(item),
                                      std::get<1>(item),
                                      std::get<2>(item),
                                      std::get<3>(item),
                                      std::get<4>(item));
         }
     }
     return {};
}

const std::vector<metadata_global>& CMetadataServer::get_globals_metadata()
{
    return globals_metadata;
}

    } // namespace rdf

