#include <cstdint>
#include <map>
#include <array>
#include "tl/tl_optional.h"
#include "MetadataServer.h"


namespace rdf
{

std::map<DF_Type, std::string> key_field_metadata
{
	{ DF_Type::abstract_building, "id" },
	{ DF_Type::activity_entry, "id" },
	{ DF_Type::agreement, "id" },
	{ DF_Type::agreement_details, "id" },
	{ DF_Type::agreement_party, "id" },
	{ DF_Type::army, "id" },
	{ DF_Type::army_controller, "id" },
	{ DF_Type::art_image_chunk, "id" },
	{ DF_Type::artifact_record, "id" },
	{ DF_Type::battlefield, "id" },
	{ DF_Type::belief_system, "id" },
	{ DF_Type::body_appearance_modifier, "id" },
	{ DF_Type::bp_appearance_modifier, "id" },
	{ DF_Type::breed, "id" },
	{ DF_Type::building, "id" },
	{ DF_Type::building_def, "id" },
	{ DF_Type::burrow, "id" },
	{ DF_Type::color_modifier_raw, "id" },
	{ DF_Type::construction, "pos" },
	{ DF_Type::creature_graphics_appointment, "token" },
	{ DF_Type::crime, "id" },
	{ DF_Type::cultural_identity, "id" },
	{ DF_Type::dance_form, "id" },
	{ DF_Type::divination_set, "id" },
	{ DF_Type::entity_population, "id" },
	{ DF_Type::entity_position, "id" },
	{ DF_Type::entity_position_assignment, "id" },
	{ DF_Type::entity_tissue_style, "id" },
	{ DF_Type::entity_uniform, "id" },
	{ DF_Type::flow_guide, "id" },
	{ DF_Type::hauling_route, "id" },
	{ DF_Type::hauling_stop, "id" },
	{ DF_Type::historical_entity, "id" },
	{ DF_Type::historical_figure, "id" },
	{ DF_Type::history_event, "id" },
	{ DF_Type::history_event_collection, "id" },
	{ DF_Type::honors_type, "id" },
	{ DF_Type::identity, "id" },
	{ DF_Type::image_set, "id" },
	{ DF_Type::incident, "id" },
	{ DF_Type::interaction, "id" },
	{ DF_Type::interaction_instance, "id" },
	{ DF_Type::interaction_source, "id" },
	{ DF_Type::invasion_info, "id" },
	{ DF_Type::item, "id" },
	{ DF_Type::job, "id" },
	{ DF_Type::machine, "id" },
	{ DF_Type::musical_form, "id" },
	{ DF_Type::nemesis_record, "id" },
	{ DF_Type::occupation, "id" },
	{ DF_Type::poetic_form, "id" },
	{ DF_Type::projectile, "id" },
	{ DF_Type::region_weather, "id" },
	{ DF_Type::report, "id" },
	{ DF_Type::rhythm, "id" },
	{ DF_Type::scale, "id" },
	{ DF_Type::schedule_info, "id" },
	{ DF_Type::site_realization_building, "id" },
	{ DF_Type::squad, "id" },
	{ DF_Type::tile_page, "token" },
	{ DF_Type::tissue, "id" },
	{ DF_Type::tissue_style_raw, "id" },
	{ DF_Type::training_assignment, "animal_id" },
	{ DF_Type::unit, "id" },
	{ DF_Type::unit_chunk, "id" },
	{ DF_Type::unit_item_use, "id" },
	{ DF_Type::unit_syndrome, "type" },
	{ DF_Type::unit_wound, "id" },
	{ DF_Type::vehicle, "id" },
	{ DF_Type::world_construction, "id" },
	{ DF_Type::world_object_data, "id" },
	{ DF_Type::world_site, "id" },
	{ DF_Type::written_content, "id" }
};

    using namespace std;

    tl::optional<string> CMetadataServer::get_key_field(DF_Type p_df_type)
    {
        auto it = key_field_metadata.find(p_df_type);
        if (it == key_field_metadata.end())
            return {};
        return it->second;
    }

    } // namespace rdf

