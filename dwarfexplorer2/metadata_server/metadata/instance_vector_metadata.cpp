#include <cstdint>
#include <map>
#include <array>
#include "tl/tl_optional.h"
#include "MetadataServer.h"


namespace rdf
{

std::map<DF_Type, std::string> instance_vector_metadata
{
	{ DF_Type::activity_entry, "$global.world.activities.all" },
	{ DF_Type::agreement, "$global.world.agreements.all" },
	{ DF_Type::army, "$global.world.armies.all" },
	{ DF_Type::army_controller, "$global.world.army_controllers.all" },
	{ DF_Type::art_image_chunk, "$global.world.art_image_chunks" },
	{ DF_Type::artifact_record, "$global.world.artifacts.all" },
	{ DF_Type::battlefield, "$global.world.world_data.battlefields" },
	{ DF_Type::belief_system, "$global.world.belief_systems.all" },
	{ DF_Type::body_detail_plan, "$global.world.raws.body_detail_plans" },
	{ DF_Type::body_template, "$global.world.raws.body_templates" },
	{ DF_Type::breed, "$global.world.world_data.breeds" },
	{ DF_Type::building, "$global.world.buildings.all" },
	{ DF_Type::building_def, "$global.world.raws.buildings.all" },
	{ DF_Type::burrow, "$global.ui.burrows.list" },
	{ DF_Type::coin_batch, "$global.world.coin_batches" },
	{ DF_Type::construction, "$global.world.constructions" },
	{ DF_Type::creature_interaction_effect, "$global.world.raws.effects.all" },
	{ DF_Type::creature_raw, "$global.world.raws.creatures.all" },
	{ DF_Type::creature_variation, "$global.world.raws.creature_variations" },
	{ DF_Type::crime, "$global.world.crimes.all" },
	{ DF_Type::cultural_identity, "$global.world.cultural_identities.all" },
	{ DF_Type::dance_form, "$global.world.dance_forms.all" },
	{ DF_Type::descriptor_color, "$global.world.raws.descriptors.colors" },
	{ DF_Type::descriptor_pattern, "$global.world.raws.descriptors.patterns" },
	{ DF_Type::descriptor_shape, "$global.world.raws.descriptors.shapes" },
	{ DF_Type::divination_set, "$global.world.divination_sets.all" },
	{ DF_Type::engraving, "$global.world.engravings" },
	{ DF_Type::entity_population, "$global.world.entity_populations" },
	{ DF_Type::entity_raw, "$global.world.raws.entities" },
	{ DF_Type::flow_guide, "$global.world.flow_guides.all" },
	{ DF_Type::hauling_route, "$global.ui.hauling.routes" },
	{ DF_Type::historical_entity, "$global.world.entities.all" },
	{ DF_Type::historical_figure, "$global.world.history.figures" },
	{ DF_Type::history_event, "$global.world.history.events" },
	{ DF_Type::history_event_collection, "$global.world.history.event_collections.all" },
	{ DF_Type::identity, "$global.world.identities.all" },
	{ DF_Type::image_set, "$global.world.image_sets.all" },
	{ DF_Type::incident, "$global.world.incidents.all" },
	{ DF_Type::inorganic_raw, "$global.world.raws.inorganics" },
	{ DF_Type::interaction, "$global.world.raws.interactions" },
	{ DF_Type::interaction_instance, "$global.world.interaction_instances.all" },
	{ DF_Type::invasion_info, "$global.ui.invasions.list" },
	{ DF_Type::item, "$global.world.items.all" },
	{ DF_Type::itemdef_ammost, "$global.world.raws.itemdefs.ammo" },
	{ DF_Type::itemdef_armorst, "$global.world.raws.itemdefs.armor" },
	{ DF_Type::itemdef_foodst, "$global.world.raws.itemdefs.food" },
	{ DF_Type::itemdef_glovesst, "$global.world.raws.itemdefs.gloves" },
	{ DF_Type::itemdef_helmst, "$global.world.raws.itemdefs.helms" },
	{ DF_Type::itemdef_instrumentst, "$global.world.raws.itemdefs.instruments" },
	{ DF_Type::itemdef_pantsst, "$global.world.raws.itemdefs.pants" },
	{ DF_Type::itemdef_shieldst, "$global.world.raws.itemdefs.shields" },
	{ DF_Type::itemdef_shoesst, "$global.world.raws.itemdefs.shoes" },
	{ DF_Type::itemdef_siegeammost, "$global.world.raws.itemdefs.siege_ammo" },
	{ DF_Type::itemdef_toolst, "$global.world.raws.itemdefs.tools" },
	{ DF_Type::itemdef_toyst, "$global.world.raws.itemdefs.toys" },
	{ DF_Type::itemdef_trapcompst, "$global.world.raws.itemdefs.trapcomps" },
	{ DF_Type::itemdef_weaponst, "$global.world.raws.itemdefs.weapons" },
	{ DF_Type::language_symbol, "$global.world.raws.language.symbols" },
	{ DF_Type::language_translation, "$global.world.raws.language.translations" },
	{ DF_Type::language_word, "$global.world.raws.language.words" },
	{ DF_Type::machine, "$global.world.machines.all" },
	{ DF_Type::musical_form, "$global.world.musical_forms.all" },
	{ DF_Type::nemesis_record, "$global.world.nemesis.all" },
	{ DF_Type::occupation, "$global.world.occupations.all" },
	{ DF_Type::plant, "$global.world.plants.all" },
	{ DF_Type::plant_raw, "$global.world.raws.plants.all" },
	{ DF_Type::poetic_form, "$global.world.poetic_forms.all" },
	{ DF_Type::reaction, "$global.world.raws.reactions.reactions" },
	{ DF_Type::region_weather, "$global.world.world_data.region_weather" },
	{ DF_Type::report, "$global.world.status.reports" },
	{ DF_Type::resource_allotment_data, "$global.world.world_data.resource_allotments" },
	{ DF_Type::rhythm, "$global.world.rhythms.all" },
	{ DF_Type::scale, "$global.world.scales.all" },
	{ DF_Type::schedule_info, "$global.world.schedules.all" },
	{ DF_Type::squad, "$global.world.squads.all" },
	{ DF_Type::syndrome, "$global.world.raws.syndromes.all" },
	{ DF_Type::tissue_template, "$global.world.raws.tissue_templates" },
	{ DF_Type::training_assignment, "$global.ui.equipment.training_assignments" },
	{ DF_Type::unit, "$global.world.units.all" },
	{ DF_Type::unit_chunk, "$global.world.unit_chunks" },
	{ DF_Type::vehicle, "$global.world.vehicles.all" },
	{ DF_Type::vermin, "$global.world.vermin.all" },
	{ DF_Type::world_construction, "$global.world.world_data.constructions.list" },
	{ DF_Type::world_geo_biome, "$global.world.world_data.geo_biomes" },
	{ DF_Type::world_landmass, "$global.world.world_data.landmasses" },
	{ DF_Type::world_object_data, "$global.world.world_data.object_data" },
	{ DF_Type::world_region, "$global.world.world_data.regions" },
	{ DF_Type::world_site, "$global.world.world_data.sites" },
	{ DF_Type::world_underground_region, "$global.world.world_data.underground_regions" },
	{ DF_Type::written_content, "$global.world.written_contents.all" }
};

    using namespace std;

    tl::optional<string> CMetadataServer::get_instance_vector(DF_Type p_df_type)
    {
        auto it = instance_vector_metadata.find(p_df_type);
        if (it == instance_vector_metadata.end())
            return {};
        return it->second;
    }

    } // namespace rdf

