#include <cstdint>
#include <map>
#include <array>
#include "tl/tl_optional.h"
#include "MetadataServer.h"


namespace rdf
{

std::map<DF_Type, std::map<std::string,std::string>> index_refers_to_metadata
{
	{
		DF_Type::body_component_info,
		{
			{ "body_part_status" , "$$._global._upglobal.caste.ref-target.body_info.body_parts[$]" }
		}
	},
	{
		DF_Type::building_furnacest,
		{
			{ "melt_remainder" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::caste_body_info,
		{
			{ "layer_part" , "$$._global.layer_idx[$].refers-to" }
		}
	},
	{
		DF_Type::caste_raw__T_bp_appearance,
		{
			{ "modifier_idx" , "$$._parent.part_idx[$].refers-to" }
		}
	},
	{
		DF_Type::color_modifier_raw,
		{
			{ "tissue_layer_id" , "$$._global.body_part_id[$].refers-to" }
		}
	},
	{
		DF_Type::creature_raw,
		{
			{ "modifier_num_patterns" , "$$._parent.modifier_class[$].refers-to" }
		}
	},
	{
		DF_Type::entity_activity_statistics,
		{
			{ "created_weapons" , "$global.world.raws.itemdefs.weapons[$]" }
		}
	},
	{
		DF_Type::entity_raw,
		{
			{ "currency_value" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::historical_entity__T_resources,
		{
			{ "discovered_creature_foods" , "(find-creature $)" }
		}
	},
	{
		DF_Type::historical_entity__T_training_knowledge,
		{
			{ "level" , "(find-instance $creature_raw $)" }
		}
	},
	{
		DF_Type::historical_figure_info__T_skills,
		{
			{ "points" , "$$._parent.skills[$]" }
		}
	},
	{
		DF_Type::history_era__T_details,
		{
			{ "civilized_races" , "(find-creature $)" }
		}
	},
	{
		DF_Type::item_body_component__T_body,
		{
			{ "body_modifiers" , "$$._global.caste.ref-target.body_appearance_modifiers[$]" }
		}
	},
	{
		DF_Type::language_translation,
		{
			{ "words" , "$global.world.raws.language.words[$]" }
		}
	},
	{
		DF_Type::plant_raw,
		{
			{ "stockpile_growth_flags" , "$$._parent.stockpile_growths[$].refers-to" }
		}
	},
	{
		DF_Type::squad_schedule_entry,
		{
			{ "order_assignments" , "$$._global._upglobal.positions[$]" }
		}
	},
	{
		DF_Type::stockpile_settings__T_ammo,
		{
			{ "type" , "$global.world.raws.itemdefs.ammo[$]" }
		}
	},
	{
		DF_Type::stockpile_settings__T_animals,
		{
			{ "enabled" , "(find-creature $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_armor,
		{
			{ "body" , "$global.world.raws.itemdefs.armor[$]" }
		}
	},
	{
		DF_Type::stockpile_settings__T_bars_blocks,
		{
			{ "bars_mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_cloth,
		{
			{ "thread_silk" , "(food-mat-by-idx $Silk $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_coins,
		{
			{ "mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_finished_goods,
		{
			{ "mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_food,
		{
			{ "meat" , "(food-mat-by-idx $Meat $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_furniture,
		{
			{ "mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_gems,
		{
			{ "rough_other_mats" , "(material-by-id $ -1)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_leather,
		{
			{ "mats" , "(food-mat-by-idx $Leather $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_refuse,
		{
			{ "corpses" , "(find-creature $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_stone,
		{
			{ "mats" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::stockpile_settings__T_weapons,
		{
			{ "weapon_type" , "$global.world.raws.itemdefs.weapons[$]" }
		}
	},
	{
		DF_Type::stockpile_settings__T_wood,
		{
			{ "mats" , "(find-plant-raw $)" }
		}
	},
	{
		DF_Type::tissue_style_raw,
		{
			{ "layer_idx" , "$$._parent.part_idx[$].refers-to" }
		}
	},
	{
		DF_Type::ui,
		{
			{ "currency_value" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::ui__T_burrows,
		{
			{ "sel_units" , "$$._parent.list_units[$]" }
		}
	},
	{
		DF_Type::ui__T_economy_prices__T_price_adjustment,
		{
			{ "weapons" , "$global.world.raws.itemdefs.weapons[$]" }
		}
	},
	{
		DF_Type::ui__T_economy_prices__T_price_setter,
		{
			{ "weapons" , "$global.world.raws.itemdefs.weapons[$]" }
		}
	},
	{
		DF_Type::ui__T_squads,
		{
			{ "sel_squads" , "$$._parent.list[$]" }
		}
	},
	{
		DF_Type::unit__T_appearance,
		{
			{ "body_modifiers" , "$$._global.caste.ref-target.body_appearance_modifiers[$]" }
		}
	},
	{
		DF_Type::unit__T_curse,
		{
			{ "body_appearance" , "$$._global.caste.ref-target.body_appearance_modifiers[$]" }
		}
	},
	{
		DF_Type::unit__T_enemy,
		{
			{ "sound_cooldown" , "$$._global.caste.ref-target.sound[$]" }
		}
	},
	{
		DF_Type::unit_appearance,
		{
			{ "body_modifiers" , "(find-creature $$._parent.race).caste[$$._parent.caste].body_appearance_modifiers[$]" }
		}
	},
	{
		DF_Type::unit_syndrome,
		{
			{ "symptoms" , "$$._global.type.ref-target.ce[$]" }
		}
	},
	{
		DF_Type::viewscreen_buildinglistst,
		{
			{ "room_value" , "$$._parent.buildings[$]" }
		}
	},
	{
		DF_Type::viewscreen_dwarfmodest,
		{
			{ "jeweler_cutgem" , "(material-by-id 0 $)" }
		}
	},
	{
		DF_Type::viewscreen_layer_choose_language_namest,
		{
			{ "list_part" , "$$._parent.list_word[$].ref-target" }
		}
	},
	{
		DF_Type::viewscreen_setupdwarfgamest__T_animals,
		{
			{ "race" , "$$._parent.caste[$].ref-target" }
		}
	},
	{
		DF_Type::world__T_arena_spawn,
		{
			{ "creature_cnt" , "(find-instance $creature_raw $)" }
		}
	},
	{
		DF_Type::world__T_arena_spawn__T_equipment,
		{
			{ "skill_levels" , "$$._parent.skills[$]" }
		}
	},
	{
		DF_Type::world__T_stockpile,
		{
			{ "seeds" , "(find-plant-raw $)" }
		}
	},
	{
		DF_Type::world_raws__T_effects,
		{
			{ "mat_types" , "$$._parent.all[$]" }
		}
	},
	{
		DF_Type::world_raws__T_plants,
		{
			{ "bushes_idx" , "$$._parent.bushes[$]" }
		}
	},
	{
		DF_Type::world_raws__T_syndromes,
		{
			{ "mat_types" , "$$._parent.all[$]" }
		}
	},
	{
		DF_Type::world_region,
		{
			{ "tree_tiles_1" , "$$._global.tree_biomes[$].refers-to" }
		}
	}
};

    using namespace std;

    tl::optional<const map<string, string>> CMetadataServer::get_fields_index_refers_to(DF_Type p_df_type)
    {
        auto it = index_refers_to_metadata.find(p_df_type);
        if (it == index_refers_to_metadata.end())
            return {};
        return it->second;
    }

    tl::optional<string> CMetadataServer::get_index_refers_to(DF_Type p_df_type, const string& p_field_name)
    {
        auto it = index_refers_to_metadata.find(p_df_type);
        if (it == index_refers_to_metadata.end())
            return {};
        auto map2 = it->second;
        auto it2  = map2.find(p_field_name);
        if (it2 == map2.end())
            return {};
        return it2->second;
    }

    }// namespace rdf

