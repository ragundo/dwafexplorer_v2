#ifndef DFNODE_H
#define DFNODE_H

#include "../metadata_server/MetadataServer.h"
#include "tl/tl_optional.h"
#include <QList>
#include <QtAlgorithms>
#include <functional>
#include <map>
#include <string>
#include <vector>

class DF_Model;

namespace rdf
{
class NodeTable;
struct NodeRoot;
} // namespace rdf

namespace rdf
{
enum class NodeType
{
    Simple,
    Compound,
    AnonymousCompound,
    Vector,
    Array,
    Union,
    AnonymousUnion,
    Root,
    Enum,
    Bitfield,
    BitfieldEntry,
    Pointer,
    Void,
    Padding,
    StaticString,
    Dummy,
    DFFlagArray,
    DFArray,
    DFLinkedList,
    Deque,
    Global
};

struct NodeRoot;

//
//------------------------------------------------------------------------------------//
//
struct NodeBase
{
  public:
    struct df_model_data
    {
        tl::optional<std::string> m_name{};
        tl::optional<std::string> m_type{};
        tl::optional<std::string> m_address{};
        tl::optional<std::string> m_value{};
        tl::optional<std::string> m_hyperlink{};
        tl::optional<std::string> m_offset{};
    } m_model_data;

    NodeBase()
    {
    }

    NodeBase(const NodeBase& p_source)
    {
        init(p_source);
    }

    std::string     m_field_path{""};
    std::string     m_field_name{""};
    DF_Type         m_df_type{rdf::DF_Type::None};
    RDF_Type        m_rdf_type{rdf::RDF_Type::None};
    rdf::NodeTable* m_node_table{nullptr};
    std::string     m_comment{""};
    uint64_t        m_address{0};
    NodeType        m_node_type;
    std::string     m_hyperlink_type{""};
    std::string     m_hyperlink_value{""};
    bool            m_has_children{false};

    virtual ~NodeBase() {}
    virtual std::string node_path_name();
    virtual bool        is_root_node() const;
    virtual int         get_node_size();
    virtual bool        can_have_children() const;
    virtual std::string path();
    virtual NodeBase*   clone() const
    {
        return new NodeBase(*this);
    }

    NodeRoot*                get_root_node();
    std::vector<std::string> split_path();
    NodeBase*                parent() const;
    int                      get_node_position() const;

  protected:
    void init(const NodeBase& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct Node : public NodeBase
{
  protected:
    std::vector<NodeBase*> m_children;

  public:
    Node()
        : NodeBase()
    {
    }

    Node(const Node& p_node)
        : NodeBase(p_node)
    {
        init(p_node);
    }

    bool      has_dummy_node() const;
    bool      can_have_children() const;
    NodeBase* find_child(const std::string& p_field_name);
    ~Node();

    NodeBase* clone() const
    {
        return new Node(*this);
    }

    void add_dummy_node();

    void add_child_node(NodeBase* p_node)
    {
        m_children.push_back(p_node);
    }

    NodeBase* get_child(std::size_t p_pos)
    {
        return m_children.at(p_pos);
    }

    std::size_t get_num_children() const
    {
        return m_children.size();
    }

    void remove_children()
    {
        m_children.clear();
    }

  protected:
    void init(const Node& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeSimple : public NodeBase
{
    NodeSimple()
        : NodeBase()
    {
        m_node_type = NodeType::Simple;
    }

    NodeSimple(const NodeSimple& p_node)
        : NodeBase(p_node) {}

    NodeBase* clone() const
    {
        return new NodeSimple(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeDummy : public NodeBase
{
    NodeDummy()
        : NodeBase()
    {
        m_node_type = NodeType::Dummy;
    }

    NodeDummy(const NodeDummy& p_node)
        : NodeBase(p_node) {}

    std::string path()
    {
        return "";
    }

    NodeBase* clone() const
    {
        return new NodeDummy(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodePadding : public NodeBase
{
    unsigned int m_size;

    NodePadding()
        : NodeBase()
    {
        m_node_type = NodeType::Padding;
    }

    NodePadding(const NodePadding& p_node)
        : NodeBase(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodePadding(*this);
    }

  protected:
    void init(const NodePadding& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeStaticString : public NodeBase
{
    unsigned int m_size;

    NodeStaticString()
        : NodeBase()
    {
        m_node_type = NodeType::StaticString;
    }

    NodeStaticString(const NodeStaticString& p_node)
        : NodeBase(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeStaticString(*this);
    }

  protected:
    void init(const NodeStaticString& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeVoid : public NodeBase
{
    NodeVoid()
        : NodeBase()
    {
        m_df_type   = DF_Type::Void;
        m_rdf_type  = RDF_Type::Void;
        m_node_type = NodeType::Void;
    }

    NodeVoid(const NodeVoid& p_node)
        : NodeBase(p_node) {}

    NodeBase* clone() const
    {
        return new NodeVoid(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeRoot : public Node
{
    NodeRoot()
        : Node()
    {
        m_node_type = NodeType::Root;
    }

    NodeRoot(const NodeRoot& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    bool is_root_node() const override
    {
        return true;
    }

    NodeBase* clone() const
    {
        return new NodeRoot(*this);
    }

    int get_node_position() const;

  protected:
    void init(const NodeRoot& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeCompound : public Node
{
    NodeCompound()
        : Node()
    {
        m_node_type = NodeType::Compound;
    }

    NodeCompound(const NodeCompound& p_node)
        : Node(p_node) {}

    NodeBase* clone() const
    {
        return new NodeCompound(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeDeque : public Node
{
    NodeDeque()
    {
        m_node_type = NodeType::Deque;
    }

    NodeDeque(const NodeDeque& p_node)
        : Node(p_node) {}

    NodeBase* clone() const
    {
        return new NodeDeque(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeUnion : public Node
{
    NodeUnion()
    {
        m_node_type = NodeType::Union;
    }

    NodeUnion(const NodeUnion& p_node)
        : Node(p_node) {}

    NodeBase* clone() const
    {
        return new NodeUnion(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeAnonymous : public Node
{
    std::string node_path_name()
    {
        return "";
    }

    NodeAnonymous()
        : Node() {}

    NodeAnonymous(const NodeAnonymous& p_node)
        : Node(p_node) {}

    NodeBase* clone() const
    {
        return new NodeAnonymous(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeEnum : public Node
{
    DF_Type     m_base_type{DF_Type::None};
    std::string m_enum_type_as_string{""};
    int         m_first_value{999999};
    int         m_last_value{0};

    NodeEnum()
    {
        m_node_type = NodeType::Enum;
    }

    NodeEnum(const NodeEnum& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeEnum(*this);
    }

  protected:
    void init(const NodeEnum& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeBitfieldEntry : public Node
{
    int  m_index;
    bool m_value;

    NodeBitfieldEntry()
    {
        m_node_type = NodeType::BitfieldEntry;
    }

    NodeBitfieldEntry(const NodeBitfieldEntry& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    std::string node_path_name() override;

    NodeBase* clone() const
    {
        return new NodeBitfieldEntry(*this);
    }

  protected:
    void init(const NodeBitfieldEntry& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeBitfield : public Node
{
    DF_Type m_index_enum{DF_Type::None};
    DF_Type m_base_type{DF_Type::None};
    NodeBitfield()
    {
        m_node_type = NodeType::Bitfield;
    }

    NodeBitfield(const NodeBitfield& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeBitfield(*this);
    }

  protected:
    void init(const NodeBitfield& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeDFFlagArray : public Node
{
    int m_num_entries{0};

    NodeDFFlagArray()
    {
        m_node_type = NodeType::DFFlagArray;
    }

    NodeDFFlagArray(const NodeDFFlagArray& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeDFFlagArray(*this);
    }

  protected:
    void init(const NodeDFFlagArray& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeDFLinkedList : public Node
{
    DF_Type m_item_type{DF_Type::None};

    NodeDFLinkedList()
        : Node()
    {
        m_node_type = NodeType::DFLinkedList;
    }

    NodeDFLinkedList(const NodeDFLinkedList& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeDFLinkedList(*this);
    }

  protected:
    void init(const NodeDFLinkedList& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeAddornements : public Node
{
    std::string m_addornements{""};

    NodeAddornements()
        : Node() {}

    NodeBase* clone() const
    {
        return new NodeAddornements(*this);
    }

    NodeAddornements(const NodeAddornements& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

  protected:
    void init(const NodeAddornements& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodePointer : public NodeAddornements
{
    NodePointer()
    {
        m_node_type = NodeType::Pointer;
    }

    NodePointer(const NodePointer& p_node)
        : NodeAddornements(p_node) {}

    NodeBase* clone() const
    {
        return new NodePointer(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
struct NodeVector : public NodeAddornements
{
  public:
    DF_Type m_enum_basetype{DF_Type::None}; // base-type for vector of enums

    NodeVector()
    {
        m_node_type = NodeType::Vector;
    }

    NodeVector(const NodeVector& p_node)
        : NodeAddornements(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeVector(*this);
    }

  protected:
    void init(const NodeVector& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeArray : public NodeAddornements
{
  public:
    std::size_t m_array_size{0};
    DF_Type     m_index_enum{DF_Type::None};
    DF_Type     m_enum_basetype{DF_Type::None}; // base-type for array of enums

    NodeArray()
    {
        m_node_type = NodeType::Array;
    }

    NodeArray(const NodeArray& p_node)
        : NodeAddornements(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeArray(*this);
    }

  protected:
    void init(const NodeArray& p_source_node);
};

//
//------------------------------------------------------------------------------------//
//
struct NodeDFArray : public NodeAddornements
{
  public:
    std::size_t m_array_size{0};

    NodeDFArray()
    {
        m_node_type = NodeType::DFArray;
    }

    NodeDFArray(const NodeDFArray& p_node)
        : NodeAddornements(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeDFArray(*this);
    }

  protected:
    void init(const NodeDFArray& p_source_node);
};

struct NodeGlobal : public Node
{
    NodeGlobal()
    {
        m_node_type = NodeType::Global;
    }

    NodeGlobal(const NodeGlobal& p_node)
        : Node(p_node)
    {
        init(p_node);
    }

    NodeBase* clone() const
    {
        return new NodeGlobal(*this);
    }
};

//
//------------------------------------------------------------------------------------//
//
NodeDummy* dummy();

//
//------------------------------------------------------------------------------------//
//
template<typename T>
T get_value_at_address(NodeBase* p_node)
{
    T* value_at_address = reinterpret_cast<T*>(p_node->m_address);
    return *value_at_address;
}

} // namespace rdf
#endif // DFNODE_H