/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model.h"
#include "df_node.h"
#include <cctype>

using namespace rdf;
using namespace std;

extern rdf::CMetadataServer MetadataServer;

//
//------------------------------------------------------------------------------------//
//
void DF_Model::insert_global_nodes(Node* p_node, const QModelIndex& p_index)
{
    auto all_globals = MetadataServer.get_globals_metadata();
    for (auto& one_global_metadata : all_globals)
    {
        auto field_name = std::get<0>(one_global_metadata);

        // Look for this node  in the table of nodes
        auto key        = "df.global." + field_name;
        auto node_maybe = p_node->m_node_table->find_node(key);
        if (node_maybe)
        {
            auto node_global = *node_maybe;
            add_child_node_to_parent_node(p_node, node_global);
            NodeBuilder::add_hyperlink(node_global);
        }
    }
}

//
//------------------------------------------------------------------------------------//
//
void DF_Model::insert_child_nodes(Node* p_node, const QModelIndex& p_index)
{
    switch (p_node->m_rdf_type)
    {
        case rdf::RDF_Type::Vector:
            insertRowsVector(p_index);
            break;
        case rdf::RDF_Type::Pointer:
            insertRowsPointer(p_index);
            break;
        case rdf::RDF_Type::Array:
            insertRowsArray(p_index);
            break;
        case rdf::RDF_Type::DFArray:
            insertRowsDFArray(p_index);
            break;
        case rdf::RDF_Type::Class:
        case rdf::RDF_Type::Struct:
        case rdf::RDF_Type::Compound:
        case rdf::RDF_Type::Union:
        case rdf::RDF_Type::AnonymousCompound:
        case rdf::RDF_Type::AnonymousUnion:
        case rdf::RDF_Type::DFLinkedList:
            insertRowsCompound(p_index);
            break;
        case rdf::RDF_Type::Bitfield:
            insertRowsBitfield(p_index);
            break;
        case rdf::RDF_Type::DFFlagArray:
            insertRowsDFFlagArray(p_index);
        default:
            break;
    }
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsBitfield(const QModelIndex& p_parent)
{
    auto bitfield_node = dynamic_cast<NodeBitfield*>(nodeFromIndex(p_parent));
    if (bitfield_node->get_num_children() > 0)
        return false;
    if (bitfield_node->m_index_enum == DF_Type::None)
    {
        auto vec_children = NodeBuilder::fill_node(bitfield_node);
        beginInsertRows(p_parent, 0, vec_children.size() - 1);
        for (auto& child : vec_children)
            bitfield_node->add_child_node(child);
        endInsertRows();
    }
    return true;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsDFFlagArray(const QModelIndex& p_parent)
{
    auto df_flag_array_node = dynamic_cast<NodeDFFlagArray*>(nodeFromIndex(p_parent));
    auto vec_children       = NodeBuilder::fill_node(df_flag_array_node);
    beginInsertRows(p_parent, 0, vec_children.size() - 1);
    for (auto& child : vec_children)
        df_flag_array_node->add_child_node(child);
    endInsertRows();

    return true;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsCompound(const QModelIndex& p_parent)
{
    Node* node         = dynamic_cast<Node*>(nodeFromIndex(p_parent));
    auto  vec_children = NodeBuilder::fill_node(node);
    beginInsertRows(p_parent, 0, vec_children.size() - 1);
    for (auto& child : vec_children)
        node->add_child_node(child);
    endInsertRows();
    return true;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsVector(const QModelIndex& p_parent)
{
    auto node        = dynamic_cast<NodeVector*>(nodeFromIndex(p_parent));
    auto vector_size = get_vector_size(node);

    if (vector_size == 0)
        return false;

    m_in_vector       = true;
    auto vec_children = NodeBuilder::fill_node(node);
    beginInsertRows(p_parent, 0, vector_size - 1);

    for (auto& child : vec_children)
        node->add_child_node(child);
    endInsertRows();
    m_in_vector = false;
    return true;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsArray(const QModelIndex& p_parent)
{
    auto node         = dynamic_cast<NodeArray*>(nodeFromIndex(p_parent));
    auto vec_children = NodeBuilder::fill_node(node);
    beginInsertRows(p_parent, 0, vec_children.size() - 1);
    for (auto& child : vec_children)
        node->add_child_node(child);
    endInsertRows();
    return true;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsPointer(const QModelIndex& p_parent)
{
    auto node         = nodeFromIndex(p_parent);
    auto node_pointer = dynamic_cast<NodePointer*>(node);

    if (node_pointer->m_df_type == rdf::DF_Type::Void)
        return false;

    uint64_t* pointer_address = reinterpret_cast<uint64_t*>(node_pointer->m_address);
    uint64_t  pointee_address = *pointer_address;
    if (pointee_address == 0)
        return false;

    auto vec_children = NodeBuilder::fill_node(node_pointer);
    beginInsertRows(p_parent, 0, vec_children.size() - 1);
    for (auto& child : vec_children)
        node_pointer->add_child_node(child);
    endInsertRows();
    return true;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertRowsDFArray(const QModelIndex& p_parent)
{
    auto node = dynamic_cast<NodeDFArray*>(nodeFromIndex(p_parent));

    if (node->get_num_children() > 0)
        return false;

    auto vec_children = NodeBuilder::fill_node(node);
    beginInsertRows(p_parent, 0, vec_children.size() - 1);
    for (auto& child : vec_children)
        node->add_child_node(child);
    endInsertRows();
    return true;
}
