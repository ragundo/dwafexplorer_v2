/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "df_model.h"
#include "df_node.h"
#include <cctype>

using namespace rdf;

extern CMetadataServer MetadataServer;

//
//---------------------------------------------------------------------------------------
//
std::string& replace_string(std::string& s, const std::string& from, const std::string& to)
{
    if (!from.empty())
        for (size_t pos = 0; (pos = s.find(from, pos)) != std::string::npos; pos += to.size())
            s.replace(pos, from.size(), to);
    return s;
}

//
//---------------------------------------------------------------------------------------
//
void decode_addornements(std::string& p_addornments, std::string& p_result)
{
    if (p_addornments.empty())
        return;

    std::string rest       = "";
    auto        first_char = p_addornments[0];

    if (p_addornments.size() > 1)
        rest = p_addornments.substr(1, 5000);

    if (first_char == 'v')
    {
        p_result.append("Vector ");
    }
    else if (first_char == '*')
    {
        p_result.append("*");
    }
    else if (first_char == '[')
    {
        size_t iterator = 0;
        while ((iterator < rest.length()) && (std::isdigit(rest[iterator])))
            iterator++;
        std::string array_size = rest.substr(0, iterator);
        p_result.append("[").append(array_size).append("]");
        rest = rest.substr(array_size.length(), 500);
    }
    decode_addornements(rest, p_result);
}

//
//---------------------------------------------------------------------------------------
//
QString DF_Model::data_from_Type(NodeBase* p_node) const
{
    if (p_node->m_node_type == NodeType::Global)
        return "-";

    if (p_node->m_model_data.m_type)
        return QString::fromStdString(*p_node->m_model_data.m_type);

    if (p_node->m_node_type == NodeType::AnonymousCompound)
    {
        p_node->m_model_data.m_type = "";
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    if (p_node->m_node_type == NodeType::AnonymousUnion)
    {
        p_node->m_model_data.m_type = "";
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    if (p_node->m_node_type == rdf::NodeType::Padding)
    {
        p_node->m_model_data.m_type = "Padding";
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    if (p_node->m_df_type == rdf::DF_Type::Static_string)
    {
        p_node->m_model_data.m_type = "char*";
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    if (p_node->m_df_type == rdf::DF_Type::Stl_string)
    {
        p_node->m_model_data.m_type = "std::string";
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    // Get df_type translated to string
    std::string type = MetadataServer.DF_Type_to_string(p_node->m_df_type);

    if (type.find("__") != std::string::npos)
    {
        p_node->m_model_data.m_type = replace_string(type, "__", "::");
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    if (type == "Void")
        type = "void*";

    if (p_node->m_node_type == NodeType::Vector)
    {
        std::string raw_addornements;
        std::string addornements_decoded;

        auto node        = dynamic_cast<const NodeVector*>(p_node);
        raw_addornements = node->m_addornements;
        if (!raw_addornements.empty())
        {
            raw_addornements = (raw_addornements.size() > 1 ? raw_addornements.substr(1, 512) : "");
            decode_addornements(raw_addornements, addornements_decoded);
            p_node->m_model_data.m_type = addornements_decoded.append(type);
            return QString::fromStdString(*p_node->m_model_data.m_type);
        }
        p_node->m_model_data.m_type = type;
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    if (p_node->m_node_type == NodeType::Pointer)
    {
        std::string raw_addornements;
        std::string addornements_decoded;

        auto node        = dynamic_cast<const NodePointer*>(p_node);
        raw_addornements = node->m_addornements;

        if (!raw_addornements.empty())
        {
            if (raw_addornements == "*")
            {
                uint64_t pointee   = *reinterpret_cast<uint64_t*>(p_node->m_address);
                auto     real_type = MetadataServer.get_df_subtype(node->m_df_type, pointee);
                if (real_type != DF_Type::None)
                    type = MetadataServer.DF_Type_to_string(real_type);
                p_node->m_model_data.m_type = type;
                return QString::fromStdString(*p_node->m_model_data.m_type);
            }

            raw_addornements = raw_addornements.substr(1, 500);
            decode_addornements(raw_addornements, addornements_decoded);
            p_node->m_model_data.m_type = addornements_decoded.append(type);
            return QString::fromStdString(*p_node->m_model_data.m_type);
        }
        p_node->m_model_data.m_type = type;
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }
    if (p_node->m_rdf_type == rdf::RDF_Type::Enum)
    {
        auto node_enum              = dynamic_cast<const NodeEnum*>(p_node);
        p_node->m_model_data.m_type = node_enum->m_enum_type_as_string;
        return QString::fromStdString(*p_node->m_model_data.m_type);
    }

    p_node->m_model_data.m_type = type;
    return QString::fromStdString(*p_node->m_model_data.m_type);
}
