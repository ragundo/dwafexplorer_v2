/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "df_model.h"
#include "df_node.h"

using namespace rdf;

extern CMetadataServer MetadataServer;

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::is_simple_type(const NodeBase* l_node) const
{
    if ((l_node->m_rdf_type == rdf::RDF_Type::Bool) ||
        (l_node->m_rdf_type == rdf::RDF_Type::int8_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::int16_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::int32_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::int64_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::uint8_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::uint16_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::uint32_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::uint64_t) ||
        (l_node->m_rdf_type == rdf::RDF_Type::Long) ||
        (l_node->m_rdf_type == rdf::RDF_Type::Char) ||
        (l_node->m_rdf_type == rdf::RDF_Type::S_float) ||
        (l_node->m_rdf_type == rdf::RDF_Type::D_float) ||
        (l_node->m_rdf_type == rdf::RDF_Type::Void))
        return true;
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::is_simple_type(rdf::RDF_Type p_type) const
{
    if ((p_type == rdf::RDF_Type::Bool) ||
        (p_type == rdf::RDF_Type::int8_t) ||
        (p_type == rdf::RDF_Type::int16_t) ||
        (p_type == rdf::RDF_Type::int32_t) ||
        (p_type == rdf::RDF_Type::int64_t) ||
        (p_type == rdf::RDF_Type::uint8_t) ||
        (p_type == rdf::RDF_Type::uint16_t) ||
        (p_type == rdf::RDF_Type::uint32_t) ||
        (p_type == rdf::RDF_Type::uint64_t) ||
        (p_type == rdf::RDF_Type::Long) ||
        (p_type == rdf::RDF_Type::Char) ||
        (p_type == rdf::RDF_Type::S_float) ||
        (p_type == rdf::RDF_Type::D_float) ||
        (p_type == rdf::RDF_Type::Void))
        return true;
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::is_simple_type(rdf::DF_Type p_type) const
{
    if ((p_type == rdf::DF_Type::Bool) ||
        (p_type == rdf::DF_Type::int8_t) ||
        (p_type == rdf::DF_Type::int16_t) ||
        (p_type == rdf::DF_Type::int32_t) ||
        (p_type == rdf::DF_Type::int64_t) ||
        (p_type == rdf::DF_Type::uint8_t) ||
        (p_type == rdf::DF_Type::uint16_t) ||
        (p_type == rdf::DF_Type::uint32_t) ||
        (p_type == rdf::DF_Type::uint64_t) ||
        (p_type == rdf::DF_Type::Long) ||
        (p_type == rdf::DF_Type::Char) ||
        (p_type == rdf::DF_Type::S_float) ||
        (p_type == rdf::DF_Type::D_float) ||
        (p_type == rdf::DF_Type::Void))
        return true;
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::is_string(const NodeBase* l_node) const
{
    if ((l_node->m_rdf_type == rdf::RDF_Type::Stl_string) ||
        (l_node->m_rdf_type == rdf::RDF_Type::Static_string) ||
        (l_node->m_rdf_type == rdf::RDF_Type::Ptr_string))
        return true;
    return false;
}

//
//------------------------------------------------------------------------------------//
//
std::string DF_Model::to_hex(uint64_t p_dec)
{
    if (p_dec == 0)
        return "0x0";
    std::string l_answer = "";
    uint64_t    l_rem;
    while (p_dec > 0)
    {
        l_rem = p_dec % 16;
        switch (l_rem)
        {
            case 0:
                l_answer = "0" + l_answer;
                break;
            case 1:
                l_answer = "1" + l_answer;
                break;
            case 2:
                l_answer = "2" + l_answer;
                break;
            case 3:
                l_answer = "3" + l_answer;
                break;
            case 4:
                l_answer = "4" + l_answer;
                break;
            case 5:
                l_answer = "5" + l_answer;
                break;
            case 6:
                l_answer = "6" + l_answer;
                break;
            case 7:
                l_answer = "7" + l_answer;
                break;
            case 8:
                l_answer = "8" + l_answer;
                break;
            case 9:
                l_answer = "9" + l_answer;
                break;
            case 10:
                l_answer = "A" + l_answer;
                break;
            case 11:
                l_answer = "B" + l_answer;
                break;
            case 12:
                l_answer = "C" + l_answer;
                break;
            case 13:
                l_answer = "D" + l_answer;
                break;
            case 14:
                l_answer = "E" + l_answer;
                break;
            case 15:
                l_answer = "F" + l_answer;
                break;
        }
        p_dec = p_dec / 16;
    }
    return "0x" + l_answer;
}

//
//------------------------------------------------------------------------------------//
//
std::string DF_Model::to_hex(uint64_t p_dec, int p_num_bits)
{
    if (p_dec == 0)
        return "0x0";
    std::string l_answer = "";
    uint64_t    l_rem;

    while ((p_dec > 0) && (p_num_bits > 0))
    {
        l_rem = p_dec % 16;
        switch (l_rem)
        {
            case 0:
                l_answer = "0" + l_answer;
                break;
            case 1:
                l_answer = "1" + l_answer;
                break;
            case 2:
                l_answer = "2" + l_answer;
                break;
            case 3:
                l_answer = "3" + l_answer;
                break;
            case 4:
                l_answer = "4" + l_answer;
                break;
            case 5:
                l_answer = "5" + l_answer;
                break;
            case 6:
                l_answer = "6" + l_answer;
                break;
            case 7:
                l_answer = "7" + l_answer;
                break;
            case 8:
                l_answer = "8" + l_answer;
                break;
            case 9:
                l_answer = "9" + l_answer;
                break;
            case 10:
                l_answer = "A" + l_answer;
                break;
            case 11:
                l_answer = "B" + l_answer;
                break;
            case 12:
                l_answer = "C" + l_answer;
                break;
            case 13:
                l_answer = "D" + l_answer;
                break;
            case 14:
                l_answer = "E" + l_answer;
                break;
            case 15:
                l_answer = "F" + l_answer;
                break;
        }
        p_dec = p_dec / 16;
        p_num_bits -= 4;
    }
    return "0x" + l_answer;
}

//
//------------------------------------------------------------------------------------//
//
std::size_t DF_Model::get_vector_size(const NodeVector* p_node)
{
    auto vector_start_address = reinterpret_cast<uint64_t*>(p_node->m_address);
    auto end                  = p_node->m_address + sizeof(void*);
    auto vector_end_address   = reinterpret_cast<uint64_t*>(end);

    if (*vector_start_address == 0)
        return 0;

    auto diff = *vector_end_address - *vector_start_address;
    if (p_node->m_df_type == DF_Type::Void)
        return diff / sizeof(void*);

    if (p_node->m_addornements.empty())
    {
        // Vector of DF_Types
        if (p_node->m_df_type == DF_Type::Void)
            // Vector of void pointers
            return diff / sizeof(void*);
        // Vector df DF type
        // Check for enum base-type
        auto size_maybe = MetadataServer.size_of_DF_Type(p_node->m_df_type);
        if (p_node->m_enum_basetype != DF_Type::None)
            size_maybe = MetadataServer.size_of_DF_Type(p_node->m_enum_basetype);
        auto df_type_size = *size_maybe;
        return diff / df_type_size;
    }

    // Remove "v"
    std::string addornements = p_node->m_addornements.substr(1, 512);
    if (addornements[0] == '*')
    {
        // Vector of pointers
        return diff / sizeof(void*);
    }
    if (addornements[0] == '[')
    {
        // Vector of arrays
        return 0; // TODO
    }
    if (addornements[0] == '{')
    {
        // {xxxx} where xxxx is a type like int16_t
        auto it = 0;
        while (addornements[it] != '}')
            it++;
        auto vector_type = addornements.substr(1, it - 1);
        // Get the DF_Type from this string
        auto df_type    = MetadataServer.DF_Type_from_string(vector_type);
        auto size_maybe = MetadataServer.size_of_DF_Type(df_type);
        return diff / *size_maybe;
    }
    auto size_maybe = MetadataServer.size_of_DF_Type(p_node->m_df_type);
    return diff / *size_maybe;
}
