#ifndef DFSortFilterProxyModel_H
#define DFSortFilterProxyModel_H

#include "QSortFilterProxyModel"

class DFSortFilterProxyModel : public QSortFilterProxyModel
{
    Q_OBJECT

  public:
    DFSortFilterProxyModel(QObject* parent = 0);

  protected:
    bool filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const override;
    bool lessThan(const QModelIndex& left, const QModelIndex& right) const override;
};
#endif