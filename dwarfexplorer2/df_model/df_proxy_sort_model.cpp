#include "df_proxy_sort_model.h"
#include "MetadataServer.h"
#include "df_model.h"
#include "df_node.h"

using namespace std;
using namespace rdf;

DFSortFilterProxyModel::DFSortFilterProxyModel(QObject* parent)
    : QSortFilterProxyModel(parent)
{
}

bool DFSortFilterProxyModel::filterAcceptsRow(int sourceRow, const QModelIndex& sourceParent) const
{
    return true;
}

bool DFSortFilterProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const
{
    QVariant leftData  = sourceModel()->data(left);
    QVariant rightData = sourceModel()->data(right);

    return leftData < rightData;
}
