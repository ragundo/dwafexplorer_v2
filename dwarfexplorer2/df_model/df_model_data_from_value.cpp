/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "df_all.h"
#include "df_model.h"
#include "df_node.h"
#include "modules/Translation.h"
#include "tl/tl_optional.h"
#include <Console.h>
#include <Core.h>
#include <DataDefs.h>
#include <Export.h>
#include <PluginManager.h>
#include <RemoteClient.h>
#include <RemoteServer.h>
#include <VersionInfo.h>
#include <cstdint>

using namespace rdf;
using namespace DFHack;

extern rdf::CMetadataServer MetadataServer;

//
//-------------------------------------------------------------------------------------
//
bool is_node_bitfield_entry(NodeBase* p_node)
{
    return (p_node->m_rdf_type == rdf::RDF_Type::Bool) && (p_node->parent()->m_rdf_type == rdf::RDF_Type::Bitfield);
}

//
//-------------------------------------------------------------------------------------
//
std::string process_pointer(NodeBase* p_node)
{
    uint64_t* pointer_address = reinterpret_cast<uint64_t*>(p_node->m_address);
    uint64_t  item_address    = *pointer_address;
    if (item_address == 0)
    {
        return "NULL";
    }
    std::string address_hex = DF_Model::to_hex(item_address);
    return address_hex;
}

//
//-------------------------------------------------------------------------------------
//
QString process_node_bitfield_entry(NodeBase* p_node)
{
    return "";
}

//
//-------------------------------------------------------------------------------------
//
bool is_node_simple(const NodeBase* p_node)
{
    switch (p_node->m_df_type)
    {
        case rdf::DF_Type::int64_t:
            return true;
        case rdf::DF_Type::uint64_t:
            return true;
        case rdf::DF_Type::int32_t:
            return true;
        case rdf::DF_Type::uint32_t:
            return true;
        case rdf::DF_Type::int16_t:
            return true;
        case rdf::DF_Type::uint16_t:
            return true;
        case rdf::DF_Type::int8_t:
            return true;
        case rdf::DF_Type::uint8_t:
            return true;
        case rdf::DF_Type::Long:
            return true;
        case rdf::DF_Type::Bool:
            return true;
        case rdf::DF_Type::Void:
            return true;
        case rdf::DF_Type::S_float:
            return true;
        case rdf::DF_Type::D_float:
            return true;
        case rdf::DF_Type::S_double:
            return true;
        default:
            break;
    }
    return false;
}

//
//-------------------------------------------------------------------------------------
//
std::string DF_Model::process_value_bool(NodeBase* p_node) const
{
    auto parent_node = p_node->parent();
    if (parent_node == nullptr)
        parent_node = m_root_node;
    if (parent_node->m_rdf_type == RDF_Type::Vector)
    {
        auto string_index = p_node->m_field_name.substr(1, 10);
        string_index      = string_index.substr(0, string_index.length() - 1);
        int index         = std::stoi(string_index);

        auto  void_pointer   = reinterpret_cast<void*>(p_node->m_address);
        auto  vector_pointer = reinterpret_cast<std::vector<char>*>(void_pointer);
        auto& vector         = *vector_pointer;
        bool  vector_value   = vector[index];
        return vector_value ? "True" : "False";
    }
    return *(reinterpret_cast<int64_t*>(p_node->m_address)) ? "True" : "False";
}

//
//-------------------------------------------------------------------------------------
//
std::string DF_Model::process_value_node_simple(NodeBase* p_node) const
{
    long  long_value;
    long* long_ptr;
    switch (p_node->m_df_type)
    {
        case rdf::DF_Type::int64_t:
            return std::to_string(*(reinterpret_cast<int64_t*>(p_node->m_address)));
        case rdf::DF_Type::uint64_t:
            return std::to_string(*(reinterpret_cast<uint64_t*>(p_node->m_address)));
        case rdf::DF_Type::int32_t:
            return std::to_string(*(reinterpret_cast<int32_t*>(p_node->m_address)));
        case rdf::DF_Type::uint32_t:
            return std::to_string(*(reinterpret_cast<uint32_t*>(p_node->m_address)));
        case rdf::DF_Type::int16_t:
            return std::to_string(*(reinterpret_cast<int16_t*>(p_node->m_address)));
        case rdf::DF_Type::uint16_t:
            return std::to_string(*(reinterpret_cast<uint16_t*>(p_node->m_address)));
        case rdf::DF_Type::int8_t:
            return std::to_string(*(reinterpret_cast<int8_t*>(p_node->m_address)));
        case rdf::DF_Type::uint8_t:
            return std::to_string(*(reinterpret_cast<uint8_t*>(p_node->m_address)));
        case rdf::DF_Type::Long:
            long_ptr   = reinterpret_cast<long*>(p_node->m_address);
            long_value = *long_ptr;
            return std::to_string(long_value);
        case rdf::DF_Type::S_float:
        case rdf::DF_Type::D_float:
        case rdf::DF_Type::S_double:
            return std::to_string(*(reinterpret_cast<float*>(p_node->m_address)));
        case rdf::DF_Type::Bool:
            return process_value_bool(p_node);
        case rdf::DF_Type::Void:
            return process_pointer(p_node);
        default:
            break;
    }
    return "Unknown1";
}

//
//-------------------------------------------------------------------------------------
//
bool is_node_void_pointer(NodeBase* p_node)
{
    if (p_node->m_node_type == NodeType::Void)
        return true;
    return false;
}

//
//-------------------------------------------------------------------------------------
//
std::string process_node_void_pointer(NodeBase* p_node)
{
    return process_pointer(p_node);
}

//
//-------------------------------------------------------------------------------------
//
bool is_node_stl_string(NodeBase* p_node)
{
    if (p_node->m_rdf_type == rdf::RDF_Type::Stl_string)
        return true;
    return false;
}

//
//-------------------------------------------------------------------------------------
//
std::string process_node_stl_string(NodeBase* p_node)
{
    auto        string_address = reinterpret_cast<std::string*>(p_node->m_address);
    std::string object         = *string_address;
    if (object.empty())
    {
        return "''";
    }
    return object;
}

//
//-------------------------------------------------------------------------------------
//
QString process_node_simple_pointer(NodeBase* p_node)
{
    return "";
}

//
//-------------------------------------------------------------------------------------
//
bool is_node_df_pointer(NodeBase* p_node)
{
    return false;
}

//
//-------------------------------------------------------------------------------------
//
QString process_node_df_pointer(NodeBase* p_node)
{
    return "";
}

//
//-------------------------------------------------------------------------------------
//
bool is_node_df_pointer_vector_entry(NodeBase* p_node)
{
    return false;
}

//
//-------------------------------------------------------------------------------------
//
QString process_node_df_pointer_vector_entry(NodeBase* p_node)
{
    return "";
}

template<typename T>
std::string decode_enum(NodeBase* p_enum_node)
{
    T*   enum_value_address = reinterpret_cast<T*>(p_enum_node->m_address);
    auto enum_value_maybe   = MetadataServer.get_enum_value(p_enum_node->m_df_type, *enum_value_address);
    if (!enum_value_maybe)
    {
        return "UNKNOWN VALUE";
    }
    return (*enum_value_maybe).first;
}

//
//-------------------------------------------------------------------------------------
//
std::string Enum_data_from_Value(NodeBase* p_node, bool p_format_value = false)
{
    auto enum_node = dynamic_cast<NodeEnum*>(p_node);

    tl::optional<const std::pair<std::string, std::string>> enum_value_maybe = tl::nullopt;
    int                                                     enum_value_number;
    std::string                                             enum_value_string;

    switch (enum_node->m_base_type)
    {
        case DF_Type::int8_t:
            enum_value_number = get_value_at_address<int8_t>(enum_node);
            enum_value_string = decode_enum<int8_t>(enum_node);
            break;
        case DF_Type::int16_t:
            enum_value_number = get_value_at_address<int16_t>(enum_node);
            enum_value_string = decode_enum<int16_t>(enum_node);
            break;
        case DF_Type::int32_t:
            enum_value_number = get_value_at_address<int32_t>(enum_node);
            enum_value_string = decode_enum<int32_t>(enum_node);
            break;
        case DF_Type::uint8_t:
            enum_value_number = get_value_at_address<uint8_t>(enum_node);
            enum_value_string = decode_enum<uint8_t>(enum_node);
            break;
        case DF_Type::uint16_t:
            enum_value_number = get_value_at_address<uint16_t>(enum_node);
            enum_value_string = decode_enum<uint16_t>(enum_node);
            break;
        case DF_Type::uint32_t:
            enum_value_number = get_value_at_address<uint32_t>(enum_node);
            enum_value_string = decode_enum<uint32_t>(enum_node);
            break;
        default:
            break;
    }

    std::string result = "[" + std::to_string(enum_value_number) + "] = " + enum_value_string;
    return result;
}

//
//-------------------------------------------------------------------------------------
//
std::string Bitfield_data_from_Value(NodeBase* p_node)
{
    int         bitfield_value;
    std::string bitfield_value_as_string;
    auto        bitfield_node = dynamic_cast<NodeBitfield*>(p_node);

    switch (bitfield_node->m_base_type)
    {
        case DF_Type::int8_t:
            bitfield_value           = get_value_at_address<int8_t>(bitfield_node);
            bitfield_value_as_string = DF_Model::to_hex(bitfield_value, 8);
            break;
        case DF_Type::int16_t:
            bitfield_value           = get_value_at_address<int16_t>(bitfield_node);
            bitfield_value_as_string = DF_Model::to_hex(bitfield_value, 16);
            break;
        case DF_Type::int32_t:
            bitfield_value           = get_value_at_address<int32_t>(bitfield_node);
            bitfield_value_as_string = DF_Model::to_hex(bitfield_value, 32);
            break;
        case DF_Type::uint8_t:
            bitfield_value           = get_value_at_address<uint8_t>(bitfield_node);
            bitfield_value_as_string = DF_Model::to_hex(bitfield_value, 8);
            break;
        case DF_Type::uint16_t:
            bitfield_value           = get_value_at_address<uint16_t>(bitfield_node);
            bitfield_value_as_string = DF_Model::to_hex(bitfield_value, 16);
            break;
        case DF_Type::uint32_t:
            bitfield_value           = get_value_at_address<uint32_t>(bitfield_node);
            bitfield_value_as_string = DF_Model::to_hex(bitfield_value, 32);
            break;
        default:
            break;
    }

    return bitfield_value_as_string;
}

//
//-------------------------------------------------------------------------------------
//
std::string DF_Model::Vector_data_from_Value(NodeBase* p_node) const
{
    auto        node_vector = dynamic_cast<const NodeVector*>(p_node);
    auto        vector_size = this->get_vector_size(node_vector);
    std::string size        = "[";
    if (vector_size == 0)
    {
        size.append("empty]");
        return size;
    }
    size.append(std::to_string(vector_size));
    if (vector_size == 1)
        size.append(" item]");
    else
        size.append(" items]");
    return size;
}

//
//-------------------------------------------------------------------------------------
//
QString DF_Model::data_from_Value(NodeBase* p_node) const
{
    if (p_node->m_node_type == NodeType::Global)
        return "-";

    if (p_node->m_model_data.m_value)
        return QString::fromStdString(*p_node->m_model_data.m_value);

    uint64_t l_address = p_node->m_address;

    // Not implemented in this OS
    if (l_address == 0)
    {
        p_node->m_model_data.m_value = "N/A";
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == RDF_Type::Pointer)
    {
        p_node->m_model_data.m_value = process_pointer(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Enum)
    {
        p_node->m_model_data.m_value = Enum_data_from_Value(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }
    if (p_node->m_rdf_type == rdf::RDF_Type::Bitfield)
    {
        p_node->m_model_data.m_value = Bitfield_data_from_Value(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::DFFlagArray)
    {
        p_node->m_model_data.m_value = process_pointer(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_node_type == rdf::NodeType::BitfieldEntry)
    {
        auto node                    = dynamic_cast<const NodeBitfieldEntry*>(p_node);
        p_node->m_model_data.m_value = node->m_value ? "True" : "False";
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Vector)
    {
        p_node->m_model_data.m_value = Vector_data_from_Value(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Array)
    {
        auto node                    = dynamic_cast<const NodeArray*>(p_node);
        p_node->m_model_data.m_value = "";
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::DFArray)
    {
        const NodeDFArray* node_df_array = dynamic_cast<const NodeDFArray*>(p_node);
        std::string        result        = "[";
        result.append(std::to_string(node_df_array->m_array_size));
        result.append("] items");
        p_node->m_model_data.m_value = result;
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (is_node_void_pointer(p_node))
    {
        p_node->m_model_data.m_value = process_node_void_pointer(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }
    if (is_node_stl_string(p_node))
    {
        p_node->m_model_data.m_value = process_node_stl_string(p_node);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_df_type == rdf::DF_Type::language_name)
    {
        // Translate the name
        df::language_name* lang   = reinterpret_cast<df::language_name*>(p_node->m_address);
        std::string        result = Translation::TranslateName(lang, false, false);
        if (result.empty())
            return "''";
        p_node->m_model_data.m_value = DF2UTF(result);
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_df_type == rdf::DF_Type::coord)
    {
        df::coord* coord             = reinterpret_cast<df::coord*>(p_node->m_address);
        p_node->m_model_data.m_value = "[X=" + std::to_string(coord->x) + ",Y=" + std::to_string(coord->y) + ",Z=" + std::to_string(coord->z) + "]";
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_df_type == rdf::DF_Type::coord2d)
    {
        df::coord2d* coord           = reinterpret_cast<df::coord2d*>(p_node->m_address);
        p_node->m_model_data.m_value = "[X=" + std::to_string(coord->x) + ",Y=" + std::to_string(coord->y) + "]";
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_df_type == rdf::DF_Type::Padding)
    {
        auto padding_node            = dynamic_cast<const NodePadding*>(p_node);
        p_node->m_model_data.m_value = "[" + std::to_string(padding_node->m_size) + " bytes" + "]";
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_df_type == rdf::DF_Type::Static_string)
    {
        auto        ss_node = dynamic_cast<const NodeStaticString*>(p_node);
        const char* value   = reinterpret_cast<const char*>(ss_node->m_address);

        std::string result("\"");
        result.append(value);
        result.append("\"");
        p_node->m_model_data.m_value = result;
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::DFLinkedList)
    {
        auto      ss_node = dynamic_cast<const NodeDFLinkedList*>(p_node);
        int       count   = 0;
        uint64_t* address = reinterpret_cast<uint64_t*>(ss_node->m_address);
        while (address != nullptr)
        {
            // First pointer is to data
            // Second pointer is previous node
            // Third pointer is next node
            count++;
            address++;
            address++;
            address = reinterpret_cast<uint64_t*>(*address);
        }
        std::string result = "[";
        result.append(std::to_string(count));
        result.append(" items]");
        p_node->m_model_data.m_value = result;
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }

    if (is_node_simple(p_node))
    {
        auto value                   = process_value_node_simple(p_node);
        p_node->m_model_data.m_value = value;
        return QString::fromStdString(*p_node->m_model_data.m_value);
    }
    p_node->m_model_data.m_value = "";
    return QString::fromStdString(*p_node->m_model_data.m_value);
}
