/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "df_node.h"
#include "MetadataServer.h"
#include "NodeTable.h"
#include <cctype>
#include <iomanip>
#include <sstream>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
NodeDummy the_dummy;

//
//------------------------------------------------------------------------------------//
//
rdf::NodeDummy* dummy()
{
    return &the_dummy;
}

//
//------------------------------------------------------------------------------------//
//
bool NodeBase::is_root_node() const
{
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool NodeBase::can_have_children() const
{
    return false;
}

//
//------------------------------------------------------------------------------------//
//
std::string NodeBase::node_path_name()
{
    auto result = m_field_name;
    result.erase(std::remove(result.begin(), result.end(), ' '), result.end());
    return result;
}

//
//------------------------------------------------------------------------------------//
//
bool Node::can_have_children() const
{
    return true;
}

//
//------------------------------------------------------------------------------------//
//
NodeBase* Node::find_child(const std::string& p_field_name)
{
    for (auto child : m_children)
    {
        if (child->m_field_name == p_field_name)
            return child;
    }
    return nullptr;
}

//
//------------------------------------------------------------------------------------//
//
Node::~Node()
{
    /*
        for (std::size_t i = 0; i < m_children.size(); i++)
        {
            if (m_children[i])
                if (m_children[i]->m_node_type != NodeType::Dummy)
                    delete m_children[i];

            m_children[i] = nullptr;
        }
        */
}

//
//------------------------------------------------------------------------------------//
//
std::string NodeBitfieldEntry::node_path_name()
{
    auto it = 0;
    while (m_field_name[it] != '[')
        it++;
    auto start = it++;
    auto end   = start + 1;
    while (m_field_name[end] != ']')
        end++;
    auto result = m_field_name.substr(start, end - start);
    return result;
}

//
//------------------------------------------------------------------------------------//
//
std::string NodeBase::path()
{
    std::string            result = "df";
    std::vector<NodeBase*> path;

    NodeBase* node_iterator = this;
    while (node_iterator && (node_iterator->m_node_type != NodeType::Root))
    {
        path.push_back(node_iterator);
        node_iterator = node_iterator->parent();
    }

    if (node_iterator && node_iterator->m_node_type == NodeType::Root)
    {
        auto node_root = dynamic_cast<NodeRoot*>(node_iterator);
        if (!node_root->m_field_path.empty())
            result = node_root->m_field_path;
    }

    int i = path.size() - 1;
    while (i >= 0)
    {
        NodeBase* node = path[i--];
        if (node != nullptr)
        {
            auto data = node->node_path_name();
            if (!data.empty())
            {
                if (data[0] == '[')
                {
                    result.append(data);
                    continue;
                }
                result.append(".");
                result.append(data);
            }
        }
    }
    result.erase(remove(result.begin(), result.end(), ' '), result.end());
    return result;
}

//
//------------------------------------------------------------------------------------//
//
int get_array_size_rec(std::string& p_addornements, DF_Type p_array_df_type)
{
    if (p_addornements[0] == 'v')
        return sizeof(std::vector<void*>);
    if (p_addornements[0] == '*')
        return sizeof(void*);
    if (p_addornements[0] == '[')
    {
        size_t it = 1;
        while (std::isdigit(p_addornements[it++]))
            ;
        auto size_st    = p_addornements.substr(1, it - 2);
        auto size_array = std::stoi(size_st);
        auto rest       = (it <= p_addornements.size() ? p_addornements.substr(it - 1, 512) : "");
        if (rest.empty())
        {
            auto size_maybe   = MetadataServer.size_of_DF_Type(p_array_df_type);
            auto df_type_size = *size_maybe;
            return size_array * df_type_size;
        }
        return size_array * get_array_size_rec(rest, p_array_df_type);
    }
    return 0;
}

//
//------------------------------------------------------------------------------------//
//
int NodeBase::get_node_size()
{
    NodeEnum*      node_enum;
    NodeBitfield*  node_bitfield;
    NodePadding*   node_padding;
    NodeAnonymous* node_anonymous;
    int            size = 0;
    switch (m_node_type)
    {
        case NodeType::Simple:
        case NodeType::Compound:
        case NodeType::Union:
        case NodeType::Deque:
            return *MetadataServer.size_of_DF_Type(m_df_type);
        case NodeType::DFFlagArray:
            return sizeof(uint8_t*) + sizeof(uint32_t); // DFArray.bits + DFArray.size
        case NodeType::DFArray:
            return sizeof(void*) + sizeof(unsigned short);
        case NodeType::DFLinkedList:
            return sizeof(void*) + sizeof(void*) + sizeof(void*); // item, prev, next pointers
        case NodeType::Dummy:
        case NodeType::Root:
        case NodeType::BitfieldEntry:
            return 0;
        case NodeType::Vector:
            return sizeof(std::vector<void*>);
        case NodeType::Enum:
            node_enum = dynamic_cast<NodeEnum*>(this);
            if (node_enum->m_base_type != DF_Type::None)
                return *MetadataServer.size_of_DF_Type(node_enum->m_base_type);
            return *MetadataServer.size_of_DF_Type(m_df_type);
        case NodeType::Bitfield:
            node_bitfield = dynamic_cast<NodeBitfield*>(this);
            if (node_bitfield->m_base_type != DF_Type::None)
                return *MetadataServer.size_of_DF_Type(node_bitfield->m_base_type);
            return *MetadataServer.size_of_DF_Type(m_df_type);
        case NodeType::Void:
        case NodeType::Pointer:
            return sizeof(void*);
        case NodeType::Padding:
            node_padding = dynamic_cast<NodePadding*>(this);
            return node_padding->m_size;
        case NodeType::StaticString:
            return sizeof(std::string);
        case NodeType::AnonymousCompound: //TODO
        case NodeType::AnonymousUnion:
            node_anonymous = dynamic_cast<NodeAnonymous*>(this);
            if (node_anonymous->has_dummy_node())
            {
                Node* parent  = dynamic_cast<Node*>(this->parent());
                int   brother = -1;
                for (size_t t = 0; t < parent->get_num_children(); t++)
                    if (parent->get_child(t) == this)
                    {
                        brother = t + 1;
                        break;
                    }
                if (brother < (int)parent->get_num_children())
                    return parent->get_child(brother)->m_address - m_address;
            }
            for (size_t i = 0; i < node_anonymous->get_num_children(); i++)
            {
                auto child = node_anonymous->get_child(i);
                size       = (size < child->get_node_size() ? child->get_node_size() : size);
            }
            return size;
        case NodeType::Array:
            break;
    }

    // Size of array
    NodeArray*  node_array   = dynamic_cast<NodeArray*>(this);
    std::string addornements = node_array->m_addornements;
    return get_array_size_rec(addornements, node_array->m_df_type);
}

//
//---------------------------------------------------------------------------------------
//
NodeRoot* NodeBase::get_root_node()
{
    NodeBase* iterator = this;
    while ((iterator != nullptr) && (!iterator->is_root_node()))
        iterator = iterator->parent();
    return dynamic_cast<NodeRoot*>(iterator);
}

//
//---------------------------------------------------------------------------------------
//
static std::vector<std::string> split(const std::string& s, char delimiter)
{
    std::vector<std::string> tokens;
    std::string              token;
    std::istringstream       tokenStream(s);
    while (getline(tokenStream, token, delimiter))
    {
        tokens.push_back(token);
    }
    return tokens;
}

//
//---------------------------------------------------------------------------------------
//

vector<string> NodeBase::split_path()
{
    auto                     vec1 = split(m_field_path, '.');
    std::vector<std::string> result;
    for (auto& token : vec1)
    {
        if (token.find('[') == string::npos)
        {
            result.push_back(token);
            continue;
        }
        auto vec2 = split(token, '[');
        for (auto& token2 : vec2)
        {
            result.push_back("[" + token2);
        }
    }
    return result;
}

//
//---------------------------------------------------------------------------------------
//
static std::string make_path_from_tokens(std::vector<string>& tokens, std::size_t end)
{
    std::string result = "";
    std::size_t i      = 0;
    bool        first  = true;
    for (size_t j = 0; j < end; j++)
        if (first)
        {
            result.append(tokens[j]);
            first = false;
        }
        else
            result.append(".").append(tokens[j]);
    return result;
}

//
//---------------------------------------------------------------------------------------
//
NodeBase* NodeBase::parent() const
{
    if (m_field_name == "@df")
        return nullptr;
    string key        = m_field_path;
    auto   node_maybe = m_node_table->find_node(key);
    if (!node_maybe)
        return nullptr;
    auto node = *node_maybe;
    return node;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBase::init(const NodeBase& p_dest_node)
{
    m_model_data      = p_dest_node.m_model_data;
    m_field_path      = p_dest_node.m_field_path;
    m_field_name      = p_dest_node.m_field_name;
    m_df_type         = p_dest_node.m_df_type;
    m_rdf_type        = p_dest_node.m_rdf_type;
    m_node_table      = p_dest_node.m_node_table;
    m_comment         = p_dest_node.m_comment;
    m_address         = p_dest_node.m_address;
    m_node_type       = p_dest_node.m_node_type;
    m_hyperlink_type  = p_dest_node.m_hyperlink_type;
    m_hyperlink_value = p_dest_node.m_hyperlink_value;
}

//
//---------------------------------------------------------------------------------------
//
void Node::init(const Node& p_node)
{
    m_children = p_node.m_children;
}

//
//---------------------------------------------------------------------------------------
//
void NodePadding::init(const NodePadding& p_node)
{
    m_size = p_node.m_size;
}

//
//---------------------------------------------------------------------------------------
//
void NodeStaticString::init(const NodeStaticString& p_node)
{
    m_size = p_node.m_size;
}

//
//---------------------------------------------------------------------------------------
//
void NodeRoot::init(const NodeRoot& p_node)
{
    //m_model = p_node.m_model;
}

//
//---------------------------------------------------------------------------------------
//
void NodeEnum::init(const NodeEnum& p_node)
{
    m_base_type           = p_node.m_base_type;
    m_enum_type_as_string = p_node.m_enum_type_as_string;
    m_first_value         = p_node.m_first_value;
    m_last_value          = p_node.m_last_value;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBitfieldEntry::init(const NodeBitfieldEntry& p_node)
{
    m_index = p_node.m_index;
    m_value = p_node.m_value;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBitfield::init(const NodeBitfield& p_node)
{
    m_index_enum = p_node.m_index_enum;
}

//
//---------------------------------------------------------------------------------------
//
void NodeDFFlagArray::init(const NodeDFFlagArray& p_node)
{
    m_num_entries = p_node.m_num_entries;
}

//
//---------------------------------------------------------------------------------------
//
void NodeDFLinkedList::init(const NodeDFLinkedList& p_node)
{
    m_item_type = p_node.m_item_type;
}

//
//---------------------------------------------------------------------------------------
//
void NodeAddornements::init(const NodeAddornements& p_node)
{
    m_addornements = p_node.m_addornements;
}

//
//---------------------------------------------------------------------------------------
//
void NodeArray::init(const NodeArray& p_node)
{
    m_array_size    = p_node.m_array_size;
    m_index_enum    = p_node.m_index_enum;
    m_enum_basetype = p_node.m_enum_basetype;
}

//
//---------------------------------------------------------------------------------------
//
void NodeVector::init(const NodeVector& p_node)
{
    m_enum_basetype = p_node.m_enum_basetype;
}

//
//---------------------------------------------------------------------------------------
//
void NodeDFArray::init(const NodeDFArray& p_node)
{
    m_array_size = p_node.m_array_size;
}

//
//---------------------------------------------------------------------------------------
//
bool Node::has_dummy_node() const
{
    return (m_children.size() == 1 && m_children[0]->m_node_type == NodeType::Dummy);
}

//
//---------------------------------------------------------------------------------------
//
int NodeBase::get_node_position() const
{
    string key                    = m_field_path;
    auto   parent_base_node_maybe = m_node_table->find_node(key);
    if (!parent_base_node_maybe)
        return -1;
    auto parent_node = dynamic_cast<Node*>(*parent_base_node_maybe);
    int  pos         = 0;
    for (size_t i = 0; i < parent_node->get_num_children(); i++)
    {
        auto item = parent_node->get_child(i);
        if (item == this)
            return pos;
        pos++;
    }
    return -1;
}

//
//---------------------------------------------------------------------------------------
//
void Node::add_dummy_node()
{
    if (m_children.empty())
        m_children.push_back(dummy());
}

} // namespace rdf