/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DF_MODEL_H
#define DF_MODEL_H

#include "metadata_server/MetadataServer.h"
#include <QAbstractItemModel>

namespace rdf
{
struct NodeBase;
struct Node;
struct NodeRoot;
struct NodeVector;
class CMetadataServer;
class NodeTable;
} // namespace rdf

class DF_Model : public QAbstractItemModel
{
    Q_OBJECT

  public:
    enum MyRoles
    {
        UrlRole = Qt::UserRole
    };

  private:
    rdf::NodeRoot* m_root_node;
    bool           m_outdated;
    QString        m_expression; // The lua expression that this model shows (df.global.world.***)
    bool           m_in_vector{false};
    bool           m_online{true};

  private:
    QString data_from_Type(rdf::NodeBase* p_node) const;
    QString data_from_Value(rdf::NodeBase* p_node) const;
    QString data_from_Structure(rdf::NodeBase* p_node) const;
    QString data_from_Name(rdf::NodeBase* p_node) const;
    QString data_from_Address(rdf::NodeBase* p_node) const;
    QString data_from_Comment(rdf::NodeBase* p_node) const;
    QString data_from_Hyperlink(rdf::NodeBase* p_node) const;

    std::string Vector_data_from_Value(rdf::NodeBase* p_node) const;

  public:
    DF_Model(QObject* p_parent = 0)
        : QAbstractItemModel(p_parent), m_root_node(nullptr), m_outdated(false)
    {
    }
    ~DF_Model();

    void           set_root(rdf::NodeRoot* p_node);
    rdf::NodeRoot* get_root();
    QModelIndex    index(int p_row, int p_column, const QModelIndex& p_parent = QModelIndex()) const;
    QModelIndex    parent(const QModelIndex& p_child_index) const;
    int            rowCount(const QModelIndex& p_parent = QModelIndex()) const;
    QVariant       data(const QModelIndex& p_index, int p_role = Qt::DisplayRole) const;
    QVariant       headerData(int p_section, Qt::Orientation p_orientation, int p_role = Qt::DisplayRole) const;
    int            columnCount(const QModelIndex& parent = QModelIndex()) const;
    bool           removeRows(int p_row, int p_count, const QModelIndex& p_parent = QModelIndex());
    bool           insertColumns(int p_column, int p_count, const QModelIndex& p_parent = QModelIndex());
    bool           removeColumns(int p_column, int p_count, const QModelIndex& p_parent = QModelIndex());
    bool           hasChildren(const QModelIndex& p_parent = QModelIndex()) const;

    void add_children_from_node(rdf::Node* p_node);

    bool insertRowsVector(const QModelIndex& p_parent);
    bool insertRowsPointer(const QModelIndex& p_parent);
    bool insertRowsSimplePointerVector(const QModelIndex& p_parent);
    bool insertRowsCompound(const QModelIndex& p_parent);
    bool insertRowsArray(const QModelIndex& p_parent);
    bool insertRowsBitfield(const QModelIndex& p_parent);
    bool insertRowsSimple(const QModelIndex& p_parent);
    bool insertRowsDFFlagArray(const QModelIndex& p_parent);
    bool insertRowsDFArray(const QModelIndex& p_parent);

    static std::string to_hex(uint64_t p_dec);
    static std::string to_hex(uint64_t p_dec, int num_bits);
    static std::size_t get_vector_size(const rdf::NodeVector* p_node);

    bool is_simple_type(const rdf::NodeBase* l_node) const;
    bool is_simple_type(rdf::RDF_Type p_type) const;
    bool is_simple_type(rdf::DF_Type p_type) const;
    bool is_string(const rdf::NodeBase* l_node) const;

    void insert_child_nodes(rdf::Node* p_node, const QModelIndex& p_index);
    void insert_global_nodes(rdf::Node* p_node, const QModelIndex& p_index);
    bool has_children_from_type(rdf::NodeBase* p_node) const;

    rdf::NodeBase* nodeFromIndex(const QModelIndex& p_index) const;

    void set_outdated();

    QModelIndex create_index(int p_row, int p_column, rdf::NodeBase* p_node);

    void    set_expression(const QString& p_lua_expression);
    QString get_expresion()
    {
        return m_expression;
    }
    void        remove_dummy_child_node(const QModelIndex& p_model_index);
    void        remove_dummy_child_node(rdf::Node* p_node);
    void        add_child_node_to_root(rdf::NodeBase* p_child_node);
    void        add_child_node_to_parent_node(rdf::Node* p_parent_node, rdf::NodeBase* p_child_node);
    QModelIndex getIndexByNode(rdf::NodeBase* p_node) const;

    void set_online(bool p_online);
    void reset_to_root();

  private:
    std::string process_value_node_simple(rdf::NodeBase* p_node) const;
    std::string process_value_bool(rdf::NodeBase* p_node) const;
};

#endif // DF_MODEL_H
