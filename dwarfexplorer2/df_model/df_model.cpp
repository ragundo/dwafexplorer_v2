/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "df_model.h"
#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "df_node.h"
#include "lisp_parser/LispParser.h"
#include <QDebug>
#include <QListIterator>
#include <QPixmap>
#include <algorithm>
#include <array>
#include <cctype>

using namespace rdf;

extern rdf::CMetadataServer MetadataServer;

extern std::array<std::array<std::string, 3>, 32>& get_bitfield_bits(DF_Type);

//
//------------------------------------------------------------------------------------//
//
void DF_Model::set_root(NodeRoot* p_node)
{
    beginResetModel();
    m_root_node = p_node;
    endResetModel();
}

//
//------------------------------------------------------------------------------------//
//
NodeRoot* DF_Model::get_root()
{
    return m_root_node;
}

//
//------------------------------------------------------------------------------------//
//
DF_Model::~DF_Model()
{
    delete m_root_node;
    m_root_node = nullptr;
}

//
//------------------------------------------------------------------------------------//
//
QModelIndex DF_Model::index(int p_row, int p_column, const QModelIndex& p_parent) const
{
    if (!m_root_node || p_row < 0 || p_column < 0)
        return QModelIndex();
    NodeBase* parentNode = nodeFromIndex(p_parent);
    if (parentNode->m_node_type == NodeType::Simple)
        return QModelIndex();
    auto node = dynamic_cast<Node*>(parentNode);
    if (p_row > node->get_num_children() - 1)
    {
        qDebug() << "ERROR in DF_Model::index p_row = " << p_row << "when size is " << node->get_num_children();
        return QModelIndex();
    }
    NodeBase* childNode = node->get_child(p_row);
    return createIndex(p_row, p_column, childNode);
}

//
//------------------------------------------------------------------------------------//
//
QModelIndex DF_Model::parent(const QModelIndex& p_child_index) const
{
    NodeBase* node = nodeFromIndex(p_child_index);
    if (!node)
        return QModelIndex();
    NodeBase* parentNode = node->parent();
    if (!parentNode)
        return QModelIndex();
    if (parentNode->m_field_name == m_root_node->m_field_name && parentNode->m_field_path == m_root_node->m_field_path)
        return QModelIndex();
    NodeBase* grandparentNode = parentNode->parent();
    if (!grandparentNode)
        return QModelIndex();
    if (grandparentNode->m_node_type == NodeType::Simple)
        return QModelIndex();
    Node* gp_node = dynamic_cast<Node*>(grandparentNode);
    if (gp_node->m_field_name == m_root_node->m_field_name && gp_node->m_field_path == m_root_node->m_field_path)
        return createIndex(0, 0, parentNode);
    int row = -1;
    for (size_t i = 0; i < gp_node->get_num_children(); i++)
    {
        auto child = gp_node->get_child(i);
        if (child == parentNode)
            row = i;
    }
    return createIndex(row, 0, parentNode);
}

//
//------------------------------------------------------------------------------------//
//
int DF_Model::rowCount(const QModelIndex& p_parent) const
{
    if (p_parent.column() > 0)
        return 0;
    NodeBase* parentNode = nodeFromIndex(p_parent);
    if (!parentNode)
        return 0;
    if (parentNode->m_node_type == NodeType::Simple)
        return 0;
    auto parent = static_cast<Node*>(parentNode);
    return parent->get_num_children();
}

//
//------------------------------------------------------------------------------------//
//
NodeBase* DF_Model::nodeFromIndex(const QModelIndex& p_index) const
{
    if (p_index.isValid())
    {
        auto result = p_index.internalPointer();
        return static_cast<NodeBase*>(result);
    }
    else
    {
        return m_root_node;
    }
}

//
//------------------------------------------------------------------------------------//
//
QString DF_Model::data_from_Name(NodeBase* p_node) const
{
    if (p_node->m_model_data.m_name)
    {
        auto result = *p_node->m_model_data.m_name;
        return QString::fromStdString(result);
    }

    if (p_node->m_node_type == NodeType::Root)
    {
        auto node_root              = dynamic_cast<NodeRoot*>(p_node);
        p_node->m_model_data.m_name = node_root->m_field_path;
        auto result                 = *p_node->m_model_data.m_name;
        return QString::fromStdString(result);
    }

    if (p_node->m_node_type == NodeType::Global)
    {
        auto node_global            = dynamic_cast<NodeGlobal*>(p_node);
        p_node->m_model_data.m_name = "globals";
        auto result                 = *p_node->m_model_data.m_name;
        return QString::fromStdString(result);
    }

    if (p_node->m_field_name.find("N/A") != std::string::npos)
    {
        p_node->m_model_data.m_name = "N/A";
        auto result                 = *p_node->m_model_data.m_name;
        return QString::fromStdString(result);
    }

    if (p_node->m_field_name[0] == '[')
    {
        auto parent = p_node->parent();
        if (parent->m_rdf_type == RDF_Type::Array)
        {
            auto node_array = dynamic_cast<NodeArray*>(parent);
            // Check for index-enum
            if (node_array->m_index_enum != DF_Type::None)
            {
                // Extract the index [??]
                int it = 1;
                while (std::isdigit(p_node->m_field_name[it]))
                    it++;
                auto index_st    = p_node->m_field_name.substr(1, it - 1);
                auto index       = std::stoi(index_st);
                auto value_maybe = MetadataServer.get_enum_value(node_array->m_index_enum, index);
                if (value_maybe)
                {
                    auto        par             = *value_maybe;
                    std::string field_name      = p_node->m_field_name + " = " + par.first;
                    p_node->m_model_data.m_name = field_name;
                    auto result                 = *p_node->m_model_data.m_name;
                    return QString::fromStdString(result);
                }
            }
        }
        if (parent->m_rdf_type == RDF_Type::Bitfield)
        {
            // [0]
            std::string key = p_node->m_field_name;
            key             = key.substr(1, 100);
            auto it         = 0;
            while (std::isdigit(key[it]))
                it++;
            key   = key.substr(0, it);
            int i = std::stoi(key);

            std::string decorated_field_name = "";
            if (i < 10)
                decorated_field_name = "[0" + std::to_string(i) + "]";
            else
                decorated_field_name = "[" + std::to_string(i) + "]";

            auto metadata_maybe = MetadataServer.get_bitfield_metadata(parent->m_df_type);
            auto metadata       = *metadata_maybe;
            auto iter           = metadata.find(i);
            if (iter != metadata.end())
                decorated_field_name = decorated_field_name + " " + (*iter).second.first;
            p_node->m_model_data.m_name = decorated_field_name;
            auto result                 = *p_node->m_model_data.m_name;
            return QString::fromStdString(result);
        }
    }

    p_node->m_model_data.m_name = p_node->m_field_name;
    auto result                 = *p_node->m_model_data.m_name;
    return QString::fromStdString(result);
}

//
//------------------------------------------------------------------------------------//
//
QString DF_Model::data_from_Address(rdf::NodeBase* p_node) const
{
    if (p_node->m_node_type == NodeType::Global)
        return "-";

    if (p_node->m_model_data.m_address)
        return QString::fromStdString(*p_node->m_model_data.m_address);

    const NodeBase* base    = dynamic_cast<const NodeBase*>(p_node);
    uint64_t        address = base->m_address;
    if (address == 0)
    {
        p_node->m_model_data.m_address = "N/A";
        return QString::fromStdString(*p_node->m_model_data.m_address);
    }
    p_node->m_model_data.m_address = to_hex(address);
    return QString::fromStdString(*p_node->m_model_data.m_address);
}

//
//------------------------------------------------------------------------------------//
//
QString DF_Model::data_from_Comment(NodeBase* p_node) const
{
    return QString::fromStdString(p_node->m_comment);
}

//
//------------------------------------------------------------------------------------//
//
QString data_fromm_Offset(NodeBase* p_node)
{
    if (p_node->m_model_data.m_offset)
        return QString::fromStdString(*p_node->m_model_data.m_offset);

    const NodeBase* base = dynamic_cast<const NodeBase*>(p_node);

    if (base->m_node_type == NodeType::Dummy || base->m_field_path == "df.global")
    {
        p_node->m_model_data.m_offset = std::string("-");
        return QString::fromStdString(*p_node->m_model_data.m_offset);
    }

    if ((base->m_df_type == DF_Type::Bool) &&
        ((base->parent()->m_node_type == NodeType::Vector) ||
         (base->parent()->m_node_type == NodeType::Array)))
    {
        p_node->m_model_data.m_offset = "-";
        return QString::fromStdString(*p_node->m_model_data.m_offset);
    }

    NodeBase* base_compound = base->parent();
    do
    {
        if (base_compound)
        {
            if (base_compound->m_node_type == NodeType::Compound)
                // sucess
                break;
            if (base_compound->m_node_type == NodeType::Vector)
            {
                p_node->m_model_data.m_offset = "-";
                return QString::fromStdString(*p_node->m_model_data.m_offset);
            }
            if (base_compound->m_node_type == NodeType::Pointer)
            {
                if (base_compound->m_address == 0)
                {
                    p_node->m_model_data.m_offset = "-";
                    return QString::fromStdString(*p_node->m_model_data.m_offset);
                }
                break;
            }
            if (base_compound->m_node_type == NodeType::DFFlagArray)
                break;
            if (base_compound->m_node_type == NodeType::DFArray)
            {
                p_node->m_model_data.m_offset = "-";
                return QString::fromStdString(*p_node->m_model_data.m_offset);
            }
            if (base_compound->m_node_type == NodeType::Bitfield)
                break;
        }
        base_compound = base_compound->parent();

    } while (base_compound != nullptr);

    if (base_compound)
    {
        if (base_compound->m_node_type == NodeType::Pointer || base_compound->m_node_type == NodeType::DFFlagArray)
        {
            uint64_t*   pointer           = reinterpret_cast<uint64_t*>(base_compound->m_address);
            uint64_t    pointee           = *pointer;
            std::string offset_hex        = DF_Model::to_hex(base->m_address - pointee);
            p_node->m_model_data.m_offset = offset_hex;
            return QString::fromStdString(*p_node->m_model_data.m_offset);
        }
        std::string offset_hex = DF_Model::to_hex(base->m_address - base_compound->m_address);
        return QString::fromStdString(offset_hex);
    }

    p_node->m_model_data.m_offset = "-";
    return QString::fromStdString(*p_node->m_model_data.m_offset);
}

//
//------------------------------------------------------------------------------------//
//
QVariant get_decoration_role(const rdf::NodeBase* p_node)
{
    if ((p_node->m_rdf_type == rdf::RDF_Type::uint8_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::uint16_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::uint32_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::uint64_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::int8_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::int16_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::int32_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::int64_t) ||
        (p_node->m_rdf_type == rdf::RDF_Type::Long) ||
        (p_node->m_rdf_type == rdf::RDF_Type::S_float) ||
        (p_node->m_rdf_type == rdf::RDF_Type::Bool))
        return QPixmap(":/circle.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Stl_string) ||
        (p_node->m_rdf_type == rdf::RDF_Type::Static_string) ||
        (p_node->m_rdf_type == rdf::RDF_Type::Ptr_string))
        return QPixmap(":/t2_small.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Compound))
        return QPixmap(":/folder.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Union))
        return QPixmap(":/folder.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::AnonymousUnion))
        return QPixmap(":/folder.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Struct))
        return QPixmap(":/folder.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Class))
        return QPixmap(":/folder.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
        return QPixmap(":/cube.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::Vector)
        return QPixmap(":/group.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::Array)
        return QPixmap(":/group.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::DFArray)
        return QPixmap(":/group.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::DFLinkedList)
        return QPixmap(":/group.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::DFFlagArray)
        return QPixmap(":/group.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::Stl_Deque)
        return QPixmap(":/group.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Enum))
        return QPixmap(":/square.png");

    if ((p_node->m_rdf_type == rdf::RDF_Type::Bitfield))
        return QPixmap(":/stripes.png");

    if (p_node->m_rdf_type == rdf::RDF_Type::DFFlagArray)
        return QPixmap(":/stripes.png");

    if ((p_node->m_node_type == rdf::NodeType::Padding))
        return QPixmap(":/circle.png");

    if ((p_node->m_node_type == rdf::NodeType::AnonymousCompound))
        return QPixmap(":/folder.png");

    return QVariant();
}

//
//------------------------------------------------------------------------------------//
//
QVariant DF_Model::data(const QModelIndex& p_index, int p_role) const
{
    if (m_root_node == nullptr)
        return QVariant();

    NodeBase* node = nodeFromIndex(p_index);

    if ((p_index.column() == 0) && (p_role == Qt::DecorationRole))
    {
        return get_decoration_role(node);
    }

    if (p_role == Qt::ToolTipRole)
        return data_from_Comment(node);

    if (p_role == UrlRole)
        return QString::fromStdString(node->path());

    if (p_role != Qt::DisplayRole)
        return QVariant();

    if (!node)
        return QVariant();

    switch (p_index.column())
    {
        case 0:
            return data_from_Name(node); // Name
        case 1:
            return data_from_Structure(node); // Structure type
        case 2:
            return data_from_Type(node); // Type
        case 3:
            return data_from_Value(node); // Value
        case 4:
            return data_from_Address(node); // Address
        case 5:
            return data_from_Hyperlink(node); // refers-to, ref-target, index-refers-to
        case 6:
            return data_fromm_Offset(node); // Offset
        case 7:
            return data_from_Comment(node); // Comment
        default:
            break;
    }
    return QVariant();
}

//
//------------------------------------------------------------------------------------//
//
QVariant DF_Model::headerData(int p_section, Qt::Orientation p_orientation, int p_role) const
{
    if (p_orientation == Qt::Horizontal && p_role == Qt::DisplayRole)
    {
        switch (p_section)
        {
            case 0:
                return tr("Name");
            case 1:
                return tr("Structure");
            case 2:
                return tr("Type");
            case 3:
                return tr("Value");
            case 4:
                return tr("Address");
            case 5:
                return tr("Hyperlink");
            case 6:
                return tr("Offset");
            case 7:
                return tr("Comment");
            default:
                return QVariant();
        }
    }
    return QVariant();
}

//
//------------------------------------------------------------------------------------//
//
int DF_Model::columnCount(const QModelIndex& /*p_parent*/) const
{
    return 8;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::removeRows(int p_row, int p_count, const QModelIndex& /*p_parent*/)
{
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::insertColumns(int p_column, int p_count, const QModelIndex& /*p_parent*/)
{
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::removeColumns(int p_column, int p_count, const QModelIndex& /*p_parent*/)
{
    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::has_children_from_type(NodeBase* p_node) const
{
    if (!m_online)
    {
        auto node = dynamic_cast<Node*>(p_node);
        if (!node)
            return false;
        if (node->has_dummy_node())
            return false;
        return node->get_num_children() > 0;
    }

    if (p_node->is_root_node() || p_node->m_node_type == NodeType::Global)
        return true;

    if (p_node->m_node_type == NodeType::BitfieldEntry)
        return false;

    return p_node->m_has_children;
    switch (p_node->m_rdf_type)
    {
        case rdf::RDF_Type::None:
            return false;
        case rdf::RDF_Type::int64_t:
        case rdf::RDF_Type::uint64_t:
        case rdf::RDF_Type::int32_t:
        case rdf::RDF_Type::int16_t:
        case rdf::RDF_Type::uint32_t:
        case rdf::RDF_Type::uint16_t:
        case rdf::RDF_Type::uint8_t:
        case rdf::RDF_Type::int8_t:
            return false;
        case rdf::RDF_Type::Void:
            return false;
        case rdf::RDF_Type::Char:
            return false;
        case rdf::RDF_Type::Long:
            return false;
        case rdf::RDF_Type::Bool:
            return false;
        case rdf::RDF_Type::Stl_string:
            return false;
        case rdf::RDF_Type::Static_string:
            return false;
        case rdf::RDF_Type::Ptr_string:
            return false;
        case rdf::RDF_Type::Padding:
            return false;
        case rdf::RDF_Type::S_float:
            return false;
        case rdf::RDF_Type::D_float:
            return false;
        case rdf::RDF_Type::S_double:
            return false;
        case rdf::RDF_Type::Pointer:
            break;
        case rdf::RDF_Type::Array:
            return true;
        case rdf::RDF_Type::DFArray:
            return true;
        case rdf::RDF_Type::Vector:
            break;
        case rdf::RDF_Type::Bitfield:
            return true;
        case rdf::RDF_Type::Enum:
            return false;
        case rdf::RDF_Type::Compound:
            return true;
        case rdf::RDF_Type::Struct:
            return true;
        case rdf::RDF_Type::AnonymousCompound:
            return true;
        case rdf::RDF_Type::Class:
            return true;
        case rdf::RDF_Type::Union:
            return true;
        case rdf::RDF_Type::AnonymousUnion:
            return true;
        case rdf::RDF_Type::DFLinkedList:
            return true;
        case rdf::RDF_Type::DFFlagArray:
            return true;

        default:
            break;
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        NodePointer* l_pointer = dynamic_cast<NodePointer*>(p_node);
        if (l_pointer == nullptr)
            return false;
        if (m_outdated)
        {
            if (l_pointer->get_num_children() > 0)
                if (l_pointer->get_child(0)->m_node_type != NodeType::Dummy)
                    return true;
            return false;
        }
        if (l_pointer->m_df_type == rdf::DF_Type::Void)
            return false;
        if (l_pointer->m_address == 0)
            return false;
        auto address = reinterpret_cast<uint64_t*>(l_pointer->m_address);
        if (*address != 0)
            return true;
        return false;
    }

    if (p_node->m_rdf_type == rdf::RDF_Type::Vector)
    {
        NodeVector* l_node = dynamic_cast<NodeVector*>(p_node);
        return l_node->get_num_children() > 0;
    }

    return false;
}

//
//------------------------------------------------------------------------------------//
//
bool DF_Model::hasChildren(const QModelIndex& p_parent) const
{
    NodeBase* node_base = nodeFromIndex(p_parent);
    if (node_base == nullptr)
        return false;
    return has_children_from_type(node_base);
}

//
//------------------------------------------------------------------------------------//
//
void DF_Model::set_outdated()
{
    m_outdated = true;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::string remove_df_global(std::string& p_string)
{
    auto it = p_string.find("df.global.");
    if (it == std::string::npos)
        return p_string;
    return p_string.substr(10, 1024);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
QString DF_Model::data_from_Hyperlink(rdf::NodeBase* p_node) const
{
    if (p_node->m_model_data.m_hyperlink)
        return QString::fromStdString(*p_node->m_model_data.m_hyperlink);

    if (p_node->m_hyperlink_type.empty())
    {
        p_node->m_model_data.m_hyperlink = "";
        return QString::fromStdString(*p_node->m_model_data.m_hyperlink);
    }
    if (!p_node->m_hyperlink_value.empty())
    {
        p_node->m_model_data.m_hyperlink = remove_df_global(p_node->m_hyperlink_value);
        return QString::fromStdString(*p_node->m_model_data.m_hyperlink);
    }

    auto model = const_cast<DF_Model*>(this);
    auto data  = LispParser::parse_hyperlink(p_node, model);
    if (!data.empty())
        p_node->m_hyperlink_value = data;
    p_node->m_model_data.m_hyperlink = remove_df_global(p_node->m_hyperlink_value);
    return QString::fromStdString(*p_node->m_model_data.m_hyperlink);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
QModelIndex DF_Model::create_index(int p_row, int p_column, rdf::NodeBase* p_node)
{
    return createIndex(p_row, p_column, p_node);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::set_expression(const QString& p_expression)
{
    m_expression = p_expression;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::remove_dummy_child_node(const QModelIndex& p_model_index)
{
    //beginRemoveRows(p_model_index, 0, 0);
    auto node = dynamic_cast<Node*>(nodeFromIndex(p_model_index));
    node->remove_children();
    //endRemoveRows();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::remove_dummy_child_node(rdf::Node* p_node)
{
    if (p_node->has_dummy_node())
        p_node->remove_children();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::add_child_node_to_root(rdf::NodeBase* p_child_node)
{
    if (p_child_node->m_node_type == NodeType::Root)
        return;
    auto start = this->m_root_node->get_num_children();
    beginInsertRows(QModelIndex(), start, start);
    this->m_root_node->add_child_node(p_child_node);
    endInsertRows();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
QModelIndex DF_Model::getIndexByNode(NodeBase* p_node) const
{
    auto             node = p_node;
    std::vector<int> positions;
    QModelIndex      result;
    if (p_node)
    {
        do
        {
            auto name = node->m_field_name;
            if (name == m_root_node->m_field_name)
            {
                positions.push_back(0);
                break;
            }

            int pos = node->get_node_position();
            positions.push_back(pos);
            node = node->parent();
        } while (node != nullptr);

        for (int i = positions.size() - 1, root_counter = 0; i >= 0; i--, root_counter++)
        {
            if (root_counter == 1)
                continue;
            auto node_index = positions[i];
            auto node       = dynamic_cast<Node*>(nodeFromIndex(result));
            if (node == p_node)
                return result;
            result = index(node_index, 0, result);
        }
    }
    return result;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::add_child_node_to_parent_node(rdf::Node* p_parent_node, rdf::NodeBase* p_child_node)
{
    if (!m_in_vector)
    {
        auto num_children = p_parent_node->get_num_children();
        auto model_index  = getIndexByNode(p_parent_node);
        beginInsertRows(model_index,
                        num_children,
                        num_children);

        p_parent_node->add_child_node(p_child_node);
        endInsertRows();
    }
    else
    {
        p_parent_node->add_child_node(p_child_node);
    }
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::set_online(bool p_online)
{
    m_online = p_online;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_Model::reset_to_root()
{
    beginResetModel();
    endResetModel();
}

void DF_Model::add_children_from_node(Node* p_node)
{
    auto model_index  = getIndexByNode(p_node);
    auto num_children = p_node->get_num_children();
    beginInsertRows(model_index, 0, num_children - 1);
    endInsertRows();
}