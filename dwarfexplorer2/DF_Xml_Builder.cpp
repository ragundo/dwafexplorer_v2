/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "DF_Xml_Builder.h"
#include "xml_parser/Field.h"
#include <QTreeWidgetItem>
#include <strstream>

using namespace rdf;
using namespace std;

//
//-------------------------------------------------------------------------------------------------------------------//
//
void add_tabs(strstream& ss, int p_depth)
{
    for (int i = 0; i < p_depth; i++)
        ss << "\t";
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void add_attributes(Field* p_field, strstream& ss)
{
    auto atts_map = p_field->get_attributes_map();
    for (auto attr : atts_map)
    {
        ss << " " << attr.first << "='" << attr.second << "'";
    }
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::add_children(Field* p_field, strstream& ss, int p_depth)
{
    for (size_t i = 0; i < p_field->get_num_children(); i++)
    {
        auto child = p_field->get_pchild(i);
        do_work_rec(child, ss, p_depth + 1);
    }
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::fill_xml_entry(Field*             p_field,
                                    strstream&         ss,
                                    int                p_depth,
                                    const std::string& p_keyword,
                                    bool               p_is_container)
{
    add_tabs(ss, p_depth);
    ss << "<" << p_keyword;
    add_attributes(p_field, ss);
    if (p_field->get_num_children() == 0)
    {
        ss << "/>\n";
        return;
    }
    ss << ">\n";
    add_children(p_field, ss, p_depth);
    add_tabs(ss, p_depth);
    ss << "</" << p_keyword << ">\n";
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::do_work_rec(Field* p_field, strstream& ss, int p_depth)
{
    switch (p_field->get_type())
    {
        case Type::Bool:
            return process_boolean_field(p_field, ss, p_depth);
        case Type::Int8:
            return process_integer_field(p_field, ss, p_depth);
        case Type::Int16:
            return process_integer_field(p_field, ss, p_depth);
        case Type::Int32:
            return process_integer_field(p_field, ss, p_depth);
        case Type::Int64:
            return process_integer_field(p_field, ss, p_depth);
        case Type::Long:
            return process_integer_field(p_field, ss, p_depth);
        case Type::UInt8:
            return process_integer_field(p_field, ss, p_depth);
        case Type::UInt16:
            return process_integer_field(p_field, ss, p_depth);
        case Type::UInt32:
            return process_integer_field(p_field, ss, p_depth);
        case Type::UInt64:
            return process_integer_field(p_field, ss, p_depth);
        case Type::Float:
            return process_float_field(p_field, ss, p_depth);
        case Type::Double:
            return process_float_field(p_field, ss, p_depth);
        case Type::StlString:
            return process_stl_string_field(p_field, ss, p_depth);
        case Type::StlVector:
            return process_stl_vector_field(p_field, ss, p_depth);
        case Type::StlBitVector:
            return process_stl_bit_vector_field(p_field, ss, p_depth);
        case Type::StlDeque:
            return process_stl_deque_field(p_field, ss, p_depth);
        case Type::StlSet:
            return process_stl_set_field(p_field, ss, p_depth);
        case Type::StlFstream:
            return process_stl_fstream_field(p_field, ss, p_depth);
        case Type::DFLinkedList:
            return process_df_linked_list_field(p_field, ss, p_depth);
        case Type::DFArray:
            return process_df_array_field(p_field, ss, p_depth);
        case Type::DFFlagArray:
            return process_df_flag_array_field(p_field, ss, p_depth);
        case Type::StaticArray:
            return process_static_array_field(p_field, ss, p_depth);
        case Type::EnumField:
            return process_enum_field(p_field, ss, p_depth);
        case Type::EnumType:
            return process_enum_typedef(p_field, ss, p_depth);
        case Type::BitField:
            return process_bitfield_field(p_field, ss, p_depth);
        case Type::Compound:
            return process_compound_field(p_field, ss, p_depth);
        case Type::Pointer:
            return process_pointer_field(p_field, ss, p_depth);
        case Type::PtrString:
            return process_ptr_string_field(p_field, ss, p_depth);
        case Type::StaticString:
            return process_static_string_field(p_field, ss, p_depth);
        case Type::Padding:
            return process_padding_field(p_field, ss, p_depth);
        case Type::Struct:
            return process_struct_typedef(p_field, ss, p_depth);
        case Type::Class:
            return process_class_typedef(p_field, ss, p_depth);
        case Type::FlagBit:
            return process_flagbit(p_field, ss, p_depth);
        default:
            break;
    };
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
string DF_Xml_Builder::build_xml(rdf::Field* p_field)
{
    // TODO crash on enabler
    if (p_field->get_attribute("type-name") == "enabler")
        return "";
    strstream ss;
    do_work_rec(p_field, ss, 0);
    string result = ss.str();
    auto   it     = result.rfind(">\n");
    if (it != string::npos)
        return result.substr(0, it + 2);
    return "";
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_symbols_elements(Field* p_field, strstream& ss, int p_depth)
{
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_flagbit(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "flag-bit");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_enum_typedef(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "enum-type");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_bitfield_typedef(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "bitfield-type");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_compound_typedef(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "compound");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_struct_typedef(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "struct-type", true);
}


//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_class_typedef(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "class", true);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_dflinkedlist_typedef(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "df-linked-list-type");
}
//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_global_object_def(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_symbol_table(Field* p_field, strstream& ss, int p_depth) {}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_compound_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "compound", true);
}
void DF_Xml_Builder::process_compound_field_type(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "compound", true);
}

void DF_Xml_Builder::process_extra_include(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_custom_methods(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_virtual_methods(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_method(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_simple_field_type(Field* p_field, strstream& ss, int p_depth) {}

void DF_Xml_Builder::process_container_field_type(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_bit_container_field_type(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_boolean_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "bool");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_integer_field(Field* p_field, strstream& ss, int p_depth)
{
    string integer = "ERROR";
    switch (p_field->get_type())
    {
        case Type::Int8:
            integer = "int8_t";
            break;
        case Type::Int16:
            integer = "int16_t";
            break;
        case Type::Int32:
            integer = "int32_t";
            break;
        case Type::Int64:
            integer = "int64_t";
            break;
        case Type::Long:
            integer = "long";
            break;
        case Type::UInt8:
            integer = "uint8_t";
            break;
        case Type::UInt16:
            integer = "uint16_t";
            break;
        case Type::UInt32:
            integer = "uint32_t";
            break;
        case Type::UInt64:
            integer = "uint64_t";
            break;
        case Type::Float:
            integer = "s-float";
            break;
        case Type::Double:
            integer = "d-float";
            break;
        default:
            break;
    };
    fill_xml_entry(p_field, ss, p_depth, integer);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_float_field(Field* p_field, strstream& ss, int p_depth) {}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_stl_string_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "stl-string");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_stl_vector_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "stl-vector");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_stl_bit_vector_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "stl-bit-vector");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_stl_deque_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "stl-deque");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_stl_set_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "stl-set");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_stl_fstream_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "stl-fstream");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_df_linked_list_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "df-linked-list");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_df_array_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "static-array", true);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_df_flag_array_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "df-flagarray");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_static_array_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "static-array");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_enum_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "enum");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_bitfield_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "bitfield");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_pointer_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "pointer");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_ptr_string_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "ptr-string");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_static_string_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "static-string");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_padding_field(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "padding");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_enum_attr(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "enum-attr");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_enum_item(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "enum-item");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::proces_flag_bit(Field* p_field, strstream& ss, int p_depth)
{
    fill_xml_entry(p_field, ss, p_depth, "flag-bit");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DF_Xml_Builder::process_global_address(Field* p_field, strstream& ss, int p_depth) {}
void DF_Xml_Builder::process_vtable_address(Field* p_field, strstream& ss, int p_depth) {}
