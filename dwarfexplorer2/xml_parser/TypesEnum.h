/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef TYPESENUM_H
#define TYPESENUM_H
#include <string>

namespace rdf
{
enum class Type
{
    None,

    BinaryTimeStamp, ///< Not a type, part of a symbol table. Used only in symbols.xml
    BitField, ///< Inner Bitfield defined inside a class or struct
    BitFieldType, ///< Global Bitfield
    BitFieldItem, ///< Individual bitfield entry for inner or global bitfields
    Bool,

    Class, ///< C++ class
    CodeHelper,
    Comment,
    Compound, ///< Inner struct defined inside another class or struct
    CustomMethods, // Colection of cmethod
    CMethod,

    DFArray,
    DFFlagArray,
    DFLinkedList,
    DFLinkedListType,
    DFStaticFlagArray,
    Double,

    Enum, ///< Inner enum defined inside a compound
    EnumType, ///< Global enum definition
    EnumItem, ///< Individual enum entry for inner or global enums
    EnumAttr, ///< Attributes associated to a enum entry
    EnumField, ///< Instantiation of a inner or global enum
    ExtraInclude,

    FlagBit,
    Float,

    GlobalObject,
    GlobalAddress, ///< Not a type, part of a symbol table. Used only in symbols.xml

    Int16,
    Int32,
    Int64,
    Int8,

    Long,

    Padding,
    Pointer,
    PtrString,

    RetType,
    Root,

    SFloat,
    StaticArray,
    StaticString,
    StlBitVector,
    StlDeque,
    StlFstream,
    StlSet,
    StlString,
    StlVector,
    Struct,
    SymbolTable, ///< Not a type, used only in symbols.xml

    TypeRef,

    UInt16,
    UInt32,
    UInt64,
    UInt8,
    Union,

    VirtualMethods,
    VMethod,
    VMethodParameter, ///< Of a virtual method
    VMethodReturnType, ///< Of a virtual method
    VTableAddress ///< Not a type, part of a symbol table. Used only in symbols.xml
};

std::string Type_to_string(const Type p_type);
} // namespace rdf
#endif // TYPESENUM_H
