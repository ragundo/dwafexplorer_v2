/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef XML_PARSER_H
#define XML_PARSER_H

#include "../../include/make_unique.h"
#include "../tinyxml/tinyxml.h"
#include "Field.h"
#include <QString>
#include <map>
#include <memory>
#include <string>
#include <vector>

class XmlParser
{
  public:
    std::map<std::string, rdf::Field*> parse_df_xml_files();
    std::unique_ptr<rdf::Field>        parser_df_xml_string(const std::string& p_xml_string);

  private:
    std::unique_ptr<rdf::Field> parse_file(const std::string& p_filename);
    void                        parse_df_structures_file(QString& p_file_name);

    void parse_composite_df_field(TiXmlElement*                p_node,
                                  std::unique_ptr<rdf::Field>& p_parent,
                                  rdf::Type                    p_type);

    void parse_enum_or_enum_type(TiXmlElement*                p_node_enum_type,
                                 std::unique_ptr<rdf::Field>& p_parent);

    void parse_bitfield(TiXmlElement*                p_node_enum_type,
                        std::unique_ptr<rdf::Field>& p_parent);

    void parse_all_df_fields(TiXmlElement*                p_node_class_struct_child_field,
                             std::unique_ptr<rdf::Field>& p_parent);

    void process_df_field(TiXmlElement*                p_node,
                          std::unique_ptr<rdf::Field>& p_parent,
                          rdf::Type                    p_type);

  private:
    TiXmlDocument                            m_doc; // The XML df.***.xml to parse
    std::vector<std::unique_ptr<rdf::Field>> m_df_structures_table; // Data contained in each df.****.xml files
};

#endif