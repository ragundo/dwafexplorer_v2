/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef FIELD_H
#define FIELD_H

#include <list>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include "TypesEnum.h"

namespace rdf
{
/*!
    \brief The Field class

    Gathers all the data that is contained in each df.***.xml files and codegen.out.xml.

    A Field has the following fields:
    - a type, described in the enum Type.
    - a map of attributes with the data gathered from the xml field.
    - possibly some field children if the field type is a container one.
    - a Field parent, in order to traverse back the hierarchy until the root.

    Each xml file that exists in the /libray/xml subdirectory of dfhack
    has a <data-definition> root node and several global types that are
    children of this root node.

    For each global type, a new Field is created and made child of the root node.
    Also, inner types defined inside global types are created also as fields of
    the global type field, and hence, grandchildren of the root node also.

    Same applies to codegen.out.xml that it stored in the /library/include/df subdirectory.
    Here there's only one file to process, but the data is similar, but it has a lot of
    more data as the dfhack tools merge all the df.***.xml files into this one
*/
class Field
{
  public:
    Field(Type p_type);

    void                                       add_attribute(const std::string& p_key,
                                                             const std::string& p_value);
    Field*                                     add_child(std::unique_ptr<Field>& p_field);
    void                                       delete_children();
    size_t                                     get_num_children() const;
    std::unique_ptr<Field>                     get_child(size_t p_index);
    Field*                                     get_pchild(size_t p_index) const;
    std::string                                get_attribute(const std::string& p_key) const;
    void                                       set_attribute(const std::string& p_key, const std::string& p_value);
    rdf::Type                                  get_type() const;
    void                                       set_type(Type p_type);
    std::map<std::string, std::string>&        get_attributes_map();
    const std::vector<std::unique_ptr<Field>>& get_subitems();
    void                                       set_parent(Field* p_parent);
    Field*                                     get_parent();
    Field*                                     get_global_type();
    std::string                                get_name() const;

  protected:
    rdf::Type                           m_type; ///< Type of the field
    std::map<std::string, std::string>  m_attributes; ///< Map<key,value> that stores all the attributes that the xml node for a field contains
    std::vector<std::unique_ptr<Field>> m_subitems; ///< List of children fields it the field is a container (vector, array, etc)
    Field*                              m_parent; ///< Parent field link for navigating backwards
};

bool is_simple_type(std::string& p_type);

//---------------------------------------------------------------------------//

using upField = std::unique_ptr<Field>;
} // namespace rdf
#endif // FIELD_H
