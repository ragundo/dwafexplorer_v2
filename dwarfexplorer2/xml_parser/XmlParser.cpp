/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "XmlParser.h"
#include "make_unique.h"
#include <QDirIterator>
#include <QFile>
#include <QTemporaryFile>
#include <algorithm>
#include <fstream>
#include <iostream>
#include <memory>
#include <sstream>

using namespace std;
using namespace rdf;

//TODO parse from resource, not file
string dfhack_directory_path2        = "D:/gitprojects/dfhack_0.47.04-beta1/";
string dwarfexplorer_generated_path2 = dfhack_directory_path2 + "plugins/dwarfexplorer2/dwarfexplorer2/metadata_server/metadata/";
string dwarfexplorer_include_path2   = dfhack_directory_path2 + "plugins/dwarfexplorer2/include/";

//
//-----------------------------------------------------------------------------------------------------------//
//
bool test_if_is_enum_type(TiXmlElement* p_node)
{
    return !strcmp(p_node->Value(), "enum-type");
}

//
//-----------------------------------------------------------------------------------------------------------//
//
bool test_if_is_bitfield_type(TiXmlElement* p_node_bitfield_type)
{
    return !strcmp(p_node_bitfield_type->Value(), "bitfield-type");
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void fill_node_attributes(TiXmlElement*             p_node, //
                          std::map<string, string>& p_attr_map //
)
{
    // Process each attribute
    for (TiXmlAttribute* attr = p_node->FirstAttribute();
         attr;
         attr = attr->Next())
    {
        string attr_name  = attr->Name(); // get attribute name
        string attr_value = attr->Value(); // get attribute value
        p_attr_map.insert(make_pair(attr_name,
                                    attr_value));
    }
}

void parse_standard_df_field(TiXmlElement*           p_node,
                             std::unique_ptr<Field>& p_parent,
                             Type                    p_type)
{
    // Create the node as a child of the parent
    auto df_field = rdf::make_unique<Field>(p_type);
    df_field->set_parent(p_parent.get());

    // Fill this node attributes from the data present
    // in its associated xml node
    fill_node_attributes(p_node,
                         df_field->get_attributes_map());

    // This node is complete. Add it to its parent
    p_parent->add_child(df_field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
bool test_if_is_enum_attr(TiXmlElement* p_node)
{
    return !strcmp(p_node->Value(), "enum-attr");
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void XmlParser::parse_composite_df_field(TiXmlElement*           p_node,
                                         std::unique_ptr<Field>& p_parent,
                                         Type                    p_type)
{
    // Create the node with its type
    auto field = rdf::make_unique<Field>(p_type);
    field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node,
                         field->get_attributes_map());

    // Process the struct children
    if (p_node->FirstChildElement())
    {
        for (TiXmlElement* node_child = p_node->FirstChildElement();
             node_child;
             node_child = node_child->NextSiblingElement())
        {
            parse_all_df_fields(node_child,
                                field);
        }
    }

    // This node is complete. Add it to its parent unless is a GlobalObject
    if (field->get_type() == Type::GlobalObject)
        return;
    p_parent->add_child(field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void parse_enum_item(TiXmlElement*           p_node_enum_field_type,
                     std::unique_ptr<Field>& p_parent,
                     int&                    p_enum_item_value)
{
    // Create the node
    auto enum_item_field = rdf::make_unique<Field>(Type::EnumItem);

    enum_item_field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node_enum_field_type,
                         enum_item_field->get_attributes_map());

    // Check if it has a "value" attribute.
    string value = enum_item_field->get_attribute("value");
    if (!value.empty())
    {
        // It has a value, update the counter
        p_enum_item_value = stoi(value);
    }

    // Update the enum-field value
    enum_item_field->add_attribute("value",
                                   to_string(p_enum_item_value++));

    // This node is complete. Add it to its parent
    p_parent->add_child(enum_item_field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void parse_enum_attr(TiXmlElement*           p_node_attr_field_type,
                     std::unique_ptr<Field>& p_parent)
{
    // Create the node
    auto enum_attr_field = rdf::make_unique<Field>(Type::EnumAttr);
    enum_attr_field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node_attr_field_type,
                         enum_attr_field->get_attributes_map());

    // This node is complete. Add it to its parent
    p_parent->add_child(enum_attr_field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void parse_enum_item_or_enum_attr(TiXmlElement*           p_node_enum_field_type,
                                  std::unique_ptr<Field>& p_parent,
                                  int&                    p_enum_item_value)
{
    // This can be a enum-item or a enum-attr
    bool is_enum_attr = test_if_is_enum_attr(p_node_enum_field_type);

    if (is_enum_attr)
        parse_enum_attr(p_node_enum_field_type,
                        p_parent);
    else // enum-item
        parse_enum_item(p_node_enum_field_type,
                        p_parent,
                        p_enum_item_value);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void XmlParser::parse_enum_or_enum_type(TiXmlElement*           p_node_enum_type,
                                        std::unique_ptr<Field>& p_parent)
{
    // <enum-type> or <enum> definition or <enum> field

    // Check if it is a <enum-type>
    bool is_enum_type  = test_if_is_enum_type(p_node_enum_type);
    bool is_enum_field = false;

    // Check if we have fields to distinguish between anonymous definition
    // and a enum field of a type
    // <enum name='type' base-type='int32_t' type-name='talk_choice_type'/>
    if (p_node_enum_type->FirstChildElement() == nullptr)
        is_enum_field = true;

    std::unique_ptr<Field> enum_field;
    // Create the node of the appropiate type
    if (is_enum_type)
        enum_field = rdf::make_unique<Field>(Type::EnumType);
    else if (is_enum_field)
        enum_field = rdf::make_unique<Field>(Type::EnumField);
    else
        enum_field = rdf::make_unique<Field>(Type::Enum);

    enum_field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node_enum_type,
                         enum_field->get_attributes_map());

    // enum fields are only references, so work only need to be done
    // in <enum> or <enum-item>
    if (!is_enum_field)
    {
        // <enum> or <enum-type> definitions
        // Unless a children specifies a different value,
        // first enum children will have a ordinal value of zero
        int current_enum_value = 0;

        if (p_node_enum_type->FirstChildElement())
        {
            for (TiXmlElement* enum_field_node_child = p_node_enum_type->FirstChildElement();
                 enum_field_node_child;
                 enum_field_node_child = enum_field_node_child->NextSiblingElement())

                // Process children
                parse_enum_item_or_enum_attr(enum_field_node_child,
                                             enum_field,
                                             current_enum_value);
            // Add one attribute with the maximun enum item value
            enum_field->add_attribute("max-value", std::to_string(current_enum_value - 1));
        }
    }

    // This node is complete. Add it to its parent
    p_parent->add_child(enum_field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void parse_flag_bit(TiXmlElement*           p_node_flag_bit_field_type,
                    std::unique_ptr<Field>& p_parent,
                    int&                    p_flag_bit_value)
{
    // Create the node
    auto flag_bit_field = rdf::make_unique<Field>(Type::FlagBit);
    flag_bit_field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node_flag_bit_field_type,
                         flag_bit_field->get_attributes_map());

    // Set its value
    flag_bit_field->add_attribute("value",
                                  std::to_string(p_flag_bit_value++));

    // This node is complete. Add it to its parent
    p_parent->add_child(flag_bit_field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void XmlParser::parse_bitfield(TiXmlElement*           p_node_bitfield_type, //
                               std::unique_ptr<Field>& p_parent //
)
{
    bool is_bitfiled_type = test_if_is_bitfield_type(p_node_bitfield_type);

    // Create the node
    auto bitfield_field = (is_bitfiled_type ? rdf::make_unique<Field>(Type::BitFieldType) : rdf::make_unique<Field>(Type::BitField));

    // We need to know who is our parent
    bitfield_field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node_bitfield_type,
                         bitfield_field->get_attributes_map());

    // Process the bitfield children
    int current_bitfield_value = 0;

    if (p_node_bitfield_type->FirstChildElement())
    {
        for (TiXmlElement* enum_field_node_child = p_node_bitfield_type->FirstChildElement();
             enum_field_node_child;
             enum_field_node_child = enum_field_node_child->NextSiblingElement())
        {
            parse_flag_bit(enum_field_node_child,
                           bitfield_field,
                           current_bitfield_value);
        }
    }

    // This node is complete. Add it to its parent
    p_parent->add_child(bitfield_field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void XmlParser::parse_all_df_fields(TiXmlElement*           p_node_class_struct_child_field,
                                    std::unique_ptr<Field>& p_parent)
{
    // Get the tag (struct, enum, int, class, compunt, enum-item, etc)
    string class_struct_children_value = p_node_class_struct_child_field->Value();

    if (!strcmp(class_struct_children_value.c_str(), "struct-type"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::Struct);

    else if (!strcmp(class_struct_children_value.c_str(), "enum-type"))
        parse_enum_or_enum_type(p_node_class_struct_child_field,
                                p_parent);

    else if (!strcmp(class_struct_children_value.c_str(), "enum"))
        parse_enum_or_enum_type(p_node_class_struct_child_field,
                                p_parent);

    else if (!strcmp(class_struct_children_value.c_str(), "int32_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Int32);

    else if (!strcmp(class_struct_children_value.c_str(), "uint32_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::UInt32);

    else if (!strcmp(class_struct_children_value.c_str(), "int16_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Int16);

    else if (!strcmp(class_struct_children_value.c_str(), "uint16_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::UInt16);

    else if (!strcmp(class_struct_children_value.c_str(), "int8_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Int8);

    else if (!strcmp(class_struct_children_value.c_str(), "uint8_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::UInt8);

    else if (!strcmp(class_struct_children_value.c_str(), "int64_t"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Int64);

    else if (!strcmp(class_struct_children_value.c_str(), "long"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Long);

    else if (!strcmp(class_struct_children_value.c_str(), "bool"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Bool);

    else if (!strcmp(class_struct_children_value.c_str(), "class-type"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::Class);

    else if (!strcmp(class_struct_children_value.c_str(), "stl-vector"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::StlVector);

    else if (!strcmp(class_struct_children_value.c_str(), "stl-deque"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::StlDeque);

    else if (!strcmp(class_struct_children_value.c_str(), "stl-set"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::StlSet);

    else if (!strcmp(class_struct_children_value.c_str(), "stl-string"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::StlString);

    else if (!strcmp(class_struct_children_value.c_str(), "ptr-string"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::PtrString);

    else if (!strcmp(class_struct_children_value.c_str(), "stl-bit-vector"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::StlBitVector);
    else if (!strcmp(class_struct_children_value.c_str(), "static-string"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::StaticString);

    else if (!strcmp(class_struct_children_value.c_str(), "stl-fstream"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::StlFstream);

    else if (!strcmp(class_struct_children_value.c_str(), "compound"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::Compound);

    else if (!strcmp(class_struct_children_value.c_str(), "bitfield"))
        parse_bitfield(p_node_class_struct_child_field,
                       p_parent);

    else if (!strcmp(class_struct_children_value.c_str(), "bitfield-type"))
        parse_bitfield(p_node_class_struct_child_field,
                       p_parent);

    else if (!strcmp(class_struct_children_value.c_str(), "pointer"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::Pointer);

    else if (!strcmp(class_struct_children_value.c_str(), "static-array"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::StaticArray);

    else if (!strcmp(class_struct_children_value.c_str(), "df-linked-list"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::DFLinkedList);

    else if (!strcmp(class_struct_children_value.c_str(), "df-linked-list-type"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::DFLinkedListType);

    else if (!strcmp(class_struct_children_value.c_str(), "df-array"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::DFArray);

    else if (!strcmp(class_struct_children_value.c_str(), "df-static-flagarray"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::DFStaticFlagArray);

    else if (!strcmp(class_struct_children_value.c_str(), "df-flagarray"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::DFFlagArray);

    else if (!strcmp(class_struct_children_value.c_str(), "virtual-methods"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::VirtualMethods);

    else if (!strcmp(class_struct_children_value.c_str(), "vmethod"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::VMethod);

    else if (!strcmp(class_struct_children_value.c_str(), "global-object"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::GlobalObject);
    else if (!strcmp(class_struct_children_value.c_str(), "padding"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Padding);

    else if (!strcmp(class_struct_children_value.c_str(), "ret-type"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::RetType);
    else if (!strcmp(class_struct_children_value.c_str(), "comment"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::Comment);
    else if (!strcmp(class_struct_children_value.c_str(), "code-helper"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::CodeHelper);
    else if (!strcmp(class_struct_children_value.c_str(), "s-float"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::SFloat);
    else if (!strcmp(class_struct_children_value.c_str(), "extra-include"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::ExtraInclude);

    else if (!strcmp(class_struct_children_value.c_str(), "custom-methods"))
        parse_composite_df_field(p_node_class_struct_child_field,
                                 p_parent,
                                 Type::CustomMethods);
    else if (!strcmp(class_struct_children_value.c_str(), "cmethod"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::CMethod);

    else if (!strcmp(class_struct_children_value.c_str(), "df-linked-list-type"))
        parse_standard_df_field(p_node_class_struct_child_field,
                                p_parent,
                                Type::DFLinkedList);

    else if (!strcmp(class_struct_children_value.c_str(), "symbol-table"))
        return;
    else
        exit(0);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void XmlParser::process_df_field(TiXmlElement*           p_node,
                                 std::unique_ptr<Field>& p_parent,
                                 Type                    p_type)
{
    // Create the node with its type
    auto field = rdf::make_unique<Field>(p_type);
    field->set_parent(p_parent.get());

    // Fill this node attributes
    fill_node_attributes(p_node,
                         field->get_attributes_map());

    // Process the struct children
    if (p_node->FirstChildElement())
    {
        for (TiXmlElement* node_child = p_node->FirstChildElement();
             node_child;
             node_child = node_child->NextSiblingElement())
        {
            parse_all_df_fields(node_child,
                                field);
        }
    }

    // This node is complete. Add it to its parent
    p_parent->add_child(field);
}

//
//-----------------------------------------------------------------------------------------------------------//
//
std::unique_ptr<Field> XmlParser::parse_file(const string& p_filename)
{
    // Try to open the xml file
    if (!m_doc.LoadFile(p_filename.c_str()))
        return std::unique_ptr<Field>(nullptr);

    // Get the first child of the node <data-definition>
    TiXmlElement* data_definition_node = m_doc.FirstChildElement("data-definition");
    if (data_definition_node == nullptr)
        return std::unique_ptr<Field>(nullptr);

    // Found a data definition, create the node
    std::unique_ptr<Field> data_definition_field = rdf::make_unique<Field>(Type::Root);

    // Iterate over the XML children nodes and add to this data definition
    for (TiXmlElement* data_definition_child_node = data_definition_node->FirstChildElement();
         data_definition_child_node;
         data_definition_child_node = data_definition_child_node->NextSiblingElement())
    {
        parse_all_df_fields(data_definition_child_node, // XML child node
                            data_definition_field // New created parser node
        );
    }

    // Return the root field with all the df-structures as children
    return data_definition_field;
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void XmlParser::parse_df_structures_file(QString& p_file_name)
{
    // Parse the xml file using the parser
    std::unique_ptr<Field> field = parse_file(p_file_name.toStdString());

    // Standard df.***.xml file
    // Transfer ownership from the parser to the compiler
    m_df_structures_table.push_back(std::move(field));
}

//
//-----------------------------------------------------------------------------------------------------------//
//
void aux(Field* p_field, std::map<std::string, Field*>& p_types_table)
{
    if (p_field->get_type() == Type::Compound)
    {
        auto field = p_field;
        // Add the compound
        string compound_name = "T_" + field->get_attribute("name");
        field                = field->get_parent();

        while (field && field->get_type() != Type::Root)
        {
            switch (field->get_type())
            {
                case Type::Class:
                case Type::Struct:
                    compound_name = field->get_attribute("type-name") + "::" + compound_name;
                    break;
                case Type::Compound:
                    compound_name = "T_" + field->get_attribute("name") + "::" + compound_name;
                    break;
                default:
                    break;
            }
            field = field->get_parent();
        }
        p_types_table[compound_name] = p_field;
    }
    for (size_t i = 0; i < p_field->get_num_children(); i++)
    {
        Field* child = p_field->get_pchild(i);
        aux(child, p_types_table);
    }
}

//
//-----------------------------------------------------------------------------------------------------------//
//
std::map<std::string, Field*> XmlParser::parse_df_xml_files()
{
    std::map<std::string, Field*> types_table;
    QDirIterator                  it(":/xml", QDirIterator::Subdirectories);
    QString                       temp_name;
    while (it.hasNext())
    {
        QFile df_xml_file(it.next());
        df_xml_file.open(QIODevice::ReadOnly);
        {
            QTemporaryFile temp;
            if (temp.open())
            {
                temp_name = temp.fileName();
                temp.close();
            }
        }
        auto  data = df_xml_file.readAll();
        QFile dest(temp_name);
        dest.open(QIODevice::WriteOnly | QIODevice::Text);
        dest.write(data);
        dest.flush();
        dest.close();
        parse_df_structures_file(temp_name);
        dest.remove();
    }
    // process each global type
    for (auto& root_node : m_df_structures_table)
    {
        for (size_t i = 0; i < root_node->get_num_children(); i++)
        {
            Field* child     = root_node->get_pchild(i);
            auto   type_name = child->get_attribute("type-name");
            if (!type_name.empty())
                if (child->get_type() != Type::GlobalObject)
                    types_table[type_name] = child;
        }
    }

    // process each compound type
    for (auto& root_node : m_df_structures_table)
    {
        for (size_t i = 0; i < root_node->get_num_children(); i++)
        {
            Field* child = root_node->get_pchild(i);
            aux(child, types_table);
        }
    }

    return types_table;
}

std::unique_ptr<rdf::Field> XmlParser::parser_df_xml_string(const std::string& p_xml_string)
{
    TiXmlDocument doc;

    char* filedata = R"(
    <data-definition>
    <struct-type type-name='history_hit_item'>
        <int32_t name='item' ref-target='item'/>
        <enum base-type='int16_t' name='item_type' type-name='item_type'/>
        <int16_t name='item_subtype' refers-to='(item-subtype-target $$._parent.item_type $)'/>
        <int16_t name='mattype' ref-target='material' aux-value='$$.matindex'/>
        <int32_t name='matindex'/>
        -- If shot by a ranged weapon:
        <int32_t name='shooter_item' ref-target='item'/>
        <enum base-type='int16_t' name='shooter_item_type' type-name='item_type'/>
        <int16_t name='shooter_item_subtype' refers-to='(item-subtype-target $$._parent.shooter_item_type $)'/>
        <int16_t name='shooter_mattype' ref-target='material' aux-value='$$.shooter_matindex'/>
        <int32_t name='shooter_matindex'/>
    </struct-type>
    </data-definition>)";

    doc.Parse((const char*)filedata, 0, TIXML_ENCODING_UTF8);

    // Get the first child of the node <data-definition>
    TiXmlElement* data_definition_node = doc.FirstChildElement("data-definition");
    if (data_definition_node == nullptr)
        return nullptr;

    // Found a data definition, create the node
    std::unique_ptr<Field> data_definition_field = rdf::make_unique<Field>(Type::Root);

    // Iterate over the XML children nodes and add to this data definition
    for (TiXmlElement* data_definition_child_node = data_definition_node->FirstChildElement();
         data_definition_child_node;
         data_definition_child_node = data_definition_child_node->NextSiblingElement())
    {
        parse_all_df_fields(data_definition_child_node, // XML child node
                            data_definition_field // New created parser node
        );
    }

    // Return the root field with all the df-structures as children
    return data_definition_field;
}