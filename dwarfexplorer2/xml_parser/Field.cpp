/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Field.h"

using namespace std;
using namespace rdf;

namespace rdf
{
//-----------------------------------------------------------------------------------------------------------//
/// \brief Field::delete_children
///
/// Deletes all the child fields that this field has in its #m_subitems vector.
//-----------------------------------------------------------------------------------------------------------//
void Field::delete_children()
{
    m_subitems.clear();
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Field::get_num_children
//!
//! Returns the number of child fields that this field have.
//! \return The size of the #m_subitems vector.
//-----------------------------------------------------------------------------------------------------------//
size_t Field::get_num_children() const
{
    return m_subitems.size();
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Field::get_child
//!
//! Returns a unique pointer to a child field
//!
//! The owning of the child is std::moved, so the vector entry will have a nullptr after this call
//! \param p_index The #m_subitems vector index to the child field that we want
//! \return the child field or nullptr if the index is incorrect
//-----------------------------------------------------------------------------------------------------------//
unique_ptr<Field> Field::get_child(size_t p_index)
{
    if (p_index >= m_subitems.size())
        return unique_ptr<Field>(nullptr);
    return move(m_subitems[p_index]);
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Returns a raw pointer to a child field
//!
//! \param p_index The #m_subitems vector index to the child field that we want
//! \return The child field or nullptr if the index is incorrect
//-----------------------------------------------------------------------------------------------------------//
Field* Field::get_pchild(size_t p_index) const
{
    if (p_index >= m_subitems.size())
        return nullptr;
    return m_subitems[p_index].get();
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Returns the attribute value of the requested key attribute.
//!
//! It searches in the #m_attributes map for the key and returns the value for the key.
//! \return std::string with the attribute value or a empty string if the key is not found
//-----------------------------------------------------------------------------------------------------------//
string Field::get_attribute(const string& p_key) const
{
    auto result = m_attributes.find(p_key);
    if (result == m_attributes.end())
        return "";
    return result->second;
}

void Field::set_attribute(const string& p_key, const string& p_value)
{
    auto result = m_attributes.find(p_key);
    if (result == m_attributes.end())
        return;
    result->second = p_value;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Returns the type of this field
//!
//! \return Type with the type of the field.
//-----------------------------------------------------------------------------------------------------------//
Type Field::get_type() const
{
    return m_type;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Sets the type of this field
//! \param p_type The Type enum class value that this field will have.
//!
//-----------------------------------------------------------------------------------------------------------//
void Field::set_type(Type p_type)
{
    m_type = p_type;
}

std::map<string, string>& Field::get_attributes_map()
{
    return m_attributes;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Sets the parent field of this field
//!
//! It adds the field to the parent's #m_subitems vector of childen fields.
//! \param p_parent The parent Field that will have this field as one of their children.
//-----------------------------------------------------------------------------------------------------------//
void Field::set_parent(Field* p_parent)
{
    m_parent = p_parent;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Returns the parent field of this one
//!
//! \return a raw pointer to the parent field or nullptr if the field type is Root
//-----------------------------------------------------------------------------------------------------------//
Field* Field::get_parent()
{
    return m_parent;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Field::get_global_type
//!
//! Returns the field node that contains all the children fields that the xml file has inside
//! \return the root field of this df.***.xml file
//-----------------------------------------------------------------------------------------------------------//
Field* Field::get_global_type()
{
    if (m_parent->get_type() == Type::Root)
        return this;

    Field* f = this;
    while (f->m_parent->get_type() != Type::Root)
        f = f->m_parent;
    return f;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Field::get_name
//!
//! Returns the field name or its anon_*** if it has not have any
//! \return the field name
//-----------------------------------------------------------------------------------------------------------//
string Field::get_name() const
{
    string result = this->get_attribute("name");
    if (result.empty())
        result = this->get_attribute("ld:anon-name");

    return result;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Adds one attribute pair <key, value> from the a XML node
//!
//! The pair is added to the #m_attributes map that stores all the attributes of a field
//-----------------------------------------------------------------------------------------------------------//
void Field::add_attribute(const string& p_key, ///< XML node attribute type (meta, type-name, etc)
                          const string& p_value ///< XML node attribute value (="bytes", etc)
)
{
    m_attributes.insert(std::make_pair(p_key,
                                       p_value));
}

//-----------------------------------------------------------------------------------------------------------//
//! Constructor
//-----------------------------------------------------------------------------------------------------------//
Field::Field(Type p_type /**< The Type that this Field will have*/)
{
    m_type = p_type;
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief Adds a child field to this field table of children.
//!
//! The child field comes owned by a unique_ptr, so it's transfered to the #m_subitems vector.
//! p_field goes null after this operation.
//! \return a raw pointer to the new added child field
//-----------------------------------------------------------------------------------------------------------//
Field* Field::add_child(unique_ptr<Field>& p_field /**< The children Field to be added */)
{
    m_subitems.push_back(std::move(p_field));
    return m_subitems[m_subitems.size() - 1].get();
}

//-----------------------------------------------------------------------------------------------------------//
//! \brief return all the field children of this field.
//! \return a const reference to the #m_subitems vector with all the field children of this field.
//-----------------------------------------------------------------------------------------------------------//
const std::vector<unique_ptr<Field>>& Field::get_subitems()
{
    return m_subitems;
}

bool is_simple_type(string& p_type)
{
    if (!p_type.compare("int32_t"))
        return true;
    if (!p_type.compare("uint32_t"))
        return true;
    if (!p_type.compare("int16_t"))
        return true;
    if (!p_type.compare("uint16_t"))
        return true;
    if (!p_type.compare("int8_t"))
        return true;
    if (!p_type.compare("uint8_t"))
        return true;
    if (!p_type.compare("int64_t"))
        return true;
    if (!p_type.compare("uint64_t"))
        return true;
    if (!p_type.compare("bool"))
        return true;
    if (!p_type.compare("void"))
        return true;
    if (!p_type.compare("void*"))
        return true;
    if (!p_type.compare("stl-string"))
        return true;
    return false;
}

std::string Type_to_string(const Type p_type)
{
    switch (p_type)
    {
        case Type::None:
            return "None";
        case Type::BinaryTimeStamp:
            return "TODO";
        case Type::BitField:
            return "bitfield";
        case Type::BitFieldType:
            return "bitfield-type";
        case Type::BitFieldItem:
            return "";
        case Type::Bool:
            return "bool";
        case Type::Class:
            return "class";
        case Type::CodeHelper:
            return "code-helper";
        case Type::Comment:
            return "comment";
        case Type::Compound:
            return "compound";
        case Type::CustomMethods:
            return "custom-methods";
        case Type::CMethod:
            return "TODO";
        case Type::DFArray:
            return "df-array";
        case Type::DFFlagArray:
            return "df-flagarray";
        case Type::DFLinkedList:
            return "df-linked-list";
        case Type::DFLinkedListType:
            return "df-linked-list-type";
        case Type::DFStaticFlagArray:
            return "df-flagarray";
        case Type::Double:
            return "double";
        case Type::Enum:
            return "enum";
        case Type::EnumType:
            return "enum-type";
        case Type::EnumItem:
            return "enum-item";
        case Type::EnumAttr:
            return "enum-attr";
        case Type::EnumField:
            return "enum";
        case Type::ExtraInclude:
            return "extra-include";
        case Type::FlagBit:
            return "flag-bit";
        case Type::Float:
            return "float";
        case Type::GlobalObject:
            return "";
        case Type::GlobalAddress:
            return "";
        case Type::Int16:
            return "int16";
        case Type::Int32:
            return "int32";
        case Type::Int64:
            return "int64";
        case Type::Int8:
            return "int8";
        case Type::Long:
            return "long";
        case Type::Padding:
            return "TODO";
        case Type::Pointer:
            return "pointer";
        case Type::PtrString:
            return "ptr-string";
        case Type::RetType:
            return "TODO";
        case Type::Root:
            return "Root";
        case Type::SFloat:
            return "s-float";
        case Type::StaticArray:
            return "static-array";
        case Type::StaticString:
            return "static-string";
        case Type::StlBitVector:
            return "stl-bit-vector";
        case Type::StlDeque:
            return "stl-deque";
        case Type::StlFstream:
            return "stl-fstream";
        case Type::StlSet:
            return "stl-set";
        case Type::StlString:
            return "stl-string";
        case Type::StlVector:
            return "stl-vector";
        case Type::Struct:
            return "struct-type";
        case Type::SymbolTable:
            return "TODO";
        case Type::TypeRef:
            return "TODO";
        case Type::UInt16:
            return "uint16_t";
        case Type::UInt32:
            return "uint32_t";
        case Type::UInt64:
            return "uint64_t";
        case Type::UInt8:
            return "uint8_t";
        case Type::Union:
            return "TODO";
        case Type::VirtualMethods:
            return "virtual-methods";
        case Type::VMethod:
            return "vmethod";
        case Type::VMethodParameter:
            return "TODO";
        case Type::VMethodReturnType:
            return "TODO";
        case Type::VTableAddress:
            return "TODO";
    }
    return "ERROR";
}
} // namespace rdf