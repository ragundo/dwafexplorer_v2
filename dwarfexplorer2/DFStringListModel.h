/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DFSTRINGLISTMODEL_H
#define DFSTRINGLISTMODEL_H

#include "Parser.h"
#include "metadata_server/MetadataServer.h"
#include <QStringListModel>

class DFStringListModel : public QStringListModel
{
  public:
    DFStringListModel();

    bool append(const QString& p_string);

    DFStringListModel& operator<<(const QString& p_string);

    void process_name(const QString& p_expression);

  private:
    Parser::ErrorCode m_error_code;
};

#endif //DFSTRINGLISTMODEL_H