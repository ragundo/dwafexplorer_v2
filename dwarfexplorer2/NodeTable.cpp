#include "NodeTable.h"
#include "df_model/df_node.h"

using namespace std;
using namespace rdf;

NodeTable::NodeTable()
{
}

//
//---------------------------------------------------------------------------------------
//
tl::optional<NodeBase*> NodeTable::find_node(string& p_node_name)
{
    if (p_node_name.empty())
        return {};

    auto nodebase = m_current_node_table.Lookup(&p_node_name[0]);
    if (nodebase)
        return nodebase;
    return {};
}

//
//---------------------------------------------------------------------------------------
//
void NodeTable::add_node(NodeBase* p_node)
{
    if (p_node->m_node_type == NodeType::Dummy)
        return;
    auto key = p_node->m_field_path +
               (p_node->m_field_name[0] != '[' ? "." : "") +
               p_node->m_field_name;
    auto nodebase = m_current_node_table.Lookup(&key[0]);
    if (nodebase)
        return;
    m_current_node_table.Insert(&key[0], p_node);
}

//
//---------------------------------------------------------------------------------------
//
void NodeTable::add_node(char* p_key, NodeBase* p_node)
{
    if (p_node->m_node_type == NodeType::Dummy)
        return;
    auto nodebase = m_current_node_table.Lookup(p_key);
    if (nodebase)
        return;
    m_current_node_table.Insert(p_key, p_node);
}

//
//---------------------------------------------------------------------------------------
//
bool NodeTable::is_empty() const
{
    return m_current_node_table.empty();
}

//
//---------------------------------------------------------------------------------------
//
void NodeTable::swap()
{
    /*
    m_prev_node_table    = m_current_node_table;
    m_current_node_table = new std::unordered_map<std::string, rdf::NodeBase*>();
    for (auto& it : *m_current_node_table)
        delete it.second;
    delete m_prev_node_table;
    m_prev_node_table = nullptr;
    */
}

//
//---------------------------------------------------------------------------------------
//
NodeTable::~NodeTable()
{
    // TODO
}