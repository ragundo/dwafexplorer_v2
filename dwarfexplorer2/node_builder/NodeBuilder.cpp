/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "NodeBuilder.h"
#include "MetadataServer.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//------------------------------------------------------------------------------------//
//
uint64_t NodeBuilder::update_address(uint64_t p_address, NodeBase* p_node)
{
    switch (p_node->m_df_type)
    {
        case rdf::DF_Type::uint8_t:
            return p_address + sizeof(uint8_t);
        case rdf::DF_Type::uint16_t:
            return p_address + sizeof(uint16_t);
        case rdf::DF_Type::uint32_t:
            return p_address + sizeof(uint32_t);
        case rdf::DF_Type::uint64_t:
            return p_address + sizeof(uint64_t);
        case rdf::DF_Type::int8_t:
            return p_address + sizeof(int8_t);
        case rdf::DF_Type::int16_t:
            return p_address + sizeof(int16_t);
        case rdf::DF_Type::int32_t:
            return p_address + sizeof(int32_t);
        case rdf::DF_Type::int64_t:
            return p_address + sizeof(int64_t);
        case rdf::DF_Type::S_float:
            return p_address + sizeof(float);
        case rdf::DF_Type::S_double:
            return p_address + sizeof(double);
        case rdf::DF_Type::Bool:
            return p_address + sizeof(bool);
        case rdf::DF_Type::Char:
            return p_address + sizeof(char);
        case rdf::DF_Type::Stl_string:
            return p_address + sizeof(std::string);
        case rdf::DF_Type::Void:
            return p_address + sizeof(void*);
        default:
            break;
    }
    // DF structure, enum or bitfield
    if (p_node->m_node_type == NodeType::Enum)
    {
        auto node_enum  = dynamic_cast<NodeEnum*>(p_node);
        auto size_maybe = MetadataServer.size_of_DF_Type(node_enum->m_base_type);
        return p_address + *size_maybe; // TODO
    }
    if (p_node->m_node_type == NodeType::Bitfield)
    {
        auto node_bitfield = dynamic_cast<NodeBitfield*>(p_node);
        auto size_maybe    = MetadataServer.size_of_DF_Type(node_bitfield->m_base_type);
        return p_address + *size_maybe; // TODO
    }
    if (p_node->m_node_type == NodeType::Vector)
    {
        auto node_vector = dynamic_cast<NodeVector*>(p_node);
        if (node_vector->m_enum_basetype != DF_Type::None)
        {
            auto size_maybe = MetadataServer.size_of_DF_Type(node_vector->m_enum_basetype);
            return p_address + *size_maybe; // TODO
        }
    }

    // DF Structure
    auto size_maybe = MetadataServer.size_of_DF_Type(p_node->m_df_type);
    return p_address + *size_maybe; // TODO
}

//
//---------------------------------------------------------------------------------------
//
DF_Type NodeBuilder::process_base_type(const string& p_addornements_string)
{
    //[5{int**_t} contains the base-type , remove it from the metadata
    DF_Type base_type_df_type = DF_Type::None;

    auto index = p_addornements_string.find("{int");
    if (index != std::string::npos)
    {
        auto index2 = ++index;
        while (p_addornements_string[index2] != '_')
        {
            index2++;
        }
        if (p_addornements_string[index2 + 1] == 't')
        {
            auto base_type_st = p_addornements_string.substr(index, index2 - index + 2);
            base_type_df_type = MetadataServer.DF_Type_from_string(base_type_st);
        }
    }
    index = p_addornements_string.find("/");
    if (index != std::string::npos)
    {
        auto index2 = ++index;
        while (p_addornements_string[index2] != '/')
            index2++;
        auto base_type_st = p_addornements_string.substr(index, index2 - index);
        auto base_type_df = MetadataServer.DF_Type_from_string(base_type_st);
        if (base_type_df != DF_Type::None)
        {
            auto data_maybe = MetadataServer.get_enum_metadata(base_type_df);
            if (data_maybe)
                base_type_df_type = std::get<0>(*data_maybe);
        }
    }
    return base_type_df_type;
}

//
//---------------------------------------------------------------------------------------
//
std::string remove_addornements(std::string& p_comment)
{
    auto pos = p_comment.find("=@");
    if (pos + 2 == p_comment.size())
        return "";
    return p_comment.substr(pos + 2, 1024);
}

//
//---------------------------------------------------------------------------------------
//
NodeBase* NodeBuilder::build_super_node_if_any(Node* p_parent_node)
{
    std::string addornements = "";

    auto base_classes = MetadataServer.get_base_classes(p_parent_node->m_df_type);
    if (!base_classes.empty())
    {
        // add base
        auto base_df_type = base_classes[0];
        // get type of base
        auto base_rdf_type = MetadataServer.df_2_rdf(base_df_type);

        auto new_node          = NodeBuilder::create_node(p_parent_node->m_address,
                                                 base_rdf_type,
                                                 base_df_type,
                                                 addornements);
        new_node->m_field_name = "super";
        new_node->m_field_path = p_parent_node->m_field_path +
                                 (p_parent_node->m_field_name[0] != '[' ? "." : "") +
                                 p_parent_node->m_field_name;
        std::string comment      = "";
        std::string addornements = "";
        // Add to the table of nodes
        new_node->m_node_table = p_parent_node->m_node_table;
        new_node->m_node_table->add_node(new_node);

        // Address
        new_node->m_address = p_parent_node->m_address;

        NodeBuilder::add_hyperlink(new_node);
        return new_node;
    }
    return nullptr;
}

/*
    if (p_model)
    {
        // Connect parent and children
        p_model->add_child_node_to_parent_node(p_parent, new_node);

        if (new_node->m_has_children)
        {
            auto node = dynamic_cast<Node*>(new_node);
            node->add_dummy_node();
        }
    }
*/

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::add_child_to_node(const metadata_standard& p_metadata,
                                    Node*                    p_parent,
                                    std::vector<NodeBase*>&  p_vec_children)
{
    rdf::DF_Type  parent_df_type     = p_parent->m_df_type;
    rdf::RDF_Type parent_rdf_type    = p_parent->m_rdf_type;
    uint64_t      parent_address     = p_parent->m_address;
    auto          field_name         = std::get<0>(p_metadata);
    auto          df_type            = std::get<1>(p_metadata);
    auto          rdf_type           = std::get<2>(p_metadata);
    std::string   comment            = std::get<3>(p_metadata);
    auto          addornements_maybe = MetadataServer.get_addornements_metadata(parent_df_type, field_name);
    string        addornements       = "";
    if (addornements_maybe)
        addornements = *addornements_maybe;

    // Address
    uint64_t child_address;

    if (parent_rdf_type == RDF_Type::AnonymousUnion)
        child_address = p_parent->m_address;

    // Check for Anonymous union or struct
    else if ((rdf_type == RDF_Type::AnonymousCompound) || (rdf_type == RDF_Type::AnonymousUnion))
    {
        if (p_vec_children.size() > 0)
        {
            auto last_child = p_vec_children[p_vec_children.size() - 1];
            child_address   = last_child->m_address + last_child->get_node_size();
        }
        else
        {
            child_address = parent_address;
        }
    }
    else
        child_address = parent_address + *rdf::OffsetsCache::get_offset(parent_df_type,
                                                                        field_name);

    auto new_node       = NodeBuilder::create_node(child_address,
                                             rdf_type,
                                             df_type,
                                             addornements);
    new_node->m_comment = comment;
    // Add it to the table of nodes
    new_node->m_field_name = std::get<0>(p_metadata);
    new_node->m_node_table = p_parent->m_node_table;
    new_node->m_field_path = p_parent->m_field_path +
                             (p_parent->m_field_name[0] != '[' ? "." : "") +
                             p_parent->m_field_name;
    new_node->m_node_table->add_node(new_node);

    if (new_node->m_has_children)
    {
        auto node = dynamic_cast<Node*>(new_node);
        node->add_dummy_node();
    }

    NodeBuilder::add_hyperlink(new_node);
    p_vec_children.push_back(new_node);
}

//
//---------------------------------------------------------------------------------------
//
std::pair<std::string, std::string> NodeBuilder::get_index_enum(const std::string& p_addornements)
{
    auto it = p_addornements.find('/');
    if (it == std::string::npos)
        return std::pair<std::string, std::string>("", "");
    auto it2 = it;
    while (p_addornements[++it2] != '/')
    {
    }
    auto        index_enum = p_addornements.substr(it + 1, it2 - it - 1);
    std::string pre        = p_addornements.substr(0, it);
    std::string post       = (it2 + 1 >= p_addornements.size() ? "" : p_addornements.substr(it2 + 1, 512));
    return std::pair<std::string, std::string>(index_enum, pre + post);
}

//
//---------------------------------------------------------------------------------------
//
NodeBase* NodeBuilder::create_node(uint64_t           p_address,
                                   rdf::RDF_Type      p_rdf_type,
                                   rdf::DF_Type       p_df_type,
                                   const std::string& p_addornements)
{
    NodeBase* new_node_base;
    Node*     new_node;
    switch (p_rdf_type)
    {
        case rdf::RDF_Type::int64_t:
        case rdf::RDF_Type::uint64_t:
        case rdf::RDF_Type::int32_t:
        case rdf::RDF_Type::int16_t:
        case rdf::RDF_Type::uint32_t:
        case rdf::RDF_Type::uint16_t:
        case rdf::RDF_Type::uint8_t:
        case rdf::RDF_Type::int8_t:
        case rdf::RDF_Type::Char:
        case rdf::RDF_Type::Long:
        case rdf::RDF_Type::Bool:
        case rdf::RDF_Type::S_float:
        case rdf::RDF_Type::D_float:
        case rdf::RDF_Type::S_double:
            new_node_base             = new rdf::NodeSimple;
            new_node_base->m_address  = p_address;
            new_node_base->m_rdf_type = p_rdf_type;
            new_node_base->m_df_type  = p_df_type;
            return new_node_base;
            break;
        case rdf::RDF_Type::Vector:
            new_node             = new rdf::NodeVector;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_vector(new_node, p_addornements);
            return new_node;
            break;
        case rdf::RDF_Type::Void:
            new_node_base            = new rdf::NodeVoid;
            new_node_base->m_address = p_address;
            return new_node_base;
            break;
        case rdf::RDF_Type::Ptr_string:
        case rdf::RDF_Type::Ptr_char:
        case rdf::RDF_Type::Stl_string:
        case rdf::RDF_Type::Static_string:
            new_node_base             = new rdf::NodeStaticString;
            new_node_base->m_address  = p_address;
            new_node_base->m_df_type  = p_df_type;
            new_node_base->m_rdf_type = p_rdf_type;
            return new_node_base;
            break;
        case rdf::RDF_Type::Padding:
            new_node_base            = new rdf::NodePadding;
            new_node_base->m_address = p_address;
            process_node_padding(new_node_base, p_addornements);
            return new_node_base;
            break;
        case rdf::RDF_Type::Class:
        case rdf::RDF_Type::Struct:
        case rdf::RDF_Type::Compound:
            new_node             = new rdf::NodeCompound;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_compound(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::Stl_Deque:
            new_node             = new rdf::NodeDeque;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_compound(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::Union:
            new_node             = new rdf::NodeUnion;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_union(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::AnonymousCompound:
            new_node              = new rdf::NodeAnonymous;
            new_node->m_address   = p_address;
            new_node->m_df_type   = p_df_type;
            new_node->m_rdf_type  = p_rdf_type;
            new_node->m_node_type = NodeType::AnonymousCompound;
            process_node_compound(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::AnonymousUnion:
            new_node              = new rdf::NodeAnonymous;
            new_node->m_address   = p_address;
            new_node->m_df_type   = p_df_type;
            new_node->m_rdf_type  = p_rdf_type;
            new_node->m_node_type = NodeType::AnonymousUnion;
            process_node_compound(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::Bitfield:
            new_node             = new rdf::NodeBitfield;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_bitfield(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::Enum:
            new_node             = new rdf::NodeEnum;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_enum(new_node, p_addornements);
            return new_node;
            break;
        case rdf::RDF_Type::Array:
            new_node             = new rdf::NodeArray;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_array(new_node, p_addornements);
            return new_node;
            break;
        case rdf::RDF_Type::Pointer:
            new_node             = new rdf::NodePointer;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_pointer(new_node, p_addornements);
            return new_node;
            break;
        case rdf::RDF_Type::DFFlagArray:
            new_node             = new rdf::NodeDFFlagArray;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_df_flag_array(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::DFArray:
            new_node             = new rdf::NodeDFArray;
            new_node->m_address  = p_address;
            new_node->m_df_type  = p_df_type;
            new_node->m_rdf_type = p_rdf_type;
            process_node_df_array(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::DFLinkedList:
            new_node                 = new rdf::NodeDFLinkedList;
            new_node->m_address      = p_address;
            new_node->m_df_type      = p_df_type;
            new_node->m_rdf_type     = p_rdf_type;
            new_node->m_has_children = true;
            process_node_df_linked_list(new_node);
            return new_node;
            break;
        case rdf::RDF_Type::Stl_fstream:
            break;
        default:
            break;
    }
    return nullptr;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node(rdf::NodeBase* p_node)
{
    vector<NodeBase*> vec_children;
    auto              node = dynamic_cast<Node*>(p_node);
    switch (p_node->m_rdf_type)
    {
        case rdf::RDF_Type::int64_t:
        case rdf::RDF_Type::uint64_t:
        case rdf::RDF_Type::int32_t:
        case rdf::RDF_Type::int16_t:
        case rdf::RDF_Type::uint32_t:
        case rdf::RDF_Type::uint16_t:
        case rdf::RDF_Type::uint8_t:
        case rdf::RDF_Type::int8_t:
        case rdf::RDF_Type::Char:
        case rdf::RDF_Type::Long:
        case rdf::RDF_Type::Bool:
        case rdf::RDF_Type::S_float:
        case rdf::RDF_Type::D_float:
        case rdf::RDF_Type::S_double:
            break;
        case rdf::RDF_Type::Vector:
            vec_children = fill_node_vector(node);
            break;
        case rdf::RDF_Type::Void:
            break;
        case rdf::RDF_Type::Ptr_string:
        case rdf::RDF_Type::Ptr_char:
        case rdf::RDF_Type::Stl_string:
        case rdf::RDF_Type::Static_string:
            break;
        case rdf::RDF_Type::Padding:
            break;
        case rdf::RDF_Type::Class:
        case rdf::RDF_Type::Struct:
        case rdf::RDF_Type::Compound:
            vec_children = fill_node_compound(node);
            break;
        case rdf::RDF_Type::Union:
            vec_children = fill_node_union(p_node);
            break;
        case rdf::RDF_Type::AnonymousUnion:
        case rdf::RDF_Type::AnonymousCompound:
            vec_children = fill_node_anonymous(p_node);
            break;
        case rdf::RDF_Type::Bitfield:
            vec_children = fill_node_bitfield(p_node);
            break;
        case rdf::RDF_Type::Enum:
            break;
        case rdf::RDF_Type::Array:
            vec_children = fill_node_array(p_node);
            break;
        case rdf::RDF_Type::Pointer:
            vec_children = fill_node_pointer(p_node);
            break;
        case rdf::RDF_Type::DFFlagArray:
            vec_children = fill_node_df_flag_array(p_node);
            break;
        case rdf::RDF_Type::DFArray:
            vec_children = fill_node_df_array(p_node);
            break;
        case rdf::RDF_Type::DFLinkedList:
            vec_children = fill_node_df_linked_list(p_node);
            break;
        case rdf::RDF_Type::Stl_Deque:
            vec_children = fill_node_deque(p_node);
            break;
        case rdf::RDF_Type::Stl_fstream:
            break;
        default:
            break;
    }
    for (auto& child : vec_children)
        if (child->m_has_children)
        {
            auto node = dynamic_cast<Node*>(child);
            node->add_dummy_node();
        }
    return vec_children;
}

} // namespace rdf
