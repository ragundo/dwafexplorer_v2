/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//---------------------------------------------------------------------------------------
//
static bool is_class_compound_or_struct(RDF_Type p_rdf_type)
{
    switch (p_rdf_type)
    {
        case RDF_Type::Class:
        case RDF_Type::Compound:
        case RDF_Type::Struct:
            return true;
        default:
            break;
    }
    return false;
}

//
//---------------------------------------------------------------------------------------
//
/*
static std::string make_path_from_tokens(std::vector<string>& tokens, std::size_t end)
{
    std::string result = "";
    std::size_t i      = 0;
    bool        first  = true;
    for (size_t j = 0; j < end; j++)
        if (first)
        {
            result.append(tokens[j]);
            first = false;
        }
        else
            result.append(".").append(tokens[j]);
    return result;
}
*/

//
//---------------------------------------------------------------------------------------
//
NodeBase* up_to_compound_or_pointer_to_compound(NodeBase* p_node)
{
    auto it = p_node->parent();
    while (it)
    {
        if (is_class_compound_or_struct(it->m_rdf_type))
            return it;
        if (it->m_rdf_type == RDF_Type::Pointer)
        {
            auto pointer_df_type  = it->m_df_type;
            auto pointee_rdf_type = MetadataServer.df_2_rdf(pointer_df_type);
            if (is_class_compound_or_struct(pointee_rdf_type))
                return it;
        }
        it = it->parent();
    }
    return nullptr;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::add_hyperlink(NodeBase* p_node)
{
    auto parent = p_node->parent();
    if (parent && parent->m_rdf_type == RDF_Type::Vector)
    {
        if (!parent->m_hyperlink_type.empty())
        {
            p_node->m_hyperlink_type = parent->m_hyperlink_type;
            return;
        }
    }

    if (parent && parent->m_rdf_type == RDF_Type::Array)
    {
        if (!parent->m_hyperlink_type.empty())
        {
            p_node->m_hyperlink_type = parent->m_hyperlink_type;
            return;
        }
    }

    auto compound_type = up_to_compound_or_pointer_to_compound(p_node);
    if (compound_type == nullptr)
        return;

    // index-refrs-to is the least prioritary
    auto index_refers_to_maybe = MetadataServer.get_index_refers_to(compound_type->m_df_type, p_node->m_field_name);
    if (index_refers_to_maybe)
    {
        p_node->m_hyperlink_type = "ind-ref-to://" + *index_refers_to_maybe;
    }
    // Check with refers-to
    auto refers_to_maybe = MetadataServer.get_refers_to_metadata_field(compound_type->m_df_type, p_node->m_field_name);
    if (refers_to_maybe)
    {
        p_node->m_hyperlink_type = "ref-to://" + *refers_to_maybe;
    }

    auto ref_target_maybe = MetadataServer.get_ref_target_metadata_field(compound_type->m_df_type, p_node->m_field_name);
    if (ref_target_maybe)
    {
        auto pair                = *ref_target_maybe;
        p_node->m_hyperlink_type = "ref-target://" + pair.first + (pair.second.empty() ? "" : "/" + pair.second);
    }
}

} // namespace rdf