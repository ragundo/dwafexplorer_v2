/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//------------------------------------------------------------------------------------//
//
std::string format_array_index(int p_value, int p_array_size)
{
    // Node name [index]
    std::stringstream ss;
    auto              max_length = std::to_string(p_array_size).length();
    std::string       field_name = "[";
    std::string       value      = std::to_string(p_value);
    ss << std::right << std::setw(max_length) << value;
    auto value_padded = ss.str();
    field_name.append(value_padded).append("]");
    return field_name;
}

//
//------------------------------------------------------------------------------------//
//
std::size_t array_size_recursive(const std::string& p_addornements, NodeArray* p_node_array)
{
    std::string empty("");
    if (!p_addornements.empty())
    {
        if (p_addornements[0] == '*')
        {
            std::string data = "";
            if (p_addornements.length() > 1)
                data = p_addornements.substr(1, 512);

            return sizeof(void*) * array_size_recursive(data,
                                                        p_node_array);
        }

        if (p_addornements[0] == 'v')
        {
            std::string data = "";
            if (p_addornements.length() > 1)
                data = p_addornements.substr(1, 512);

            return sizeof(std::vector<void*>) * array_size_recursive(data,
                                                                     p_node_array);
        }

        if (p_addornements[0] == '[')
        {
            std::string addornements = p_addornements.substr(1, 500);
            size_t      index        = 0;
            while (std::isdigit(addornements[index++]))
                ;
            size_t      array_size = std::stoi(addornements.substr(0, index - 1));
            std::string rest       = (addornements.length() > index ? addornements.substr(index, 512) : empty);
            if (rest.empty())
            {
                auto size_maybe   = MetadataServer.size_of_DF_Type(p_node_array->m_df_type);
                auto df_type_size = *size_maybe;
                return array_size * df_type_size;
            }
            return array_size * array_size_recursive(rest, p_node_array);
        }

        if (p_addornements[0] == '{')
        {
            std::string data = p_addornements.substr(1, 1024);
            int         it   = 0;
            while (data[it] != '}')
                it++;
            auto base_type_st = data.substr(0, it - 1);
            auto base_type    = MetadataServer.DF_Type_from_string(base_type_st);
            auto size_maybe   = MetadataServer.size_of_DF_Type(base_type);
            auto df_type_size = *size_maybe;
            return df_type_size;
        }
    }
    return 1;
}

//
//------------------------------------------------------------------------------------//
//
std::size_t get_array_element_size(NodeArray* p_node_array)
{
    std::string addornements = p_node_array->m_addornements;
    // Remove you own addornements
    addornements      = addornements.substr(1, 500);
    std::size_t index = 0;
    while (std::isdigit(addornements[index++]))
        ;
    int array_size = std::stoi(addornements.substr(0, index - 1));
    if (index >= addornements.length())
    {
        if (p_node_array->m_enum_basetype != DF_Type::None)
        {
            auto size_maybe   = MetadataServer.size_of_DF_Type(p_node_array->m_enum_basetype);
            auto df_type_size = *size_maybe;
            return df_type_size;
        }
        auto size_maybe   = MetadataServer.size_of_DF_Type(p_node_array->m_df_type);
        auto df_type_size = *size_maybe;
        return df_type_size;
    }
    std::string rest = addornements.substr(index - 1, 512);
    return array_size_recursive(rest, p_node_array);
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_array(rdf::NodeBase* p_node_array)
{
    vector<NodeBase*> children_vec;
    auto              node_array = dynamic_cast<NodeArray*>(p_node_array);

    uint64_t item_address = node_array->m_address;

    // Remove array qualifier
    auto addornements = node_array->m_addornements.substr(1, 500);
    while (std::isdigit(addornements[0]))
        addornements = addornements.substr(1, 500);

    if (addornements.empty() || addornements[0] == '{')
        return fill_node_array_plain(node_array,
                                     item_address,
                                     addornements);

    if (addornements[0] == '*')
        return fill_node_array_pointer(node_array,
                                       item_address,
                                       addornements);

    if (addornements[0] == 'v')
        return fill_node_array_vector(node_array,
                                      item_address,
                                      addornements);

    if (addornements[0] == '[')
        return fill_node_array_array(node_array,
                                     item_address,
                                     addornements);

    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_array_plain(rdf::NodeArray* p_node_array,
                                                     uint64_t        p_item_address,
                                                     string&         p_addornements)
{
    vector<NodeBase*> children_vec;

    // Arrays of simple types and vector of DF structures, bitfields or enums
    for (unsigned int i = 0; i < p_node_array->m_array_size; i++)
    {
        std::string field_name = "[" + std::to_string(i) + "]";
        auto        n_pve      = NodeBuilder::create_node(p_item_address,
                                              MetadataServer.df_2_rdf(p_node_array->m_df_type),
                                              p_node_array->m_df_type,
                                              p_addornements);
        n_pve->m_field_name    = field_name;
        // Add it to the table of nodes
        n_pve->m_node_table = p_node_array->m_node_table;
        n_pve->m_field_path = p_node_array->m_field_path +
                              (p_node_array->m_field_name[0] != '[' ? "." : "") +
                              p_node_array->m_field_name;
        n_pve->m_node_table->add_node(n_pve);

        children_vec.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        p_item_address += get_array_element_size(p_node_array);
    }
    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_array_pointer(rdf::NodeArray* p_node_array,
                                                       uint64_t        p_item_address,
                                                       string&         p_addornements)
{
    vector<NodeBase*> children_vec;

    // Array of pointers
    for (unsigned int i = 0; i < p_node_array->m_array_size; i++)
    {
        std::string field_name = "[" + std::to_string(i) + "]";
        auto        n_pve      = NodeBuilder::create_node(p_item_address,
                                              RDF_Type::Pointer,
                                              p_node_array->m_df_type,
                                              p_addornements);
        n_pve->m_field_name    = field_name;
        // Add it to the table of nodes
        n_pve->m_node_table = p_node_array->m_node_table;
        n_pve->m_field_path = p_node_array->m_field_path +
                              (p_node_array->m_field_name[0] != '[' ? "." : "") +
                              p_node_array->m_field_name;
        n_pve->m_node_table->add_node(n_pve);

        children_vec.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        p_item_address += sizeof(void*);
    }
    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_array_vector(rdf::NodeArray* p_node_array,
                                                      uint64_t        p_item_address,
                                                      string&         p_addornements)
{
    vector<NodeBase*> children_vec;
    // Array of vectors
    for (unsigned int i = 0; i < p_node_array->m_array_size; i++)
    {
        std::string field_name = "[" + std::to_string(i) + "]";
        auto        n_pve      = NodeBuilder::create_node(p_item_address,
                                              RDF_Type::Vector,
                                              p_node_array->m_df_type,
                                              p_addornements);
        n_pve->m_field_name    = field_name;
        n_pve->m_node_table    = p_node_array->m_node_table;
        n_pve->m_field_path    = p_node_array->m_field_path +
                              (p_node_array->m_field_name[0] != '[' ? "." : "") +
                              p_node_array->m_field_name;

        // Add it to the table of nodes
        n_pve->m_node_table->add_node(n_pve);
        children_vec.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        p_item_address += sizeof(std::vector<void*>);
    }
    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_array_array(rdf::NodeArray* p_node_array,
                                                     uint64_t        p_item_address,
                                                     string&         p_addornements)
{
    vector<NodeBase*> children_vec;
    // Array of arrays
    for (unsigned int i = 0; i < p_node_array->m_array_size; i++)
    {
        std::string field_name = "[" + std::to_string(i) + "]";
        auto        n_pve      = NodeBuilder::create_node(p_item_address,
                                              RDF_Type::Array,
                                              p_node_array->m_df_type,
                                              p_addornements);
        n_pve->m_field_name    = field_name;
        // Add it to the table of nodes
        n_pve->m_node_table = p_node_array->m_node_table;
        n_pve->m_field_path = p_node_array->m_field_path +
                              (p_node_array->m_field_name[0] != '[' ? "." : "") +
                              p_node_array->m_field_name;
        n_pve->m_node_table->add_node(n_pve);
        children_vec.push_back(n_pve);

        NodeBuilder::add_hyperlink(n_pve);
        p_item_address += get_array_element_size(p_node_array);
    }
    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_array(rdf::NodeBase*     p_node,
                                     const std::string& p_addornements)
{
    std::string addornements;
    auto        array_node = dynamic_cast<NodeArray*>(p_node);
    // Check for index-enum
    auto index_enum = get_index_enum(p_addornements);
    if (!index_enum.first.empty())
    {
        array_node->m_index_enum   = MetadataServer.DF_Type_from_string(index_enum.first);
        addornements               = index_enum.second;
        array_node->m_addornements = addornements;
    }
    else
        array_node->m_addornements = p_addornements;

    // Remove array qualifier & get size
    addornements = p_addornements.substr(1, 500);
    int i        = 0;
    while (std::isdigit(addornements[0]))
    {
        addornements = addornements.substr(1, 500);
        i++;
    }
    auto size_as_string      = p_addornements.substr(1, i);
    auto array_size          = std::stoi(size_as_string);
    array_node->m_array_size = array_size;

    array_node->m_enum_basetype = process_base_type(p_addornements);

    if (array_size != 0)
        array_node->m_has_children = true;
}

} // namespace rdf