/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_compound(rdf::NodeBase* p_node)
{
    auto node = dynamic_cast<Node*>(p_node);
    auto size = MetadataServer.get_num_children(p_node->m_df_type);
    if (size > 0)
    {
        p_node->m_has_children = true;
        return;
    }

    // Size = 0 but it it's derived class ...
    auto base_classes = MetadataServer.get_base_classes(p_node->m_df_type);
    if (!base_classes.empty())
        p_node->m_has_children = true;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_union(rdf::NodeBase* p_node)
{
    auto node = dynamic_cast<Node*>(p_node);
    auto size = MetadataServer.get_num_children(p_node->m_df_type);
    if (size > 0)
        p_node->m_has_children = true;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_padding(rdf::NodeBase* p_node, const std::string& p_addornements)
{
    auto padding_node = dynamic_cast<NodePadding*>(p_node);
    // Get the padding size from the addornements #3#
    if (p_addornements[0] == '#')
    {
        auto it = 1;
        while (std::isdigit(p_addornements[it]))
            it++;
        auto padding_size_st = p_addornements.substr(1, it - 1);
        padding_node->m_size = std::stoi(padding_size_st);
    }
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_enum(rdf::NodeBase* p_node, const std::string& p_addornements)
{
    auto enum_node           = dynamic_cast<NodeEnum*>(p_node);
    auto enum_metadata_maybe = MetadataServer.get_enum_metadata(p_node->m_df_type);
    if (enum_metadata_maybe)
    {
        auto enum_metadata       = *enum_metadata_maybe;
        enum_node->m_base_type   = std::get<0>(enum_metadata);
        enum_node->m_first_value = std::get<1>(enum_metadata);
        enum_node->m_last_value  = std::get<2>(enum_metadata);

        // Enums can have its base type different in the field declaration
        auto real_base_type = process_base_type(p_addornements);
        if (real_base_type != DF_Type::None)
            enum_node->m_base_type = real_base_type;
        enum_node->m_enum_type_as_string = MetadataServer.DF_Type_to_string(enum_node->m_df_type);
    }
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_bitfield(rdf::NodeBase* p_node)
{
    auto bitfield_node           = dynamic_cast<NodeBitfield*>(p_node);
    auto bitfield_metadata_maybe = MetadataServer.get_bitfield_basetype(p_node->m_df_type);
    if (bitfield_metadata_maybe)
    {
        bitfield_node->m_base_type    = *bitfield_metadata_maybe;
        bitfield_node->m_has_children = true;
    }
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_df_flag_array(rdf::NodeBase* p_node)
{
    auto df_flag_array_node = dynamic_cast<NodeDFFlagArray*>(p_node);
    // check for index enum
    auto index_enum_maybe = MetadataServer.get_enum_metadata(p_node->m_df_type);
    if (index_enum_maybe)
    {
        df_flag_array_node->m_num_entries  = std::get<2>(*index_enum_maybe) - std::get<1>(*index_enum_maybe) + 1;
        df_flag_array_node->m_has_children = true;
        return;
    }
    // no index enum but int16 etc
    switch (p_node->m_df_type)
    {
        case DF_Type::uint8_t:
        case DF_Type::int8_t:
            df_flag_array_node->m_num_entries  = 8;
            df_flag_array_node->m_has_children = true;
            break;
        case DF_Type::uint16_t:
        case DF_Type::int16_t:
            df_flag_array_node->m_num_entries  = 16;
            df_flag_array_node->m_has_children = true;
            break;
        case DF_Type::uint32_t:
        case DF_Type::int32_t:
            df_flag_array_node->m_num_entries  = 32;
            df_flag_array_node->m_has_children = true;
            break;
        default:
            break;
    }
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_pointer(rdf::NodeBase* p_node, const std::string& p_addornements)
{
    auto pointer_node            = dynamic_cast<NodePointer*>(p_node);
    pointer_node->m_addornements = p_addornements;
    uint64_t* pointer_address    = reinterpret_cast<uint64_t*>(p_node->m_address);
    if (pointer_address != 0)
    {
        uint64_t pointee_address = *pointer_address;
        if (pointee_address != 0)
            p_node->m_has_children = true;
    }
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_df_linked_list(rdf::NodeBase* p_node)
{
    auto node            = dynamic_cast<NodeDFLinkedList*>(p_node);
    auto item_type_maybe = MetadataServer.get_linked_list_type(p_node->m_df_type);
    if (item_type_maybe)
        node->m_item_type = *item_type_maybe;
    p_node->m_has_children = true;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_df_array(rdf::NodeBase* p_node)
{
    auto            node                  = dynamic_cast<NodeDFArray*>(p_node);
    auto            offset                = node->m_address + sizeof(void*);
    unsigned short* pointer_df_flag_array = reinterpret_cast<unsigned short*>(offset);
    node->m_array_size                    = *pointer_df_flag_array;
    if (node->m_array_size > 0)
        ;
    p_node->m_has_children = true;
}

} // namespace rdf