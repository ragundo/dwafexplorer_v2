/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//----------------------------------------------------------------------------------------------------------------------
///
/// @brief Fills a pointer node with its children
///
/// @param p_node_pointer The parent node which we'll add nodes as children
/// @return std::vector<rdf::NodeBase*> vector of NodeBase* that are the chikldren of this node
std::vector<rdf::NodeBase*> NodeBuilder::fill_node_pointer(rdf::NodeBase* p_node_pointer)
{
    vector<NodeBase*> children_vec;
    auto              node_pointer = dynamic_cast<NodePointer*>(p_node_pointer);

    uint64_t*   pointer_address = reinterpret_cast<uint64_t*>(node_pointer->m_address);
    uint64_t    pointee_address = *pointer_address;
    std::string addornements    = node_pointer->m_addornements;

    DF_Type real_df_type = MetadataServer.get_df_subtype(node_pointer->m_df_type, pointee_address);
    if (real_df_type == DF_Type::None)
        real_df_type = node_pointer->m_df_type;
    RDF_Type real_rdf_type = MetadataServer.df_2_rdf(real_df_type);

    auto linked_list_maybe = MetadataServer.get_linked_list_type(real_df_type);

    if (addornements.empty() || addornements == "*")
    {
        std::string child_addornements = "";
        auto        num_children       = MetadataServer.get_num_children(real_df_type);
        if (num_children == 0)
            num_children = 1;

        if ((real_rdf_type == RDF_Type::Class) ||
            (real_rdf_type == RDF_Type::Struct) ||
            (real_rdf_type == RDF_Type::Compound) ||
            (real_rdf_type == RDF_Type::AnonymousCompound) ||
            (real_rdf_type == RDF_Type::AnonymousUnion) ||
            linked_list_maybe)
        {
            auto n_pve          = NodeBuilder::create_node(pointee_address,
                                                  real_rdf_type,
                                                  real_df_type,
                                                  child_addornements);
            n_pve->m_node_table = p_node_pointer->m_node_table;
            n_pve->m_field_name = p_node_pointer->m_field_name;
            n_pve->m_field_path = p_node_pointer->m_field_path;

            children_vec = NodeBuilder::fill_node(n_pve);
            /*
            if (p_model)
            {
                auto node_pointer = dynamic_cast<Node*>(p_node_pointer);
                for (auto& child : children_vec)
                {
                    if (child->m_has_children)
                    {
                        auto node_child = dynamic_cast<Node*>(child);
                        if (node_child->get_num_children() == 0)
                            node_child->add_dummy_node();
                    }
                    p_model->add_child_node_to_parent_node(node_pointer, child);
                    NodeBuilder::add_hyperlink(child);
                }
                if (n_pve->m_has_children)
                {
                    auto node_n_pve = dynamic_cast<Node*>(n_pve);
                    node_n_pve->add_dummy_node();
                }
            }
*/
            delete n_pve;
        }
        else // Pointer to simple structure
        {
            auto n_pve          = NodeBuilder::create_node(pointee_address,
                                                  real_rdf_type,
                                                  real_df_type,
                                                  child_addornements);
            n_pve->m_field_name = "N/A" + std::to_string(n_pve->m_address);
            n_pve->m_node_table = p_node_pointer->m_node_table;
            n_pve->m_node_table = p_node_pointer->m_node_table;
            n_pve->m_field_path = p_node_pointer->m_field_path +
                                  (p_node_pointer->m_field_name[0] != '[' ? "." : "") +
                                  p_node_pointer->m_field_name;
            n_pve->m_node_table->add_node(n_pve);

            children_vec.push_back(n_pve);
            NodeBuilder::add_hyperlink(n_pve);
        }
        return children_vec;
    }

    if (addornements[0] == '*')
    {
        std::string child_addornement = (addornements.size() > 1 ? addornements.substr(1, 512) : "");
        switch (child_addornement[0])
        {
            case '*':
                real_rdf_type = rdf::RDF_Type::Pointer;
                break;
            case 'v':
                real_rdf_type = rdf::RDF_Type::Vector;
                break;
            case '[':
                real_rdf_type = rdf::RDF_Type::Array;
                break;
            default:
                break;
        }

        auto n_pve          = NodeBuilder::create_node(pointee_address,
                                              real_rdf_type,
                                              real_df_type,
                                              child_addornement);
        n_pve->m_field_name = "N/A" + std::to_string(n_pve->m_address);
        n_pve->m_node_table = p_node_pointer->m_node_table;
        n_pve->m_field_path = p_node_pointer->m_field_path +
                              (p_node_pointer->m_field_name[0] != '[' ? "." : "") +
                              p_node_pointer->m_field_name;
        n_pve->m_node_table->add_node(n_pve);

        children_vec.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        return children_vec;
    }

    if (addornements[0] == 'v')
    {
        // Pointer to vector
        std::string child_addornement = (addornements.size() > 1 ? addornements.substr(1, 512) : "");

        auto n_pve          = NodeBuilder::create_node(pointee_address,
                                              RDF_Type::Vector,
                                              real_df_type,
                                              child_addornement);
        n_pve->m_field_name = "N/A" + std::to_string(n_pve->m_address);
        n_pve->m_node_table = p_node_pointer->m_node_table;
        n_pve->m_field_path = p_node_pointer->m_field_path +
                              (p_node_pointer->m_field_name[0] != '[' ? "." : "") +
                              p_node_pointer->m_field_name;
        n_pve->m_node_table->add_node(n_pve);

        children_vec.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        return children_vec;
    }
    if (addornements[0] == '[')
    {
        // Pointer to array
        std::string child_addornement = (addornements.size() > 1 ? addornements.substr(1, 512) : "");
        auto        n_pve             = NodeBuilder::create_node(pointee_address,
                                              RDF_Type::Array,
                                              real_df_type,
                                              child_addornement);

        n_pve->m_field_name = "N/A";
        n_pve->m_node_table = p_node_pointer->m_node_table;
        n_pve->m_field_path = p_node_pointer->m_field_path +
                              (p_node_pointer->m_field_name[0] != '[' ? "." : "") +
                              p_node_pointer->m_field_name;
        n_pve->m_node_table->add_node(n_pve);

        children_vec.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        return children_vec;
    }
    return children_vec;
}

} // namespace rdf