/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_compound(rdf::Node* p_node)
{
    vector<NodeBase*> vec_children;

    auto super_child = build_super_node_if_any(p_node);
    if (super_child)
        vec_children.push_back(super_child);

    auto child_fields = MetadataServer.get_fields_metadata2(p_node->m_df_type);
    if (!child_fields)
        return vec_children;

    for (auto& child_field_metadata : *child_fields)
        add_child_to_node(child_field_metadata,
                          p_node,
                          vec_children);

    return vec_children;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_union(rdf::NodeBase* p_node)
{
    vector<NodeBase*> vec_children;

    auto node         = dynamic_cast<Node*>(p_node);
    auto child_fields = MetadataServer.get_fields_metadata2(node->m_df_type);
    if (!child_fields)
        return vec_children;

    std::string parent_path = p_node->path();
    for (auto& child_field_metadata : *child_fields)
        add_child_to_node(child_field_metadata,
                          node,
                          vec_children);
    return vec_children;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_anonymous(rdf::NodeBase* p_node)
{
    vector<NodeBase*> children_vec;

    auto node         = dynamic_cast<Node*>(p_node);
    auto child_fields = MetadataServer.get_fields_metadata2(node->m_df_type);
    if (!child_fields)
        return children_vec;

    for (auto& child_field_metadata : *child_fields)
        add_child_to_node(child_field_metadata,
                          node,
                          children_vec);
    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_bitfield(rdf::NodeBase* p_node)
{
    vector<NodeBase*> children_vec;
    auto              node = dynamic_cast<Node*>(p_node);

    unsigned int num_bits       = 0;
    int64_t      bitfield_value = 0;
    auto         bitfield_node  = dynamic_cast<NodeBitfield*>(p_node);

    switch (bitfield_node->m_base_type)
    {
        case DF_Type::int8_t:
            bitfield_value = get_value_at_address<int8_t>(bitfield_node);
            num_bits       = 8;
            break;
        case DF_Type::int16_t:
            bitfield_value = get_value_at_address<int16_t>(bitfield_node);
            num_bits       = 16;
            break;
        case DF_Type::int32_t:
            bitfield_value = get_value_at_address<int32_t>(bitfield_node);
            num_bits       = 32;
            break;
        case DF_Type::uint8_t:
            bitfield_value = get_value_at_address<uint8_t>(bitfield_node);
            num_bits       = 8;
            break;
        case DF_Type::uint16_t:
            bitfield_value = get_value_at_address<uint16_t>(bitfield_node);
            num_bits       = 16;
            break;
        case DF_Type::uint32_t:
            bitfield_value = get_value_at_address<uint32_t>(bitfield_node);
            num_bits       = 32;
            break;
        default:
            break;
    }
    auto         metadata_maybe = MetadataServer.get_bitfield_metadata(p_node->m_df_type);
    auto         metadata       = *metadata_maybe;
    unsigned int mask           = 1;
    for (unsigned int i = 0; i < num_bits; i++)
    {
        std::string field_name    = "";
        std::string field_comment = "";

        auto it = metadata.find(i);
        if (it != metadata.end())
        {
            field_name    = (*it).second.first;
            field_comment = (*it).second.second;
        }
        if ((bitfield_value & mask) || (field_name.length() > 0))
        {
            auto* n_pve = new NodeBitfieldEntry();

            n_pve->m_field_name = "[" + std::to_string(i) + "]";
            n_pve->m_rdf_type   = rdf::RDF_Type::Bool;
            n_pve->m_df_type    = rdf::DF_Type::Bool;
            n_pve->m_index      = i;
            n_pve->m_comment    = field_comment;
            n_pve->m_value      = bitfield_value & mask;
            n_pve->m_address    = bitfield_node->m_address + (i < 8 ? 0 : (i < 16 ? 1 : (i < 24 ? 2 : 3)));

            // Add it to the table of nodes
            n_pve->m_node_table = p_node->m_node_table;
            n_pve->m_field_path = p_node->m_field_path +
                                  (p_node->m_field_name[0] != '[' ? "." : "") +
                                  p_node->m_field_name;
            n_pve->m_node_table->add_node(n_pve);
            children_vec.push_back(n_pve);
        }
        mask = mask << 1;
    }
    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_df_linked_list(rdf::NodeBase* p_node)
{
    vector<NodeBase*> children_vec;
    auto              node         = dynamic_cast<NodeDFLinkedList*>(p_node);
    auto              address      = node->m_address;
    std::string       addornements = "*";

    // Create pointer item
    auto child          = create_node(address,
                             RDF_Type::Pointer,
                             node->m_item_type,
                             addornements);
    child->m_field_name = "item";
    // Add it to the table of nodes
    child->m_node_table = p_node->m_node_table;
    child->m_field_path = p_node->m_field_path +
                          (p_node->m_rdf_type != RDF_Type::Array && p_node->m_rdf_type != RDF_Type::Vector ? "." : "") +
                          p_node->m_field_name;
    child->m_node_table->add_node(child);
    if (child->m_address)
    {
        auto child_pointee = reinterpret_cast<uint64_t*>(child->m_address);
        if (*child_pointee != 0)
            child->m_has_children = true;
    }

    address += sizeof(void*);
    children_vec.push_back(child);

    // create pointer prev
    child = create_node(address,
                        RDF_Type::Pointer,
                        p_node->m_df_type,
                        addornements);

    child->m_field_name = "prev";
    // Add it to the table of nodes
    child->m_node_table = p_node->m_node_table;
    child->m_field_path = p_node->m_field_path +
                          (p_node->m_field_name[0] != '[' ? "." : "") +
                          p_node->m_field_name;
    if (child->m_address)
    {
        auto child_pointee = reinterpret_cast<uint64_t*>(child->m_address);
        if (*child_pointee != 0)
            child->m_has_children = true;
    }

    address += sizeof(void*);
    children_vec.push_back(child);

    // create pointer next
    child               = create_node(address,
                        RDF_Type::Pointer,
                        p_node->m_df_type,
                        addornements);
    child->m_field_name = "next";
    // Add it to the table of nodes
    child->m_node_table = p_node->m_node_table;
    child->m_field_path = p_node->m_field_path +
                          (p_node->m_field_name[0] != '[' ? "." : "") +
                          p_node->m_field_name;
    child->m_node_table->add_node(child);
    if (child->m_address)
    {
        auto child_pointee = reinterpret_cast<uint64_t*>(child->m_address);
        if (*child_pointee != 0)
            child->m_has_children = true;
    }
    children_vec.push_back(child);

    return children_vec;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_df_flag_array(rdf::NodeBase* p_node)
{
    vector<NodeBase*> children_vec;
    auto              df_flag_array_node = dynamic_cast<NodeDFFlagArray*>(p_node);

    // DFFlagArray bits
    uint8_t** a                     = reinterpret_cast<uint8_t**>(df_flag_array_node->m_address);
    uint8_t*  pointer_df_flag_array = *a;

    unsigned int mask           = 1;
    uint8_t      bitfield_value = 0;
    auto         df_type        = df_flag_array_node->m_df_type;

    for (auto i = 0; i < df_flag_array_node->m_num_entries; i++)
    {
        auto        enum_data_maybe = MetadataServer.get_enum_value(df_type, i);
        std::string field_name      = "";
        if (enum_data_maybe)
        {
            field_name = (*enum_data_maybe).first;
        }

        if (i % 8 == 0)
            bitfield_value = *pointer_df_flag_array;
        //if ((bitfield_value & mask) || (field_name.length() > 0))
        if (true)
        {
            auto* n_pve         = new NodeBitfieldEntry();
            n_pve->m_field_name = field_name;
            if (field_name.length() == 0)
            {
                //                if (i < 10)
                //                    n_pve->m_field_name = "[0" + std::to_string(i) + "]";
                //                else
                n_pve->m_field_name = "[" + std::to_string(i) + "]";
            }
            else
            {
                //if (i < 10)
                //  n_pve->m_field_name = "[0" + std::to_string(i) + "] " + field_name;
                //else
                n_pve->m_field_name = "[" + std::to_string(i) + "] " + field_name;
            }
            n_pve->m_rdf_type = rdf::RDF_Type::Bool;
            n_pve->m_df_type  = rdf::DF_Type::Bool;
            n_pve->m_index    = i;
            n_pve->m_comment  = "";
            n_pve->m_value    = bitfield_value & mask;
            n_pve->m_address  = reinterpret_cast<uint64_t>(pointer_df_flag_array);

            // Add it to the table of nodes
            n_pve->m_node_table = p_node->m_node_table;
            n_pve->m_field_path = p_node->m_field_path + "." + p_node->m_field_name;
            n_pve->m_node_table->add_node(n_pve);
            children_vec.push_back(n_pve);
        }
        mask = mask << 1;
        if ((i + 1) % 8 == 0)
        {
            pointer_df_flag_array++;
            mask = 1;
        }
    }
    return children_vec;
}

vector<NodeBase*> NodeBuilder::fill_node_deque(rdf::NodeBase* p_node)
{
    vector<NodeBase*> children_vec;
    auto              queue_node = dynamic_cast<NodeDeque*>(p_node);
    return children_vec;
}

} // namespace rdf