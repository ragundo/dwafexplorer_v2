/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef NODEBUILDER_H
#define NODEBUILDER_H

#include "MetadataServer.h"
#include <utility>

namespace rdf
{
struct NodeBase;
struct NodeArray;
struct NodeGlobal;
struct Node;
struct NodeDummy;
struct NodeVector;
class NodeTable;
class CMetadataServer;
} // namespace rdf

namespace rdf
{
class NodeBuilder
{
  public:
    static NodeBase* create_node(uint64_t           p_address,
                                 rdf::RDF_Type      p_rdf_type,
                                 rdf::DF_Type       p_df_type,
                                 const std::string& addornements);

    static std::vector<rdf::NodeBase*> fill_node(rdf::NodeBase* p_node);

    static void add_hyperlink(NodeBase* p_node);

    static void build_node_compound(rdf::Node* p_node);

  private:
    static std::pair<std::string, std::string> get_index_enum(const std::string& p_addornements);

    static DF_Type process_base_type(const std::string& p_addornements_string);

    static uint64_t update_address(uint64_t  p_address,
                                   NodeBase* p_node);

    static NodeBase* build_super_node_if_any(Node* p_parent_node);

    static void add_child_to_node(const metadata_standard&     p_metadata,
                                  Node*                        p_parent,
                                  std::vector<rdf::NodeBase*>& p_vec_children);

    static std::vector<rdf::NodeBase*> fill_node_vector(rdf::Node* p_node_vector);
    static std::vector<rdf::NodeBase*> fill_node_df_linked_list(rdf::NodeBase* p_node);
    static std::vector<rdf::NodeBase*> fill_node_array(rdf::NodeBase* p_node_array);
    static std::vector<rdf::NodeBase*> fill_node_pointer(rdf::NodeBase* p_node_pointer);
    static std::vector<rdf::NodeBase*> fill_node_compound(rdf::Node* p_node);
    static std::vector<rdf::NodeBase*> fill_node_union(rdf::NodeBase* p_node);
    static std::vector<rdf::NodeBase*> fill_node_anonymous(rdf::NodeBase* p_node);
    static std::vector<rdf::NodeBase*> fill_node_bitfield(rdf::NodeBase* p_node);
    static std::vector<rdf::NodeBase*> fill_node_df_flag_array(rdf::NodeBase* p_node);
    static std::vector<rdf::NodeBase*> fill_node_df_array(rdf::NodeBase* p_node);
    static std::vector<rdf::NodeBase*> fill_node_deque(rdf::NodeBase* p_node);

    static void process_node_array(rdf::NodeBase* p_node, const std::string& p_addornements);
    static void process_node_enum(rdf::NodeBase* p_node, const std::string& p_addornements);
    static void process_node_vector(rdf::NodeBase* p_node, const std::string& p_addornements);
    static void process_node_compound(rdf::NodeBase* p_node);
    static void process_node_union(rdf::NodeBase* p_node);
    static void process_node_padding(rdf::NodeBase* p_node, const std::string& p_addornements);
    static void process_node_bitfield(rdf::NodeBase* p_node);
    static void process_node_df_flag_array(rdf::NodeBase* p_node);
    static void process_node_pointer(rdf::NodeBase* p_node, const std::string& p_addornements);
    static void process_node_df_linked_list(rdf::NodeBase* p_node);
    static void process_node_df_array(rdf::NodeBase* p_node);

    static std::vector<rdf::NodeBase*> fill_node_array_plain(rdf::NodeArray* p_node_array,
                                                             uint64_t        p_item_address,
                                                             std::string&    p_addornements);

    static std::vector<rdf::NodeBase*> fill_node_array_pointer(rdf::NodeArray* p_node_array,
                                                               uint64_t        p_item_address,
                                                               std::string&    p_addornements);

    static std::vector<rdf::NodeBase*> fill_node_array_vector(rdf::NodeArray* p_node_array,
                                                              uint64_t        p_item_address,
                                                              std::string&    p_addornements);

    static std::vector<rdf::NodeBase*> fill_node_array_array(rdf::NodeArray* p_node_array,
                                                             uint64_t        p_item_address,
                                                             std::string&    p_addornements);
};

} // namespace rdf
#endif // NODEBUILDER_H
