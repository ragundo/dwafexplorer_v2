/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//------------------------------------------------------------------------------------//
//
void fill_simple_entry(NodeBase* p_pve,
                       Node*     p_node,
                       size_t    p_size,
                       uint64_t  p_address,
                       DF_Type   p_df_type,
                       RDF_Type  p_rdf_type)
{
    p_pve->m_address    = p_address;
    p_pve->m_df_type    = p_df_type;
    p_pve->m_rdf_type   = p_rdf_type;
    p_pve->m_comment    = "";
    p_pve->m_node_type  = NodeType::Simple;
    p_pve->m_node_table = p_node->m_node_table;
    p_pve->m_field_path = p_node->m_field_path + "." + p_node->m_field_name;
}

//
//------------------------------------------------------------------------------------//
//
NodeBase* fill_df_array_entry(NodeDFArray* p_parent_node,
                              size_t       p_index,
                              uint64_t     p_address)
{
    std::string field_name;
    field_name = "[";
    field_name.append(std::to_string(p_index)).append("]");

    switch (p_parent_node->m_df_type)
    {
        case rdf::DF_Type::uint8_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(uint8_t), p_address, DF_Type::uint8_t, RDF_Type::uint8_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::uint16_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(uint16_t), p_address, DF_Type::uint16_t, RDF_Type::uint16_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::uint32_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(uint32_t), p_address, DF_Type::uint32_t, RDF_Type::uint32_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::uint64_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(uint64_t), p_address, DF_Type::uint64_t, RDF_Type::uint64_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::int8_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(int8_t), p_address, DF_Type::int8_t, RDF_Type::int8_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::int16_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(int16_t), p_address, DF_Type::int16_t, RDF_Type::int16_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::int32_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(int32_t), p_address, DF_Type::int32_t, RDF_Type::int32_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::int64_t: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(int64_t), p_address, DF_Type::int64_t, RDF_Type::int64_t);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::Bool: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(bool), p_address, DF_Type::Bool, RDF_Type::Bool);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        case rdf::DF_Type::Stl_string: {
            auto n_pve = new NodeSimple;
            fill_simple_entry(n_pve, p_parent_node, sizeof(std::string), p_address, DF_Type::Stl_string, RDF_Type::Stl_string);
            n_pve->m_field_name = field_name;
            n_pve->m_address    = p_address;
            n_pve->m_node_table->add_node(n_pve);
            return n_pve;
        }
        default:
            break;
    }
    return nullptr;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_df_array(rdf::NodeBase* p_node)
{
    vector<NodeBase*> children_vec;
    auto              node_df_array = dynamic_cast<NodeDFArray*>(p_node);

    uint64_t* item_address_pointer = reinterpret_cast<uint64_t*>(node_df_array->m_address);
    uint64_t  item_address         = *item_address_pointer;

    for (unsigned int i = 0; i < node_df_array->m_array_size; i++)
    {
        auto child = fill_df_array_entry(node_df_array, i, item_address);
        children_vec.push_back(child);
        switch (node_df_array->m_df_type)
        {
            case rdf::DF_Type::uint8_t:
                item_address += sizeof(uint8_t);
                break;
            case rdf::DF_Type::uint16_t:
                item_address += sizeof(uint16_t);
                break;
            case rdf::DF_Type::uint32_t:
                item_address += sizeof(uint32_t);
                break;
            case rdf::DF_Type::uint64_t:
                item_address += sizeof(uint64_t);
                break;
            case rdf::DF_Type::int8_t:
                item_address += sizeof(int8_t);
                break;
            case rdf::DF_Type::int16_t:
                item_address += sizeof(int16_t);
                break;
            case rdf::DF_Type::int32_t:
                item_address += sizeof(int32_t);
                break;
            case rdf::DF_Type::int64_t:
                item_address += sizeof(int64_t);
                break;
            case rdf::DF_Type::Bool:
                item_address += sizeof(bool);
                break;
            case rdf::DF_Type::Stl_string:
                item_address += sizeof(std::string);
                break;
            default:
                break;
        }
    }
    return children_vec;
}

} // namespace rdf
