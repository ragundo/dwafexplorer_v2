/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "MetadataServer.h"
#include "NodeBuilder.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "lisp_parser/LispParser.h"
#include "metadata_server/offsets_cache.h"
#include <cctype>
#include <iomanip>
#include <sstream>
#include <tuple>
#include <utility>

extern rdf::CMetadataServer MetadataServer;

using namespace std;

namespace rdf
{
//
//------------------------------------------------------------------------------------//
//
bool vector_has_data(const NodeVector* p_node)
{
    auto vector_start_address = reinterpret_cast<uint64_t*>(p_node->m_address);
    auto end                  = p_node->m_address + sizeof(void*);
    auto vector_end_address   = reinterpret_cast<uint64_t*>(end);

    if (*vector_start_address == 0)
        return false;

    auto diff = *vector_end_address - *vector_start_address;
    return diff != 0;
}

//
//------------------------------------------------------------------------------------//
//
std::string format_vector_index(int p_value, int p_vector_size)
{
    // Node name [index]
    std::stringstream ss;
    auto              max_length = std::to_string(p_vector_size).length();
    std::string       field_name = "[";
    std::string       value      = std::to_string(p_value);
    ss << std::right << std::setw(max_length) << value;
    auto value_padded = ss.str();
    field_name.append(value_padded).append("]");
    return field_name;
}

//
//------------------------------------------------------------------------------------//
//
std::size_t check_vector_size(const NodeVector* p_node)
{
    uint64_t  vector_start_address_v = (p_node->m_address);
    auto      end                    = p_node->m_address + sizeof(void*);
    uint64_t  vector_end_address_v   = (end);
    uint64_t* vector_start_address   = reinterpret_cast<uint64_t*>(vector_start_address_v);
    uint64_t* vector_end_address     = reinterpret_cast<uint64_t*>(vector_end_address_v);

    if (*vector_start_address == 0)
        return 0;

    return *vector_end_address - *vector_start_address;
}

//
//---------------------------------------------------------------------------------------
//
NodeBase* create_vector_entry(NodeVector*  p_node_vector,
                              size_t       p_index,
                              uint64_t     p_address,
                              int          p_vector_size,
                              std::string& p_addornements)
{
    auto rdf_type    = RDF_Type::Void;
    auto parent_path = p_node_vector->path();

    if (p_node_vector->m_df_type != DF_Type::Void)
        rdf_type = MetadataServer.df_2_rdf(p_node_vector->m_df_type);

    //std::string field_name = format_vector_index(p_index, p_vector_size);
    std::string field_name = "[" + std::to_string(p_index) + "]";

    if (p_addornements.empty() || p_addornements[0] == '{')
    {
        auto n_pve          = NodeBuilder::create_node(p_address,
                                              rdf_type,
                                              p_node_vector->m_df_type,
                                              p_addornements);
        n_pve->m_field_name = field_name;

        // Add to the table of nodes
        n_pve->m_node_table = p_node_vector->m_node_table;
        n_pve->m_field_path = p_node_vector->m_field_path +
                              (p_node_vector->m_field_name[0] != '[' ? "." : "") +
                              p_node_vector->m_field_name;
        n_pve->m_node_table->add_node(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
        return n_pve;
    }
    if (p_addornements[0] == '*')
    {
        uint64_t pointee   = *(reinterpret_cast<uint64_t*>(p_address));
        DF_Type  real_type = p_node_vector->m_df_type;
        if (pointee != 0)
        {
            auto the_type = MetadataServer.get_df_subtype(p_node_vector->m_df_type, pointee);
            if (the_type != DF_Type::None)
                real_type = the_type;
            auto n_pve          = NodeBuilder::create_node(p_address,
                                                  RDF_Type::Pointer,
                                                  real_type,
                                                  p_addornements);
            n_pve->m_field_name = field_name;
            n_pve->m_node_table = p_node_vector->m_node_table;
            n_pve->m_field_path = p_node_vector->m_field_path +
                                  (p_node_vector->m_field_name[0] != '[' ? "." : "") +
                                  p_node_vector->m_field_name;
            n_pve->m_node_table->add_node(n_pve);

            NodeBuilder::add_hyperlink(n_pve);
			return n_pve;
        }
    }
    return nullptr;
}

//
//---------------------------------------------------------------------------------------
//
std::vector<rdf::NodeBase*> fill_node_vector_of_bool(rdf::Node* p_node_vector, size_t p_vector_size)
{
    std::vector<rdf::NodeBase*> vec_children;
    auto                        node_vector    = dynamic_cast<NodeVector*>(p_node_vector);
    auto                        parent_path    = p_node_vector->path();
    std::string                 child_metadata = "";

    std::string field_name;
    for (unsigned int i = 0; i < p_vector_size; i++)
    {
        field_name = format_vector_index(i, p_vector_size);

        auto n_pve          = NodeBuilder::create_node(node_vector->m_address,
                                              RDF_Type::Bool,
                                              DF_Type::Bool,
                                              child_metadata);
        n_pve->m_field_name = field_name;
        n_pve->m_node_table = p_node_vector->m_node_table;
        n_pve->m_field_path = p_node_vector->m_field_path +
                              (p_node_vector->m_field_name[0] != '[' ? "." : "") +
                              p_node_vector->m_field_name;
        n_pve->m_node_table->add_node(n_pve);

        vec_children.push_back(n_pve);
        NodeBuilder::add_hyperlink(n_pve);
    }
    return vec_children;
}

//
//---------------------------------------------------------------------------------------
//
vector<NodeBase*> NodeBuilder::fill_node_vector(rdf::Node* p_node_vector)
{
    std::vector<rdf::NodeBase*> vec_children;
    auto                        node_vector    = dynamic_cast<NodeVector*>(p_node_vector);
    auto                        vector_address = node_vector->m_address;
    uint64_t*                   pointer        = reinterpret_cast<uint64_t*>(vector_address);
    uint64_t                    item_address   = *pointer;
    auto                        parent_path    = p_node_vector->path();

    if (item_address == 0)
        return vector<NodeBase*>();

    auto vector_size = DF_Model::get_vector_size(node_vector);

    if (node_vector->m_df_type == DF_Type::Bool)
        return fill_node_vector_of_bool(p_node_vector, vector_size);

    std::string addornements = node_vector->m_addornements;

    node_vector->m_enum_basetype = process_base_type(addornements);

    // First process vector of simple types and vector of DF structures, bitfields or enums
    if (addornements.empty() || (addornements == "v"))
    {
        vector<NodeBase*> vec_children;
        addornements = "";
        for (size_t i = 0; i < vector_size; i++)
        {
            // Create a new vector entry
            auto child = create_vector_entry(node_vector, i, item_address, vector_size, addornements);
            vec_children.push_back(child);
            item_address = update_address(item_address, node_vector);
        }
        return vec_children;
    }

    if ((addornements[0] == 'v') && (addornements[1] == '*'))
    {
        // Vector of pointers
        // Remove "v"
        addornements = addornements.substr(1, 512);

        for (size_t i = 0; i < vector_size; i++)
        {
            // Create a new vector entry
            auto child = create_vector_entry(node_vector, i, item_address, vector_size, addornements);
            vec_children.push_back(child);
            item_address += sizeof(void*);
        }
        return vec_children;
    }

    if ((addornements[0] == 'v') && (addornements[1] == '{'))
    {
        // Vector of enums with a base-type
        // Remove "v"
        addornements = addornements.substr(1, 512);

        for (size_t i = 0; i < vector_size; i++)
        {
            // Create a new vector entry
            auto child           = create_vector_entry(node_vector, i, item_address, vector_size, addornements);
            auto item_size_maybe = MetadataServer.size_of_DF_Type(node_vector->m_enum_basetype);
            if (!item_size_maybe)
            {
                vec_children.clear();
                return vec_children;
            }
            vec_children.push_back(child);
            item_address += *item_size_maybe;
        }
        return vec_children;
    }

    return vec_children;
}

//
//---------------------------------------------------------------------------------------
//
void NodeBuilder::process_node_vector(rdf::NodeBase* p_node, const std::string& p_addornements)
{
    auto vector_node  = dynamic_cast<NodeVector*>(p_node);
    auto addornements = p_addornements;
    //vint**_t contains the base-type , remove it from the metadata
    auto index = p_addornements.find("vint");
    if (index != std::string::npos)
    {
        auto index2 = ++index;
        while (p_addornements[index2] != '_')
        {
            index2++;
        }
        if (p_addornements[index2 + 1] == 't')
        {
            auto base_type_st            = p_addornements.substr(index, index2 - index + 2);
            vector_node->m_enum_basetype = MetadataServer.DF_Type_from_string(base_type_st);
            addornements                 = addornements.substr(0, index) + addornements.substr(index2 + 2, 1024);
            if (addornements == "v")
                addornements = "";
        }
    }
    vector_node->m_addornements = addornements;
    p_node->m_has_children      = vector_has_data(vector_node);
}

} // namespace rdf