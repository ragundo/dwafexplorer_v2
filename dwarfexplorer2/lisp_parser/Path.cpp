#include "Path.h"
#include "DFNodeGenerator.h"

using namespace std;
using namespace rdf;

Path::Path()
{
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
Path::Path(NodeBase* p_node)
{
    m_tree_leaf = p_node;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
Path::Path(NodeParser& p_node)
{
    if (p_node.isNodeBase())
    {
        m_tree_leaf = p_node.getNodeBase();
        return;
    }
    if (p_node.isMetadata())
    {
        // Warning metadata only, no pointer
        m_subtree_nodes.push(p_node.getMetadata());
        m_size++;
    }
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool Path::is_valid()
{
    return m_tree_leaf || m_size > 0;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::push(NodeParser& p_node)
{
    if (p_node.isNodeBase())
    {
        m_tree_leaf = p_node.getNodeBase();
        clear_nodes();
        return;
    }
    if (p_node.isMetadata())
    {
        m_subtree_nodes.push(p_node);
        m_size++;
    }
    if (p_node.isConstant())
    {
        m_subtree_nodes.push(p_node);
        m_size++;
    }
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::pop()
{
    if (m_size > 0)
    {
        m_subtree_nodes.pop();
        m_size--;
        return;
    }
    if (m_tree_leaf)
    {
        m_tree_leaf = m_tree_leaf->parent();
        m_size      = 0;
        return;
    }
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser Path::top()
{
    if (m_size > 0)
        return m_subtree_nodes.top();
    if (m_tree_leaf)
        return NodeParser(m_tree_leaf);
    return NodeParser();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::string Path::to_string()
{
    std::string result = "";
    if (m_size > 0)
    {
        auto copy  = m_subtree_nodes;
        auto entry = copy.top();
        result     = entry.get_node_name();
        copy.pop();
        while (!copy.empty())
        {
            entry     = copy.top();
            auto data = entry.get_node_name();

            if (result[0] == '[')
            {
                result = data + result;
                copy.pop();
                continue;
            }
            result = data + "." + result;
            copy.pop();
        }
    }

    if (m_tree_leaf)
    {
        if (result.empty())
            return m_tree_leaf->path();

        if (result[0] == '[')
            result = m_tree_leaf->path() + result;
        else
        {
            if (!result.empty())
                result = m_tree_leaf->path() + "." + result;
        }
    }

    return result;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::clear()
{
    m_size = 0;
    clear_nodes();
    m_tree_leaf = nullptr;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::clear_nodes()
{
    m_size = 0;
    while (!m_subtree_nodes.empty())
        m_subtree_nodes.pop();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool Path::hasError()
{
    return !m_error_text.empty();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool Path::hasValue()
{
    return !m_path_value.empty();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::setError(const std::string& p_error)
{
    m_error_text = p_error;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::string Path::getError()
{
    return m_error_text;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::setValue(const std::string& p_value)
{
    m_path_value = p_value;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::string Path::getValue()
{
    if (!m_path_value.empty())
        return m_path_value;
    return to_string();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void Path::append_nodes(Path& p_path)
{
    if (p_path.top().isNodeBase())
    {
        return;
    }

    // add nodes
    std::stack<NodeParser> copy_stack(p_path.m_subtree_nodes);
    std::stack<NodeParser> reverse_stack;
    while (!copy_stack.empty())
    {
        reverse_stack.push(copy_stack.top());
        copy_stack.pop();
    }
    while (!reverse_stack.empty())
    {
        m_subtree_nodes.push(reverse_stack.top());
        reverse_stack.pop();
    }
}