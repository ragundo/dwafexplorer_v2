/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */
#ifndef PATH_H
#define PATH_H

#include "NodeParser.h"
#include <stack>
#include <string>

namespace rdf
{
class Path
{
    NodeBase*              m_tree_leaf{nullptr}; // The last leaf of the tree path that is a NodeBase
    std::stack<NodeParser> m_subtree_nodes; // Children of m_tree_leaf that are Metadata;
    int                    m_size{0};
    std::string            m_error_text{""};
    std::string            m_path_value{""};

  public:
    Path();
    Path(NodeBase* p_node);
    Path(NodeParser& p_node);

    bool        is_valid();
    void        push(NodeParser& p_node);
    void        pop();
    NodeParser  top();
    std::string to_string();
    void        clear();
    void        clear_nodes();
    bool        hasError();
    bool        hasValue();
    void        setError(const std::string& p_error);
    std::string getError();
    void        setValue(const std::string& p_value);
    std::string getValue();
    void        append_nodes(Path& p_path);
};
} // namespace rdf
#endif