/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef NODE_PARSER_H
#define NODE_PARSER_H
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include <stack>
#include <string>

namespace rdf
{
class NodeParser
{
    rdf::NodeBase*         m_pointer{nullptr};
    rdf::DF_MetadataGlobal m_metadata;
    int32_t                m_constant{-1};
    std::string            m_node_value{""};
    std::string            m_error_text{""};

  public:
    NodeParser()
        : m_pointer(nullptr) {}
    NodeParser(rdf::NodeBase* p_pointer, rdf::DF_MetadataGlobal& p_metadata)
        : m_pointer(p_pointer), m_metadata(p_metadata) {}
    NodeParser(rdf::NodeBase* p_pointer)
        : m_pointer(p_pointer) {}
    NodeParser(const rdf::DF_MetadataGlobal& p_metadata)
        : m_metadata(p_metadata) {}
    NodeParser(int32_t p_constant)
        : m_constant(p_constant) {}
    NodeParser(const std::string& p_error)
        : m_metadata(), m_error_text(p_error) {}

    bool               isValid() const;
    bool               isNodeBase() const;
    bool               isMetadata() const;
    bool               isConstant() const;
    NodeBase*          getNodeBase();
    DF_MetadataGlobal& getMetadata();
    int32_t            getConstant();
    DF_Type            get_DF_Type();
    RDF_Type           get_RDF_Type();
    std::string        get_node_name();
    std::string        get_node_value();
    std::string        get_addornements();
    std::string        get_error();
    uint64_t           get_address();
    bool               hasError() const;
    void               setError(const std::string& p_error);
    void               set_node_name(const std::string& p_value);
    void               set_node_value(const std::string& p_value);
    void               set_rdf_type(const rdf::RDF_Type& p_rdf_type);
};

} // namespace rdf
#endif // NODE_PARSER_H