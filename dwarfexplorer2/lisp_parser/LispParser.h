/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef LISP_PARSER_H
#define LISP_PARSER_H
#include "MetadataServer.h"
#include "NodeParser.h"
#include "Path.h"
#include <string>
#include <utility>

class DF_Model;

namespace rdf
{
class LispParser
{
  public:
    static std::string parse_hyperlink(rdf::NodeBase* p_selected_node, DF_Model* p_model);

  private:
    static NodeParser parse_lisp_function(rdf::NodeBase* p_selected_node,
                                          std::string&   p_expression);
};

} // namespace rdf

#endif //LISP_PARSER_H