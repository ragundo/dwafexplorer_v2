/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "LispParser.h"
#include "DFNodeGenerator.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_utils.h"
#include "metadata_server/offsets_cache.h"
#include "tl/tl_optional.h"
#include <cctype>
#include <sstream>

using namespace std;
using namespace rdf;

extern CMetadataServer MetadataServer;

int64_t aux_value_special;

void parse_lisp_expression(Path&         p_current_path,
                           NodeParser    p_source_node,
                           const string& p_lisp_expression,
                           int           p_aux_value,
                           DF_Model*     p_model);

//
//--------------------------------------------------------------------------------------------------------------------//
//
pair<string, int> get_parentesis_expression(const std::string& p_expression, size_t start)
{
    if (p_expression[start] != '(')
        return pair<string, int>("", string::npos);
    auto pos = start + 1;
    while (pos < p_expression.length() && p_expression[pos] != ')')
        pos++;
    if (pos == p_expression.length())
        return pair<string, int>("", string::npos);
    return pair<string, int>(p_expression.substr(start + 1, pos - start - 1), pos + 1);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
pair<string, int> get_array_expression(const std::string& p_expression, size_t start)
{
    if (p_expression[start] != '[')
        return pair<string, int>("", string::npos);
    auto pos = start + 1;
    while (pos < p_expression.length() && p_expression[pos] != ']')
        pos++;
    if (pos == p_expression.length())
        return pair<string, int>("", string::npos);
    return pair<string, int>(p_expression.substr(start + 1, pos - start - 1), pos + 1);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
pair<string, int> get_parameter(const std::string& p_expression, size_t start)
{
    auto pos = start;
    while (pos < p_expression.length() && p_expression[pos] != ' ')
        pos++;
    return pair<string, int>(p_expression.substr(start, pos - start), pos + 1);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string get_identifier(const string p_expression, size_t pos)
{
    int    delta          = 0;
    string end_delimiters = "$( [.";
    while ((pos + delta < p_expression.size()) &&
           (end_delimiters.find(p_expression[pos + delta]) == string::npos))
        delta++;
    if (delta == 0)
        return "";
    string identifier = p_expression.substr(pos, delta);
    return identifier;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::size_t get_vector_size(NodeParser& p_node_vector)
{
    auto start                = p_node_vector.get_address();
    auto vector_start_address = reinterpret_cast<uint64_t*>(start);
    //auto end                  = vector_start_address + sizeof(void*);
    auto end                = start + sizeof(void*);
    auto vector_end_address = reinterpret_cast<uint64_t*>(end);

    if (*vector_start_address == 0)
        return 0;

    auto diff = *vector_end_address - *vector_start_address;
    if (p_node_vector.get_DF_Type() == DF_Type::Void)
        return diff / sizeof(void*);

    if (p_node_vector.get_addornements().empty())
    {
        // Vector of DF_Types
        if (p_node_vector.get_DF_Type() == DF_Type::Void)
            // Vector of void pointers
            return diff / sizeof(void*);
        // Vector df DF type
        auto size_maybe = MetadataServer.size_of_DF_Type(p_node_vector.get_DF_Type());
        if (p_node_vector.isNodeBase())
        {
            auto node_vec = dynamic_cast<NodeVector*>(p_node_vector.getNodeBase());
            if (node_vec->m_enum_basetype != DF_Type::None)
                size_maybe = MetadataServer.size_of_DF_Type(node_vec->m_enum_basetype);
        }
        return diff / *size_maybe; // TODO
    }

    // Remove "v"
    std::string addornements = p_node_vector.get_addornements().substr(1, 512);
    if (addornements[0] == '*')
    {
        // Vector of pointers
        return diff / sizeof(void*);
    }
    if (addornements[0] == '[')
    {
        // Vector of arrays
        return 0; // TODO
    }

    auto size_maybe = MetadataServer.size_of_DF_Type(p_node_vector.get_DF_Type());

    // Check for enum base-type
    auto index = addornements.find("vint");
    if (index != std::string::npos)
    {
        auto index2 = ++index;
        while (addornements[index2] != '_')
        {
            index2++;
        }
        if (addornements[index2 + 1] == 't')
        {
            auto base_type_st  = addornements.substr(index, index2 - index + 2);
            auto enum_basetype = MetadataServer.DF_Type_from_string(base_type_st);
            size_maybe         = MetadataServer.size_of_DF_Type(enum_basetype);
        }
    }

    return diff / *size_maybe;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
tl::optional<int> get_array_size(const string& p_addornements)
{
    size_t pos = 0;
    while ((pos < p_addornements.size()) && (p_addornements[pos++] != '['))
    {
    }
    if (pos == p_addornements.size())
        return {};
    auto pos2 = pos;
    while ((pos2 < p_addornements.size()) && (std::isdigit(pos2++)))
    {
    }
    auto size_as_st = p_addornements.substr(pos, pos2 - pos + 1);
    return std::stoi(size_as_st);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int64_t get_index_vector(const string& p_string)
{
    int64_t value;
    if (p_string[0] == '[')
    {
        auto it = 1;
        while ((p_string[it] == ' ') || (isdigit(p_string[it])))
        {
            it++;
        }
        if (p_string[it] == ']')
        {
            string idx = p_string.substr(1, it - 1);
            value      = stol(idx);
            return value;
        }
    }
    return -12345;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string translate_refers_to_expression(const string& p_expression)
{
    if (p_expression == "$")
        return "@";

    auto   result_expression = p_expression;
    size_t index             = 0;
    while (index < result_expression.size())
    {
        if (result_expression[index] != '$')
        {
            index++;
            continue;
        }
        size_t index2 = index + 1;
        while (index2 < result_expression.size() && result_expression[index2] == '$')
        {
            index2++;
        }
        if (index2 - index == 1)
        {
            result_expression[index] = '@';
        }
        index = index2;
    }
    return result_expression;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void initialize_node_simple(NodeBase* p_node)
{
    switch (p_node->m_df_type)
    {
        case rdf::DF_Type::int64_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<int64_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::uint64_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<uint64_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::int32_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<int32_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::uint32_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<uint32_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::int16_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<int16_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::uint16_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<uint16_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::int8_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<int8_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::uint8_t:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<uint8_t*>(p_node->m_address)));
            return;
        case rdf::DF_Type::Long:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<long*>(p_node->m_address)));
            return;
        case rdf::DF_Type::S_float:
        case rdf::DF_Type::D_float:
        case rdf::DF_Type::S_double:
            p_node->m_model_data.m_value = std::to_string(*(reinterpret_cast<float*>(p_node->m_address)));
            return;
        default:
            break;
    }
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int64_t get_node_simple(uint64_t p_address, DF_Type p_df_type)
{
    switch (p_df_type)
    {
        case rdf::DF_Type::int64_t:
            return *(reinterpret_cast<int64_t*>(p_address));
        case rdf::DF_Type::uint64_t:
            return *(reinterpret_cast<uint64_t*>(p_address));
        case rdf::DF_Type::int32_t:
            return *(reinterpret_cast<int32_t*>(p_address));
        case rdf::DF_Type::uint32_t:
            return *(reinterpret_cast<uint32_t*>(p_address));
        case rdf::DF_Type::int16_t:
            return *(reinterpret_cast<int16_t*>(p_address));
        case rdf::DF_Type::uint16_t:
            return *(reinterpret_cast<uint16_t*>(p_address));
        case rdf::DF_Type::int8_t:
            return *(reinterpret_cast<int8_t*>(p_address));
        case rdf::DF_Type::uint8_t:
            return *(reinterpret_cast<uint8_t*>(p_address));
        case rdf::DF_Type::Long:
            return *(reinterpret_cast<long*>(p_address));
        case rdf::DF_Type::S_float:
        case rdf::DF_Type::D_float:
        case rdf::DF_Type::S_double:
            return *(reinterpret_cast<float*>(p_address));
        default:
            break;
    }
    return -1234;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int64_t get_node_simple(DF_MetadataGlobal& p_metadata)
{
    return get_node_simple(p_metadata.m_address,
                           p_metadata.m_df_type);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int64_t get_node_simple(NodeParser& p_node)
{
    return get_node_simple(p_node.get_address(),
                           p_node.get_DF_Type());
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser process_parameter(Path&       p_current_path,
                             NodeParser& p_source_node,
                             string&     param_text,
                             int64_t     p_aux_value,
                             DF_Model*   p_model)
{
    // Remove the current node because it w
    p_current_path.pop();
    parse_lisp_expression(p_current_path,
                          p_source_node,
                          param_text,
                          p_aux_value,
                          p_model);
    return p_current_path.top();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_art_image()
{
    return "$(find-instance $art_image_chunk $$$).images[$]";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_caste_raw()
{
    return "$global.world.raws.creatures.all[$$$].caste[$]";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_entity_tissue_style()
{
    return "(find-by-id $(find-instance $historical_entity $$$).tissue_styles.all $id $)";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_interaction_source()
{
    return "(find-by-id $(find-instance $interaction $$$).sources $id $)";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_material()
{
    return "(material-by-id $ $$$)";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_activity_event()
{
    return "(find-by-id $(find-instance $activity_entry $$$).events $event_id $))";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_plant_growth()
{
    return "$global.world.raws.plants.all[$$$].growths[$]";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_unit_action()
{
    return "(find-by-id $(find-instance $unit $$$).actions $id $))";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_abstract_building()
{
    return "(find-by-id $(find-instance $world_site $$$).buildings $id $)";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_site_realization_building()
{
    return "(find-by-id $(find-instance $world_site $$$).realization.buildings $id $)";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
string find_instance_code_helper(const string& p_type)
{
    if (p_type == "art_image")
        return find_instance_art_image();
    if (p_type == "caste_raw")
        return find_instance_caste_raw();
    if (p_type == "entity_tissue_style")
        return find_instance_entity_tissue_style();
    if (p_type == "interaction_source")
        return find_instance_interaction_source();
    if (p_type == "material")
        return find_instance_material();
    if (p_type == "activity_event")
        return find_instance_activity_event();
    if (p_type == "plant_growth")
        return find_instance_plant_growth();
    if (p_type == "unit_action")
        return find_instance_unit_action();
    if (p_type == "abstract_building")
        return find_instance_abstract_building();
    if (p_type == "site_realization_building")
        return find_instance_site_realization_building();
    return "";
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser find_instance(NodeBase* p_node, DF_Type p_df_type, string& p_key, string& p_aux_key)
{
    switch (p_df_type)
    {
        case DF_Type::art_image:
            //return find_instance_art_image(p_node, p_key);
            break;
        default:
            break;
    }
    DF_MetadataGlobal result;
    result.m_comment = "Unknow find-instance";
    return result;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser find_by_id_code_helper(NodeBase* p_node, string& p_vector, string& p_key, int64_t p_value)
{
    return NodeParser();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser material_by_id_code_helper(Path&       p_current_path,
                                      NodeParser& p_source_node,
                                      string&     expression,
                                      size_t&     pos,
                                      int64_t     p_aux_value,
                                      DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return NodeParser();
    }
    pos++;
    auto param_1_pair     = get_parameter(expression, pos);
    auto param_1_mat_type = param_1_pair.first;
    pos                   = param_1_pair.second;

    auto param_2_pair         = get_parameter(expression, pos);
    auto param_2_mat_type_idx = param_2_pair.first;
    pos                       = param_2_pair.second;

    // param1 y param2 pueden ser $ o 0, -1

    Path path_param1;
    Path path_param2;
    auto mat_type_node      = process_parameter(path_param1,
                                           p_source_node,
                                           param_1_mat_type,
                                           p_aux_value,
                                           p_model);
    auto mat_type_value     = get_node_simple(mat_type_node);
    auto mat_type_idx_node  = process_parameter(path_param2,
                                               p_source_node,
                                               param_2_mat_type_idx,
                                               p_aux_value,
                                               p_model);
    auto mat_type_idx_value = get_node_simple(mat_type_idx_node);

    /*
    <ragundo> So it mat-idx is present and > 0 the expression is false?
<_Q> yes
<_Q> which means it wouldn't match Coke or Charcoal
<_Q> but would match all of the other hardcoded mats
<_Q> the next one is for mat-type==0 (INORGANIC), which returns the specified inorganic raw if it exists, otherwise "magma"
<_Q> (i.e. INORGANIC:NONE)
<_Q> then 19-218 for CREATURE_MAT, 219-418 for what's effectively HISTFIG_MAT, and 419-618 for PLANT_MAT
<_Q> I guess the last case is mostly superfluous, just there to catch anything that wasn't explicitly defined
<_Q> I guess _that's_ what will catch coke/charcoal
Q> food-mat-by-idx relies on the organic_mat_category enum
<_Q> world.raws.mat_table
<ragundo> like the one for df.world_region_feature
<_Q> the first parameter is Meat/Fish/UnpreparedFish/Eggs/Plants/etc.
<_Q> which corresponds to the index into mat_table.organic_types/organic_indexes
<_Q> (which are arrays of vectors)
<_Q> and the second parameter is the index into those vectors
<_Q> categories 1/2/3 (fish/rawfish/eggs) give you race/caste, while the others give you type/index
    */
    if (mat_type_idx_value < 0 && mat_type_value > 0)
    {
        // $raws.mat_table.builtin[mat_type_value]
        string vector = "$global.world.raws.mat_table.builtin[$]";
        parse_lisp_expression(p_current_path,
                              mat_type_node,
                              vector,
                              0,
                              p_model);
    }

    if (mat_type_value == 0)
    {
        //$raws.inorganics[mat-idx].material || $raws.mat_table.builtin[0]
        // 1st expression
        Path   p2;
        string vector1 = "$global.world.raws.inorganics[$].material";
        parse_lisp_expression(p2,
                              mat_type_idx_node,
                              vector1,
                              0,
                              p_model);

        // 2nd expression
        Path              p3;
        string            vector2 = "$global.world.raws.mat_table.builtin[$]";
        DF_MetadataGlobal literal;
        aux_value_special = 0;
        literal.m_address = reinterpret_cast<uint64_t>(&aux_value_special);
        literal.m_df_type = DF_Type::int64_t;
        NodeParser param(literal);
        parse_lisp_expression(p3,
                              param,
                              vector2,
                              0,
                              p_model);
        // TODO or
    }
    if (mat_type_value >= 19 && mat_type_value <= 218)
    {
        // $raws.creatures.all[mat-idx].material[ mat-type - 19] || $raws.mat_table.builtin[19]
        // 1st expression
        Path   p4;
        string vector3 = "$global.world.raws.all.material";
        parse_lisp_expression(p4,
                              mat_type_idx_node,
                              vector3,
                              0,
                              p_model);

        // TODO access  rest of expression

        // 2nd expression
        string            vector4 = "$global.world.raws.mat_table.builtin[$]";
        DF_MetadataGlobal literal;
        aux_value_special = 19;
        literal.m_address = reinterpret_cast<uint64_t>(&aux_value_special);
        literal.m_df_type = DF_Type::int64_t;
        NodeParser param(literal);
        parse_lisp_expression(p4,
                              param,
                              vector4,
                              0,
                              p_model);
        // TODO or
    }
    if (mat_type_value >= 219 && mat_type_value <= 418)
    {
        // hfig = (find-figure mat-idx)
        // $raws.creatures.all[$hfig.race].material[ mat-type - 219] || $raws.mat_table.builtin[19])
        Path              p6;
        DF_MetadataGlobal literal;
        string            expression_hfig = "(find-figure $)";
        parse_lisp_expression(p6,
                              mat_type_idx_node,
                              expression_hfig,
                              0,
                              p_model);

        Path   p7;
        string vector7 = "$global.world.raws.all.[$].material";
        parse_lisp_expression(p7,
                              p6.top(),
                              vector7,
                              0,
                              p_model);
        // TODO access  rest of expression

        Path p8;
        aux_value_special = 219;
        literal.m_address = reinterpret_cast<uint64_t>(&aux_value_special);
        literal.m_df_type = DF_Type::int64_t;
        NodeParser param(literal);
        string     vector8 = "$global.world.raws.mat_table.builtin[$]";
        parse_lisp_expression(p8,
                              param,
                              vector8,
                              0,
                              p_model);
    }
    if (mat_type_value >= 419 && mat_type_value <= 618)
    {
        //$raws.plants.all[mat-idx].material[ mat-type- 419] || $raws.mat_table.builtin[419])
        Path   p9;
        string vector9 = "$global.world.raws.all.material";

        // 1st expression
        parse_lisp_expression(p9,
                              mat_type_idx_node,
                              vector9,
                              0,
                              p_model);
        // TODO access  rest of expression

        // 2nd expression
        Path              p10;
        string            vector10 = "$global.world.raws.mat_table.builtin[$]";
        DF_MetadataGlobal literal;
        aux_value_special = 419;
        literal.m_address = reinterpret_cast<uint64_t>(&aux_value_special);
        literal.m_df_type = DF_Type::int64_t;
        NodeParser param(literal);
        parse_lisp_expression(p10,
                              param,
                              vector10,
                              0,
                              p_model);
    }
    if (mat_type_value < 0)
    {
        //$raws.mat_table.builtin[mat-type]
        string vector = "$global.world.raws.mat_table.builtin[$]";
        parse_lisp_expression(p_current_path,
                              mat_type_node,
                              vector,
                              0,
                              p_model);
    }
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser food_mat_by_idx_code_helper(NodeBase* p_node, int64_t p_param1, int64_t p_parama2)
{
    return NodeParser();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool check_index_type(NodeParser& p_node)
{
    if (p_node.isConstant())
        return true;
    auto df_type = p_node.get_DF_Type();
    switch (df_type)
    {
        case DF_Type::int8_t:
        case DF_Type::int16_t:
        case DF_Type::int32_t:
        case DF_Type::int64_t:
        case DF_Type::uint8_t:
        case DF_Type::uint16_t:
        case DF_Type::uint32_t:
        case DF_Type::uint64_t:
            return true;
        default:
            break;
    }
    return false;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void global_worker(Path& p_current_path)
{
    auto node  = p_current_path.top();
    bool found = true;
    // go to the parent struct, class, compound or pointer
    RDF_Type it_rdf = node.get_RDF_Type();
    while (it_rdf != RDF_Type::Class &&
           it_rdf != RDF_Type::Struct &&
           it_rdf != RDF_Type::Pointer)
    {
        p_current_path.pop();
        node = p_current_path.top();
        if (!node.isValid())
        {
            found = false;
            break;
        }
        auto item_st = MetadataServer.DF_Type_to_string(node.get_DF_Type());
        if (item_st.find("_T_") != string::npos)
            continue;
        it_rdf = node.get_RDF_Type();

        // Compounds can have a global typename
        if (it_rdf == RDF_Type::Compound)
        {
            found = true;
            break;
        }
    }

    if (!found)
    {
        NodeParser result;
        result.setError("ERROR invalid NodeParser");
        p_current_path.push(result);
    }
    else
        p_current_path.push(node);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser compound_worker(Path& p_current_path)
{
    auto node  = p_current_path.top();
    bool found = true;
    // go to the parent struct, class, compound or pointer
    RDF_Type it_rdf = node.get_RDF_Type();
    while (it_rdf != RDF_Type::Class &&
           it_rdf != RDF_Type::Struct &&
           it_rdf != RDF_Type::Compound &&
           it_rdf != RDF_Type::Pointer)
    {
        p_current_path.pop();
        node = p_current_path.top();
        if (!node.isValid())
        {
            found = false;
            break;
        }
        it_rdf = node.get_RDF_Type();
    }

    if (!found)
        return NodeParser("EROR invalid NodeParser");
    return node;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser get_any_compound_parent(NodeBase* p_node)
{
    // go to the parent struct, class, compound or pointer
    auto it = p_node;
    while (it &&
           it->m_rdf_type != RDF_Type::Compound &&
           it->m_rdf_type != RDF_Type::Class &&
           it->m_rdf_type != RDF_Type::Struct &&
           it->m_rdf_type != RDF_Type::Pointer)
        it = it->parent();
    if (it)
        return NodeParser(it);
    return NodeParser("ERROR in get_any_compound_parent");
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_Type get_enum_basetype(NodeParser& p_enum_node)
{
    if (p_enum_node.isNodeBase())
    {
        NodeEnum* n_enum = dynamic_cast<NodeEnum*>(p_enum_node.getNodeBase());
        if (n_enum)
        {
            if (n_enum->m_base_type != DF_Type::None)
                return n_enum->m_base_type;
            return MetadataServer.get_enum_base_type(n_enum->m_df_type);
        }

        return DF_Type::None;
    }
    // Metadata
    auto& metadata     = p_enum_node.getMetadata();
    auto  addornements = metadata.m_addornements;
    if (!addornements.empty())
        return MetadataServer.DF_Type_from_string(addornements);
    return DF_Type::None;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser find_field_with_value_in_vector(NodeParser p_vector_node,
                                           string&    p_key_field,
                                           int64_t    p_value)
{
    auto df_type_size_maybe = MetadataServer.size_of_DF_Type(p_vector_node.get_DF_Type());
    auto df_type_size       = *df_type_size_maybe; // TODO
    // Locate the offset of the key-field respecto to the structure beginning
    auto data = OffsetsCache::get_offset(p_vector_node.get_DF_Type(), p_key_field);
    if (!data)
    {
        return NodeParser("Unknown field");
    }
    auto field_offset   = *data;
    auto vector_address = reinterpret_cast<uint64_t*>(p_vector_node.get_address());
    auto vector_start   = *vector_address;

    auto field_metadata_maybe = MetadataServer.find_field(p_vector_node.get_DF_Type(),
                                                          p_key_field);
    auto field_type           = DF_Type::None;
    if (!field_metadata_maybe)
    {
        //TODO mirar en base classes
        return NodeParser("ERROR key not found");
    }

    field_type = (*field_metadata_maybe).m_df_type;

    auto vector_size = get_vector_size(p_vector_node);

    //TODO usar busqueda binaria
    for (size_t vector_index = 0; vector_index < vector_size; vector_index++)
    {
        uint64_t vector_entry = 0;
        if (p_vector_node.get_addornements() == "v*")
        {
            // it's a vector of pointers to df_type
            // Follow the pointer
            auto pointer_address = reinterpret_cast<uint64_t*>(vector_start + vector_index * sizeof(void*));
            if (pointer_address == nullptr)
                continue;
            vector_entry = *pointer_address;
        }
        else // Vector of df_type
            vector_entry = vector_start + vector_index * df_type_size;

        auto field_address      = vector_entry + field_offset;
        auto vector_entry_value = get_node_simple(field_address,
                                                  field_type);
        if (vector_entry_value == p_value)
        {
            DF_MetadataGlobal result;
            result.m_name     = "[" + std::to_string(vector_index) + "]";
            result.m_address  = vector_entry;
            result.m_df_type  = p_vector_node.get_DF_Type();
            result.m_rdf_type = MetadataServer.df_2_rdf(p_vector_node.get_DF_Type());
            NodeParser result_node(result);
            return result_node;
        }
    }
    // Key not found
    NodeParser result;
    result.set_node_value("N/A");
    result.setError("ERROR key not found");
    return result;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser locate_entry_in_vector(NodeParser& p_node_vector,
                                  int64_t     p_entry)
{
    auto      df_type_size_maybe    = MetadataServer.size_of_DF_Type(p_node_vector.get_DF_Type());
    uint64_t* vector_address        = reinterpret_cast<uint64_t*>(p_node_vector.get_address());
    auto      vector_start          = *vector_address;
    auto      vector_items_rdf_type = MetadataServer.df_2_rdf(p_node_vector.get_DF_Type());
    auto      enum_type             = DF_Type::None;

    if (vector_items_rdf_type == RDF_Type::Enum)
    {
        // Check for base type
        if (p_node_vector.isNodeBase())
        {
            auto node_vector = dynamic_cast<NodeVector*>(p_node_vector.getNodeBase());
            if (node_vector)
            {
                enum_type = node_vector->m_df_type;
                if (node_vector->m_enum_basetype != DF_Type::None)
                {
                    enum_type          = node_vector->m_enum_basetype;
                    df_type_size_maybe = MetadataServer.size_of_DF_Type(enum_type);
                }
            }
        }
        else
        {
            string addornnemets = p_node_vector.get_addornements();
            auto   enum_type    = MetadataServer.DF_Type_from_string(addornnemets);
            df_type_size_maybe  = MetadataServer.size_of_DF_Type(enum_type);
        }
    }

    auto vector_size = get_vector_size(p_node_vector);

    if (vector_size == 0)
    {
        // Empty vector
        NodeParser result;
        result.set_node_value("N/A");
        return result;
    }
    if ((size_t)p_entry >= vector_size)
    {
        // Out of bounds
        NodeParser result;
        result.set_node_value("N/A");
        result.setError("ERROR: Index out of bounds");
        return result;
    }
    if (p_node_vector.get_addornements().substr(0, 2) == "v*")
    {
        // it's a vector of pointers to df_type
        // Follow the pointer
        DF_MetadataGlobal result;
        result.m_name     = "[" + std::to_string(p_entry) + "]";
        result.m_address  = vector_start + p_entry * sizeof(void*);
        result.m_df_type  = p_node_vector.get_DF_Type();
        result.m_rdf_type = RDF_Type::Pointer;
        NodeParser node_result(result);
        return node_result;
    }
    // Vector of df_type

    DF_MetadataGlobal result;
    result.m_name     = "[" + std::to_string(p_entry) + "]";
    result.m_address  = vector_start + p_entry * *df_type_size_maybe; // TODO
    result.m_df_type  = p_node_vector.get_DF_Type();
    result.m_rdf_type = MetadataServer.df_2_rdf(p_node_vector.get_DF_Type());
    if (vector_items_rdf_type == RDF_Type::Enum)
        result.m_addornements = MetadataServer.DF_Type_to_string(enum_type);
    NodeParser node_result(result);
    return node_result;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool is_array_of_pointers(const string p_expression)
{
    if (p_expression[0] != '[')
        return false;
    size_t it = 1;
    while ((it < p_expression.length()) && std::isdigit(p_expression[it++]))
    {
    }
    return p_expression[it] == '*';
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool is_vector_of_pointers(const string p_expression)
{
    if (p_expression[0] != 'v')
        return false;
    return p_expression[1] == '*';
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool is_vector_type(NodeParser& p_node)
{
    if (p_node.isNodeBase())
        return p_node.getNodeBase()->m_rdf_type == RDF_Type::Vector;
    else
        return p_node.getMetadata().m_rdf_type == RDF_Type::Vector;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool is_array_type(NodeParser& p_node)
{
    if (p_node.isNodeBase())
        return p_node.getNodeBase()->m_rdf_type == RDF_Type::Array;
    else
        return p_node.getMetadata().m_rdf_type == RDF_Type::Array;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool is_vector_or_array(NodeParser& p_node)
{
    return is_vector_type(p_node) || is_array_type(p_node);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
bool is_node_expanded(Node* p_node)
{
    if (p_node->get_num_children() == 0)
        return false;
    if (p_node->get_num_children() > 1)
        return true;
    if (p_node->has_dummy_node())
        return false;
    return true;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeBase* locate_field(Node* p_node, const string& p_identifier)
{
    // TODO buscar en anonymous unions and structs
    for (size_t i = 0; i < p_node->get_num_children(); i++)
    {
        auto child = p_node->get_child(i);
        if (child->m_field_name == p_identifier)
            return child;
    }
    auto key1 = p_node->m_field_path +
                (p_node->m_field_name[0] != '[' ? "." : "") +
                p_node->m_field_name;
    auto key = key1 +
               (p_identifier[0] != '[' ? "." : "") +
               p_identifier;
    auto node_maybe = p_node->m_node_table->find_node(key);
    if (!node_maybe)
        return nullptr;
    return *node_maybe;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void expand_node(Node*         p_node,
                 const string& p_child_field_name,
                 DF_Model*     p_model)
{
    auto   node_table = p_node->m_node_table;
    string expression;
    if (p_node->m_node_type == NodeType::Global)
        expression = "df.global";
    else
    {
        auto aux_expression = p_node->m_field_path +
                              (p_node->m_field_name[0] != '[' ? "." : "") +
                              p_node->m_field_name;
        expression = aux_expression +
                     (p_child_field_name[0] != '[' ? "." : "") +
                     p_child_field_name;
    }

    auto node_maybe = node_table->find_node(expression);
    if (node_maybe)
        return;

    DFNode_Generator::process_expression(expression,
                                         node_table);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser locate_entry_in_array(NodeParser& p_node_array,
                                 int64_t     p_entry)
{
    auto df_type            = p_node_array.get_DF_Type();
    auto addornements_maybe = MetadataServer.get_addornements_metadata(df_type, p_node_array.get_node_name());
    auto array_size         = -1;
    if (addornements_maybe)
    {
        auto addornements = *addornements_maybe;
        // Compute the array size
        auto it = 1;
        while ((size_t)it < addornements.size() && std::isdigit(addornements[it]))
            it++;
        auto array_size_as_string = addornements.substr(1, std::min(1, it - 1));
        array_size                = std::stoi(array_size_as_string);
        if (p_entry >= array_size)
        {
            NodeParser result;
            result.set_node_value("N/A");
            result.setError("ERROR: Index out of bounds");
            return result;
        }
    }

    auto df_type_size_maybe = MetadataServer.size_of_DF_Type(df_type);
    auto rdf_type           = MetadataServer.df_2_rdf(p_node_array.get_DF_Type());
    auto enum_type          = DF_Type::None;
    if (rdf_type == RDF_Type::Enum)
    {
        if (p_node_array.isNodeBase())
        {
            auto node_array = dynamic_cast<NodeArray*>(p_node_array.getNodeBase());
            if (node_array)
            {
                enum_type = node_array->m_df_type;
                if (node_array->m_enum_basetype != DF_Type::None)
                    df_type_size_maybe = MetadataServer.size_of_DF_Type(node_array->m_enum_basetype);
            }
        }
        else
        {
            string addornnemets = p_node_array.get_addornements();
            auto   type         = MetadataServer.DF_Type_from_string(addornnemets);
            df_type_size_maybe  = MetadataServer.size_of_DF_Type(type);
        }
    }
    auto array_address = p_node_array.get_address();

    if (is_array_of_pointers(p_node_array.get_addornements()))
    {
        // it's a vector of pointers to df_type
        // Follow the pointer
        DF_MetadataGlobal result;
        result.m_name     = "[" + std::to_string(p_entry) + "]";
        result.m_address  = array_address + p_entry * sizeof(void*);
        result.m_df_type  = p_node_array.get_DF_Type();
        result.m_rdf_type = RDF_Type::Pointer;
        return NodeParser(result);
    }

    // Array of df_type

    DF_MetadataGlobal result;
    result.m_name     = "[" + std::to_string(p_entry) + "]";
    result.m_address  = array_address + p_entry * *df_type_size_maybe; // TODO
    result.m_df_type  = p_node_array.get_DF_Type();
    result.m_rdf_type = rdf_type;
    if (rdf_type == RDF_Type::Enum)
        result.m_addornements = MetadataServer.DF_Type_to_string(enum_type);

    return NodeParser(result);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
NodeParser get_field_from_identifier(const string& identifier,
                                     NodeParser    p_node_parser,
                                     DF_Model*     p_model)
{
    if (p_node_parser.isNodeBase())
    {
        // identifier is not array or vector or pointer
        auto node = dynamic_cast<Node*>(p_node_parser.getNodeBase());
        if (node == nullptr)
        {
            // identifier is not a field, or is a field with no children, not possible with []
            return NodeParser("ERROR [] in non container");
        }

        if (!is_node_expanded(node))
            expand_node(node, identifier, p_model);

        // Look for the identifier as a field of destination_node
        auto child = locate_field(node, identifier);
        if (child != nullptr)
        {
            // It's a field
            NodeParser field(child);
            return field;
        }
        // Not a field
        return NodeParser();
    }

    // locate a field using DF_MetatadataGlobal
    // Use expression_resutl instead of destination_node
    // Check for field
    auto maybe_field = MetadataServer.find_field(p_node_parser.get_DF_Type(),
                                                 identifier);
    if (maybe_field)
    {
        auto field_data = *maybe_field;

        auto offset = OffsetsCache::get_offset(p_node_parser.get_DF_Type(),
                                               identifier);
        if (!offset)
        {
            return NodeParser();
        }

        auto maybe_field_addornements = MetadataServer.get_addornements_metadata(p_node_parser.get_DF_Type(),
                                                                                 identifier);

        // Update value
        DF_MetadataGlobal child;
        if (maybe_field_addornements)
            child.m_addornements = *maybe_field_addornements;
        child.m_comment  = identifier;
        child.m_df_type  = field_data.m_df_type;
        child.m_name     = field_data.m_name;
        child.m_rdf_type = field_data.m_rdf_type;
        if (p_node_parser.get_RDF_Type() == RDF_Type::Pointer)
        {
            // Follow to pointee
            auto pointee_ptr = reinterpret_cast<uint64_t*>(p_node_parser.get_address());
            child.m_address  = *pointee_ptr + *offset;
        }
        else
            child.m_address = p_node_parser.get_address() + *offset;
        NodeParser result(child);
        return result;
    }
    return NodeParser();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void process_ref_target_aux_value(Path&       p_current_path,
                                  NodeParser& p_source_node,
                                  string&     p_aux_value,
                                  DF_Model*   p_model)
{
    // process aux-value

    // $$ = $$._parent
    string aux_value = p_aux_value.substr(2, 512);
    if (p_aux_value.substr(0, 3) == "$$.")
        aux_value = "$$._parent" + aux_value;

    // Parse the expression and get a node
    parse_lisp_expression(p_current_path,
                          p_source_node,
                          aux_value,
                          -1,
                          p_model);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int64_t get_ref_target_node_value(NodeParser p_current_node)
{
    if (p_current_node.isNodeBase())
    {
        NodeBase* p_node = p_current_node.getNodeBase();

        initialize_node_simple(p_node);
        auto value           = *p_node->m_model_data.m_value;
        auto value_as_number = std::stol(value);
        return value_as_number;
    }
    if (p_current_node.isMetadata())
    {
        auto& node_metadata = p_current_node.getMetadata();
        if (check_index_type(p_current_node))
        {
            return get_node_simple(p_current_node);
        }
    }
    return -12345;
}

//
//--------------------------------------------------------------------------------------------------------------------//
// aux-value $  = field value
// aux-value $$ = containing structure of the field ie $$.parent
// expression $$ = value of the aux-value
// expression $ = value of the field;
// $global.world.raws.creatures.all[$$].caste[$]
//                        aux-value ^^        ^ field value
void process_ref_target(Path&       p_current_path,
                        NodeParser& p_source_node,
                        string&     p_ref_target,
                        string&     p_aux_value,
                        DF_Model*   p_model)
{
    int64_t aux_value_as_num = -1;
    auto    node_actual      = p_current_path.top();
    // This is only for providing info , the values is calculated for each vector entry
    if (node_actual.get_RDF_Type() == rdf::RDF_Type::Vector)
        return;

    if (!p_aux_value.empty())
    {
        Path dummy_path;
        process_ref_target_aux_value(dummy_path,
                                     node_actual,
                                     p_aux_value,
                                     p_model);

        if (dummy_path.hasError())
            return;
        auto aux_value_node = dummy_path.top();
        aux_value_as_num    = get_node_simple(aux_value_node);
    }

    if (!p_aux_value.empty() && aux_value_as_num == -1)
    {
        p_current_path.setValue("N/A");
        return;
    }

    // Get the node value
    auto node_value_as_number = get_ref_target_node_value(node_actual);
    if (node_value_as_number == -1)
    {
        p_current_path.setValue("N/A");
        return;
    }

    // Get the type of the ref-target expression
    // it can be in a instance-vector or in a lisp function
    auto dest_df_type = MetadataServer.DF_Type_from_string(p_ref_target);
    if (dest_df_type == DF_Type::None)
    {
        p_current_path.setError("ERROR type not found");
        return;
    }

    string ref_target_string;
    // Look for the type in instance vector
    auto instance_vector_maybe = MetadataServer.get_instance_vector(dest_df_type);

    bool used_find_instance = false;
    if (instance_vector_maybe)
    {
        // The type is in instance-vector table
        ref_target_string = *instance_vector_maybe;
    }
    else
    {
        // Find the vector using find-instance method of th type
        ref_target_string  = find_instance_code_helper(p_ref_target);
        used_find_instance = true;
    }

    parse_lisp_expression(p_current_path,
                          p_source_node,
                          ref_target_string,
                          aux_value_as_num,
                          p_model);

    if (p_current_path.hasError())
        return;

    if (used_find_instance)
        return;

    // We are located now in the target vector
    // Locate key-field attribute if any
    string key_field       = "";
    auto   key_field_maybe = MetadataServer.get_key_field(dest_df_type);
    if (key_field_maybe)
        key_field = *key_field_maybe;

    auto node = p_current_path.top();
    if (is_vector_type(node))
    {
        auto real_aux_value = aux_value_as_num != -1 ? aux_value_as_num : node_value_as_number;
        auto data           = key_field.empty()
                        ? locate_entry_in_vector(node,
                                                 real_aux_value)
                        : find_field_with_value_in_vector(node,
                                                          key_field,
                                                          real_aux_value);
        if (data.hasError())
        {
            p_current_path.setError(data.get_error());
            return;
        }
        p_current_path.push(data);
        return;
    }
    p_current_path.setError("ERROR TODO process_ref_target");
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void process_dollar(Path&       p_current_path,
                    NodeParser& p_source_node,
                    string&     identifier,
                    string&     expression,
                    size_t&     pos,
                    int64_t     p_aux_value,
                    DF_Model*   p_model)
{
    if ((pos + 1 < expression.size()) && (expression[pos + 1] == '$'))
    {
        if ((pos + 2 < expression.size()) && (expression[pos + 2] == '$'))
        {
            // $$$
            // use aux-value
            DF_MetadataGlobal aux_value_metadata;
            identifier                   = "$$$";
            aux_value_special            = p_aux_value;
            aux_value_metadata.m_address = reinterpret_cast<uint64_t>(&aux_value_special);
            aux_value_metadata.m_df_type = DF_Type::int64_t;
            NodeParser result(aux_value_metadata);
            p_current_path.push(result);
            pos += 3;
            return;
        }
        if (pos + 2 < expression.size())
        {
            auto ok1 = expression[pos + 2] == '.';
            auto ok2 = expression[pos + 2] == '(';
            auto ok3 = expression[pos + 2] == ')';
            if (!ok1 && !ok2 && !ok3)
            {
                p_current_path.setError("ERROR invalid character after $");
            }
        }
        // $$
        identifier = "$$";
        NodeParser result(p_source_node);
        p_current_path.push(result);
        pos += 2;
        if ((pos < expression.size()) && (expression[pos] == '.'))
        {
            // $$.
            pos++;
        }
        return;
    }

    identifier = "$";
    if ((pos + 1 < expression.length()) && (expression[pos + 1] == '('))
    {
        //$(....
        pos++;
        auto par_exp = get_parentesis_expression(expression, pos);
        pos          = par_exp.second;
        parse_lisp_expression(p_current_path,
                              p_source_node,
                              par_exp.first,
                              p_aux_value,
                              p_model);
        return;
    }
    if (pos + 1 < expression.length())
    {
        auto is_global = expression.substr(pos, 8); // 8 = size of $global.
        if (is_global == "$global.")
        {
            identifier = "$global.";
            p_current_path.clear();
            auto       node_base      = p_source_node.getNodeBase();
            string     key            = "df.global";
            auto       node_maybe     = node_base->m_node_table->find_node(key);
            auto       real_root_node = *node_maybe;
            NodeParser result         = NodeParser(real_root_node);
            p_current_path.push(result);
            pos += 8;
            return;
        }

        pos++;
        expression = expression.substr(pos, 1512);
        parse_lisp_expression(p_current_path,
                              p_source_node,
                              expression,
                              p_aux_value,
                              p_model);
        return;
    }
    // $
    NodeParser result(p_source_node);
    //nodes_path.push_back(result);
    p_current_path.push(result);
    pos++;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void process_parentesis(Path&       p_current_path,
                        NodeParser& p_source_node,
                        string&     identifier,
                        string&     expression,
                        size_t&     pos,
                        int64_t     p_aux_value,
                        DF_Model*   p_model)
{
    // (.....)
    auto par_exp = get_parentesis_expression(expression, pos);
    pos          = par_exp.second;
    parse_lisp_expression(p_current_path,
                          p_source_node,
                          par_exp.first,
                          p_aux_value,
                          p_model);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void process_array(Path&       p_current_path,
                   NodeParser& p_source_node,
                   string&     identifier,
                   string&     expression,
                   size_t&     pos,
                   int64_t     p_aux_value,
                   DF_Model*   p_model)
{
    // Array or vector access
    auto node = p_current_path.top();
    if (!is_vector_or_array(node))
    {
        p_current_path.setError("ERROR [] in non container field");
        return;
    }
    // Get the array expression
    auto   array_data          = get_array_expression(expression, pos);
    string array_expression_st = array_data.first;
    pos                        = array_data.second;

    // Evaluate the expression
    Path dummy_path;
    parse_lisp_expression(dummy_path,
                          p_source_node,
                          array_expression_st,
                          p_aux_value,
                          p_model);
    // pop the node from path

    auto array_value_expression = dummy_path.top();
    if (array_value_expression.hasError())
        return;

    int64_t array_value;
    // The expression must be an index (int,etc)
    if (!check_index_type(array_value_expression))
    {
        // It's not a int, but it can be a enum
        if (array_value_expression.get_RDF_Type() == RDF_Type::Enum)
        {
            // It's a enum. Get the base-type for the enum
            auto enum_basetype = get_enum_basetype(array_value_expression);
            if (enum_basetype == DF_Type::None)
            {
                p_current_path.setError("ERROR index is not a integer");
                return;
            }
            array_value = get_node_simple(array_value_expression.get_address(),
                                          enum_basetype);
        }
    }
    else // simple type int16_t, etc
    {
        // Evaluate the value
        if (array_value_expression.isConstant())
            array_value = array_value_expression.getConstant();
        else
            array_value = get_node_simple(array_value_expression);
    }

    if (array_value == -1)
    {
        p_current_path.setValue("N/A");
        return;
    }

    // Now access the array or vector
    if (node.get_RDF_Type() == RDF_Type::Vector)
    {
        NodeParser vector_entry = locate_entry_in_vector(node, array_value);
        if (vector_entry.hasError())
        {
            p_current_path.setError(vector_entry.get_error());
            return;
        }
        p_current_path.push(vector_entry);
        if (pos < expression.length())
            if (expression[pos] == '.')
                pos++;
        return;
    }
    if (node.get_RDF_Type() == RDF_Type::Array)
    {
        // Array
        NodeParser array_entry = locate_entry_in_array(node, array_value);
        if (array_entry.hasError())
        {
            p_current_path.setError(array_entry.get_error());
            return;
        }
        p_current_path.push(array_entry);

        if (pos < expression.length())
            if (expression[pos] == '.')
                pos++;
        return;
    }

    p_current_path.setError("ERROR not array or vector");
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void process_arroba(Path& p_current_path, NodeParser& p_source_node, string& identifier, string& expression, size_t& pos, int64_t p_aux_value)
{
    // index-refers-to value
    DF_MetadataGlobal result;
    aux_value_special = p_aux_value;
    result.m_address  = reinterpret_cast<uint64_t>(&aux_value_special);
    result.m_df_type  = DF_Type::int64_t;
    result.m_rdf_type = RDF_Type::int64_t;
    auto np_result    = NodeParser(result);
    p_current_path.push(np_result);
    pos += 1;
}

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_global(Path& p_current_path, NodeParser& p_source_node, string& identifier, string& expression, size_t& pos, int64_t p_aux_value)
{
    global_worker(p_current_path);

    if (pos < expression.length() && expression[pos] != '.')
        p_current_path.setError("ERROR wrong characters after _global");
    pos++;
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_parent(Path& p_current_path, NodeParser& p_source_node, string& identifier, string& expression, size_t& pos, int64_t p_aux_value)
{
    p_current_path.pop();
    if (pos < expression.size() && expression[pos] != '.')
        return p_current_path.setError("ERROR wrong characters after _parent");
    pos++;
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_ref_target(Path&       p_current_path,
                                   NodeParser& p_source_node,
                                   string&     identifier,
                                   string&     expression,
                                   size_t&     pos,
                                   int64_t     p_aux_value,
                                   DF_Model*   p_model)
{
    //pos++;
    // current node
    auto node = p_current_path.top();
    Path copy_path(p_current_path);
    auto node_actual = copy_path.top();
    while (node_actual.get_node_name()[0] == '[')
    {
        copy_path.pop();
        node_actual = copy_path.top();
    }
    // Node that contains the ref-target info
    Path copy_path2(p_current_path);
    auto parent_node      = compound_worker(copy_path2);
    auto ref_target_maybe = MetadataServer.get_ref_target_metadata_field(parent_node.get_DF_Type(),
                                                                         node_actual.get_node_name());
    if (ref_target_maybe)
    {
        auto st_ref_target = (*ref_target_maybe).first;
        auto st_aux_value  = (*ref_target_maybe).second;
        process_ref_target(p_current_path,
                           node,
                           st_ref_target,
                           st_aux_value,
                           p_model);
        if (pos < expression.length())
            if (expression[pos] == '.')
                pos++;

        return;
    }
    p_current_path.setError("ERROR wrong NodeParser in process_identifer_ref_target");
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_refers_to(Path&       p_current_path,
                                  NodeParser& p_source_node,
                                  string&     identifier,
                                  string&     expression,
                                  size_t&     pos,
                                  int64_t     p_aux_value,
                                  DF_Model*   p_model)
{
    //pos++;

    auto node = p_current_path.top();

    if (get_node_simple(node) == -1)
    {
        p_current_path.setValue("N/A");
        return;
    }

    Path copy_path(p_current_path);
    auto node_actual = copy_path.top();
    while (node_actual.get_node_name()[0] == '[')
    {
        copy_path.pop();
        node_actual = copy_path.top();
    }

    // Node that contains the refers-to info
    Path copy_path2(p_current_path);
    auto parent_node = compound_worker(copy_path2);

    auto refers_to_maybe = MetadataServer.get_refers_to_metadata_field(parent_node.get_DF_Type(),
                                                                       node_actual.get_node_name());
    if (!refers_to_maybe)
    {
        p_current_path.setError("ERROR refers-to not found");
        return;
    }
    auto st_refers_to = *refers_to_maybe;
    p_current_path.pop();
    parse_lisp_expression(p_current_path,
                          node,
                          st_refers_to,
                          p_aux_value,
                          p_model);
    if (pos < expression.length())
        if (expression[pos] == '.')
            pos++;
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_index_refers_to(Path&       p_current_path,
                                        NodeParser& p_source_node,
                                        string&     identifier,
                                        string&     expression,
                                        size_t&     pos,
                                        int64_t     p_aux_value,
                                        DF_Model*   p_model)
{
    //pos++;

    auto node = p_current_path.top();

    if (get_node_simple(node) == -1)
    {
        p_current_path.setValue("N/A");
        return;
    }

    Path copy_path(p_current_path);
    auto node_actual = copy_path.top();
    while (node_actual.get_node_name()[0] == '[')
    {
        copy_path.pop();
        node_actual = copy_path.top();
    }

    // Node that contains the refers-to info
    Path copy_path2(p_current_path);
    auto parent_node = compound_worker(copy_path2);

    auto index_refer_to_maybe = MetadataServer.get_index_refers_to(parent_node.get_DF_Type(),
                                                                   node_actual.get_node_name());
    if (!index_refer_to_maybe)
    {
        p_current_path.setError("ERROR index-refers-to not found");
        return;
    }
    // Change $ to @ (index in the vector)
    auto st_index_refers_to    = *index_refer_to_maybe;
    auto expression_translated = translate_refers_to_expression(st_index_refers_to);

    // if the expression begins with $$._parent.**** we need to add another _parent
    if (expression_translated.substr(0, 10) == "$$._parent")
    {
        expression_translated = "$$._parent._parent" + expression_translated.substr(11, 12024);
    }

    p_current_path.pop();
    parse_lisp_expression(p_current_path,
                          node,
                          expression_translated,
                          p_aux_value,
                          p_model);

    if (pos < expression.length())
        if (expression[pos] == '.')
            pos++;
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_key(Path& p_current_path, NodeParser& p_source_node, string& identifier, string& expression, size_t& pos, int64_t p_aux_value)
{
    pos++;
    auto node = p_current_path.top();
    if (node.isValid())
    {
        DF_MetadataGlobal expression_result;
        auto              iv = get_index_vector(p_source_node.get_node_name());
        if (iv == -12345)
            iv = get_index_vector(p_source_node.get_node_name());
        if (iv == -12345)
        {
            p_current_path.setError("ERROR in process_identifier_key");
            return;
        }
        aux_value_special = iv;
        DF_MetadataGlobal result_mt;
        result_mt.m_address = reinterpret_cast<uint64_t>(&aux_value_special);
        result_mt.m_df_type = DF_Type::int64_t;
        NodeParser result(result_mt);
        p_current_path.push(result);
        return;
    }
    p_current_path.setError("ERROR Invalid identifier");
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_by_id(Path&       p_current_path, //path to the current evaluated node
                                   NodeParser& p_source_node, // the node source of the hyperlink expression
                                   string&     expression, //  expression to evaluate
                                   size_t&     pos, // cursor position in expression
                                   int64_t     p_aux_value, // auxilary value used in ref-target
                                   DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;

    // Get function parameters
    // Vector to look for
    auto param_1_pair   = get_parameter(expression, pos);
    auto param_1_vector = param_1_pair.first;
    pos                 = param_1_pair.second;
    // field f the df vector structure to check
    auto param_2_pair     = get_parameter(expression, pos);
    auto param_2_keyfield = param_2_pair.first;
    pos                   = param_2_pair.second;
    // value to check
    auto param_3_pair  = get_parameter(expression, pos);
    auto param_3_value = param_3_pair.first;
    pos                = param_3_pair.second;

    // Process parameters
    Path path_param1(p_current_path);
    auto vector_node = process_parameter(path_param1,
                                         p_source_node,
                                         param_1_vector,
                                         p_aux_value,
                                         p_model);

    if (param_2_keyfield[0] == '$')
        param_2_keyfield = param_2_keyfield.substr(1, 512);

    Path path_param3(p_current_path);
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_3_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_entity(Path&       p_current_path,
                                    NodeParser& p_source_node,
                                    string&     identifier,
                                    string&     expression,
                                    size_t&     pos,
                                    int64_t     p_aux_value,
                                    DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.entities.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_unit(Path&       p_current_path,
                                  NodeParser& p_source_node,
                                  string&     identifier,
                                  string&     expression,
                                  size_t&     pos,
                                  int64_t     p_aux_value,
                                  DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.units.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_item(Path&       p_current_path,
                                  NodeParser& p_source_node,
                                  string&     identifier,
                                  string&     expression,
                                  size_t&     pos,
                                  int64_t     p_aux_value,
                                  DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.items.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_nemesis(Path&       p_current_path,
                                     NodeParser& p_source_node,
                                     string&     identifier,
                                     string&     expression,
                                     size_t&     pos,
                                     int64_t     p_aux_value,
                                     DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.nemesis.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_artifact(Path&       p_current_path,
                                      NodeParser& p_source_node,
                                      string&     identifier,
                                      string&     expression,
                                      size_t&     pos,
                                      int64_t     p_aux_value,
                                      DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.artifacts.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_building(Path&       p_current_path,
                                      NodeParser& p_source_node,
                                      string&     identifier,
                                      string&     expression,
                                      size_t&     pos,
                                      int64_t     p_aux_value,
                                      DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.buildings.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_activity(Path&       p_current_path,
                                      NodeParser& p_source_node,
                                      string&     identifier,
                                      string&     expression,
                                      size_t&     pos,
                                      int64_t     p_aux_value,
                                      DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.activities.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_squad(Path&       p_current_path,
                                   NodeParser& p_source_node,
                                   string&     identifier,
                                   string&     expression,
                                   size_t&     pos,
                                   int64_t     p_aux_value,
                                   DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.squads.all";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_inorganic(Path&       p_current_path,
                                       NodeParser& p_source_node,
                                       string&     identifier,
                                       string&     expression,
                                       size_t&     pos,
                                       int64_t     p_aux_value,
                                       DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector = "$global.world.raws.inorganics.all";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = locate_entry_in_vector(vector_node,
                                         value_to_search);
    if (result.hasError())
        p_current_path.setError(result.get_error());
    else
        p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_plant_raw(Path&       p_current_path,
                                       NodeParser& p_source_node,
                                       string&     identifier,
                                       string&     expression,
                                       size_t&     pos,
                                       int64_t     p_aux_value,
                                       DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector = "$global.world.raws.plants.all";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_index;
    auto value_node_where_to_look = process_parameter(path_index,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = locate_entry_in_vector(vector_node,
                                         value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_creature(Path&       p_current_path,
                                      NodeParser& p_source_node,
                                      string&     identifier,
                                      string&     expression,
                                      size_t&     pos,
                                      int64_t     p_aux_value,
                                      DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector = "$global.world.raws.creatures.all";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = locate_entry_in_vector(vector_node,
                                         value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_figure(Path&       p_current_path,
                                    NodeParser& p_source_node,
                                    string&     identifier,
                                    string&     expression,
                                    size_t&     pos,
                                    int64_t     p_aux_value,
                                    DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.history.figures";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

//----------------------------------------------------------------------------------------------------------------//
void process_identifier_find_burrow(Path&       p_current_path,
                                    NodeParser& p_source_node,
                                    string&     identifier,
                                    string&     expression,
                                    size_t&     pos,
                                    int64_t     p_aux_value,
                                    DF_Model*   p_model)
{
    if (pos >= expression.size() || expression[pos] != ' ')
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair  = get_parameter(expression, pos);
    auto param_1_value = param_1_pair.first;
    pos                = param_1_pair.second;

    string df_vector        = "$global.world.burrows.list";
    string param_2_keyfield = "id";

    Path path_vector;
    auto vector_node = process_parameter(path_vector,
                                         p_source_node,
                                         df_vector,
                                         p_aux_value,
                                         p_model);

    Path path_param3;
    auto value_node_where_to_look = process_parameter(path_param3,
                                                      p_source_node,
                                                      param_1_value,
                                                      p_aux_value,
                                                      p_model);

    auto value_to_search = get_node_simple(value_node_where_to_look);
    auto result          = find_field_with_value_in_vector(vector_node,
                                                  param_2_keyfield,
                                                  value_to_search);
    if (result.hasError())
    {
        p_current_path.setError(result.get_error());
        return;
    }
    p_current_path = path_vector;
    p_current_path.push(result);
};

void process_identifier_item_subype_target(Path&       p_current_path,
                                           NodeParser& p_source_node,
                                           string&     expression,
                                           size_t&     pos,
                                           int64_t     p_aux_value,
                                           DF_Model*   p_model)
{
    if (pos >= expression.size() || (expression[pos] != ' '))
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair = get_parameter(expression, pos);
    auto param_1_type = param_1_pair.first;
    pos               = param_1_pair.second;

    auto param_2_pair    = get_parameter(expression, pos);
    auto param_2_subtype = param_2_pair.first;
    pos                  = param_2_pair.second;

    // Process parameters

    // Process parameters
    Path path_param1(p_current_path);
    auto type_node  = process_parameter(path_param1,
                                       p_source_node,
                                       param_1_type,
                                       p_aux_value,
                                       p_model);
    auto type_value = get_node_simple(type_node);

    Path path_param2(p_current_path);
    auto subtype_node = process_parameter(path_param2,
                                          p_source_node,
                                          param_2_subtype,
                                          p_aux_value,
                                          p_model);

    auto subtype_value = get_node_simple(subtype_node);
    if (subtype_value == -1)
    {
        p_current_path.setValue("N/A");
        return;
    }

    // type must be a enum
    auto maybe = MetadataServer.get_enum_value(type_node.get_DF_Type(), type_value);
    if (!maybe)
    {
        p_current_path.setError("ERROR in item-subtype");
        return;
    }

    auto enum_as_text = (*maybe).first;

    if (enum_as_text == "WEAPON")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.weapons";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "TRAPCOMP")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.trapcomps";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "TOY")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.toys";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "TOOL")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.tools";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "INSTRUMENT")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.instruments";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "ARMOR")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.armor";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "SIEGEAMMO")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.siege_ammo";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "GLOVES")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.gloves";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "SHOES")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.shoes";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "SHIELD")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.shield";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "HELM")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.helms";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "PANTS")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.pants";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    if (enum_as_text == "FOOD")
    {
        Path   raws_path;
        string raws_vector = "$global.world.raws.itemdefs.food";
        auto   vector_node = process_parameter(raws_path, p_source_node, raws_vector, 0, p_model);
        auto   result_node = locate_entry_in_vector(vector_node, subtype_value);
        p_current_path.push(vector_node);
        p_current_path.push(result_node);
        return;
    }
    p_current_path.setValue("N/A");
};

void process_identifier_foot_mat_by_idx(Path&       p_current_path,
                                        NodeParser& p_source_node,
                                        string&     expression,
                                        size_t&     pos,
                                        int64_t     p_aux_value,
                                        DF_Model*   p_model)
{
    if (pos >= expression.size() || (expression[pos] != ' '))
    {
        p_current_path.setError("ERROR wrong characters after function");
        return;
    }
    pos++;
    auto param_1_pair = get_parameter(expression, pos);
    auto param_1      = param_1_pair.first;
    pos               = param_1_pair.second;

    auto param_2_pair = get_parameter(expression, pos);
    auto param_2      = param_2_pair.first;
    pos               = param_2_pair.second;

    // auto param_1_value = parse_lisp_expression(p_current_path, p_source_node, param_1, p_aux_value);
    // auto param_3_value = parse_lisp_expression(p_current_path, p_source_node, param_2, p_aux_value);
    // return NodeParser();
};

bool process_known_functions(Path&       p_current_path,
                             NodeParser& p_source_node,
                             string&     identifier,
                             string&     expression,
                             size_t&     pos,
                             int64_t     p_aux_value,
                             DF_Model*   p_model)
{
    bool upglobal = false;
    if (identifier == "_upglobal")
    {
        // upglobal = parent + global
        process_identifier_parent(p_current_path,
                                  p_source_node,
                                  identifier,
                                  expression,
                                  pos,
                                  p_aux_value);
        if (p_current_path.hasError())
            return true;
        pos--;
        upglobal = true;
    }
    if (upglobal || (identifier == "_global"))
    {
        process_identifier_global(p_current_path,
                                  p_source_node,
                                  identifier,
                                  expression,
                                  pos,
                                  p_aux_value);
        return true;
    }
    else if (identifier == "_parent")
    {
        process_identifier_parent(p_current_path,
                                  p_source_node,
                                  identifier,
                                  expression,
                                  pos,
                                  p_aux_value);
        return true;
    }
    else if (identifier == "ref-target")
    {
        process_identifier_ref_target(p_current_path,
                                      p_source_node,
                                      identifier,
                                      expression,
                                      pos,
                                      p_aux_value,
                                      p_model);
        return true;
    }
    else if (identifier == "refers-to")
    {
        process_identifier_refers_to(p_current_path,
                                     p_source_node,
                                     identifier,
                                     expression,
                                     pos,
                                     p_aux_value,
                                     p_model);
        return true;
    }
    else if (identifier == "index-refers-to")
    {
        process_identifier_index_refers_to(p_current_path,
                                           p_source_node,
                                           identifier,
                                           expression,
                                           pos,
                                           p_aux_value,
                                           p_model);
        return true;
    }
    else if (identifier == "_key")
    {
        process_identifier_key(p_current_path,
                               p_source_node,
                               identifier,
                               expression,
                               pos,
                               p_aux_value);
        return true;
    }
    else if (identifier == "find-by-id")
    {
        process_identifier_find_by_id(p_current_path,
                                      p_source_node,
                                      expression,
                                      pos,
                                      p_aux_value,
                                      p_model);
        return true;
    }
    else if (identifier == "find-entity")
    {
        process_identifier_find_entity(p_current_path,
                                       p_source_node,
                                       identifier,
                                       expression,
                                       pos,
                                       p_aux_value,
                                       p_model);
        return true;
    }
    else if (identifier == "find-unit")
    {
        process_identifier_find_unit(p_current_path,
                                     p_source_node,
                                     identifier,
                                     expression,
                                     pos,
                                     p_aux_value,
                                     p_model);
        return true;
    }
    else if (identifier == "find-item")
    {
        process_identifier_find_item(p_current_path,
                                     p_source_node,
                                     identifier,
                                     expression,
                                     pos,
                                     p_aux_value,
                                     p_model);
        return true;
    }
    else if (identifier == "find-nemesis")
    {
        process_identifier_find_nemesis(p_current_path,
                                        p_source_node,
                                        identifier,
                                        expression,
                                        pos,
                                        p_aux_value,
                                        p_model);
        return true;
    }
    else if (identifier == "find-artifact")
    {
        process_identifier_find_artifact(p_current_path,
                                         p_source_node,
                                         identifier,
                                         expression,
                                         pos,
                                         p_aux_value,
                                         p_model);
        return true;
    }
    else if (identifier == "find-building")
    {
        process_identifier_find_building(p_current_path,
                                         p_source_node,
                                         identifier,
                                         expression,
                                         pos,
                                         p_aux_value,
                                         p_model);
        return true;
    }
    else if (identifier == "find-activity")
    {
        process_identifier_find_activity(p_current_path,
                                         p_source_node,
                                         identifier,
                                         expression,
                                         pos,
                                         p_aux_value,
                                         p_model);
        return true;
    }
    else if (identifier == "find-squad")
    {
        process_identifier_find_squad(p_current_path,
                                      p_source_node,
                                      identifier,
                                      expression,
                                      pos,
                                      p_aux_value,
                                      p_model);
        return true;
    }
    else if (identifier == "find-inorganic")
    {
        process_identifier_find_inorganic(p_current_path,
                                          p_source_node,
                                          identifier,
                                          expression,
                                          pos,
                                          p_aux_value,
                                          p_model);
        return true;
    }
    else if (identifier == "find-plant-raw")
    {
        process_identifier_find_plant_raw(p_current_path,
                                          p_source_node,
                                          identifier,
                                          expression,
                                          pos,
                                          p_aux_value,
                                          p_model);
        return true;
    }
    else if (identifier == "find-creature")
    {
        process_identifier_find_creature(p_current_path,
                                         p_source_node,
                                         identifier,
                                         expression,
                                         pos,
                                         p_aux_value,
                                         p_model);
        return true;
    }
    else if (identifier == "find-figure")
    {
        process_identifier_find_figure(p_current_path,
                                       p_source_node,
                                       identifier,
                                       expression,
                                       pos,
                                       p_aux_value,
                                       p_model);
        return true;
    }
    else if (identifier == "find-burrow")
    {
        process_identifier_find_burrow(p_current_path,
                                       p_source_node,
                                       identifier,
                                       expression,
                                       pos,
                                       p_aux_value,
                                       p_model);
        return true;
    }
    else if (identifier == "item-subtype-target")
    {
        process_identifier_item_subype_target(p_current_path,
                                              p_source_node,
                                              expression,
                                              pos,
                                              p_aux_value,
                                              p_model);
        return true;
    }
    else if (identifier == "food-mat-by-idx")
    {
        process_identifier_foot_mat_by_idx(p_current_path,
                                           p_source_node,
                                           expression,
                                           pos,
                                           p_aux_value,
                                           p_model);
        return true;
    }
    return false;
};

//
//--------------------------------------------------------------------------------------------------------------------//
//
void process_identifier(Path&       p_current_path,
                        NodeParser& p_source_node,
                        string&     identifier,
                        string&     expression,
                        size_t&     pos,
                        int64_t     p_aux_value,
                        DF_Model*   p_model)
{
    // identifier, function, etc
    identifier = get_identifier(expression, pos);

    if (identifier.empty())
        pos++;

    pos += identifier.length();
    auto was_processed = process_known_functions(p_current_path,
                                                 p_source_node,
                                                 identifier,
                                                 expression,
                                                 pos,
                                                 p_aux_value,
                                                 p_model);
    if (was_processed)
        return;

    auto ok  = pos < expression.length() && (expression[pos] == '.' || expression[pos] == '[' || expression[pos] == ' ');
    auto end = pos >= expression.length();
    if (end)
    {
        // process identifier
        // Check if it's a field
        auto node        = p_current_path.top();
        auto maybe_field = get_field_from_identifier(identifier,
                                                     node,
                                                     p_model);
        if (maybe_field.isValid())
        {
            // The identifier was a field
            p_current_path.push(maybe_field);
            return;
        }

        // The identifier is not a field and it's not a known identifier
        p_current_path.setError("ERROR unknown identifier");
        return;
    }
    if (ok)
    {
        if (expression[pos] == '.' || expression[pos] == ' ')
            pos++;
        auto node             = p_current_path.top();
        auto field_expression = get_field_from_identifier(identifier,
                                                          node,
                                                          p_model);

        if (field_expression.isValid())
        {
            // The identifier was a field
            p_current_path.push(field_expression);
            return;
        }

        // The identifier is not a field and it's not a known identifier
        p_current_path.setError("ERROR unknown identifier");
        return;
    }
    p_current_path.setError("ERROR in process_identifier");
};

//
//--------------------------------------------------------------------------------------------------------------------//
//
void parse_lisp_expression(Path&         p_current_path,
                           NodeParser    p_source_node,
                           const string& p_lisp_expression,
                           int           p_aux_value,
                           DF_Model*     p_model)
{
    //vector<NodeParser> nodes_path; // all the nodes visited
    string     identifier;
    NodeParser current_node;
    string     expression = p_lisp_expression;
    size_t     pos        = -1;

    // Main loop
    pos++;
    // Iterate over the characters in the expression
    while (pos < expression.size())
    {
        identifier = "";

        if ((expression[pos] == '$'))
        {
            process_dollar(p_current_path,
                           p_source_node,
                           identifier,
                           expression,
                           pos,
                           p_aux_value,
                           p_model);
            if (p_current_path.hasError())
                return;
            continue;
        }

        if (expression[pos] == '(')
        {
            process_parentesis(p_current_path,
                               p_source_node,
                               identifier,
                               expression,
                               pos,
                               p_aux_value,
                               p_model);
            if (p_current_path.hasError())
                return;
            if (p_current_path.hasValue())
                return;
            continue;
        }

        if (expression[pos] == '[')
        {
            process_array(p_current_path,
                          p_source_node,
                          identifier,
                          expression,
                          pos,
                          p_aux_value,
                          p_model);

            if (p_current_path.hasError())
                return;
            if (p_current_path.hasValue())
                return;

            continue;
        }

        if (expression[pos] == '@')
        {
            process_arroba(p_current_path,
                           p_source_node,
                           identifier,
                           expression,
                           pos,
                           p_aux_value);
            if (p_current_path.hasError())
                return;
            if (p_current_path.hasValue())
                return;
        }
        // identifier
        process_identifier(p_current_path,
                           p_source_node,
                           identifier,
                           expression,
                           pos,
                           p_aux_value,
                           p_model);

        if (p_current_path.hasError())
            return;
        if (p_current_path.hasValue())
            return;
    }
}

//--------------------------------------------------------------------------------------------------------------------//
/**
 * @brief Analizes a node for hyperlinks like ref-to://LISP expression
 *
 * @param p_node  The node that contains the hyperlink
 * @return pair<NodeBase*, string>  Hyperlink node destination and its path
 */
string LispParser::parse_hyperlink(NodeBase* p_source_node, DF_Model* p_model)
{
    // Check if there's a hyperlink string
    auto it = p_source_node->m_hyperlink_type.find("://");

    if (it == string::npos)
        return "";

    // Get the hyperlink type and it's expression
    auto   hyperlink_type       = p_source_node->m_hyperlink_type.substr(0, it);
    auto   hyperlink_expression = p_source_node->m_hyperlink_type.substr(it + 3, 1024);
    string aux_value            = ""; // for ref-target

    // Only ref-target has aux-value attributes
    auto it3 = hyperlink_expression.find('/');
    if (it3 != string::npos)
    {
        // aux-value is on the right of the /
        aux_value            = hyperlink_expression.substr(it3 + 1, 1024);
        hyperlink_expression = hyperlink_expression.substr(0, it3);
    }

    // Vector and arrays only show the hyperlink source code. Their entries show the real values
    if (p_source_node->m_rdf_type == RDF_Type::Vector || p_source_node->m_rdf_type == RDF_Type::Array)
    {
        return hyperlink_expression;
    }

    // Process each hyperlink type
    Path       node_path(p_source_node);
    NodeParser data_node(p_source_node);

    if (hyperlink_type == "ref-target")
    {
        process_ref_target(node_path,
                           data_node,
                           hyperlink_expression,
                           aux_value,
                           p_model);
        if (node_path.hasError())
            return node_path.getError();
        return node_path.getValue();
    }

    if (hyperlink_type == "ref-to")
    {
        parse_lisp_expression(node_path,
                              data_node,
                              hyperlink_expression,
                              -1,
                              p_model);
        if (node_path.hasError())
            return node_path.getError();
        return node_path.getValue();
    }

    if (hyperlink_type == "ind-ref-to")
    {
        // Aux-value is the index position in the vector,
        int64_t value = get_index_vector(data_node.get_node_name());
        if (value == -12345)
            value = get_index_vector(data_node.get_node_name());
        if (value == -12345)
            return "ERROR in process_identifier_key";

        // Change [$] to [@] for the parser
        auto hyperlink_translated = translate_refers_to_expression(hyperlink_expression);
        if (hyperlink_expression == "$")
            hyperlink_expression = "@";
        else
        {
            auto iter = hyperlink_expression.find("[$]");
            if (iter != string ::npos)
                hyperlink_expression[iter + 1] = '@';
        }

        // if the expression begins with $$._parent.**** we need to add another _parent
        if (hyperlink_expression.substr(0, 10) == "$$._parent")
        {
            hyperlink_expression = "$$._parent._parent." + hyperlink_expression.substr(11, 12024);
        }

        if (value != -1)
        {
            parse_lisp_expression(node_path,
                                  data_node,
                                  hyperlink_translated,
                                  value,
                                  p_model);
            if (node_path.hasError())
                return node_path.getError();
            return node_path.getValue();
        }
    }
    return "Unknow hyperlink";
}