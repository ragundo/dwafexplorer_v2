/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "NodeParser.h"

namespace rdf
{
//
//-------------------------------------------------------------------------------------------------------------------//
//
bool NodeParser::isValid() const
{
    return m_pointer != nullptr || m_metadata.m_address != 0 || m_constant != -1;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool NodeParser::isNodeBase() const
{
    return m_pointer != nullptr;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool NodeParser::isMetadata() const
{
    return !isNodeBase() && m_metadata.m_address != 0;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool NodeParser::isConstant() const
{
    return m_constant != -1;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
NodeBase* NodeParser::getNodeBase()
{
    return m_pointer;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
DF_MetadataGlobal& NodeParser::getMetadata()
{
    return m_metadata;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
int32_t NodeParser::getConstant()
{
    return m_constant;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool NodeParser::hasError() const
{
    return !m_error_text.empty();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void NodeParser::setError(const std::string& p_error)
{
    m_error_text = p_error;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string NodeParser::get_error()
{
    return m_error_text;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
DF_Type NodeParser::get_DF_Type()
{
    if (m_pointer)
        return m_pointer->m_df_type;
    else
        return m_metadata.m_df_type;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
RDF_Type NodeParser::get_RDF_Type()
{
    if (m_pointer)
        return m_pointer->m_rdf_type;
    else
        return m_metadata.m_rdf_type;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string NodeParser::get_node_name()
{
    if (m_pointer)
        return m_pointer->m_field_name;
    else
        return m_metadata.m_name;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string NodeParser::get_node_value()
{
    return m_node_value;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string NodeParser::get_addornements()
{
    if (m_pointer)
    {
        auto node_add = dynamic_cast<NodeAddornements*>(m_pointer);
        if (node_add != nullptr)
            return node_add->m_addornements;
        return "";
    }
    return m_metadata.m_addornements;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
uint64_t NodeParser::get_address()
{
    if (m_pointer)
        return m_pointer->m_address;
    else
        return m_metadata.m_address;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void NodeParser::set_node_name(const std::string& p_name)
{
    if (m_pointer)
        m_pointer->m_field_name = p_name;
    else
        m_metadata.m_name = p_name;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void NodeParser::set_node_value(const std::string& p_value)
{
    m_node_value = p_value;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void NodeParser::set_rdf_type(const rdf::RDF_Type& p_rdf_type)
{
    if (m_pointer)
        m_pointer->m_rdf_type = p_rdf_type;
    else
        m_metadata.m_rdf_type = p_rdf_type;
}

} // namespace rdf