#ifndef NODETABLE_H
#define NODETABLE_H
#include "Trie.h"
#include "tl/tl_optional.h"
#include <map>
#include <memory>
#include <string>
#include <unordered_map>
#include <vector>

namespace rdf
{
struct NodeBase;
}

namespace rdf
{
class NodeTable
{
  private:
    //std::unordered_map<std::string, rdf::NodeBase*>* m_current_node_table; // The current table of nodes in use
    nPatriciaTrie<NodeBase*> m_current_node_table;
    /**
     * @brief Previous table of nodes
     *
     */
    //std::unordered_map<std::string, rdf::NodeBase*>* m_prev_node_table;
    nPatriciaTrie<NodeBase*> m_prev_node_table;

  public:
    NodeTable();
    ~NodeTable();
    tl::optional<rdf::NodeBase*> find_node(std::string& p_node_name);
    void                         add_node(rdf::NodeBase* p_node);
    void                         add_node(char* p_key, rdf::NodeBase* p_node);
    bool                         is_empty() const;
    void                         swap();
};

} // namespace rdf
#endif
