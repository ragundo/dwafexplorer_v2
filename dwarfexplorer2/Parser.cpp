/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Parser.h"
#include "MetadataServer.h"
#include "lisp_parser/LispParser.h"
#include "tl/tl_expected.h"
#include <QStringList>
#include <cctype>

using namespace rdf;

extern CMetadataServer MetadataServer;

using ExpectedNodeParser = tl::expected<NodeParser, Parser::ErrorCode>;

//
//-------------------------------------------------------------------------------------------------------------------//
//
Parser::Parser()
{
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool isnumber(const QString& p_string)
{
    bool number_ok;
    int  num = p_string.toInt(&number_ok);
    if (!number_ok)
    {
        // ERROR, not a number
        return false;
    }
    return true;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string remove_array_addornements(const std::string p_string)
{
    if (p_string.empty())
        return p_string;
    if (p_string[0] != '[')
        return p_string; // ERROR
    size_t it = 1;
    while (std::isdigit(p_string[it++]))
        ;
    if (it == p_string.length())
        return "";
    return p_string.substr(1, it - 1);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string remove_array_access(const QString& p_field_name)
{
    QStringList l_parts2 = p_field_name.split("[", QString::SkipEmptyParts);
    if (l_parts2.empty())
        return p_field_name.toStdString();
    return l_parts2[0].toStdString();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
tl::optional<std::vector<QString>> check_array_access(const QString& p_token)
{
    QStringList l_parts2 = p_token.split("[", QString::SkipEmptyParts);

    std::string          data = p_token.toStdString();
    std::vector<QString> array_access;
    if (l_parts2.size() > 1)
    {
        data = l_parts2[0].toStdString();

        for (int h = 1; h < l_parts2.size(); h++)
        {
            QString index = l_parts2[h];
            index.remove(index.indexOf("]"), 1);
            if (!isnumber(index))
            {
                // ERROR wrong expression in array index
                return {};
            }
            array_access.push_back(index);
        }
    }
    return array_access;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
tl::optional<QString> check_vector_access(const QString& p_token)
{
    int it = 0;
    while (it < p_token.size())
    {
        if (p_token[it] == '[')
            break;
        it++;
    }
    if (it == p_token.size())
    {
        // ERROR no [ found
        return {};
    }
    int begin = ++it;
    while (it < p_token.size())
    {
        if (p_token[it] == ']')
            break;
        it++;
    }
    if (it == p_token.size())
    {
        // ERROR no ] found
        return {};
    }
    if (it + 1 != p_token.size())
    {
        // ERROR more caracters after ]
        return {};
    }
    int     end   = it - 1;
    QString index = p_token.mid(begin, end - begin + 1);

    if (!isnumber(index))
    {
        // ERROR wrong expression in array index
        return {};
    }
    return index;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
ExpectedNodeParser Parser::parse_global(QString& p_token, rdf::NodeParser& p_current_node)
{
    auto metadata_global_maybe = MetadataServer.find_global(p_token.toStdString());
    if (metadata_global_maybe)
    {
        // We have metadada for this global
        // Update the node
        p_current_node.getMetadata().m_comment  = (*metadata_global_maybe).m_comment;
        p_current_node.getMetadata().m_df_type  = (*metadata_global_maybe).m_df_type;
        p_current_node.getMetadata().m_name     = (*metadata_global_maybe).m_name;
        p_current_node.getMetadata().m_rdf_type = (*metadata_global_maybe).m_rdf_type;
        p_current_node.getMetadata().m_address  = (*metadata_global_maybe).m_address;
        auto addornements_maybe                 = MetadataServer.get_addornements_globals_metadata((*metadata_global_maybe).m_name);
        if (addornements_maybe)
            p_current_node.getMetadata().m_addornements = *addornements_maybe;
        return p_current_node;
    }
    // Global not found
    return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::GlobalNotFound);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
tl::optional<int> locate_char(const QString& p_expression, char p_ch, int p_pos)
{
    while (p_pos < p_expression.size())
    {
        if (p_expression[p_pos] == p_ch)
            break;
        p_pos++;
    }
    if (p_pos >= p_expression.size())
        return {};
    return p_pos;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
tl::optional<QString> validate_index_expression(const QString& p_expression, int p_pos = 0)
{
    int start = p_pos;

    int end = p_pos;

    bool digits = false;
    auto ch     = p_expression.at(0);
    if (!isnumber(ch))
        return {};
    end++;
    p_pos++;

    bool digits_allowed  = true;
    bool pointer_allowed = false;
    while (p_pos < p_expression.size())
    {
        ch = p_expression[p_pos];
        if (digits_allowed && isnumber(ch))
        {
            p_pos++;
            end++;
            continue;
        }

        if (digits_allowed && ch == '/')
        {
            auto it2 = p_pos + 1;
            while (p_expression[it2] != '/')
                it2++;
            p_pos = it2 + 1;
            break;
        }

        if (!digits_allowed && isnumber(ch))
            return {};
        if (!pointer_allowed && ch != '*')
            return {};
        if (!pointer_allowed && ch == '*')
        {
            pointer_allowed = true;
            p_pos++;
            continue;
        }
        if (pointer_allowed && ch != '*')
            return {};
    }
    return p_expression.mid(start, end);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
tl::optional<std::vector<QString>> get_array_expression_sizes(QStringList& array_groups)
{
    std::vector<QString> result;

    for (int h = 0; h < array_groups.size(); h++)
    {
        QString index_array = array_groups[h];
        auto    maybe_valid = validate_index_expression(index_array);
        if (!maybe_valid)
        {
            // Expression not correct
            return {};
        }
        auto index = *maybe_valid;
        result.push_back(index_array);
    }
    return result;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
ExpectedNodeParser Parser::process_array(QString&     p_expression,
                                         NodeParser&  p_current_node,
                                         std::string& p_addornements)
{
    // Array expression
    // Get the array dimmension from the metadata
    QString addornements = QString::fromStdString(p_addornements);

    // Check for array groups like [5[4*[6
    QStringList array_groups = addornements.split("[", QString::SkipEmptyParts);

    if (array_groups.size() == 0)
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayWithoutAccess);

    // compute the array sizes of the expression, removing the pointers
    std::vector<QString> array_sizes_maybe = *get_array_expression_sizes(array_groups);

    // Now array_size contains the size of each dimmension of the array

    // Check the expression submitted
    int it = 0;
    // look for '['
    auto maybe_it = locate_char(p_expression, '[', it);
    if (!maybe_it)
    {
        // ERROR no [
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayMissingOpening);
    }
    it = *maybe_it;

    int  start     = ++it; // first char after [
    int  end       = -1;
    auto maybe_it2 = locate_char(p_expression, ']', it);
    if (!maybe_it2)
    {
        // ERROR no ] found
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayMissingClosing);
    }
    it                  = *maybe_it2;
    end                 = it - 1;
    QString index       = p_expression.mid(start, end - start + 1);
    auto    index_maybe = validate_index_expression(index);
    if (!index_maybe)
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayWrongAccessExpression);

    // Check for out ot bounds
    auto it2 = 1;
    while (std::isdigit(p_addornements[it2]))
        it2++;
    auto size_st         = p_addornements.substr(1, it2 - 1);
    p_addornements       = p_addornements.substr(it2, 1024);
    auto size_df         = std::stoi(size_st);
    auto size_expression = (*index_maybe).toInt();
    if (size_expression > size_df)
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayIndexOutOfBounds);

    if (p_addornements[0] == '/')
    {
        auto it3 = 1;
        while (p_addornements[it3] != '/')
            it3++;
        it3++;
        p_addornements = p_addornements.substr(it3, 1024);
    }

    // Done with this array
    p_expression        = p_expression.mid(it + 1, p_expression.size());
    auto new_array_name = '[' + *index_maybe + ']';
    p_current_node.set_node_name(new_array_name.toStdString());

    // No characters allowed between [][]
    it = 0;
    while (!p_expression.isEmpty() && it < p_expression.size())
    {
        if (p_expression[it] == ' ')
        {
            it++;
            continue;
        }
        if (p_expression[it] == '[')
        {
            // New array or vector access
            if (p_addornements[0] == '[')
            {
                p_current_node.set_rdf_type(RDF_Type::Array);
                auto expected = process_array(p_expression, p_current_node, p_addornements);
                if (!expected)
                    return expected;
                p_current_node = *expected;
                continue;
            }
            if (p_addornements[0] == 'v')
            {
                p_current_node.set_rdf_type(RDF_Type::Vector);
                auto expected = process_vector(p_expression, p_current_node, p_addornements);
                if (!expected)
                    return expected;
                p_current_node = *expected;
                continue;
            }
            if (p_addornements[0] == '*')
            {
                auto expected = process_pointer(p_expression, p_current_node, p_addornements);
                if (!expected)
                    return expected;
                p_current_node = *expected;
                continue;
            }
        }
        // ERROR character not space betweenn [] []
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayExprBetweenOpeningClosing);
    }

    // We must return the array type
    auto rdf_maybe = MetadataServer.df_2_rdf(p_current_node.get_DF_Type());
    if (rdf_maybe != RDF_Type::None)
    {
        p_current_node.set_rdf_type(rdf_maybe);
        return p_current_node;
    }
    // ERROR RDF_Type not found
    return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::WrongRDFType);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
ExpectedNodeParser Parser::process_vector(QString&     p_expression,
                                          NodeParser&  p_current_node,
                                          std::string& p_addornements)
{
    // Vector expression
    // Check the expression submitted
    int it                   = 0;
    int current_vector_index = 0;

    // look for '['
    auto maybe_it = locate_char(p_expression, '[', it);
    if (!maybe_it)
    {
        // ERROR no [
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayMissingOpening);
    }
    it = *maybe_it;

    int  start     = ++it; // first char after [
    int  end       = -1;
    auto maybe_it2 = locate_char(p_expression, ']', it);
    if (!maybe_it2)
    {
        // ERROR no ] found
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayMissingClosing);
    }
    it                  = *maybe_it2;
    end                 = it - 1;
    QString index       = p_expression.mid(start, end - start + 1);
    auto    index_maybe = validate_index_expression(index);

    if (!index_maybe)
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayWrongAccessExpression);

    if (current_vector_index > 1)
    {
        // ERROR [][][][] when the array is [][]
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArraySizesMismatch);
    }

    // Done with this vector
    p_expression         = p_expression.mid(it + 1, p_expression.size());
    p_addornements       = p_addornements.substr(1, 1024);
    auto new_vector_name = "[" + *index_maybe + "]";
    p_current_node.set_node_name(new_vector_name.toStdString());

    // No characters allowed between [][]
    it = 0;
    while (!p_expression.isEmpty() && it < p_expression.size())
    {
        if (p_expression[it] == ' ')
        {
            it++;
            continue;
        }
        if (p_expression[it] == '[')
        {
            // New array or vector access
            if (p_addornements[0] == '[')
            {
                p_current_node.set_rdf_type(RDF_Type::Array);
                auto expected = process_array(p_expression, p_current_node, p_addornements);
                if (!expected)
                    return expected;
                p_current_node = *expected;
                continue;
            }
            if (p_addornements[0] == 'v')
            {
                p_current_node.set_rdf_type(RDF_Type::Vector);
                auto expected = process_vector(p_expression, p_current_node, p_addornements);
                if (!expected)
                    return expected;
                p_current_node = *expected;
                continue;
            }
            if (p_addornements[0] == '*')
            {
                auto expected = process_pointer(p_expression, p_current_node, p_addornements);
                if (!expected)
                    return expected;
                p_current_node = *expected;
                continue;
            }
        }
        // ERROR character not space betweenn [] []
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayExprBetweenOpeningClosing);
    }

    // We must return the vector type
    auto rdf_maybe = MetadataServer.df_2_rdf(p_current_node.get_DF_Type());
    if (rdf_maybe != RDF_Type::None)
    {
        p_current_node.getMetadata().m_rdf_type = rdf_maybe;
        return p_current_node;
    }
    // ERROR RDF_Type not found
    return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::WrongRDFType);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
ExpectedNodeParser Parser::process_pointer(QString&     p_token,
                                           NodeParser&  p_current_node,
                                           std::string& p_addornements)
{
    auto info_maybe = MetadataServer.find_field(p_current_node.get_DF_Type(), p_token.toStdString());
    if (!info_maybe)
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::ArrayExprBetweenOpeningClosing);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
ExpectedNodeParser Parser::parse_field(QString& p_token, NodeParser& p_current_node)
{
    // Remove any possible array or vector access in the field name
    std::string field_name = remove_array_access(p_token);
    // Check correct field name
    auto metadata_maybe = MetadataServer.find_field(p_current_node.get_DF_Type(),
                                                    field_name);
    if (!metadata_maybe)
    {
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::FieldNotFound);
    }

    auto addornements_maybe = MetadataServer.get_addornements_metadata(p_current_node.get_DF_Type(),
                                                                       field_name);
    // update the metadata with the new field
    p_current_node.getMetadata().m_comment  = (*metadata_maybe).m_comment;
    p_current_node.getMetadata().m_df_type  = (*metadata_maybe).m_df_type;
    p_current_node.getMetadata().m_name     = (*metadata_maybe).m_name;
    p_current_node.getMetadata().m_rdf_type = (*metadata_maybe).m_rdf_type;

    std::string addornements = "";
    if (addornements_maybe)
    {
        p_current_node.getMetadata().m_addornements = *addornements_maybe;
        addornements                                = *addornements_maybe;
    }

    if (addornements.empty() || !p_token.contains('['))
        return p_current_node;

    auto it = 0;
    while (p_token[it] != '[')
        it++;
    auto array_expression = p_token.mid(it, 1024);

    while (!addornements.empty() && !array_expression.isEmpty())
    {
        if (addornements[0] == '[')
        {
            auto expected = process_array(array_expression, p_current_node, addornements);
            if (!expected)
                return expected;
            p_current_node = *expected;
        }
        if (!array_expression.isEmpty() && addornements[0] == 'v')
        {
            auto expected = process_vector(array_expression, p_current_node, addornements);
            if (!expected)
                return expected;
            p_current_node = *expected;
        }
        if (!array_expression.isEmpty() && addornements[0] == '*')
        {
            auto expected = process_pointer(array_expression, p_current_node, addornements);
            if (!expected)
                return expected;
            p_current_node = *expected;
        }
    }

    // Normal (not array/vector) field
    return p_current_node;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
ExpectedNodeParser Parser::parse_expression(const QString& p_expression)
{
    NodeParser result_node;
    QString    expression = p_expression;

    // Remove df::global::
    auto init = expression.mid(0, 9);
    // All expressions must begin with df.global.*****
    if (init == "df.global" && init == expression)
    {
        // Show the globals
        auto globals_metadata = MetadataServer.get_globals_metadata();
    }
    if (init == "df.global")
    {
        // Remove df.global. from the expression to begin with the first global
        expression = expression.mid(10);
        if (expression.size() == 0)
            return result_node;
        // Split the expression into tolkens using ".""
        QStringList parts = expression.split(".", QString::SkipEmptyParts);
        if (!parts.empty())
        {
            int      i       = 0;
            uint64_t address = 0;
            for (auto& token : parts)
            {
                if (i == 0)
                {
                    // The first token nust be a global
                    auto expected_node = parse_global(token, result_node);
                    if (!expected_node)
                        return tl::unexpected<Parser::ErrorCode>(expected_node.error());
                    result_node = *expected_node;
                    i++;
                    continue;
                }
                // i > 0
                // The expression is a field of some global
                auto expected_node = parse_field(token, result_node);
                if (!expected_node)
                    return tl::unexpected<Parser::ErrorCode>(expected_node.error());
                result_node = *expected_node;
                i++;
                continue;
            }
            while (++i < parts.size())
                ;
            return result_node;
        }
        // (parts.empty())
        //  ERROR The expression is just df.global
        return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::NoGlobalProvided);
    }
    // Error init != "df.global."
    return tl::unexpected<Parser::ErrorCode>(Parser::ErrorCode::NotDfGlobalAtStart);
}
