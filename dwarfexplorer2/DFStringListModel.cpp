/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "DFStringListModel.h"
#include "Parser.h"
#include "lisp_parser/LispParser.h"
#include <cctype>
#include <tl/tl_expected.h>

using namespace rdf;

extern rdf::CMetadataServer MetadataServer;

//
//-------------------------------------------------------------------------------------------------------------------//
//
DFStringListModel::DFStringListModel()
    : m_error_code{Parser::ErrorCode::None}
{
    append("df.global");
    // Add all the globals as keywords
    auto all_globals = MetadataServer.get_globals_metadata();
    for (const auto& a_global : all_globals)
    {
        QString new_word = "df::global::" + QString::fromStdString(std::get<0>(a_global));
        append(new_word);
    }
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool DFStringListModel::append(const QString& p_string)
{
    bool ok = insertRows(rowCount(), 1);
    if (ok)
        ok = setData(index(rowCount() - 1), p_string);
    return ok;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
DFStringListModel& DFStringListModel::operator<<(const QString& p_string)
{
    append(p_string);
    return *this;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
int num_dots(const QString& p_string)
{
    int result = 0;
    for (int i = 0; i < p_string.length(); i++)
        if (p_string[i] == '.')
            result++;
    return result;
}

void auxiliar_array_rec(std::vector<std::string>& p_result, const std::string& p_expression)
{
    if (!p_expression.empty() && p_expression[0] == '[')
    {
        auto array_expression = p_expression.substr(1, 1024);
        auto it               = 0;
        while (std::isdigit(array_expression[it]))
            it++;
        auto number_st = array_expression.substr(0, it);
        p_result.push_back("[0.." + number_st + "]");
        auxiliar_array_rec(p_result, array_expression.substr(it, 1024));
    }
}

std::vector<std::string> auxiliar_array(const std::string& p_addornements)
{
    std::vector<std::string> result;
    auxiliar_array_rec(result, p_addornements);
    return result;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStringListModel::process_name(const QString& p_expression)
{
    removeRows(0, rowCount());

    auto no_dot = p_expression.mid(0, p_expression.length() - 1);
    if (no_dot == "df")
    {
        append("df.global");
        return;
    }
    if (no_dot == "df.global")
    {
        // Add al the globals
        auto all_globals = MetadataServer.get_globals_metadata();
        for (auto& one_global_metadata : all_globals)
        {
            auto field_name = std::get<0>(one_global_metadata);
            auto full_name  = "df.global." + field_name;
            append(QString::fromStdString(full_name));
        }
        return;
    }

    Parser parser;
    auto   expected_node_parser = parser.parse_expression(no_dot);
    if (!expected_node_parser)
    {
        m_error_code = expected_node_parser.error();
        return;
    }

    auto the_node_parser = *expected_node_parser;
    auto all_fields      = MetadataServer.get_fields_metadata(the_node_parser.get_DF_Type());

    // Sort the fields before inserting them
    std::vector<QString> candidates;
    for (const auto& some_field_metadata : all_fields)
    {
        QString new_candidate = p_expression + QString::fromStdString(some_field_metadata.m_name);
        candidates.push_back(new_candidate);

        auto child_rdf = some_field_metadata.m_rdf_type;

        if (child_rdf == RDF_Type::Vector)
        {
            candidates.push_back(new_candidate + "[]");
        }

        if (child_rdf == RDF_Type::Array)
        {
            auto array_candidate = new_candidate;
            candidates.push_back(array_candidate);
            auto addornements_maybe = MetadataServer.get_addornements_metadata(the_node_parser.get_DF_Type(),
                                                                               some_field_metadata.m_name);
            if (addornements_maybe)
            {
                auto addornements = *addornements_maybe;
                auto arrays       = auxiliar_array(addornements);
                for (size_t i = 0; i < arrays.size(); i++)
                {
                    array_candidate += QString::fromStdString(arrays[i]);
                    candidates.push_back(array_candidate);
                }
            }
        }
    }

    std::sort(candidates.begin(), candidates.end());

    for (const auto& new_data : candidates)
    {
        // add the words for autocomplete
        append(new_data);
    }
}