/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef PARSER_H
#define PARSER_H
#include "lisp_parser/NodeParser.h"
#include "metadata_server/MetadataServer.h"
#include "tl/tl_expected.h"
#include <QString>
#include <utility>

namespace rdf
{
class CMetadataServer;
} // namespace rdf

class Parser
{
  public:
    Parser();

  public:
    enum class ErrorCode
    {
        None,
        NotDfGlobalAtStart,
        NoGlobalProvided,
        GlobalNotFound,
        FieldNotFound,
        ArrayWithNoSize,
        ArrayWithoutAccess,
        ArraySizesMismatch,
        ArrayMissingOpening,
        ArrayMissingClosing,
        ArrayExprBetweenOpeningClosing,
        ArrayIndexOutOfBounds,
        ArrayWrongAccessExpression,
        WrongRDFType
    };

    using ExpectedNodeParser = tl::expected<rdf::NodeParser, Parser::ErrorCode>;

  public:
    ExpectedNodeParser parse_expression(const QString& p_expression);

  private:
    ExpectedNodeParser parse_global(QString&         p_token,
                                    rdf::NodeParser& p_current_node);
    ExpectedNodeParser parse_field(QString&         p_token,
                                   rdf::NodeParser& p_current_node);
    ExpectedNodeParser process_array(QString&         p_token,
                                     rdf::NodeParser& p_current_node,
                                     std::string&     p_addornements);
    ExpectedNodeParser process_vector(QString&         p_token,
                                      rdf::NodeParser& p_current_node,
                                      std::string&     p_addornements);
    ExpectedNodeParser process_pointer(QString&         p_token,
                                       rdf::NodeParser& p_current_node,
                                       std::string&     p_addornements);
};

#endif