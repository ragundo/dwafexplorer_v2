/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DFSTRUCTUREWINDOW_H
#define DFSTRUCTUREWINDOW_H

#include "DockManager.h"
#include "xml_parser/Field.h"
#include <QMainWindow>

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QUndoStack;
class QLabel;
class QDragEnterEvent;
class QDropEvent;
class QTreeView;
class QItemSelection;
class QTextEdit;
class QSplitter;
class QTabWidget;
QT_END_NAMESPACE

class QHexView;
class XML_Highlighter;
class DF_Model;
class DFStructureWidget;

namespace rdf
{
struct NodeBase;
}

class DFStructureWindow : public QMainWindow
{
    Q_OBJECT

  public:
    DFStructureWindow(QMainWindow*                        parent,
                      std::map<std::string, rdf::Field*>* p_types_table,
                      DF_Model*                           p_df_model,
                      const QString&                      p_expression);

    QTreeView* get_treeview();

  public:
    void update_statusbar(rdf::NodeBase* p_node);

    void create_tab(DF_Model*      p_df_model,
                    const QString& p_expression);

  public slots:
    void on_hyperlink_value_in_selection(bool);
    void on_current_tab_changed(int index);
    void on_selection_has_coordinate(int x, int y, int z);

  private slots:
    void about();
    void open_structure_in_new_window();
    void open_structure_in_new_tab();
    void open_hyperlink_in_new_window();
    void open_hyperlink_in_new_tab();
    void on_tab_close_request(int);
    void on_paused();
    void on_resumed();

  private:
    void init(DF_Model* p_model, const QString& p_expression);
    void createActions();
    void createMenus();
    void createStatusBar();
    void createToolBars();

    QMenu* fileMenu;
    QMenu* structureMenu;
    QMenu* hyperlinkMenu;
    QMenu* editMenu;
    QMenu* helpMenu;

    QToolBar* fileToolBar;
    QToolBar* editToolBar;

    QAction* openAct;
    QAction* saveAct;
    QAction* saveAsAct;
    QAction* saveReadable;
    QAction* closeAct;
    QAction* exitAct;

    QAction* undoAct;
    QAction* redoAct;
    QAction* saveSelectionReadable;

    QAction* aboutAct;
    QAction* aboutQtAct;
    QAction* optionsAct;
    QAction* findAct;
    QAction* findNextAct;

    QAction* openStructureInNewWindow;
    QAction* openStructureInNewTab;
    QAction* openHyperlinkInNewWindow;
    QAction* openHyperlinkInNewTab;

    QLabel *lbAddress, *lbAddressName, *lb_online;
    QLabel *lbOverwriteMode, *lbOverwriteModeName;
    QLabel *lbSize, *lbSizeName;

    QTabWidget* m_tab_widget;

    //  The types table that contains all the Field for each df structuture type
    std::map<std::string, rdf::Field*>* m_types_table;

    bool m_online{true};
};

#endif // DFSTRUCTUREWINDOW_H