/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DWARFEXPLORERMAINWINDOW_H
#define DWARFEXPLORERMAINWINDOW_H

#include "DFStringListModel.h"
#include "NodeTable.h"
#include "xml_parser/XmlParser.h"
#include <Core.h>
#include <QCloseEvent>
#include <QMainWindow>
#include <QTreeWidgetItem>

QT_BEGIN_NAMESPACE
namespace Ui
{
class DwarfExplorerMainWindow;
}
QT_END_NAMESPACE

namespace rdf
{
class CMetadataServer;
}

class DFStructureWindow;

class EventProxy;
class QStringListModel;

class DwarfExplorerMainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    explicit DwarfExplorerMainWindow(std::shared_ptr<EventProxy>&& proxy, QWidget* parent = nullptr);
    ~DwarfExplorerMainWindow();

    const std::map<std::string, rdf::Field*>& get_types_table()
    {
        return m_types_table;
    }

    void create_window_from_expression(const QString& p_lua_expression);
    void create_tab_from_expression(const QString& p_lua_expression, DFStructureWindow* p_window);
    void create_window_from_node(rdf::NodeBase* p_selected_node);
    void create_tab_from_node(rdf::NodeBase* p_selected_node, DFStructureWindow* p_window);
    void locate_coordinate_in_map(int x, int y, int z);

  private:
    void update_tree(const QString& p_expression);

  private slots:
    void on_suspend_action_triggered();
    void on_resume_action_triggered();
    void on_lineEdit_textChanged(const QString& arg1);
    void on_pushButton_clicked();
    void on_treeWidget_currentItemChanged(QTreeWidgetItem* current, QTreeWidgetItem* previous);
    void on_tree_widget_double_clicked(QTreeWidgetItem* item, int column);

  private:
    Ui::DwarfExplorerMainWindow* ui;

  signals:
    void paused();
    void resumed();

  private:
    std::shared_ptr<EventProxy>        event_proxy;
    DFHack::CoreSuspender*             m_core_suspender;
    bool                               m_suspended;
    DFStringListModel*                 m_df_string_list_model;
    std::map<std::string, rdf::Field*> m_types_table;
    XmlParser                          m_xml_parser;
    rdf::NodeTable                     m_node_table;
};
#endif // DWARFEXPLORERMAINWINDOW_H
