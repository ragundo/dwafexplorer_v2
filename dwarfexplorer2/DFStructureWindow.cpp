/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "QHexView/qhexview.h"
#include <Core.h>
#include <QAction>
#include <QApplication>
#include <QColorDialog>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFileDialog>
#include <QFontDatabase>
#include <QFontDialog>
#include <QItemSelection>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QSplitter>
#include <QStatusBar>
#include <QTabBar>
#include <QTabWidget>
#include <QTextEdit>
#include <QToolBar>
#include <QTreeView>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <algorithm>
#include <chrono>
#include <set>

#include "DFStructureWidget.h"
#include "DFStructureWindow.h"
#include "DF_Xml_Builder.h"
#include "DwarfExplorerMainWindow.h"
#include "MetadataServer.h"
#include "QHexView/document/buffer/qmemorybuffer.h"
#include "XML_Highlighter.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "df_utils.h"
#include "df_xml_model/df_xml_item.h"
#include "df_xml_model/df_xml_model.h"
#include "lisp_parser/LispParser.h"
#include "xml_parser/Field.h"

extern rdf::CMetadataServer MetadataServer;
extern rdf::DF_XML_Item*    xml_node_dummy();

/*****************************************************************************/
/* Public methods */
/*****************************************************************************/
DFStructureWindow::DFStructureWindow(QMainWindow*                        p_parent,
                                     std::map<std::string, rdf::Field*>* p_types_table,
                                     DF_Model*                           p_model,
                                     const QString&                      p_expression)
    : QMainWindow(p_parent), m_types_table(p_types_table)
{
    init(p_model, p_expression);
}

/*****************************************************************************/
/* Private Slots */
/*****************************************************************************/
void DFStructureWindow::about()
{
    QMessageBox::about(this, tr("About QHexEdit"),
                       tr("The QHexEdit example is a short Demo of the QHexEdit Widget."));
}

/*****************************************************************************/
// Private Methods
/*****************************************************************************/
void DFStructureWindow::init(DF_Model* p_model, const QString& p_expression)
{
    setWindowIcon(QIcon(":/icon/dwarf.png"));
    createActions();
    createMenus();
    createToolBars();
    createStatusBar();

    m_tab_widget = new QTabWidget(this);
    m_tab_widget->setTabPosition(QTabWidget::TabPosition::South);
    m_tab_widget->setTabShape(QTabWidget::TabShape::Triangular);
    //m_tab_widget->setElideMode(Qt::ElideLeft);
    m_tab_widget->setTabsClosable(true);
    //m_tab_widget->setUsesScrollButtons(true);

    connect(m_tab_widget,
            &QTabWidget::tabCloseRequested,
            this,
            &DFStructureWindow::on_tab_close_request);

    connect(m_tab_widget,
            &QTabWidget::currentChanged,
            this,
            &DFStructureWindow::on_current_tab_changed);

    auto main_window = dynamic_cast<DwarfExplorerMainWindow*>(parent());

    auto ok1 = connect(main_window,
                       &DwarfExplorerMainWindow::paused,
                       this,
                       &DFStructureWindow::on_paused);

    auto ok2 = connect(main_window,
                       &DwarfExplorerMainWindow::resumed,
                       this,
                       &DFStructureWindow::on_resumed);

    setCentralWidget(m_tab_widget);
    auto new_tab = new DFStructureWidget(m_tab_widget, m_types_table, p_model, p_expression);
    m_tab_widget->addTab(new_tab, p_expression);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::createActions()
{
    /*
    openAct = new QAction(QIcon(":/images/open.png"), tr("&Open..."), this);
    openAct->setShortcuts(QKeySequence::Open);
    openAct->setStatusTip(tr("Open an existing file"));
    connect(openAct, SIGNAL(triggered()), this, SLOT(open()));

    saveAct = new QAction(QIcon(":/images/save.png"), tr("&Save"), this);
    saveAct->setShortcuts(QKeySequence::Save);
    saveAct->setStatusTip(tr("Save the document to disk"));
    connect(saveAct, SIGNAL(triggered()), this, SLOT(save()));

    saveAsAct = new QAction(tr("Save &As..."), this);
    saveAsAct->setShortcuts(QKeySequence::SaveAs);
    saveAsAct->setStatusTip(tr("Save the document under a new name"));
    connect(saveAsAct, SIGNAL(triggered()), this, SLOT(saveAs()));

    saveReadable = new QAction(tr("Save &Readable..."), this);
    saveReadable->setStatusTip(tr("Save document in readable form"));
    connect(saveReadable, SIGNAL(triggered()), this, SLOT(saveToReadableFile()));
*/
    exitAct = new QAction(tr("E&xit"), this);
    exitAct->setShortcuts(QKeySequence::Quit);
    exitAct->setStatusTip(tr("Exit the application"));
    connect(exitAct, SIGNAL(triggered()), qApp, SLOT(closeAllWindows()));

    openStructureInNewWindow = new QAction(tr("Open in new window"), this);
    connect(openStructureInNewWindow, SIGNAL(triggered()), this, SLOT(open_structure_in_new_window()));

    openStructureInNewTab = new QAction(tr("Open in new tab"), this);
    connect(openStructureInNewTab, SIGNAL(triggered()), this, SLOT(open_structure_in_new_tab()));

    openHyperlinkInNewWindow = new QAction(tr("Open in new window"), this);
    connect(openHyperlinkInNewWindow, SIGNAL(triggered()), this, SLOT(open_hyperlink_in_new_window()));

    openHyperlinkInNewTab = new QAction(tr("Open in new tab"), this);
    connect(openHyperlinkInNewTab, SIGNAL(triggered()), this, SLOT(open_hyperlink_in_new_tab()));
    /*
    undoAct = new QAction(QIcon(":/images/undo.png"), tr("&Undo"), this);
    undoAct->setShortcuts(QKeySequence::Undo);
    connect(undoAct, SIGNAL(triggered()), hexEdit, SLOT(undo()));

    redoAct = new QAction(QIcon(":/images/redo.png"), tr("&Redo"), this);
    redoAct->setShortcuts(QKeySequence::Redo);
    connect(redoAct, SIGNAL(triggered()), hexEdit, SLOT(redo()));

    saveSelectionReadable = new QAction(tr("&Save Selection Readable..."), this);
    saveSelectionReadable->setStatusTip(tr("Save selection in readable form"));
    connect(saveSelectionReadable, SIGNAL(triggered()), this, SLOT(saveSelectionToReadableFile()));
*/
    aboutAct = new QAction(tr("&About"), this);
    aboutAct->setStatusTip(tr("Show the application's About box"));
    connect(aboutAct, SIGNAL(triggered()), this, SLOT(about()));

    aboutQtAct = new QAction(tr("About &Qt"), this);
    aboutQtAct->setStatusTip(tr("Show the Qt library's About box"));
    connect(aboutQtAct, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
    /*
    findAct = new QAction(QIcon(":/images/find.png"), tr("&Find/Replace"), this);
    findAct->setShortcuts(QKeySequence::Find);
    findAct->setStatusTip(tr("Show the Dialog for finding and replacing"));
    connect(findAct, SIGNAL(triggered()), this, SLOT(showSearchDialog()));

    findNextAct = new QAction(tr("Find &next"), this);
    findNextAct->setShortcuts(QKeySequence::FindNext);
    findNextAct->setStatusTip(tr("Find next occurrence of the searched pattern"));
    connect(findNextAct, SIGNAL(triggered()), this, SLOT(findNext()));

    optionsAct = new QAction(tr("&Options"), this);
    optionsAct->setStatusTip(tr("Show the Dialog to select applications options"));
    connect(optionsAct, SIGNAL(triggered()), this, SLOT(showOptionsDialog()));
*/
}
//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::createMenus()
{
    fileMenu = menuBar()->addMenu(tr("&File"));
    fileMenu->addAction(exitAct);

    structureMenu = menuBar()->addMenu(tr("&Structure"));
    structureMenu->addAction(openStructureInNewWindow);
    structureMenu->addAction(openStructureInNewTab);

    hyperlinkMenu = menuBar()->addMenu(tr("&Hyperlink"));
    hyperlinkMenu->addAction(openHyperlinkInNewWindow);
    hyperlinkMenu->addAction(openHyperlinkInNewTab);

    helpMenu = menuBar()->addMenu(tr("&Help"));
    helpMenu->addAction(aboutAct);
    helpMenu->addAction(aboutQtAct);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::createStatusBar()
{
    lb_online = new QLabel();
    lb_online->setText(tr("ONLINE"));
    statusBar()->addPermanentWidget(lb_online, 100);

    // Address Label
    lbAddressName = new QLabel();
    lbAddressName->setText(tr("Hyperlink:"));
    statusBar()->addPermanentWidget(lbAddressName);
    lbAddress = new QLabel();
    lbAddress->setFrameShape(QFrame::Panel);
    lbAddress->setFrameShadow(QFrame::Sunken);
    lbAddress->setMinimumWidth(70);
    statusBar()->addPermanentWidget(lbAddress);
    //statusBar()->showMessage(tr("Ready"), 2000);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::createToolBars()
{
    //    fileToolBar = addToolBar(tr("File"));
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::update_statusbar(rdf::NodeBase* p_node)
{
    if (p_node->m_hyperlink_type.empty())
        lbAddress->setText("");
    auto data = QString::fromStdString(p_node->m_hyperlink_type);
    lbAddress->setText(data);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::string remove_super(std::string& p_expression)
{
    auto it = p_expression.find(".super");
    if (it == std::string::npos)
        return p_expression;
    return p_expression.substr(0, it);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::open_structure_in_new_window()
{
    // Get the selected tab
    auto page = dynamic_cast<DFStructureWidget*>(m_tab_widget->currentWidget());
    // Get selected row in the treeview
    QModelIndexList indexes = page->get_treeview()->selectionModel()->selectedIndexes();
    if (indexes.size() == 0)
        return;

    QModelIndex selectedIndex = indexes.at(0);
    // Get the df_model
    auto      model        = page->get_treeview()->model();
    DF_Model* global_model = static_cast<DF_Model*>(model);

    // Get the selected node
    rdf::NodeBase* node = global_model->nodeFromIndex(selectedIndex);

    auto main_window        = dynamic_cast<DwarfExplorerMainWindow*>(this->parent());
    auto expression_to_open = node->m_field_path;
    if (node->m_field_name[0] != '[')
        expression_to_open.append(".").append(node->m_field_name);
    else
        expression_to_open.append(node->m_field_name);
    auto real_expression_to_open = remove_super(expression_to_open);
    main_window->create_window_from_node(node);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::open_structure_in_new_tab()
{
    // Get the selected tab
    auto page = dynamic_cast<DFStructureWidget*>(m_tab_widget->currentWidget());
    // Get selected row in the treeview
    QModelIndexList indexes = page->get_treeview()->selectionModel()->selectedIndexes();
    if (indexes.size() == 0)
        return;

    QModelIndex selectedIndex = indexes.at(0);
    // Get the df_model
    auto      model        = page->get_treeview()->model();
    DF_Model* global_model = static_cast<DF_Model*>(model);

    // Get the selected node
    rdf::NodeBase* node = global_model->nodeFromIndex(selectedIndex);

    auto main_window        = dynamic_cast<DwarfExplorerMainWindow*>(this->parent());
    auto expression_to_open = node->m_field_path;
    if (node->m_field_name[0] != '[')
        expression_to_open.append(".").append(node->m_field_name);
    else
        expression_to_open.append(node->m_field_name);
    auto real_expression_to_open = remove_super(expression_to_open);
    main_window->create_tab_from_node(node, this);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::open_hyperlink_in_new_window()
{
    // Get the selected tab
    auto page = dynamic_cast<DFStructureWidget*>(m_tab_widget->currentWidget());
    // Get selected row in the treeview
    QModelIndexList indexes = page->get_treeview()->selectionModel()->selectedIndexes();
    if (indexes.size() == 0)
        return;

    QModelIndex selectedIndex = indexes.at(0);
    // Get the df_model
    auto      model        = page->get_treeview()->model();
    DF_Model* global_model = static_cast<DF_Model*>(model);

    // Get the selected node
    rdf::NodeBase* node = global_model->nodeFromIndex(selectedIndex);

    auto main_window = dynamic_cast<DwarfExplorerMainWindow*>(this->parent());
    if (!node->m_model_data.m_hyperlink)
        return;
    auto expression_to_open = "df.global." + *node->m_model_data.m_hyperlink;

    main_window->create_window_from_expression(QString::fromStdString(expression_to_open));
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::open_hyperlink_in_new_tab()
{
    // Get the selected tab
    auto page = dynamic_cast<DFStructureWidget*>(m_tab_widget->currentWidget());
    // Get selected row in the treeview
    QModelIndexList indexes = page->get_treeview()->selectionModel()->selectedIndexes();
    if (indexes.size() == 0)
        return;

    QModelIndex selectedIndex = indexes.at(0);
    // Get the df_model
    auto      model        = page->get_treeview()->model();
    DF_Model* global_model = static_cast<DF_Model*>(model);

    // Get the selected node
    rdf::NodeBase* node = global_model->nodeFromIndex(selectedIndex);

    auto main_window = dynamic_cast<DwarfExplorerMainWindow*>(this->parent());
    if (!node->m_model_data.m_hyperlink)
        return;
    auto expression_to_open = "df.global." + *node->m_model_data.m_hyperlink;

    main_window->create_tab_from_expression(QString::fromStdString(expression_to_open), this);
}

QTreeView* DFStructureWindow::get_treeview()
{
    auto current_page   = m_tab_widget->currentWidget();
    auto current_widget = dynamic_cast<DFStructureWidget*>(current_page);
    return current_widget->get_treeview();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::create_tab(DF_Model* p_df_model, const QString& p_expression)
{
    auto new_tab = new DFStructureWidget(m_tab_widget, m_types_table, p_df_model, p_expression);
    m_tab_widget->addTab(new_tab, p_expression);
    m_tab_widget->setCurrentIndex(m_tab_widget->count() - 1);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::on_tab_close_request(int p_index)
{
    QWidget* tab = dynamic_cast<QWidget*>(m_tab_widget->currentWidget());
    disconnect(tab, nullptr, nullptr, nullptr);
    tab->close();
    delete tab;
    tab = NULL;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::on_hyperlink_value_in_selection(bool p_has_hyperlink)
{
    hyperlinkMenu->setEnabled(p_has_hyperlink);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::on_current_tab_changed(int p_index)
{
    DFStructureWidget* current_page = dynamic_cast<DFStructureWidget*>(m_tab_widget->widget(p_index));
    //Change the title to match the current page
    auto df_model = dynamic_cast<DF_Model*>(current_page->get_treeview()->model());
    setWindowTitle("Dwarf Explorer - " + df_model->get_expresion());
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::on_selection_has_coordinate(int x, int y, int z)
{
    if (x == -3000 || y == -3000 || z == -3000)
        return;

    auto parent_window = dynamic_cast<DwarfExplorerMainWindow*>(parent());
    if (parent_window)
        parent_window->locate_coordinate_in_map(x, y, z);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::on_paused()
{
    lb_online->setText("ONLINE:");
    m_online = true;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWindow::on_resumed()
{
    lb_online->setText("OFFLINE:");
    m_online = false;
}
