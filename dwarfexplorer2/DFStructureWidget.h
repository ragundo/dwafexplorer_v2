/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DFSTRUCTUREWIDGET_H
#define DFSTRUCTUREWIDGET_H

#include "DockManager.h"
#include "xml_parser/Field.h"
#include <QWidget>

QT_BEGIN_NAMESPACE
class QAction;
class QMenu;
class QUndoStack;
class QLabel;
class QDragEnterEvent;
class QDropEvent;
class QTreeView;
class QItemSelection;
class QTextEdit;
class QSplitter;
class QTabWidget;
QT_END_NAMESPACE

class QHexView;
class XML_Highlighter;
class DF_Model;

namespace rdf
{
struct NodeBase;
}

class DFStructureWidget : public QWidget
{
    Q_OBJECT

  public:
    DFStructureWidget(QTabWidget*                         parent,
                      std::map<std::string, rdf::Field*>* p_types_table,
                      DF_Model*                           p_model,
                      const QString&                      p_expression);
    QTreeView* get_treeview();

  signals:
    void hyperlink_selection_has_value(bool);
    void selection_has_coordinate(int x, int y, int z);

  public slots:
    void on_treeView_selection_changed(const QItemSelection& p_index_selected, const QItemSelection& p_index_deselected);
    void on_treeView_expanded(const QModelIndex& p_index);
    void on_treeView_xml_expanded(const QModelIndex& p_index);
    void on_hyperlink_changed(const QString& p_value);

  private slots:
    void on_paused();
    void on_resumed();

  private:
    void create_dock(QTabWidget*    p_parent_tabwidget,
                     DF_Model*      p_model,
                     const QString& p_expression);
    void process_xml_tree(rdf::NodeBase* p_node_selected);
    void process_memory(rdf::NodeBase* p_node_selected);

    QLabel *lbAddress, *lbAddressName;
    QLabel *lbOverwriteMode, *lbOverwriteModeName;
    QLabel *lbSize, *lbSizeName;

    QTabWidget*      m_tabwidget;
    QTreeView*       m_treeview; // Main tree
    QHexView*        m_hexview; // memory view
    QTreeView*       m_df_xml_treeview; // XML source view
    QTextEdit*       m_xml_text_viewer;
    XML_Highlighter* m_highlighter;

    // The main container for docking
    ads::CDockManager* m_DockManager;

    //  The types table that contains all the Field for each df structuture type
    std::map<std::string, rdf::Field*>* m_types_table;
    bool                                m_online;
};

#endif // DFSTRUCTUREWIDGET_H