#ifndef DFNODEGENERATOR_H
#define DFNODEGENERATOR_H

#include "NodeTable.h"
#include <string>

namespace rdf
{
struct Node;

class DFNode_Generator
{
  public:
    static NodeBase* process_expression(std::string& p_expression,
                                        NodeTable*   p_node_table);

  private:
    static NodeBase* process_globals(NodeTable* p_node_table);

    static NodeBase* process_global(std::string& p_token,
                                    std::string& p_current_path,
                                    NodeBase*    p_current_node,
                                    NodeTable*   p_node_table);

    static NodeBase* process_array_or_vector(std::string& p_token,
                                             std::string& p_current_path,
                                             NodeBase*    p_current_node,
                                             NodeTable*   p_node_table);

    static NodeBase* process_field(std::string& p_token,
                                   std::string& p_current_path,
                                   NodeBase*    p_current_node,
                                   NodeTable*   p_node_table);
};

} // namespace rdf
#endif //DFNODEGENERATOR_H