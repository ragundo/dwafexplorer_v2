/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "df_xml_model.h"
#include "df_xml_item.h"
#include "metadata_server/MetadataServer.h"
#include <QPixmap>

using namespace rdf;

extern DF_XML_Item*    xml_node_dummy();
extern CMetadataServer MetadataServer;

DF_XML_Model::DF_XML_Model(QObject* p_parent, std::map<std::string, rdf::Field*>* p_types_table)
    : QAbstractItemModel(p_parent), m_types_table(p_types_table)
{
    m_root_node = new DF_XML_Item("root", "node", nullptr);
}

//
//------------------------------------------------------------------------------------//
//
QVariant get_decoration_role(rdf::DF_XML_Item* p_node)
{
    if (p_node->get_key() == "uint8_t" ||
        p_node->get_key() == "uint16_t" ||
        p_node->get_key() == "uint32_t" ||
        p_node->get_key() == "uint64_t" ||
        p_node->get_key() == "int8_t" ||
        p_node->get_key() == "int8" ||
        p_node->get_key() == "int16_t" ||
        p_node->get_key() == "int16" ||
        p_node->get_key() == "int32_t" ||
        p_node->get_key() == "int32" ||
        p_node->get_key() == "int64_t" ||
        p_node->get_key() == "long" ||
        p_node->get_key() == "s-float" ||
        p_node->get_key() == "bool")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "stl-string" ||
        p_node->get_key() == "static-string" ||
        p_node->get_key() == "ptr-string")
        return QPixmap(":/t2_small.png");

    if (p_node->get_key() == "compound")
        return QPixmap(":/folder.png");

    if (p_node->get_key() == "union")
        return QPixmap(":/folder.png");

    if (p_node->get_key() == "struct")
        return QPixmap(":/folder.png");

    if (p_node->get_key() == "class")
        return QPixmap(":/folder.png");

    if (p_node->get_key() == "pointer")
        return QPixmap(":/cube.png");

    if (p_node->get_key() == "stl-vector")
        return QPixmap(":/group.png");

    if (p_node->get_key() == "stl-bit-vector")
        return QPixmap(":/group.png");

    if (p_node->get_key() == "static-array")
        return QPixmap(":/group.png");

    if (p_node->get_key() == "df-linked-list")
        return QPixmap(":/group.png");

    if (p_node->get_key() == "df-flag-array")
        return QPixmap(":/group.png");

    if (p_node->get_key() == "enum")
        return QPixmap(":/square.png");

    if (p_node->get_key() == "enum-type")
        return QPixmap(":/square.png");

    if (p_node->get_key() == "bitfield")
        return QPixmap(":/stripes.png");

    if (p_node->get_key() == "bitfield-type")
        return QPixmap(":/stripes.png");

    if (p_node->get_key() == "df-flagarray")
        return QPixmap(":/stripes.png");

    if (p_node->get_key() == "padding")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "xml-type")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "Definition")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "name")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "count")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "type-name")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "ref-target")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "refers-to")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "index-refers-to")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "comment")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "instance-vector")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "key-field")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "original-name")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "since")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "base-type")
        return QPixmap(":/circle.png");

    if (p_node->get_key() == "max-value")
        return QPixmap(":/circle.png");

    return QVariant();
}

//
//---------------------------------------------------------------------------------------
//
QModelIndex DF_XML_Model::index(int p_row, int p_column, const QModelIndex& p_parent) const
{
    if (!hasIndex(p_row, p_column, p_parent))
        return QModelIndex();

    DF_XML_Item* parentItem;

    if (!p_parent.isValid())
        parentItem = m_root_node;
    else
        parentItem = static_cast<DF_XML_Item*>(p_parent.internalPointer());

    DF_XML_Item* childItem = parentItem->child(p_row);
    if (childItem)
        return createIndex(p_row, p_column, childItem);
    return QModelIndex();
}

//
//---------------------------------------------------------------------------------------
//
QModelIndex DF_XML_Model::parent(const QModelIndex& p_child_index) const
{
    if (!p_child_index.isValid())
        return QModelIndex();

    DF_XML_Item* childItem  = static_cast<DF_XML_Item*>(p_child_index.internalPointer());
    DF_XML_Item* parentItem = childItem->parentItem();

    if (parentItem == nullptr)
        return QModelIndex();

    if (parentItem == m_root_node)
        return QModelIndex();

    return createIndex(parentItem->row(), 0, parentItem);
}

//
//---------------------------------------------------------------------------------------
//
int DF_XML_Model::rowCount(const QModelIndex& p_parent) const
{
    DF_XML_Item* parentItem;

    //    if (p_parent.column() > 1)
    //        return 0;

    if (!p_parent.isValid())
        parentItem = m_root_node;
    else
        parentItem = static_cast<DF_XML_Item*>(p_parent.internalPointer());

    return parentItem->childCount();
}

//
//------------------------------------------------------------------------------------//
//
DF_XML_Item* DF_XML_Model::nodeFromIndex(const QModelIndex& p_index) const
{
    if (p_index.isValid())
    {
        return static_cast<DF_XML_Item*>(p_index.internalPointer());
    }
    else
    {
        return m_root_node;
    }
}

//
//---------------------------------------------------------------------------------------
//
QVariant DF_XML_Model::data(const QModelIndex& p_index, int p_role) const
{
    if (!p_index.isValid())
        return QVariant();

    if ((p_index.column() == 0) && (p_role == Qt::DecorationRole))
    {
        auto item = nodeFromIndex(p_index);
        if (item->childCount() > 0)
            return QPixmap(":/folder.png");
        return QPixmap(":/circle.png");
    }

    if (p_role != Qt::DisplayRole)
        return QVariant();

    DF_XML_Item* item = static_cast<DF_XML_Item*>(p_index.internalPointer());

    return item->data(p_index.column());
}

//
//---------------------------------------------------------------------------------------
//
Qt::ItemFlags DF_XML_Model::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return Qt::NoItemFlags;

    return QAbstractItemModel::flags(index);
}

//
//---------------------------------------------------------------------------------------
//
int DF_XML_Model::columnCount(const QModelIndex& parent) const
{
    return 2;
}

//
//---------------------------------------------------------------------------------------
//
bool DF_XML_Model::insert_child_nodes(const QModelIndex& p_parent_index, bool p_is_root_node)
{
    if (!p_parent_index.isValid())
    {
        // root node
        auto num_nodes = construct_node(m_root_node,
                                        true); // Only compute the num of nodes
        beginInsertRows(p_parent_index, 0, num_nodes);
        construct_node(m_root_node);
        endInsertRows();
        return true;
    }

    auto         num_rows    = rowCount(p_parent_index);
    DF_XML_Item* parent_item = static_cast<DF_XML_Item*>(p_parent_index.internalPointer());
    auto         field       = parent_item->get_field();
    // Compute the num of children to add
    auto start     = num_rows - 1 < 0 ? 0 : num_rows - 1;
    auto num_nodes = construct_node(parent_item,
                                    true); // Only compute the num of nodes
    beginInsertRows(p_parent_index, start, num_nodes);
    construct_node(parent_item);
    endInsertRows();
    return true;
}

//
//---------------------------------------------------------------------------------------
//
bool DF_XML_Model::insert_child_nodes(Field* p_field)
{
    if (p_field == nullptr)
        return false;

    // Locate the field in the model
    auto parent_index_maybe = get_index_from_field(p_field);
    if (!parent_index_maybe)
        return false;
    return insert_child_nodes(*parent_index_maybe);
}

//
//---------------------------------------------------------------------------------------
//
bool DF_XML_Model::delete_dummy_node(const QModelIndex& p_parent_index)
{
    if (!p_parent_index.isValid())
        return false;

    beginRemoveRows(p_parent_index, 0, 1);
    auto item = static_cast<rdf::DF_XML_Item*>(p_parent_index.internalPointer());
    item->clear_children();
    endRemoveRows();
    return true;
}

//
//---------------------------------------------------------------------------------------
//

bool DF_XML_Model::hasChildren(const QModelIndex& p_parent) const
{
    if (!p_parent.isValid())
        return m_root_node->childCount() != 0;

    auto field = static_cast<DF_XML_Item*>(p_parent.internalPointer());
    if (field == nullptr)
        return false;
    return field->childCount() != 0;
}

//
//---------------------------------------------------------------------------------------
//
void DF_XML_Model::begin_reset_model()
{
    this->beginResetModel();
}

//
//---------------------------------------------------------------------------------------
//
void DF_XML_Model::end_reset_model()
{
    this->endResetModel();
}

//
//---------------------------------------------------------------------------------------
//
DF_XML_Model::~DF_XML_Model()
{
    delete m_root_node;
}

//
//---------------------------------------------------------------------------------------
//
QVariant DF_XML_Model::headerData(int section, Qt::Orientation orientation,
                                  int role) const
{
    if (orientation == Qt::Horizontal && role == Qt::DisplayRole)
    {
        if (section == 0)
            return QVariant("Key");
        if (section == 1)
            return QVariant("Value");
    }

    return QVariant();
}

//
//---------------------------------------------------------------------------------------
//
DF_XML_Item* DF_XML_Model::get_root_node()
{
    return m_root_node;
}

//
//---------------------------------------------------------------------------------------
//
void DF_XML_Model::set_root_node(DF_XML_Item* p_node)
{
    beginResetModel();
    delete m_root_node;
    m_root_node = p_node;
    endResetModel();
}

//
//---------------------------------------------------------------------------------------
//
void DF_XML_Model::refresh()
{
    emit dataChanged(index(0, 0), index(0, 1));
}

tl::optional<QModelIndex> DF_XML_Model::get_index_from_field_rec(DF_XML_Item*       p_parent_item,
                                                                 int                p_tree_row,
                                                                 const QModelIndex& p_parent_index,
                                                                 Field*             p_field)
{
    for (int i = 0; i < p_parent_item->childCount(); i++)
    {
        auto child = p_parent_item->child(i);
        if (child->get_field() == p_field)
        {
            // found
            return this->index(p_tree_row, 0, p_parent_index);
        }
        // Not found, check children of child
        auto child_index = index(i, 0, p_parent_index);
        get_index_from_field_rec(child,
                                 0,
                                 child_index,
                                 p_field);
    }
    return {};
}

//
//---------------------------------------------------------------------------------------
//
tl::optional<QModelIndex> DF_XML_Model::get_index_from_field(rdf::Field* p_field)
{
    if (m_root_node->get_field() == p_field)
        return QModelIndex();
    return get_index_from_field_rec(m_root_node, 0, QModelIndex(), p_field);
}

//
//---------------------------------------------------------------------------------------
//
std::string get_compound_name(Field* p_field)
{
    std::string result = "T_" + p_field->get_attribute("name");
    auto        iter   = p_field->get_parent();
    while (iter != nullptr && iter->get_type() != Type::Root)
    {
        if (iter->get_type() == Type::Compound)
        {
            auto compound_name = iter->get_attribute("name");
            result             = "T_" + compound_name + "::" + result;
            iter               = iter->get_parent();
            continue;
        }
        auto name = iter->get_attribute("type-name");
        result    = name + "::" + result;
        iter      = iter->get_parent();
    }
    return result;
}

//
//---------------------------------------------------------------------------------------
//
int DF_XML_Model::construct_node(DF_XML_Item* p_parent_node,
                                 bool         p_only_compute_num_nodes)
{
    auto num_nodes = 0;
    auto field     = p_parent_node->get_field();

    // XML tag
    if (!p_only_compute_num_nodes)
    {
        auto item_xml = new rdf::DF_XML_Item("xml-type",
                                             Type_to_string(field->get_type()),
                                             nullptr,
                                             p_parent_node);
        p_parent_node->appendChild(item_xml);
    }
    num_nodes++;

    // Add attributes
    auto              attributes_map     = field->get_attributes_map();
    bool              has_pointer        = false;
    bool              has_typename       = false;
    bool              has_index_enum     = 0;
    std::string       pointer_type_value = "";
    std::string       typename_value     = "";
    std::string       index_enum_value   = "";
    rdf::DF_XML_Item* item_pointer       = nullptr;
    rdf::DF_XML_Item* item_typename      = nullptr;
    rdf::DF_XML_Item* item_index_enum    = nullptr;

    for (auto& it : attributes_map)
    {
        std::string attribute_key   = it.first;
        std::string attribute_value = it.second;

        if (attribute_key == "pointer-type")
        {
            pointer_type_value = attribute_value;
            has_pointer        = true;
            continue;
        }
        if (attribute_key == "type-name")
        {
            typename_value = attribute_value;
            has_typename   = true;
            continue;
        }
        if (attribute_key == "index-enum")
        {
            index_enum_value = attribute_value;
            has_index_enum   = true;
            continue;
        }

        if (!p_only_compute_num_nodes)
        {
            rdf::DF_XML_Item* item_attribute = new rdf::DF_XML_Item(attribute_key,
                                                                    attribute_value,
                                                                    nullptr,
                                                                    p_parent_node);
            p_parent_node->appendChild(item_attribute);
        }
        num_nodes++;
    }

    DF_Type     df_type  = DF_Type::None;
    std::string xml_file = "";

    if (has_pointer)
        if (p_parent_node->get_value() != pointer_type_value)
        {
            // Add the data for the pointer
            auto it = m_types_table->find(pointer_type_value);
            if (it != m_types_table->end())
            {
                rdf::Field* pointer_field = (*it).second;
                if (!p_only_compute_num_nodes)
                {
                    item_pointer = new rdf::DF_XML_Item("pointer-type",
                                                        pointer_type_value,
                                                        pointer_field,
                                                        p_parent_node);
                    item_pointer->add_dummy_node();
                    p_parent_node->appendChild(item_pointer);
                }
                num_nodes++;
                df_type = MetadataServer.DF_Type_from_string(pointer_type_value);
            }
            else
            {
                // Simple type
                if (!p_only_compute_num_nodes)
                {
                    item_pointer = new rdf::DF_XML_Item("pointer-type",
                                                        pointer_type_value,
                                                        nullptr,
                                                        p_parent_node);
                    p_parent_node->appendChild(item_pointer);
                }
                num_nodes++;
            }
        }

    if (has_typename)
        if (p_parent_node->get_value() != typename_value)
        {
            // Add the data for the typename
            auto it = m_types_table->find(typename_value);
            if (it != m_types_table->end())
            {
                rdf::Field* typename_field = (*it).second;
                if (!p_only_compute_num_nodes)
                {
                    item_typename = new rdf::DF_XML_Item("type-name",
                                                         typename_value,
                                                         typename_field,
                                                         p_parent_node);
                    item_typename->add_dummy_node();
                    p_parent_node->appendChild(item_typename);
                    df_type = MetadataServer.DF_Type_from_string(typename_value);
                }
                num_nodes++;
            }
            else
            {
                // Simple type
                if (!p_only_compute_num_nodes)
                {
                    item_typename = new rdf::DF_XML_Item("type-name",
                                                         typename_value,
                                                         nullptr,
                                                         p_parent_node);
                    p_parent_node->appendChild(item_typename);
                }
                num_nodes++;
            }
        }

    if (has_index_enum)
        if (p_parent_node->get_value() != index_enum_value)
        {
            // Add the data for the typename
            auto it = m_types_table->find(index_enum_value);
            if (it != m_types_table->end())
            {
                rdf::Field* index_enum_field = (*it).second;
                if (!p_only_compute_num_nodes)
                {
                    item_index_enum = new rdf::DF_XML_Item("index-enum",
                                                           index_enum_value,
                                                           index_enum_field,
                                                           p_parent_node);
                    item_index_enum->add_dummy_node();
                    p_parent_node->appendChild(item_index_enum);
                    df_type = MetadataServer.DF_Type_from_string(index_enum_value);
                }
                num_nodes++;
            }
        }

    // Children
    if (field->get_num_children() > 0)
        if (p_parent_node->get_key() != "Children")
        {
            if (!p_only_compute_num_nodes)
            {
                // Select the parent
                rdf::DF_XML_Item* parent = nullptr;
                if (has_pointer)
                    parent = item_pointer;
                else if (has_typename)
                    parent = item_typename;
                else if (has_index_enum)
                    parent = item_index_enum;
                else
                {
                    auto children_root_item = new rdf::DF_XML_Item("Children", // key
                                                                   "", // value
                                                                   field, // Field
                                                                   p_parent_node); // parent node

                    p_parent_node->appendChild(children_root_item);
                    parent = children_root_item;
                    num_nodes++;
                }
                if (parent)
                {
                    if (parent->childCount() == 1 && parent->child(0) == xml_node_dummy())
                        parent->clear_children();
                    for (size_t j = 0; j < field->get_num_children(); j++)
                    {
                        auto child      = field->get_pchild(j);
                        auto child_item = new rdf::DF_XML_Item(Type_to_string(child->get_type()),
                                                               child->get_attribute("name"),
                                                               child,
                                                               parent);
                        if (child->get_num_children() > 0)
                            child_item->add_dummy_node();
                        parent->appendChild(child_item);
                    }
                }
            }
        }
    // XML source file
    if (df_type == DF_Type::None)
    {
        if (field->get_type() == Type::Compound)
        {
            auto full_name = get_compound_name(field);
            full_name      = full_name.substr(0, full_name.find(':'));
            df_type        = MetadataServer.DF_Type_from_string(full_name);
        }
    }
    if (df_type != DF_Type::None)
    {
        auto maybe = MetadataServer.get_xml_file_metadata(df_type);
        if (maybe)
        {
            std::string xml_file = *maybe;
            if (!p_only_compute_num_nodes)
            {
                auto item_xml_file = new rdf::DF_XML_Item("Definition",
                                                          *maybe,
                                                          nullptr,
                                                          p_parent_node);
                p_parent_node->appendChild(item_xml_file);
            }
            num_nodes++;
        }
    }
    return num_nodes;
}
