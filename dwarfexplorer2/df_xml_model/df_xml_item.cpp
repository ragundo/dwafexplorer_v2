/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "df_xml_item.h"

namespace rdf
{
DF_XML_Item node_dummy("", "", nullptr);

DF_XML_Item* xml_node_dummy()
{
    return &node_dummy;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_XML_Item::DF_XML_Item(DF_XML_Item* parent)
    : m_key(""), m_value(""), m_field(nullptr), m_parentItem(parent)
{
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_XML_Item::DF_XML_Item(Field*       p_field,
                         DF_XML_Item* p_parent)
    : m_key(""), m_value(""), m_field(p_field), m_parentItem(p_parent)
{
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_XML_Item::DF_XML_Item(const std::string& p_key,
                         const std::string& p_value,
                         Field*             p_field,
                         DF_XML_Item*       p_parent)
    : m_key(p_key), m_value(p_value), m_field(p_field), m_parentItem(p_parent)
{
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_XML_Item::~DF_XML_Item()
{
    for (auto& child : m_childItems)
        if (child != xml_node_dummy())
            delete child;
    m_childItems.clear();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::appendChild(DF_XML_Item* item)
{
    m_childItems.push_back(item);
    item->m_parentItem = this;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_XML_Item* DF_XML_Item::child(int row)
{
    if (row < 0)
        return nullptr;
    if ((size_t)row >= m_childItems.size())
        return nullptr;
    return m_childItems.at(row);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int DF_XML_Item::childCount() const
{
    return m_childItems.size();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int DF_XML_Item::row() const
{
    if (m_parentItem)
    {
        int row = 0;
        for (auto& child : m_parentItem->m_childItems)
        {
            if (this == child)
                return row;
            row++;
        }
    }
    return -1;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
int DF_XML_Item::columnCount() const
{
    return 2;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
QVariant DF_XML_Item::data(int column) const
{
    if (column < 0 || column >= 2)
        return QVariant();
    if (column == 0)
        return QString::fromStdString(m_key);
    return QString::fromStdString(m_value);
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
DF_XML_Item* DF_XML_Item::parentItem()
{
    return m_parentItem;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::clear()
{
    m_value = std::string("");
    m_key   = std::string("");
    clear_children();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::clear_children()
{
    for (auto& child : m_childItems)
        if (child != &node_dummy)
            delete child;
    m_childItems.clear();
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
Field* DF_XML_Item::get_field()
{
    return m_field;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::string DF_XML_Item::get_key()
{
    return m_key;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
std::string DF_XML_Item::get_value()
{
    return m_value;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::set_field(Field* p_field)
{
    m_field = p_field;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::set_key(const std::string& p_key)
{
    m_key = p_key;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::set_value(const std::string& p_value)
{
    m_value = p_value;
}

//
//--------------------------------------------------------------------------------------------------------------------//
//
void DF_XML_Item::add_dummy_node()
{
    appendChild(xml_node_dummy());
}

} // namespace rdf