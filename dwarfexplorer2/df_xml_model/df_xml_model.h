/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DF_XML_MODEL_H
#define DF_XML_MODEL_H

#include "tl/tl_optional.h"
#include <QAbstractItemModel>

namespace rdf
{
class DF_XML_Item;
class Field;
} // namespace rdf

namespace rdf
{
class DF_XML_Model : public QAbstractItemModel
{
    Q_OBJECT

  private:
    rdf::DF_XML_Item*                   m_root_node;
    std::map<std::string, rdf::Field*>* m_types_table;

  public:
    DF_XML_Model(QObject* p_parent, std::map<std::string, rdf::Field*>* p_types_table);
    ~DF_XML_Model();

    DF_XML_Item* get_root_node();
    void         set_root_node(DF_XML_Item* p_node);

    QModelIndex   index(int p_row, int p_column, const QModelIndex& p_parent = QModelIndex()) const;
    QModelIndex   parent(const QModelIndex& p_child_index) const;
    int           rowCount(const QModelIndex& p_parent = QModelIndex()) const;
    QVariant      data(const QModelIndex& p_index, int p_role = Qt::DisplayRole) const;
    int           columnCount(const QModelIndex& parent = QModelIndex()) const;
    bool          hasChildren(const QModelIndex& p_parent = QModelIndex()) const;
    Qt::ItemFlags flags(const QModelIndex& index) const;
    QVariant      headerData(int section, Qt::Orientation orientation, int role) const;

    void begin_reset_model();
    void end_reset_model();

    bool insert_child_nodes(const QModelIndex& p_index,
                            bool               p_only_compute_num_nodes = false);

    bool insert_child_nodes(rdf::Field* p_field);

    bool delete_dummy_node(const QModelIndex& p_index);

    void refresh();

    rdf::DF_XML_Item* nodeFromIndex(const QModelIndex& p_index) const;

  private:
    int construct_node(DF_XML_Item* p_parent_node,
                       bool         p_only_compute_num_nodes = false);

    tl::optional<QModelIndex> get_index_from_field(rdf::Field* p_field);

    tl::optional<QModelIndex> get_index_from_field_rec(DF_XML_Item*       p_parent_item,
                                                       int                p_tree_row,
                                                       const QModelIndex& p_parent_index,
                                                       Field*             p_field);
};

} // namespace rdf
#endif