/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DF_XML_ITEM_H
#define DF_XML_ITEM_H
#include "xml_parser/Field.h"
#include <QVariant>
#include <vector>

namespace rdf
{
class DF_XML_Item
{
  public:
    DF_XML_Item(DF_XML_Item* parent = nullptr);

    DF_XML_Item(Field*       p_field,
                DF_XML_Item* parent = nullptr);

    DF_XML_Item(const std::string& p_key,
                const std::string& p_value,
                Field*             p_field,
                DF_XML_Item*       parent = nullptr);

    ~DF_XML_Item();

    void appendChild(DF_XML_Item* child);
    void add_dummy_node();

    DF_XML_Item* child(int row);
    int          childCount() const;
    int          columnCount() const;
    QVariant     data(int column) const;
    int          row() const;
    DF_XML_Item* parentItem();
    void         clear();
    void         clear_children();
    rdf::Field*  get_field();
    std::string  get_key();
    std::string  get_value();
    void         set_key(const std::string&);
    void         set_value(const std::string&);
    void         set_field(rdf::Field*);

  private:
    std::vector<DF_XML_Item*> m_childItems;
    std::string               m_key;
    std::string               m_value;
    Field*                    m_field;
    DF_XML_Item*              m_parentItem;
};

DF_XML_Item* xml_node_dummy();

} // namespace rdf
#endif