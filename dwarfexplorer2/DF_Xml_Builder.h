/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#ifndef DF_XML_BUILDER_H
#define DF_XML_BUILDER_H
#include <string>
#include <strstream>

namespace rdf
{
class Field;
}

class DF_Xml_Builder
{
  public:
    std::string build_xml(rdf::Field* p_field);

  private:
    void process_shared_elements(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_shared_attributes(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_data_definition(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_typedef_elements(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_symbols_elements(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_enum_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_bitfield_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_compound_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_struct_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_class_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_dflinkedlist_typedef(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_global_object_def(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_symbol_table(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_compound_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_extra_include(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_custom_methods(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_virtual_methods(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_method(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_simple_field_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_compound_field_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_container_field_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_bit_container_field_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_boolean_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_integer_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_float_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_stl_string_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_stl_vector_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_stl_bit_vector_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_stl_deque_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_stl_set_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_stl_fstream_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_df_linked_list_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_df_array_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_df_flag_array_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_static_array_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_enum_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_bitfield_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_pointer_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_ptr_string_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_static_string_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_padding_field(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_enum_attr(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_enum_item(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_enum_item_attr(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void proces_flag_bit(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_integer_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_signed_integer_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_unsigned_integer_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_os_type(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_binary_timestamp(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_MD5_hash(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_global_address(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_vtable_address(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void do_work_rec(rdf::Field* p_field, std::strstream& ss, int depth);
    void add_children(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void process_flagbit(rdf::Field* p_field, std::strstream& ss, int p_depth);
    void fill_xml_entry(rdf::Field*        p_field,
                        std::strstream&    ss,
                        int                p_depth,
                        const std::string& p_keyword,
                        bool               p_is_container = false);
};

#endif //DF_XML_BUILDER_H
