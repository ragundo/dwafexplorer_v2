/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "DataDefs.h"
#include <Export.h>
#include <PluginManager.h>
#include <df/coord.h>
#include <modules/Gui.h>
#include <modules/World.h>
#include <tinythread.h>

#include "./ui_DwarfExplorerMainWindow.h"
#include "DFStructureWindow.h"
#include "DwarfExplorerMainWindow.h"
#include "EventProxy.h"
#include "MetadataServer.h"
#include "Parser.h"
#include "lisp_parser/LispParser.h"
#include <Console.h>
#include <QCompleter>
#include <QDebug>
#include <QMessageBox>
#include <QStringList>
#include <QStringListModel>
#include <RemoteClient.h>
#include <RemoteServer.h>
#include <VersionInfo.h>
#include <chrono>
#include <cstdint>

#include "DFNodeGenerator.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "df_xml_model/df_xml_model.h"
#include "node_builder/NodeBuilder.h"

#include "DataDefs.h"
#include "df/global_objects.h"
#include "df/ui_sidebar_mode.h"

using namespace rdf;

extern rdf::CMetadataServer MetadataServer;
extern int                  g_x, g_y, g_z;

//
//------------------------------------------------------------
//
DwarfExplorerMainWindow::DwarfExplorerMainWindow(std::shared_ptr<EventProxy>&& proxy, QWidget* parent)
    : QMainWindow(parent),
      ui(new Ui::DwarfExplorerMainWindow),
      event_proxy(std::move(proxy)),
      m_core_suspender(nullptr),
      m_suspended(false)
{
    setWindowIcon(QIcon(":/icon/dwarf.png"));
    setAttribute(Qt::WA_DeleteOnClose);
    ui->setupUi(this);

    // Connect signal for double click in treewidget
    bool ok = connect(this->ui->treeWidget,
                      &QTreeWidget::itemDoubleClicked,
                      this,
                      &DwarfExplorerMainWindow::on_tree_widget_double_clicked);

    // Get the types table that the xml parser created from the xml files
    m_types_table = m_xml_parser.parse_df_xml_files();

    // Create the QCompleter for auto completion in the line editor
    m_df_string_list_model = new DFStringListModel();
    QCompleter* completer  = new QCompleter(this);
    completer->setCaseSensitivity(Qt::CaseInsensitive);
    completer->setModel(m_df_string_list_model);
    ui->lineEdit->setCompleter(completer);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
DwarfExplorerMainWindow::~DwarfExplorerMainWindow()
{
    delete ui;
    delete m_df_string_list_model;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::on_lineEdit_textChanged(const QString& arg1)
{
    if (arg1[arg1.length() - 1] != '.')
        return;
    m_df_string_list_model->process_name(arg1);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
bool is_expression_already_in_used_expressions_tree(const QString& p_expression, QTreeWidget* p_tree_widget)
{
    // If the expression is correct but it's one of the favourites, don't do anything
    if (p_expression == "df.global")
        return true;
    if (p_expression == "df.global.world")
        return true;
    if (p_expression == "df.global.world.world_data")
        return true;
    if (p_expression == "df.global.world.units")
        return true;
    if (p_expression == "df.global.world.buildings")
        return true;
    if (p_expression == "df.global.world.history.figures")
        return true;

    // Look for it in the recent subtree
    bool                    found = false;
    QTreeWidgetItemIterator current(p_tree_widget);
    while (*current)
    {
        QString text_twi = (*current)->text(0);
        if (text_twi == p_expression)
        {
            found = true;
            break;
        }
        ++current;
    }
    return found;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void add_current_expression_to_used_expressions_tree(const QString& p_expression, QTreeWidget* p_tree_widget)
{
    // Add the expression to the recent used entry
    QString search_for = "Recently used";

    QList<QTreeWidgetItem*> clist = p_tree_widget->findItems(
        search_for,
        Qt::MatchContains | Qt::MatchRecursive,
        0);

    // Put the new inserted item as the first child
    foreach (QTreeWidgetItem* parent, clist)
    {
        QTreeWidgetItem* new_entry = new QTreeWidgetItem;
        parent->addChild(new_entry);
        for (int i = parent->childCount() - 1; i > 0; i--)
        {
            parent->child(i)->setText(0, parent->child(i - 1)->text(0));
        }

        parent->child(0)->setText(0, p_expression);
        break;
    }
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::update_tree(const QString& p_expression)
{
    if (!is_expression_already_in_used_expressions_tree(p_expression, this->ui->treeWidget))
        add_current_expression_to_used_expressions_tree(p_expression, this->ui->treeWidget);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<DF_Model*, NodeBase*> process_df_global_plus(const QString& p_expression, NodeTable* p_node_table)
{
    // Parse the expression using a Parser object.
    // This parser only checks that the expression is sintactically correct
    // It don't check, for example, if a vector access is out of bounds
    Parser parser;
    auto   expected_parsed_expression = parser.parse_expression(p_expression);

    if (!expected_parsed_expression)
        return std::pair<DF_Model*, NodeBase*>(nullptr, nullptr);

    // The expression is sintactically correct

    // Create a new model
    auto new_model = new DF_Model();

    // Create the root node for this subtree (not visible in the TreeView)
    auto n_root          = new NodeRoot;
    n_root->m_rdf_type   = RDF_Type::None;
    n_root->m_df_type    = DF_Type::None;
    n_root->m_node_type  = NodeType::Root;
    n_root->m_node_table = p_node_table;
    new_model->set_root(n_root);

    auto expression   = p_expression.toStdString();
    auto current_node = DFNode_Generator::process_expression(expression,
                                                             p_node_table);

    if (current_node == nullptr)
    {
        return std::pair<DF_Model*, NodeBase*>(nullptr, nullptr);
        delete new_model;
    }

    // Add the node to the root
    new_model->add_child_node_to_root(current_node);
    n_root->m_has_children = true;

    auto node = dynamic_cast<Node*>(current_node);

    n_root->m_field_name = node->parent()->m_field_name;
    n_root->m_field_path = node->parent()->m_field_path;

    if (node->m_has_children)
    {
        // Node not expanded yet in any window
        if (node->get_num_children() == 0)
            node->add_dummy_node();
        else if (!node->has_dummy_node()) // Node is already expanded in another window
            new_model->add_children_from_node(node);
    }

    return std::pair<DF_Model*, NodeBase*>(new_model, current_node);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::on_treeWidget_currentItemChanged(QTreeWidgetItem* current,
                                                               QTreeWidgetItem* previous)
{
    if (current)
    {
        if (current->text(0) == "Most used")
            return;
        if (current->text(0) == "Current Selection")
            return;
        if (current->text(0) == "Recently used")
            return;
        if (current->text(0) == "Globals")
        {
            this->ui->lineEdit->setText("df.global");
            return;
        }
        if (current->text(0) == "World")
        {
            this->ui->lineEdit->setText("df.global.world");
            return;
        }
        if (current->text(0) == "World Data")
        {
            this->ui->lineEdit->setText("df.global.world.world_data");
            return;
        }
        if (current->text(0) == "All units")
        {
            this->ui->lineEdit->setText("df.global.world.units");
            return;
        }
        if (current->text(0) == "All buildings")
        {
            this->ui->lineEdit->setText("df.global.world.buildings");
            return;
        }
        if (current->text(0) == "All historical Figures")
        {
            this->ui->lineEdit->setText("df.global.world.history.figures");
            return;
        }
        if (current->text(0) == "Unit")
        {
            // Gui::getSelectedUnit
            this->ui->lineEdit->setText("$selected-unit");
            return;
        }
        if (current->text(0) == "Building")
        {
            // Gui::getSelectedBuilding
            this->ui->lineEdit->setText("$selected-building");
            return;
        }

        this->ui->lineEdit->setText(current->text(0));
    }
}

//
//---------------------------------------------------------------------------------------
//
void DwarfExplorerMainWindow::on_suspend_action_triggered()
{
    if (m_core_suspender == nullptr)
        m_core_suspender = new DFHack::CoreSuspender;
    else
        m_core_suspender->lock();

    m_suspended = true;

    ui->stackedWidget->setCurrentIndex(1);
    ui->suspend_action->setEnabled(false);
    ui->resume_action->setEnabled(true);

    emit paused();
}

//
//---------------------------------------------------------------------------------------
//
void DwarfExplorerMainWindow::on_resume_action_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
    ui->resume_action->setEnabled(false);
    ui->suspend_action->setEnabled(true);
    if (m_core_suspender != nullptr)
    {
        m_core_suspender->unlock();
        delete m_core_suspender;
        m_core_suspender = nullptr;
    }
    m_suspended = false;
    emit resumed();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::on_tree_widget_double_clicked(QTreeWidgetItem* item, int column)
{
    auto item_text = item->text(0);
    if (item_text == "Globals")
        this->ui->lineEdit->setText("df.global");
    else if (item_text == "World")
        this->ui->lineEdit->setText("df.global.world");
    else if (item_text == "World Data")
        this->ui->lineEdit->setText("df.global.world.world_data");
    else if (item_text == "All units")
        this->ui->lineEdit->setText("df.global.world.units");
    else if (item_text == "All buildings")
        this->ui->lineEdit->setText("df.global.world.buildings");
    else if (item_text == "All historical Figures")
        this->ui->lineEdit->setText("df.global.world.history.figures");
    else if (item_text == "Unit")
        this->ui->lineEdit->setText("$selected-unit");
    else if (item_text == "Building")
        this->ui->lineEdit->setText("$selected-building");
    else if (item_text == "Most used")
        return;
    else if (item_text == "Current Selection")
        return;
    else if (item_text == "Recently used")
        return;
    else
        // From here are the entries in the recently used subtree
        this->ui->lineEdit->setText(item->text(0));

    // Fire signal for double click
    emit this->ui->pushButton->animateClick();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::create_window_from_expression(const QString& p_lua_expression)
{
    std::pair<DF_Model*, NodeBase*> data;
    data = process_df_global_plus(p_lua_expression, &m_node_table);

    DF_Model* new_model    = data.first;
    NodeBase* current_node = data.second;

    if (new_model == nullptr)
    {
        QMessageBox message_box;
        message_box.setText("ERROR: expression is incorrect");
        message_box.exec();
        return;
    }

    // expression is correct
    // Add the expression to the recent used ones
    update_tree(p_lua_expression);

    // Create a new Window to show this expression
    DFStructureWindow* new_window = new DFStructureWindow(this,
                                                          &m_types_table,
                                                          new_model,
                                                          p_lua_expression);
    new_model->setParent(new_window);
    new_model->set_expression(p_lua_expression);

    new_window->setWindowTitle("Dwarf Explorer - " + p_lua_expression);

    // Show the window
    new_window->show();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::create_window_from_node(rdf::NodeBase* p_selected_node)
{
    // Create a new model
    auto new_model = new DF_Model();

    // Create the root node for this subtree (not visible in the TreeView)
    auto n_root          = new NodeRoot;
    n_root->m_rdf_type   = RDF_Type::None;
    n_root->m_df_type    = DF_Type::None;
    n_root->m_node_type  = NodeType::Root;
    n_root->m_node_table = p_selected_node->m_node_table;
    n_root->m_field_name = p_selected_node->parent()->m_field_name;
    n_root->m_field_path = p_selected_node->parent()->m_field_path;

    // Set it in the model
    new_model->set_root(n_root);

    // Add the source node to the root
    new_model->add_child_node_to_root(p_selected_node);

    // expression is correct
    // Add the expression to the recent used ones
    auto new_expression = QString::fromStdString(p_selected_node->path());
    update_tree(new_expression);

    // Create a new Window to show this expression
    DFStructureWindow* new_window = new DFStructureWindow(this,
                                                          &m_types_table,
                                                          new_model,
                                                          new_expression);
    new_model->setParent(new_window);
    new_model->set_expression(new_expression);

    new_window->setWindowTitle("Dwarf Explorer - " + new_expression);

    // Show the window
    new_window->show();
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::create_tab_from_expression(const QString& p_lua_expression, DFStructureWindow* p_window)
{
    std::pair<DF_Model*, NodeBase*> data = process_df_global_plus(p_lua_expression, &m_node_table);

    DF_Model* new_model    = data.first;
    NodeBase* current_node = data.second;

    if (new_model == nullptr)
    {
        QMessageBox message_box;
        message_box.setText("ERROR: expression is incorrect");
        message_box.exec();
        return;
    }

    // expression is correct
    // Add the expression to the recent used ones
    update_tree(p_lua_expression);

    // Create a new Window to show this expression
    DFStructureWindow* new_window = new DFStructureWindow(this,
                                                          &m_types_table,
                                                          new_model,
                                                          p_lua_expression);
    new_model->setParent(new_window);
    new_model->set_expression(p_lua_expression);

    // Create a new tab to show this expression
    p_window->create_tab(new_model,
                         p_lua_expression);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::create_tab_from_node(rdf::NodeBase* p_selected_node, DFStructureWindow* p_window)
{
    // Create a new model
    auto new_model = new DF_Model();

    // Create the root node for this subtree (not visible in the TreeView)
    auto n_root            = new NodeRoot;
    n_root->m_rdf_type     = RDF_Type::None;
    n_root->m_df_type      = DF_Type::None;
    n_root->m_node_type    = NodeType::Root;
    n_root->m_node_table   = p_selected_node->m_node_table;
    n_root->m_field_name   = p_selected_node->parent()->m_field_name;
    n_root->m_field_path   = p_selected_node->parent()->m_field_path;
    n_root->m_has_children = true;

    // Set it in the model
    new_model->set_root(n_root);

    // Add the source node to the root
    new_model->add_child_node_to_root(p_selected_node);

    // expression is correct
    // Add the expression to the recent used ones
    auto new_expression = QString::fromStdString(p_selected_node->path());
    update_tree(new_expression);

    new_model->setParent(p_window);
    new_model->set_expression(new_expression);

    // Create a new tab to show this expression
    p_window->create_tab(new_model,
                         new_expression);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::on_pushButton_clicked()
{
    create_window_from_expression(ui->lineEdit->text());
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DwarfExplorerMainWindow::locate_coordinate_in_map(int x, int y, int z)
{
    g_x = x;
    g_y = y;
    g_z = z;

    DFHack::World::SetPauseState(true);
    m_core_suspender->unlock();
    tthread::this_thread::sleep_for(tthread::chrono::milliseconds(50));
    m_core_suspender->lock();
}