/*
 * Copyright 2020 ragundo
 *
 * This file is part of dwarfexplorer plugin for DFHack
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 *
 */

#include "Core.h"
#include "DataDefs.h"
#include "df/global_objects.h"
#include "df/world.h"

#include "DFStructureWidget.h"
#include "DFStructureWindow.h"
#include "DwarfExplorerMainWindow.h"
#include "QHexView/qhexview.h"
#include <QAction>
#include <QApplication>
#include <QColorDialog>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QFileDialog>
#include <QFontDatabase>
#include <QFontDialog>
#include <QItemSelection>
#include <QLabel>
#include <QMenuBar>
#include <QMessageBox>
#include <QSplitter>
#include <QStatusBar>
#include <QTabBar>
#include <QTabWidget>
#include <QTextEdit>
#include <QToolBar>
#include <QTreeView>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QVBoxLayout>
#include <algorithm>
#include <set>

#include "DFStructureWidget.h"
#include "DFStructureWindow.h"
#include "DF_Xml_Builder.h"
#include "DwarfExplorerMainWindow.h"
#include "MetadataServer.h"
#include "QHexView/document/buffer/qmemorybuffer.h"
#include "XML_Highlighter.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "df_utils.h"
#include "df_xml_model/df_xml_item.h"
#include "df_xml_model/df_xml_model.h"
#include "lisp_parser/LispParser.h"
#include "xml_parser/Field.h"

#include "df/army.h"
#include "df/building.h"
#include "df/construction.h"
#include "df/coord.h"
#include "df/coord_rect.h"
#include "df/deep_vein_hollow.h"
#include "df/engraving.h"
#include "df/global_objects.h"
#include "df/item.h"
#include "df/job.h"
#include "df/machine.h"
#include "df/plant.h"
#include "df/report.h"
#include "df/ui.h"
#include "df/ui_sidebar_mode.h"
#include "df/unit.h"
#include "df/vehicle.h"
#include "df/vermin.h"

extern rdf::CMetadataServer MetadataServer;
extern rdf::DF_XML_Item*    xml_node_dummy();

using namespace std;

/*
<ragundo> Why is there union-tag-field and union-tag-attr?. Aren't they exclusive to each other?
<lethosor> no, it's a bit tricky to explain, I'll hunt down links
<lethosor> union-tag-field=job_type means the sibling job_type field tells you what item of the enum is valid: https://github.com/DFHack/df-structures/blob/0.47.04-r1/df.units.xml#L1631
<lethosor> and union-tag-attr tells you which enum attr to look at to match the enum value to the union item: https://github.com/DFHack/df-structures/blob/0.47.04-r1/df.job-types.xml#L927
<lethosor> so if job_type = RecoverWounded, then look at info.bed_id
*/

/*****************************************************************************/
/* Public methods */
/*****************************************************************************/
DFStructureWidget::DFStructureWidget(QTabWidget*                         p_parent,
                                     std::map<std::string, rdf::Field*>* p_types_table,
                                     DF_Model*                           p_model,
                                     const QString&                      p_expression)
    : QWidget()
{
    m_online      = true;
    m_types_table = p_types_table;
    create_dock(p_parent, p_model, p_expression);
    auto window      = p_parent->parent();
    auto main_window = dynamic_cast<DwarfExplorerMainWindow*>(window->parent());

    connect(main_window,
            &DwarfExplorerMainWindow::paused,
            this,
            &DFStructureWidget::on_paused);

    connect(main_window,
            &DwarfExplorerMainWindow::resumed,
            this,
            &DFStructureWidget::on_resumed);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
QTreeView* DFStructureWidget::get_treeview()
{
    return m_treeview;
}

/*****************************************************************************/
// Private Methods
/*****************************************************************************/

void DFStructureWidget::create_dock(QTabWidget*    p_parent_tabwidget,
                                    DF_Model*      p_model,
                                    const QString& p_expression)
{
    // Create the dock manager. Because the parent parameter is a QMainWindow
    // the dock manager registers itself as the central widget.
    m_DockManager = new ads::CDockManager(this);

    // Create content - this can be any application specific widget
    m_treeview = new QTreeView();
    m_treeview->setSelectionMode(QAbstractItemView::SingleSelection);
    //m_treeview->setSortingEnabled(true);
    m_treeview->setModel(p_model);
    //m_treeview->setStyleSheet("background-image: url(:/icon/dwarf.png) ; opacity: 5; background-repeat: no-repeat; background-position: center;");

    m_hexview = new QHexView();
    m_hexview->setReadOnly(true);

    m_df_xml_treeview = new QTreeView();
    m_df_xml_treeview->setSelectionMode(QAbstractItemView::SingleSelection);

    m_xml_text_viewer = new QTextEdit();
    QFont font        = QFontDatabase::systemFont(QFontDatabase::FixedFont);
    m_highlighter     = new XML_Highlighter(m_xml_text_viewer->document());
    font.setFixedPitch(true);
    font.setPointSize(10);
    const int    tabStop = 4; // 4 characters
    QFontMetrics metrics(font);
    m_xml_text_viewer->setTabStopWidth(tabStop * metrics.width(' '));
    m_xml_text_viewer->setFont(font);
    m_xml_text_viewer->setReadOnly(true);
    m_xml_text_viewer->setLineWrapMode(QTextEdit::LineWrapMode::NoWrap);

    auto xml_model = new rdf::DF_XML_Model(this, m_types_table);
    m_df_xml_treeview->setModel(xml_model);

    // Dock widget with the main tree
    // as the dock widget content
    // TODO titulo = expression
    ads::CDockWidget* DockWidget_tree = new ads::CDockWidget("Tree View");
    DockWidget_tree->setWidget(m_treeview);

    // Dock widget with the memory view
    ads::CDockWidget* DockWidget_hex = new ads::CDockWidget("Memory View");
    DockWidget_hex->setWidget(m_hexview);

    // Dock widget with the metadata view
    ads::CDockWidget* DockWidget_xml_tree = new ads::CDockWidget("Metadata View");
    DockWidget_xml_tree->setWidget(m_df_xml_treeview);

    // Dock widget with the xml source
    ads::CDockWidget* DockWidget_xml_source = new ads::CDockWidget("XML View");
    DockWidget_xml_source->setWidget(m_xml_text_viewer);

    // Add the docks
    // Center = TreeView
    m_DockManager->addDockWidget(ads::CenterDockWidgetArea, DockWidget_tree);
    auto botom_area = m_DockManager->addDockWidget(ads::BottomDockWidgetArea, DockWidget_hex);
    auto tab_area   = m_DockManager->addDockWidget(ads::RightDockWidgetArea, DockWidget_xml_tree, botom_area);
    m_DockManager->addDockWidgetTabToArea(DockWidget_xml_source, tab_area);

    auto mainLayout = new QVBoxLayout();
    mainLayout->addWidget(m_DockManager);
    setLayout(mainLayout);

    // Treeview expansion signal
    bool ok = connect(this->m_treeview,
                      &QTreeView::expanded,
                      this,
                      &DFStructureWidget::on_treeView_expanded);

    // XML Treeview expansion signal
    ok = connect(this->m_df_xml_treeview,
                 &QTreeView::expanded,
                 this,
                 &DFStructureWidget::on_treeView_xml_expanded);

    // Selection changed in the main tree
    auto sel_model = this->m_treeview->selectionModel();
    ok             = connect(sel_model,
                 &QItemSelectionModel::selectionChanged,
                 this,
                 &DFStructureWidget::on_treeView_selection_changed);

    auto df_structure_window = dynamic_cast<DFStructureWindow*>(p_parent_tabwidget->parent());

    ok = connect(this,
                 &DFStructureWidget::hyperlink_selection_has_value,
                 df_structure_window,
                 &DFStructureWindow::on_hyperlink_value_in_selection);

    ok = connect(this,
                 &DFStructureWidget::selection_has_coordinate,
                 df_structure_window,
                 &DFStructureWindow::on_selection_has_coordinate);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void update_metadata(QHexMetadata* p_metadata, rdf::NodeBase* p_node_selected, uint64_t p_new_address)
{
    int offset = p_node_selected->m_address - p_new_address;
    int line   = offset / 16;
    int column = offset % 16;
    int size   = p_node_selected->get_node_size();

    p_metadata->clear();

    auto current_line   = line;
    auto current_column = column;
    while (size > 0)
    {
        auto highlighted_columns = std::min(16 - current_column, std::min(size, 16));
        p_metadata->background(current_line,
                               current_column,
                               highlighted_columns,
                               Qt::yellow);
        current_line++;
        current_column = 0;
        size -= highlighted_columns;
    }
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void fill_child_item(QTreeWidgetItem* p_child_item, rdf::Field* p_child)
{
    std::string type_st = rdf::Type_to_string(p_child->get_type());
    p_child_item->setText(0, QString::fromStdString(type_st));
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
QPixmap get_tree_icon(const std::string& p_type)
{
    // TODO return QPixmanp segun tipo
    return QPixmap(":/circle.png");
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
int get_array_size_from_addornements_rec(std::string& p_addornements, size_t it)
{
    while (it < p_addornements.size() && p_addornements[it] != '[')
        it++;
    if (it < p_addornements.size() && p_addornements[it] == '[')
    {
        auto start = it;
        it++;
        while (std::isdigit(p_addornements[it]))
            it++;
        auto size = p_addornements.substr(start + 1, it - start - 1);
        return std::stoi(size) * get_array_size_from_addornements_rec(p_addornements, it);
    }
    return 1;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
int get_array_size_from_addornements(std::string& p_addornements)
{
    return get_array_size_from_addornements_rec(p_addornements, 0);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWidget::on_treeView_xml_expanded(const QModelIndex& p_index)
{
    if (!p_index.isValid())
        return;
    this->setCursor(Qt::WaitCursor);

    auto               model     = m_df_xml_treeview->model();
    rdf::DF_XML_Model* xml_model = static_cast<rdf::DF_XML_Model*>(model);
    auto               item      = static_cast<rdf::DF_XML_Item*>(p_index.internalPointer());

    if (item != nullptr)
    {
        if (item->childCount() == 1 && item->child(0) == rdf::xml_node_dummy())
        {
            // Remove dummy node
            xml_model->delete_dummy_node(p_index);
            // Inset child nodes
            xml_model->insert_child_nodes(p_index);
        }
    }

    this->setCursor(Qt::ArrowCursor);
}

void fill_coord_report(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::report* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::report*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::report*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_engraving(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::engraving* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::engraving*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::engraving*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_item(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::item* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::item*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::item*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_job(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::job* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::job*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::job*>(p_node->m_address);

    if (df_structure == nullptr)
        return;

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_construction(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::construction* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::construction*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::construction*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_army(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::army* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::army*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::army*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_plant(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::plant* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::plant*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::plant*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_vehicle(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::vehicle* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::vehicle*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::vehicle*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_unit(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::unit* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::unit*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::unit*>(p_node->m_address);

    if (!(df_structure->flags1.whole & 0x2U)) // Inactive
    {
        x = df_structure->pos.x;
        y = df_structure->pos.y;
        z = df_structure->pos.z;

        df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;
    }
}

void fill_coord_vermin(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::vermin* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::vermin*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::vermin*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_deep_vein_hollow(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    df::deep_vein_hollow* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::deep_vein_hollow*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::deep_vein_hollow*>(p_node->m_address);

    x = df_structure->pos.x;
    y = df_structure->pos.y;
    z = df_structure->pos.z;
}

void fill_coord_building(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::building* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::building*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::building*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->centerx;
    y = df_structure->centery;
    z = df_structure->z;
}

void fill_coord_machine(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::machine* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::machine*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::machine*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    x = df_structure->x;
    y = df_structure->y;
    z = df_structure->z;
}

void fill_coord_coord_rect(rdf::NodeBase* p_node, int& x, int& y, int& z)
{
    df::coord_rect* df_structure;
    if (p_node->m_rdf_type == rdf::RDF_Type::Pointer)
    {
        uint64_t* pointer = reinterpret_cast<uint64_t*>(p_node->m_address);
        df_structure      = reinterpret_cast<df::coord_rect*>(*pointer);
    }
    else
        df_structure = reinterpret_cast<df::coord_rect*>(p_node->m_address);

    df::global::ui->main.mode = df::enums::ui_sidebar_mode::LookAround;

    auto deltax = df_structure->x2 - df_structure->x1;
    auto deltay = df_structure->y2 - df_structure->y1;
    if (deltax < 0)
        deltax = -deltax;
    if (deltay < 0)
        deltay = -deltay;
    x = df_structure->x1 + deltax / 2;
    y = df_structure->y1 + deltay / 2;
    z = df_structure->z;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWidget::on_treeView_selection_changed(const QItemSelection& p_index_selected,
                                                      const QItemSelection& p_index_deselected)
{
    QModelIndexList indexes = p_index_selected.indexes();
    QModelIndex&    index   = indexes[0];

    // Get the df_model
    auto      model        = m_treeview->model();
    DF_Model* global_model = static_cast<DF_Model*>(model);

    // Get the selected node
    rdf::NodeBase* node_selected = global_model->nodeFromIndex(index);

    // Fill memory
    if (m_online)
        process_memory(node_selected);
    // XML tree
    //    process_xml_tree(node_selected);

    // auto url = QUrl(QStringLiteral("https://dwarffortresswiki.org/index.php/DF2014:Sawgrass"));
    // m_web_view->setUrl(url);

    auto stacked_widget      = parentWidget();
    auto tab_widget          = stacked_widget->parent();
    auto df_window           = tab_widget->parent();
    auto df_structure_window = dynamic_cast<DFStructureWindow*>(df_window);
    df_structure_window->update_statusbar(node_selected);
    if (node_selected->m_rdf_type == rdf::RDF_Type::Array ||
        node_selected->m_rdf_type == rdf::RDF_Type::Vector ||
        !node_selected->m_model_data.m_hyperlink)
    {
        emit hyperlink_selection_has_value(false);
        return;
    }
    auto hyperlink_value = *node_selected->m_model_data.m_hyperlink;
    if (hyperlink_value == "N/A")
    {
        emit hyperlink_selection_has_value(false);
        return;
    }
    emit hyperlink_selection_has_value(!hyperlink_value.empty());

    int coord_x = -3000, coord_y = -3000, coord_z = -3000;

    if (node_selected->m_df_type == rdf::DF_Type::coord)
    {
        auto the_coord = reinterpret_cast<df::coord*>(node_selected->m_address);
        if (the_coord)
        {
            coord_x = the_coord->x;
            coord_y = the_coord->x;
            coord_z = the_coord->z;

            emit selection_has_coordinate(coord_x,
                                          coord_y,
                                          coord_z);
            return;
        }
    }

    rdf::NodeBase* it    = node_selected;
    bool           found = false;
    do
    {
        switch (it->m_rdf_type)
        {
            case rdf::RDF_Type::Class:
            case rdf::RDF_Type::Compound:
            case rdf::RDF_Type::Struct:
            case rdf::RDF_Type::Pointer:
            case rdf::RDF_Type::Union:
                found = true;
                break;
            default:
                it = it->parent();
        }
    } while (!found && it->m_node_type != rdf::NodeType::Root);

    if (found)
    {
        auto df_type      = it->m_df_type;
        auto base_classes = MetadataServer.get_base_classes(it->m_df_type);
        if (!base_classes.empty())
            df_type = base_classes.at(base_classes.size() - 1);

        switch (df_type)
        {
            case rdf::DF_Type::report:
                fill_coord_report(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::engraving:
                fill_coord_engraving(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::item:
                fill_coord_item(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::job:
                fill_coord_job(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::construction:
                fill_coord_construction(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::deep_vein_hollow:
                fill_coord_deep_vein_hollow(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::plant:
                fill_coord_plant(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::vehicle:
                fill_coord_vehicle(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::unit:
                fill_coord_unit(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::vermin:
                fill_coord_vermin(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::building:
                fill_coord_building(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::coord_rect:
                fill_coord_coord_rect(it, coord_x, coord_y, coord_z);
                break;
            case rdf::DF_Type::machine:
                fill_coord_machine(it, coord_x, coord_y, coord_z);
                break;
            default:
                break;
        }
    }
    if (m_online)
        emit selection_has_coordinate(coord_x,
                                      coord_y,
                                      coord_z);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWidget::process_xml_tree(rdf::NodeBase* p_node_selected)
{
    // XML tree
    auto parent      = dynamic_cast<DwarfExplorerMainWindow*>(this->parent());
    auto types_table = parent->get_types_table();

    auto                     path       = p_node_selected->path();
    auto                     tokens_pre = split(path, '.');
    std::vector<std::string> tokens;
    for (auto& token : tokens_pre)
    {
        auto it = token.find('[');
        if (it == std::string::npos)
        {
            tokens.push_back(token);
            continue;
        }
        auto pre = token.substr(0, it);
        tokens.push_back(pre);
        while (it < token.length() && it < std::string::npos)
        {
            while (token[it] != ']')
                it++;
            tokens.push_back("[]");
            it++;
            while (it < token.length() && token[it] != '[')
                it++;
        }
    }

    bool found_global = false;
    // Look for the type
    rdf::Field* current_field = nullptr;
    for (auto& token : tokens)
    {
        if (token == "df")
            continue;
        if (token == "global")
            continue;
        if (token == "[]")
        {
            // Array or vector access
            auto pointer_type = current_field->get_attribute("pointer-type");
            if (!pointer_type.empty())
            {
                // get the type-name from the pointer type
                auto it3 = types_table.find(pointer_type);
                if (it3 != types_table.end())
                {
                    current_field = (*it3).second;
                    continue;
                }
            }
        }
        if (token == "super")
        {
            // TODO
            continue;
        }
        if (!found_global)
        {
            // Process global
            auto maybe_global = MetadataServer.find_global(token);
            if (!maybe_global)
                return;
            auto global_metadata = *maybe_global;
            auto global_type_st  = MetadataServer.DF_Type_to_string(global_metadata.m_df_type);
            auto it              = types_table.find(global_type_st);
            if (it == types_table.end())
                return;
            current_field = (*it).second;
            found_global  = true;
            continue;
        }
        // No []
        // Look in the children for the token
        bool found = false;
        for (size_t i = 0; i < current_field->get_num_children(); i++)
        {
            auto child      = current_field->get_pchild(i);
            auto child_name = child->get_attribute("name");
            if (!child_name.empty())
            {
                if (child_name == token)
                {
                    // found child
                    found         = true;
                    current_field = child;
                    break;
                }
            }
        }
        if (found)
            continue;

        // Not found
        auto child_type_name = current_field->get_attribute("type-name");
        if (!child_type_name.empty())
        {
            auto it2 = types_table.find(child_type_name);
            if (it2 != types_table.end())
            {
                current_field = (*it2).second;
                for (size_t i = 0; i < current_field->get_num_children(); i++)
                {
                    auto child      = current_field->get_pchild(i);
                    auto child_name = child->get_attribute("name");
                    if (!child_name.empty())
                    {
                        if (child_name == token)
                        {
                            // found child
                            found         = true;
                            current_field = child;
                            break;
                        }
                    }
                }
                if (!found)
                    return;
            }
        }
    }

    auto df_xml_model = dynamic_cast<rdf::DF_XML_Model*>(m_df_xml_treeview->model());
    auto node_root    = new rdf::DF_XML_Item(current_field);
    df_xml_model->set_root_node(node_root);
    df_xml_model->insert_child_nodes(current_field);
    DF_Xml_Builder builder;
    auto           text = builder.build_xml(current_field);
    m_xml_text_viewer->clear();
    QString text_to_show = QString::fromStdString(text);
    m_xml_text_viewer->setPlainText(text_to_show);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<uint64_t, int> compute_buffer_for_global(rdf::NodeBase* p_node_selected)
{
    int new_size = 0;

    uint64_t new_address = p_node_selected->m_address;
    if (p_node_selected->m_rdf_type == rdf::RDF_Type::Array)
    {
        auto node_array = dynamic_cast<rdf::NodeArray*>(p_node_selected);
        auto array_size = get_array_size_from_addornements(node_array->m_addornements);
        new_size        = array_size * *MetadataServer.size_of_DF_Type(p_node_selected->m_df_type);
    }
    else
        new_size = *MetadataServer.size_of_DF_Type(p_node_selected->m_df_type);

    return std::pair<uint64_t, int>(new_address, new_size);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<uint64_t, int> compute_buffer_for_vector(rdf::NodeBase* p_node_selected, uint64_t p_current_address)
{
    auto node_vector = reinterpret_cast<rdf::NodeVector*>(p_node_selected);
    auto vector_size = DF_Model::get_vector_size(node_vector);
    if (vector_size == 0)
        return std::pair<uint64_t, int>(p_current_address, vector_size);
    auto     vector_data = reinterpret_cast<uint64_t*>(node_vector->m_address);
    auto     vector_end  = reinterpret_cast<uint64_t*>(node_vector->m_address + sizeof(void*));
    uint64_t new_address = *vector_data;
    int      new_size    = *vector_end - *vector_data;
    return std::pair<uint64_t, int>(new_address, new_size);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<uint64_t, int> compute_buffer_for_pointer_or_df_flag_array(rdf::NodeBase* p_node_selected)
{
    auto     pointer_address = reinterpret_cast<uint64_t*>(p_node_selected->m_address);
    uint64_t new_address     = *pointer_address;
    int      new_size        = *MetadataServer.size_of_DF_Type(p_node_selected->m_df_type);
    return std::pair<uint64_t, int>(new_address, new_size);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<uint64_t, int> compute_buffer_for_df_linked_list(rdf::NodeBase* p_node_selected)
{
    uint64_t new_address = p_node_selected->m_address;

    // Linked list are 3 consecutive pointers (item, prev, next)
    int new_size = 3 * sizeof(void*);

    return std::pair<uint64_t, int>(new_address, new_size);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<uint64_t, int> compute_buffer_for_root(rdf::NodeBase* p_node_selected, rdf::NodeBase* p_previous_node)
{
    int new_size;

    uint64_t new_address = p_previous_node->m_address;
    if (p_node_selected->m_rdf_type == rdf::RDF_Type::Array)
    {
        auto node_array = dynamic_cast<rdf::NodeArray*>(p_node_selected);
        auto array_size = get_array_size_from_addornements(node_array->m_addornements);
        new_size        = array_size * *MetadataServer.size_of_DF_Type(p_node_selected->m_df_type);
    }
    else
        new_size = *MetadataServer.size_of_DF_Type(p_previous_node->m_df_type);
    return std::pair<uint64_t, int>(new_address, new_size);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::pair<uint64_t, int> compute_buffer(rdf::NodeBase* p_node_selected, uint64_t p_current_address)
{
    uint64_t new_address = 0;
    int      new_size    = 0;
    bool     new_buffer  = false;

    auto parent = p_node_selected->parent();
    if (parent && parent->m_node_type == rdf::NodeType::Root)
    {
        // Global
        return compute_buffer_for_global(p_node_selected);
    }

    auto previous = p_node_selected;
    auto iterator = p_node_selected->parent();
    while (iterator)
    {
        if (iterator->m_node_type == rdf::NodeType::Vector)
            return compute_buffer_for_vector(iterator, p_current_address);

        if (iterator->m_node_type == rdf::NodeType::Pointer || iterator->m_node_type == rdf::NodeType::DFFlagArray)
            return compute_buffer_for_pointer_or_df_flag_array(iterator);

        if (iterator->m_node_type == rdf::NodeType::DFLinkedList)
            return compute_buffer_for_df_linked_list(iterator);

        if (iterator->m_node_type == rdf::NodeType::Root)
            return compute_buffer_for_root(p_node_selected, previous);

        previous = iterator;
        iterator = iterator->parent();
    }

    return std::pair<uint64_t, int>(new_address, new_size);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWidget::process_memory(rdf::NodeBase* p_node_selected)
{
    uint64_t new_address     = 0;
    int      new_size        = 0;
    auto     document        = m_hexview->document();
    auto     current_address = document->baseAddress();

    QHexPosition top;
    top.line        = 0;
    top.column      = 0;
    top.nibbleindex = 0;
    if (p_node_selected == nullptr)
    {
        new_address = 0;
        new_size    = 1;
    }
    else
    {
        auto the_data = compute_buffer(p_node_selected, current_address);
        new_address   = the_data.first;
        new_size      = the_data.second;
    }

    if (new_address == current_address)
    {
        update_metadata(document->metadata(), p_node_selected, new_address);
        QHexPosition pos;
        pos.line        = (p_node_selected->m_address - new_address) / 16;
        pos.column      = (p_node_selected->m_address - new_address) % 16;
        pos.nibbleindex = 0;
        document->cursor()->moveTo(pos);
    }
    else
    {
        // New buffer for the new structure
        char* data         = reinterpret_cast<char*>(new_address);
        auto  new_document = QHexDocument::fromMemory<QMemoryBuffer>(data, new_size);
        new_document->setBaseAddress(new_address);
        QHexMetadata* metadata = new_document->metadata();
        update_metadata(metadata, p_node_selected, new_address);
        m_hexview->setDocument(new_document);
        m_hexview->setReadOnly(true);
        new_document->cursor()->moveTo(top);
        delete document;
    }
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWidget::on_treeView_expanded(const QModelIndex& p_index)
{
    if (!m_online)
        return;

    using namespace rdf;

    auto           model        = m_treeview->model();
    DF_Model*      global_model = static_cast<DF_Model*>(model);
    rdf::NodeBase* node_base    = global_model->nodeFromIndex(p_index);
    rdf::Node*     node         = dynamic_cast<rdf::Node*>(node_base);

    this->setCursor(Qt::WaitCursor);

    if (node != nullptr && node->has_dummy_node())
    {
        // Remove dummy node
        global_model->remove_dummy_child_node(p_index);

        if (node->m_node_type != NodeType::Global)
            global_model->insert_child_nodes(node, p_index);
        else
            global_model->insert_global_nodes(node, p_index);
    }
    this->setCursor(Qt::ArrowCursor);
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
void DFStructureWidget::on_hyperlink_changed(const QString& p_value)
{
    // tabwidget->DFStructureWindow->DFStructureMainWindow
    auto df_structure_main_window = dynamic_cast<DwarfExplorerMainWindow*>(parent()->parent()->parent());
    df_structure_main_window->statusBar()->showMessage(p_value, 2000);
}

void DFStructureWidget::on_paused()
{
    m_online      = true;
    auto df_model = dynamic_cast<DF_Model*>(m_treeview->model());
    df_model->set_online(true);
}

void DFStructureWidget::on_resumed()
{
    m_online      = false;
    auto df_model = dynamic_cast<DF_Model*>(m_treeview->model());
    df_model->set_online(false);
}