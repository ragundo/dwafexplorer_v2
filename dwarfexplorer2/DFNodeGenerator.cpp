#include "DFNodeGenerator.h"
#include "NodeTable.h"
#include "df_model/df_model.h"
#include "df_model/df_node.h"
#include "df_utils.h"
#include "metadata_server/MetadataServer.h"
#include "node_builder/NodeBuilder.h"
#include <vector>

extern rdf::CMetadataServer MetadataServer;

using namespace std;
using namespace rdf;

//
//-------------------------------------------------------------------------------------------------------------------//
//
std::vector<std::string> expression_to_parts(string& p_expression)
{
    std::vector<std::string> pieces;
    auto                     parts = split(p_expression, '.');
    for (auto& part : parts)
    {
        if (part.find('[') != string::npos)
        {
            auto parts_array = split(part, '[');
            for (auto& part_array : parts_array)
            {
                if (part_array.find(']') != string::npos)
                    pieces.push_back("[" + part_array);
                else
                    pieces.push_back(part_array);
            }
            continue;
        }
        pieces.push_back(part);
    }
    return pieces;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
NodeBase* DFNode_Generator::process_globals(NodeTable* p_node_table)
{
    if (p_node_table->is_empty())
        return nullptr;

    // Add all the globals to the node table
    auto all_globals = MetadataServer.get_globals_metadata();
    for (auto& one_global_metadata : all_globals)
    {
        string global_field_name   = std::get<0>(one_global_metadata);
        string global_comment      = std::get<3>(one_global_metadata);
        string global_addornements = "";
        auto   addornements_maybe  = MetadataServer.get_addornements_globals_metadata(global_field_name);
        if (addornements_maybe)
            global_addornements = *addornements_maybe;

        auto new_node = NodeBuilder::create_node(std::get<4>(one_global_metadata), //address
                                                 std::get<2>(one_global_metadata), // RDF_Type
                                                 std::get<1>(one_global_metadata), // DF_Type
                                                 global_addornements);

        if (new_node == nullptr)
            return nullptr;

        new_node->m_field_name = std::get<0>(one_global_metadata);
        new_node->m_comment    = global_comment;

        // Add it to the table of nodes
        new_node->m_node_table = p_node_table;
        new_node->m_field_path = "df.global";
        new_node->m_node_table->add_node(new_node);
        if (new_node->m_has_children)
        {
            auto node = dynamic_cast<Node*>(new_node);
            node->add_dummy_node();
        }
        string key             = "df.global";
        auto   node_root_maybe = p_node_table->find_node(key);
        if (node_root_maybe)
        {
            auto some_node   = *node_root_maybe;
            auto node_global = dynamic_cast<NodeGlobal*>(some_node);
            node_global->add_child_node(new_node);
        }
    }
    return nullptr;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
NodeBase* DFNode_Generator::process_global(string&    p_token,
                                           string&    p_current_path,
                                           NodeBase*  p_current_node,
                                           NodeTable* p_node_table)
{
    auto maybe_node = p_node_table->find_node(p_current_path);
    if (maybe_node)
        return *maybe_node;
    return nullptr;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
NodeBase* DFNode_Generator::process_array_or_vector(string&    p_token,
                                                    string&    p_current_path,
                                                    NodeBase*  p_current_node,
                                                    NodeTable* p_node_table)
{
    auto maybe_node = p_node_table->find_node(p_current_path);
    if (maybe_node)
        return *maybe_node;

    // Array or vector access
    rdf::Node* parent_node = dynamic_cast<rdf::Node*>(p_current_node);

    auto end          = p_token.find(']');
    auto array_access = p_token.substr(1, end - 1);
    NodeBuilder::fill_node(p_current_node);

    // Look for the node
    auto key = parent_node->m_field_path +
               (parent_node->m_field_name[0] == '[' ? "" : ".") +
               parent_node->m_field_name +
               (p_token[0] == '[' ? "" : ".") +
               p_token;

    maybe_node = parent_node->m_node_table->find_node(key);
    if (maybe_node)
        return *maybe_node;

    /*

    rdf::Node* node = dynamic_cast<rdf::Node*>(p_current_node);
    for (auto& child : node->m_children)
    {
        auto child_name = child->m_field_name;

        child_name = child_name.substr(1, 100);
        child_name = child_name.substr(0, child_name.length() - 1);
        while (child_name[0] == ' ')
            child_name = child_name.substr(1, 100);
        if (array_access == child_name)
        {
            // Found it
            if (!child->can_have_children())
            {
                auto child_node = dynamic_cast<rdf::Node*>(child);
                // Clear the dummy child
                p_model->remove_dummy_child_node(child_node);
            }
            return child;
        }
    }
    */
    return nullptr;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
NodeBase* DFNode_Generator::process_field(string&    p_token,
                                          string&    p_current_path,
                                          NodeBase*  p_current_node,
                                          NodeTable* p_node_table)
{
    auto maybe_node = p_node_table->find_node(p_current_path);
    if (maybe_node)
        return *maybe_node;

    bool must_expand = false;
    switch (p_current_node->m_rdf_type)
    {
        case rdf::RDF_Type::Vector:
        case rdf::RDF_Type::Pointer:
        case rdf::RDF_Type::Array:
        case rdf::RDF_Type::DFArray:
        case rdf::RDF_Type::Class:
        case rdf::RDF_Type::Struct:
        case rdf::RDF_Type::Compound:
        case rdf::RDF_Type::Union:
        case rdf::RDF_Type::AnonymousCompound:
        case rdf::RDF_Type::AnonymousUnion:
        case rdf::RDF_Type::DFLinkedList:
        case rdf::RDF_Type::Bitfield:
        case rdf::RDF_Type::DFFlagArray:
            must_expand = true;
        default:
            break;
    }
    if (must_expand)
    {
        auto node = dynamic_cast<Node*>(p_current_node);

        NodeBuilder::fill_node(p_current_node);

        // Look for the node
        auto key = node->m_field_path +
                   (node->m_field_name[0] == '[' ? "" : ".") +
                   node->m_field_name +
                   (p_token[0] == '[' ? "" : ".") +
                   p_token;

        auto maybe_node = node->m_node_table->find_node(key);
        if (maybe_node)
            return *maybe_node;
    }
    return nullptr;
}

//
//-------------------------------------------------------------------------------------------------------------------//
//
NodeBase* DFNode_Generator::process_expression(string&    p_expression,
                                               NodeTable* p_node_table)
{
    auto maybe_node = p_node_table->find_node(p_expression);
    if (maybe_node)
    {
        auto node = dynamic_cast<Node*>(*maybe_node);
        return node;
    }

    // Not found
    std::vector<std::string> pieces = expression_to_parts(p_expression);

    NodeBase* current_node = nullptr;
    NodeRoot* n_root       = nullptr;
    string    current_path = "";
    bool      global_found = false;
    for (auto& token : pieces)
    {
        if (token == "df")
        {
            current_path = token;
            if (p_node_table->is_empty())
            {
                // Create the root node
                n_root               = new NodeRoot;
                n_root->m_field_name = "@df";
                n_root->m_rdf_type   = RDF_Type::None;
                n_root->m_df_type    = DF_Type::None;
                n_root->m_node_type  = NodeType::Root;
                n_root->m_node_table = p_node_table;
                n_root->m_field_path = "";
                p_node_table->add_node("df", n_root);
            }
            else
            {
                string key                = "df";
                auto   current_node_maybe = p_node_table->find_node(key);
                if (!current_node_maybe)
                    return nullptr;
                n_root = dynamic_cast<NodeRoot*>(*current_node_maybe);
            }
            current_node = n_root;
            continue;
        }

        if (token == "global")
        {
            current_path.append(".global");

            string key                = "df.global";
            auto   current_node_maybe = p_node_table->find_node(key);
            if (!current_node_maybe)
            {
                auto n_global            = new NodeGlobal;
                n_global->m_rdf_type     = RDF_Type::None;
                n_global->m_df_type      = DF_Type::None;
                n_global->m_node_table   = p_node_table;
                n_global->m_field_name   = "global";
                n_global->m_field_path   = "df";
                n_global->m_has_children = true;
                p_node_table->add_node("df.global", n_global);

                // Add all the globals
                process_globals(p_node_table);

                current_node = n_global;
                continue;
            }
            auto n_global = dynamic_cast<NodeGlobal*>(*current_node_maybe);
            current_node  = n_global;
            continue;
        }

        if (!global_found)
        {
            // This field must be a global
            current_path.append(".").append(token);
            auto maybe_node = p_node_table->find_node(current_path);
            if (!maybe_node)
                return nullptr;
            current_node = *maybe_node;
            // Only one global, from now, only children of some global
            global_found = true;
            continue;
        }

        if (token.find('[') != std::string::npos)
        {
            current_path.append(token);
            current_node = process_array_or_vector(token,
                                                   current_path,
                                                   current_node,
                                                   p_node_table);
            continue;
        }
        // Field child of some global
        // Expand the node and look for the field with name contained in token
        current_path.append(".").append(token);
        current_node = process_field(token,
                                     current_path,
                                     current_node,
                                     p_node_table);
    }
    return current_node;
}